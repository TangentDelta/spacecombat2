--[[
Space Combat Manufacturing - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

AddCSLuaFile()

--Local table called this, because screw typing SC.Manufacturing.<whatever> constantly.
local this = {}
this.Recipes = {}
this.DefaultRecipes = {}

--Prevents things from erroring out when a recipe doesn't exist, just incase someone didn't check if it was valid.
this.InvalidRecipe = {time=1,capacitor=0,requires={},produces={}}

--Utility function to see if a string is valid
local function CheckString(str)
	str = string.Trim(str)
	return ((str ~= "") and (str ~= nil))
end

--Chekcs if a type is valid
function this.IsValidType(type)
	return CheckString(type) and not (this.Recipes[type] == nil)
end

--Checks if a recipe is valid
function this.IsValidRecipe(name, type)
	if (this.Recipes[type] == nil) or (this.Recipes[type][name] == nil) then return false end

	return true
end

--Gets a recipe's table
function this.GetRecipe(name, type)
	if not this.IsValidRecipe(name, type) then return this.InvalidRecipe end

	return this.Recipes[type][name]
end

--Fetches the tables of all recipes of the specified type
function this.GetRecipes(type)
	return this.Recipes[type] or {}
end

--Returns a list of all the recipe names of a type
function this.GetRecipeList(type)
	return table.GetKeys(this.Recipes[type] or {})
end

--Gets the default recipe for a type
function this.GetDefaultRecipe(type)
	local name = this.DefaultRecipes[type]
	if name == nil then
		return "", this.InvalidRecipe
	end

	return name, this.GetRecipe(name, type)
end

--Adds a new recipe to the table
function this.AddRecipe(name, type, time, capacitor, requires, produces, default)
	if not CheckString(name) or not CheckString(type) or not time or not capacitor or not requires or not produces or this.IsValidRecipe(name, type) then return end

    -- hack because i'm being lazy
    requires.Energy = capacitor

	local tbl = {time=time,capacitor=capacitor,requires=requires,produces=produces}

	if not this.Recipes[type] then this.Recipes[type] = {} end

	this.Recipes[type][name] = tbl

	if default then this.DefaultRecipes[type] = name end
end

--Default refinery recipes
this.AddRecipe("Water", "Refinery", 3, 30000, {["Ice"]=5000}, {["Water"]=75000}, true)
this.AddRecipe("Ship Fuel", "Refinery", 10, 20000, {["Polonium Nitrate"]=160, ["Nubium"]=40, ["Empty Canister"]=20}, {["Ship Fuel Canister"]=20})
this.AddRecipe("Nubium", "Refinery", 20, 180000, {["Solid Nubium"]=600}, {["Nubium"]=60})
this.AddRecipe("Anubium", "Refinery", 30, 120000, {["Polonium Nitrate"]=20, ["Nubium"]=800, ["Titanite"]=60, ["Trinium"]=200}, {["Anubium"]=40})
this.AddRecipe("Tritanium", "Refinery", 15, 30000, {["Veldspar"]=200}, {["Tritanium"]=400, ["Iron"]=200})
this.AddRecipe("Polonium Nitrate", "Refinery", 15, 5000, {["Nitrogen"]=4000, ["Oxygen"]=12000, ["Polonium"]=2000}, {["Polonium Nitrate"]=2000})
this.AddRecipe("Lithium Isotope Separation", "Refinery", 15, 3000, {["Lithium"]=1000}, {["Lithium-6"]=100, ["Lithium-7"]=900})
this.AddRecipe("Ground Beef", "Refinery", 30, 50000, {["Beef"]=50}, {["Ground Beef"]=50})
this.AddRecipe("Burger Patty", "Refinery", 300, 150000, {["Ground Beef"]=50}, {["Burger Patty"]=5})
this.AddRecipe("Bread", "Refinery", 300, 50000, {["Wheat"]=500, ["Water"]=5000}, {["Bread"]=10})
this.AddRecipe("Battle-Bread", "Refinery", 600, 50000, {["Bread"]=10, ["Processed Cheese"]=50, ["Dorito Monster Concentrate"]=10}, {["Battle-Bread"]=10})
this.AddRecipe("Cheese", "Refinery", 60, 30000, {["Milk"]=1000}, {["Cheese"]=500})
this.AddRecipe("Processed Cheese", "Refinery", 60, 30000, {["Annatto"]=100, ["Cheese"]=500}, {["Processed Cheese"]=500})
this.AddRecipe("Dorito", "Refinery", 60, 30000, {["Corn"]=1000, ["Cheese"]=500}, {["Dorito"]=100})
this.AddRecipe("Dorito Monster Concentrate", "Refinery", 300, 300000, {["Dorito"]=500, ["Monster Energy"]=5000}, {["Dorito Monster Concentrate"]=5000})

--Default factory recipes
this.AddRecipe("Empty Canister", "Factory", 10, 20000, {["Iron"]=500}, {["Empty Canister"]=100}, true)
this.AddRecipe("Tiny Capacitor Charge", "Factory", 10, 5000, {["Empty Canister"]=50, ["Anubium"]=50}, {["Tiny Capacitor Charge"]=50})
this.AddRecipe("Small Capacitor Charge", "Factory", 10, 10000, {["Empty Canister"]=50, ["Anubium"]=500}, {["Small Capacitor Charge"]=50})
this.AddRecipe("Medium Capacitor Charge", "Factory", 15, 15000, {["Empty Canister"]=50, ["Anubium"]=1000}, {["Medium Capacitor Charge"]=50})
this.AddRecipe("Large Capacitor Charge", "Factory", 20, 20000, {["Empty Canister"]=50, ["Anubium"]=2000}, {["Large Capacitor Charge"]=50})
this.AddRecipe("X-Large Capacitor Charge", "Factory", 20, 25000, {["Empty Canister"]=50, ["Anubium"]=5000}, {["X-Large Capacitor Charge"]=50})
this.AddRecipe("Nanite Canister", "Factory", 10, 75000, {["Empty Canister"]=500, ["Anubium"]=5, ["Nubium"]=60, ["Iron"]=100, ["Tritanium"]=500}, {["Nanite Canister"]=500})
this.AddRecipe("Anti-Hydrogen Particle Canister", "Factory", 30, 100000, {["Empty Canister"]=100, ["Antihydrogen"]=100}, {["Anti-Hydrogen Particle Canister"]=100})
this.AddRecipe("Anti-Anubium Particle Canister", "Factory", 30, 100000, {["Empty Canister"]=100, ["Antianubium"]=100}, {["Anti-Anubium Particle Canister"]=100})
this.AddRecipe("Iron Particle Canister", "Factory", 30, 50000, {["Empty Canister"]=100, ["Iron"]=100}, {["Iron Particle Canister"]=100})
this.AddRecipe("Depleted Uranium Particle Canister", "Factory", 30, 75000, {["Empty Canister"]=100, ["Depleted Uranium"]=100}, {["Depleted Uranium Particle Canister"]=100})
this.AddRecipe("Kleiner Particle Canister", "Factory", 300, 750000, {["Anti-Anubium Particle Canister"]=10, ["Kleiner"]=1}, {["Kleiner Particle Canister"]=10})
this.AddRecipe("Rocket Fuel Canister", "Factory", 10, 75000, {["Empty Canister"]=100, ["Methane"]=20000, ["Oxygen"]=20000}, {["Rocket Fuel Canister"]=100})
this.AddRecipe("Lifesupport Canister", "Factory", 10, 75000, {["Empty Canister"]=100, ["Oxygen"]=40000}, {["Lifesupport Canister"]=100})
this.AddRecipe("Battle-Burger", "Factory", 10, 75000, {["Processed Cheese"]=50, ["Bread"]=20, ["Burger Patty"]=40}, {["Battle-Burger"]=10})

--Default reprocessor recipes
this.AddRecipe("Empty Canister", "Reprocessor", 10, 30000, {["Empty Canister"]=50}, {["Iron"]=200}, true)
this.AddRecipe("Scrap Metal", "Reprocessor", 20, 30000, {["Scrap Metal"]=500}, {["Iron"]=200, ["Tritanium"]=50})
this.AddRecipe("Destroyed Consoles", "Reprocessor", 25, 40000, {["Destroyed Consoles"]=50}, {["Scrap Metal"]=100, ["Electronic Circuitry"]=10})
this.AddRecipe("Nubium Generator Core", "Reprocessor", 40, 60000, {["Nubium Generator Core"]=15}, {["Destroyed Consoles"]=60, ["Scrap Metal"]=500, ["Plasma Conduits"]=70, ["Nubium"]=5000, ["Anubium"]=500})
this.AddRecipe("Plasma Conduits", "Reprocessor", 20, 20000, {["Plasma Conduits"]=50}, {["Scrap Metal"]=20, ["Contaminated Coolant"]=5000})
this.AddRecipe("Contaminated Coolant", "Reprocessor", 5, 10000, {["Contaminated Coolant"]=500}, {["Water"]=5000, ["Radioactive Materials"]=500})
this.AddRecipe("Damaged Shield Emitter", "Reprocessor", 30, 55000, {["Damaged Shield Emitter"]=50}, {["Scrap Metal"]=100, ["Destroyed Consoles"]=20, ["Contaminated Coolant"]=750, ["Plasma Conduits"]=20})
this.AddRecipe("Radioactive Materials", "Reprocessor", 10, 15000, {["Radioactive Materials"]=50}, {["Polonium Nitrate"]=200, ["Uranium"]=50, ["Nubium"]=25})
this.AddRecipe("Electronic Circuitry", "Reprocessor", 20, 30000, {["Electronic Circuitry"]=50}, {["Voltarium"]=200, ["Tritanium"]=50})

--Default farm recipes
this.AddRecipe("Corn", "Farm", 300, 30000, {}, {["Corn"]=500})
this.AddRecipe("Annatto", "Farm", 300, 30000, {}, {["Annatto"]=500})
this.AddRecipe("Cow", "Farm", 300, 30000, {["Corn"]=2500, ["Wheat"]=1000, ["Water"]=10000}, {["Cow"]=1})
this.AddRecipe("Wheat", "Farm", 300, 30000, {}, {["Wheat"]=500})
this.AddRecipe("Milk", "Farm", 600, 90000, {["Cow"]=1}, {["Milk"]=5000})
this.AddRecipe("Beef", "Farm", 600, 90000, {["Cow"]=1}, {["Beef"]=50})
this.AddRecipe("Kleiner", "Farm", 900, 1500000, {["Corn"]=2500, ["Cheese"]=1500, ["Dorito Monster Concentrate"]=10000, ["Battle-Burger"]=1}, {["Kleiner"]=1})

SC.Manufacturing = this