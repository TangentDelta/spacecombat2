--[[
Space Combat Careers and Ship Crews - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

local Player = FindMetaTable("Player")

SC.Careers = {}
SC.Careers.Careers = {}

--MySQL Functions
if SERVER then
    require( "mysql" )
    function SC.Careers.MYSQLConnected()
	    --Grab the MYSQL object
	    local SQL = SC.MYSQL

	    --Make sure the table exists
	    mysql.query( SQL, "CREATE TABLE IF NOT EXISTS `sc_skills` ( uid INTEGER, skill CHAR(32), time INTEGER );" )
	    mysql.query( SQL, "CREATE TABLE IF NOT EXISTS `sc_careers` ( uid INTEGER, training CHAR(32))" )
    end
end

--Career functions
--Add Career - Adds a career to be unlocked
--Takes:
--	Name - The name of the career
--	Desc - A description of the career
--	Func - A function that is called to check if the player can access the career, should accept an input of player
--	Skills - A table full of skills the player can learn with the class, expects the input format to be {{name="MyCareerSkill", desc="My Career", requirements={}, rank=1, callback=function(ply, level) ply:ChatPrint("skill trained to "..level) end}}
function SC.Careers.AddCareer(name, desc, func, skills)
	local career = {}

	career.name = name
	career.desc = desc
	career.func = func
	career.skills = skills

	SC.Careers.Careers[name] = career
end

--Player functions

--Get Career Status - Attempts to retrieve a player's progress in a career
--Takes:
--	Name - The name of the career
--Returns:
--	Successful - True or false value dictating the success or failure of the operation
--	Data - The current status of the player's career, formatted like {training="a skill", timeremaining=500, skills={table of trained skills}}
function Player:GetCareerStatus(name)
	--Is everything ready?
    if not SERVER then return end
	if not IsValid(self) or not self:IsPlayer() then return end
	if not self.SC.Careers then self:SetupCareers() end

	--Make sure the data is in the correct format
	if not self.SC.Careers[name]["training"] then
		local skills = self.SC.Careers[name][skills]

		self.SC.Careers[name] = {training, timeremaining, skills=skills}
	end

	--Return data
	return self.SC.Careers[name]
end

--Set Career - Attempts to set a player's career
--Takes:
--	Name - The name of the career
--Returns:
--	Successful - True or false value dictating the success or failure of the operation
function Player:SetCareer(name)
    if not SERVER then return false end

	--Fetch the career information from the loaded careers
	local career = self:GetCareerStatus(name)

	--Is everything we need to set the career valid?
	if not IsValid(self) or not career or not career.name or not career.func(self) then return false end
	if not self.SC then self.SC = {} end

	--Set the career on the player
	self.SC.Career = career

	--Run through the various skill functions
	for i,k in pairs(career.skills) do
		--Do we have a callback and a level?
		if k.callback and k.level then
			--Run the callback so the skill's effects are applied
			k.callback(self, k.level)
		end
	end

	--Yay we did it!
	return true
end

--Setup Careers - Sets up the basic information for careers to work
function Player:SetupCareers()
    if not SERVER then return end
	if not self.SC then self.SC = {} end

	self.SC.Careers = table.Copy(SC.Careers.Careers)

	self:SetCareer("Civilian")
end
--hook.Add("PlayerInitialSpawn", "Career Setup", function(ply) ply:SetupCareers() end)