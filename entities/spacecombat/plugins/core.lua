--[[
Space Combat Core - Created by Steeveeo/Lt.Brandon/Others

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

AddCSLuaFile()

if SERVER then
    --Core Octree

    --TODO: Replace this entire octree section with a class, copy pasting so much code is wasteful
    --Global Tables
    SC.CoreOctree = {}
    SC.CoreOctree.Builder = {}
    SC.CoreOctree.Octant = {}
    local OctantFactory = SC.CoreOctree.Octant

    --Octant Functions
    function OctantFactory:New(Origin, EntitiesWithin, Axis, Size, IsRoot, Parent)
        if not Origin or not Axis or not Size or ((not IsRoot) and (not Parent)) then
            SC.Error("[Space Combat 2 - Octree Builder] Unable to create octant, invalid parameters\n", 5)
            return false
        end

        local NewOctant = {
            Parent = Parent;
            Axis = Axis;
            Size = Size;
            Radius = math.max(Size.x, Size.y, Size.z) * 0.5;
            Volume = Size.x * Size.y * Size.z;
            Position = Origin;
            Children = nil;
            IsRoot = IsRoot;
            IsBlocked = false;
            Root = Parent and Parent.Root;
            OctantCount = 1;
            EntitiesWithin = EntitiesWithin;
        }

	    setmetatable(NewOctant,self)

        if not IsRoot then
            NewOctant.Root.OctantCount = NewOctant.Root.OctantCount + 1
            NewOctant.Parent.OctantCount = NewOctant.Parent.OctantCount + 1
        else
            NewOctant.Root = NewOctant
        end

        return NewOctant
    end

    function OctantFactory:IsLeaf()
        return self.Children ~= nil
    end

    function OctantFactory:IsPointInOctant(Point)
        local Center = self.Position
        local Size = self.Size * 0.5
        local Min = Center - Size
        local Max = Center + Size

        return Point:WithinAABox(Min, Max)
    end

    function OctantFactory:GetPointOctant(Point)
        if self:IsLeaf() and self:IsPointInOctant(Point) then
            return self
        elseif self.Children then
            local Octants = self.Children
            for I = 1, 8 do
                local SubOctant = Octants[I]
                if SubOctant:IsPointInOctant(Point) then
                    return SubOctant:GetPointOctant(Point)
                end
            end
        else
            SC.Error("[Space Combat 2 - Octree Builder] - Unable to find the octant which held the point!\n", 5)
            return false
        end
    end

    -- The below function AABBOBBIntersect is based on
    -- work provided by Geometric Tools LLC under the Boost Software License.
    -- As required by the license, below are the copyright statement and license.
    -- Geometric Tools LLC, Redmond WA 98052
    -- Copyright (c) 1998-2015
    -- Distributed under the Boost Software License, Version 1.0.
    -- http://www.boost.org/LICENSE_1_0.txt
    -- http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt

    local function AABBOBBIntersect(Pos, Extents, OBB)
        -- Get the centered form of the aligned box.  The axes are implicitly
        -- A0[1] = (1,0,0), A0[2] = (0,1,0), and A0[3] = (0,0,1).
        local C0, E0 = Pos, Extents * 0.5 --I think this wants half extents?

        -- Convenience variables.
        local C1 = OBB.Center * 0.5
        local A1 = OBB.Axis
        local E1 = OBB.Extents * 0.5 --I think this wants half extents?

        local E0_X, E0_Y, E0_Z = E0.x, E0.y, E0.z
        local E1_X, E1_Y, E1_Z = E1.x, E1.y, E1.z

        local cutoff = 0.999
        local existsParallelPair = false

        -- Compute the difference of box centers.
        local D = C1 - C0
        local D_X, D_Y, D_Z = D.x, D.y, D.z

        local dot01_1 = {0, 0, 0}      -- dot01[i][j] = Dot(A0[i],A1[j]) = A1[j][i]
        local absDot01_1 = {0, 0, 0}   -- |dot01[i][j]|
        local r0, r1, r                 -- interval radii and distance between centers
        local r01                       -- r0 + r1

        -- Test for separation on the axis C0 + t*A0.X
        for i = 1, 3 do
            local x = A1[i].X
            local ax = math.abs(x)
            dot01_1[i] = x
            absDot01_1[i] = ax
            if ax >= cutoff then
                existsParallelPair = true
            end
        end

        local absDot01_1_1, absDot01_1_2, absDot01_1_3 = absDot01_1[1], absDot01_1[2], absDot01_1[3]

        r = math.abs(D_X)
        r1 = E1_X * absDot01_1_1 + E1_Y * absDot01_1_2 + E1_Z * absDot01_1_3
        r01 = E0_X + r1
        if r > r01 then
            return false
        end

        local dot01_2 = {0, 0, 0}      -- dot01[i][j] = Dot(A0[i],A1[j]) = A1[j][i]
        local absDot01_2 = {0, 0, 0}   -- |dot01[i][j]|

        -- Test for separation on the axis C0 + t*A0.Y
        for i = 1, 3 do
            local y = A1[i].Y
            local ay = math.abs(y)
            dot01_2[i] = y
            absDot01_2[i] = ay
            if ay >= cutoff then
                existsParallelPair = true
            end
        end

        local absDot01_2_1, absDot01_2_2, absDot01_2_3 = absDot01_2[1], absDot01_2[2], absDot01_2[3]

        r = math.abs(D_Y)
        r1 = E1_X * absDot01_2_1 + E1_Y * absDot01_2_2 + E1_Z * absDot01_2_3
        r01 = E0_Y + r1
        if r > r01 then
            return false
        end

        local dot01_3 = {0, 0, 0}      -- dot01[i][j] = Dot(A0[i],A1[j]) = A1[j][i]
        local absDot01_3 = {0, 0, 0}   -- |dot01[i][j]|

        -- Test for separation on the axis C0 + t*A0[3].
        for i = 1, 3 do
            local z = A1[i].Z
            local az = math.abs(z)
            dot01_3[i] = z
            absDot01_3[i] = az
            if az >= cutoff then
                existsParallelPair = true
            end
        end

        local absDot01_3_1, absDot01_3_2, absDot01_3_3 = absDot01_3[1], absDot01_3[2], absDot01_3[3]

        r = math.abs(D_Z)
        r1 = E1_X * absDot01_3_1 + E1_Y * absDot01_3_2 + E1_Z * absDot01_3_3
        r01 = E0_Z + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A1[1].
        r = math.abs(D:Dot(A1[1]))
        r0 = E0_X * absDot01_1_1 + E0_Y * absDot01_2_1 + E0_Z * absDot01_3_1
        r01 = r0 + E1_X
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A1[2].
        r = math.abs(D:Dot(A1[2]))
        r0 = E0_X * absDot01_1_2 + E0_Y * absDot01_2_2 + E0_Z * absDot01_3_2
        r01 = r0 + E1_Y
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A1[3].
        r = math.abs(D:Dot(A1[3]))
        r0 = E0_X * absDot01_1_3 + E0_Y * absDot01_2_3 + E0_Z * absDot01_3_3
        r01 = r0 + E1_Z
        if r > r01 then
            return false
        end

        -- At least one pair of box axes was parallel, so the separation is
        -- effectively in 2D.  The edge-edge axes do not need to be tested.
        if existsParallelPair then
            return true
        end

        -- Test for separation on the axis C0 + t*A0[1]xA1[1].
        r = math.abs(D_Z * dot01_2[1] - D_Y * dot01_3[1])
        r0 = E0_Y * absDot01_3_1 + E0_Z * absDot01_2_1
        r1 = E1_Y * absDot01_1_3 + E1_Z * absDot01_1_2
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[1]xA1[2].
        r = math.abs(D_Z * dot01_2[2] - D_Y * dot01_3[2])
        r0 = E0_Y * absDot01_3_2 + E0_Z * absDot01_2_2
        r1 = E1_X * absDot01_1_3 + E1_Z * absDot01_1_1
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[1]xA1[3].
        r = math.abs(D_Z * dot01_2[3] - D_Y * dot01_3[3])
        r0 = E0_Y * absDot01_3_3 + E0_Z * absDot01_2_3
        r1 = E1_X * absDot01_1_2 + E1_Y * absDot01_1_1
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[2]xA1[1].
        r = math.abs(D_X * dot01_3[1] - D_Z * dot01_1[1])
        r0 = E0_X * absDot01_3_1 + E0_Z * absDot01_1_1
        r1 = E1_Y * absDot01_2_3 + E1_Z * absDot01_2_2
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[2]xA1[2].
        r = math.abs(D_X * dot01_3[2] - D_Z * dot01_1[2])
        r0 = E0_X * absDot01_3_2 + E0_Z * absDot01_1_2
        r1 = E1_X * absDot01_2_3 + E1_Z * absDot01_2_1
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[2]xA1[3].
        r = math.abs(D_X * dot01_3[3] - D_Z * dot01_1[3])
        r0 = E0_X * absDot01_3_3 + E0_Z * absDot01_1_3
        r1 = E1_X * absDot01_2_2 + E1_Y * absDot01_2_1
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[3]xA1[1].
        r = math.abs(D_Y * dot01_1[1] - D_X * dot01_2[1])
        r0 = E0_X * absDot01_2_1 + E0_Y * absDot01_1_1
        r1 = E1_Y * absDot01_3_3 + E1_Z * absDot01_3_2
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[3]xA1[2].
        r = math.abs(D_Y * dot01_1[2] - D_X * dot01_2[2])
        r0 = E0_X * absDot01_2_2 + E0_Y * absDot01_1_2
        r1 = E1_X * absDot01_3_3 + E1_Z * absDot01_3_1
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        -- Test for separation on the axis C0 + t*A0[3]xA1[3].
        r = math.abs(D_Y * dot01_1[3] - D_X * dot01_2[3])
        r0 = E0_X * absDot01_2_3 + E0_Y * absDot01_1_3
        r1 = E1_X * absDot01_3_2 + E1_Y * absDot01_3_1
        r01 = r0 + r1
        if r > r01 then
            return false
        end

        return true
    end

	--Check if anything is inside the Octant. Returns if there's anything in the octant and
	--if the bounding box found completely overlaps the octant itself.
	--	Returns: boolean hasContents, boolean objectEnvelops
    function OctantFactory:Check()
        -- Root octant contains everything
        if self.IsRoot then
            return true, false
        end

        local HasContent = false
        local Bounds = self.Parent.EntitiesWithin

		for i, k in ipairs(Bounds) do
            if AABBOBBIntersect(self.Position, self.Size, k) then
                HasContent = true
                table.insert(self.EntitiesWithin, k)
            end
        end

        return HasContent, false
    end

    function OctantFactory:Subdivide()
        local Origin = self.Position
        local NewScanVolume = self.Size * 0.5
        local Dist = NewScanVolume * 0.25

        local Children = {}
        Children[1] = OctantFactory:New(Origin + Vector(Dist.x, Dist.y, Dist.z), {}, self.Axis, NewScanVolume, false, self)
        Children[2] = OctantFactory:New(Origin + Vector(Dist.x, -Dist.y, Dist.z), {}, self.Axis, NewScanVolume, false, self)
        Children[3] = OctantFactory:New(Origin + Vector(-Dist.x, Dist.y, Dist.z), {}, self.Axis, NewScanVolume, false, self)
        Children[4] = OctantFactory:New(Origin + Vector(-Dist.x, -Dist.y, Dist.z), {}, self.Axis, NewScanVolume, false, self)
        Children[5] = OctantFactory:New(Origin + Vector(Dist.x, Dist.y, -Dist.z), {}, self.Axis, NewScanVolume, false, self)
        Children[6] = OctantFactory:New(Origin + Vector(Dist.x, -Dist.y, -Dist.z), {}, self.Axis, NewScanVolume, false, self)
        Children[7] = OctantFactory:New(Origin + Vector(-Dist.x, Dist.y, -Dist.z), {}, self.Axis, NewScanVolume, false, self)
        Children[8] = OctantFactory:New(Origin + Vector(-Dist.x, -Dist.y, -Dist.z), {}, self.Axis, NewScanVolume, false, self)

        self.Children = Children
    end
    OctantFactory.__index = OctantFactory

    --Builder Functions
    function SC.CoreOctree.Builder:New(Core, EntCenter, Size, MaxDepth, Bounds, MaxTime, FinishedCallback)
        local obj = {}
        setmetatable(obj,self)
        obj.Core = Core
        obj.EntCenter = EntCenter
        obj.Axis = {EntCenter:GetForward(), EntCenter:GetRight(), EntCenter:GetUp()}
        obj.Size = Size
        obj.MaxDepth = MaxDepth
        obj.MaxTime = MaxTime
        obj.Bounds = Bounds
        obj.RootOctant = OctantFactory:New(Core:GetBoxCenter(), Bounds, obj.Axis, obj.Size, true)
        obj.Finished = false
        obj.CurDepth = 0
        obj.CurParent = obj.RootOctant
        obj.CurOctant = obj.RootOctant
        obj.OctantIndex = {}
        obj.OctantIndex[0] = 1
        obj.FinishedCallback = FinishedCallback
        obj.StartTime = -1
        obj.StopTime = -1
        obj.Volume = 0

        if SC.Debug and SC.DebugLevel <= 3 then
            local Min = -obj.Size * 0.5
            local Max = obj.Size * 0.5

            debugoverlay.SweptBox(EntCenter:LocalToWorld(Core:GetBoxCenter()), EntCenter:LocalToWorld(Core:GetBoxCenter()), Min, Max, EntCenter:GetAngles(), 10, Color(255,0,255,255))
        end

        return obj
    end

    local color_red = Color(255, 0, 0)
    function SC.CoreOctree.Builder:Rebuild(running)
        if not running then self.StartTime = SysTime() end
        local Finished = self.Finished
        local FinishTime = SysTime() + self.MaxTime
        local Time = SysTime()
        local DoDebug = SC.Debug and SC.DebugLevel <= 3

        while Time < FinishTime and not Finished and IsValid(self.Core) and IsValid(self.EntCenter) do
            Time = SysTime()

            local CurOctant = self.CurOctant

            --Check if this octant has content
			local HasContent, IsEnveloped = CurOctant:Check()

            --If we have content in the current octant, figure out if its blocked or needs to subdivide
            if HasContent then
                --We are at max depth or are completely blocked, pop back up a level
                if IsEnveloped or self.CurDepth >= self.MaxDepth then
                    CurOctant.IsBlocked = true
                    self.Volume = self.Volume + CurOctant.Volume

                    if DoDebug then
                        local Center = CurOctant.Position
                        local Size = CurOctant.Size * 0.5
                        local Min = Center - Size
                        local Max = Center + Size
                        local Col = color_white
                        if self.CurDepth < self.MaxDepth then
                            Col = color_red
                        end
                        debugoverlay.SweptBox(self.EntCenter:LocalToWorld(Center - self.Core:GetBoxCenter()), self.EntCenter:LocalToWorld(Center - self.Core:GetBoxCenter()), Min, Max, self.EntCenter:GetAngles(), 10, Col)
                    end
                --Else subdivide and iterate downwards
                else
                    CurOctant:Subdivide()
                    self.CurDepth = self.CurDepth + 1

                    self.OctantIndex[self.CurDepth] = 0
                    self.CurParent = self.CurOctant
                end
            end

            --If the current octant is a leaf or is blocked, go back up the tree and look for the next octant
            if not HasContent or self.CurOctant.IsBlocked then
                --Iterate back up the Octree
                while self.OctantIndex[self.CurDepth] >= 8 do
                    self.CurDepth = self.CurDepth - 1
                    self.CurOctant = self.CurParent
                    self.CurParent = self.CurOctant.Parent

                    if self.CurDepth < 1 then
                        break
                    end
                end

                if self.CurDepth < 1 then
                    Finished = true
                end
            end

            --Next Octant
            if not Finished then
                self.OctantIndex[self.CurDepth] = self.OctantIndex[self.CurDepth] + 1
                self.CurOctant = self.CurParent.Children[self.OctantIndex[self.CurDepth]]
            end
        end

        if not Finished then
            if IsValid(self.Core) and IsValid(self.EntCenter) then
                SC.Msg("[Space Combat 2 - Octree Builder] - Built "..self.RootOctant.OctantCount.." Octants\n", 3)
                timer.Simple(0.1, function()
                    if self then
                        self:Rebuild(true)
                    end
                end)
            else
                SC.Error("[Space Combat 2 - Octree Builder] - Failed, invalid entity", 5)
                self.Finished = true
                self.Failed = true
                self.StopTime = SysTime()

                if self.FinishedCallback then
                    self:FinishedCallback(self)
                end
            end
        else
            self.StopTime = SysTime()
            SC.Msg("[Space Combat 2 - Octree Builder] - Finished Building. Generated "..self.RootOctant.OctantCount.." Octants.\n", 4)
            self.Finished = true

            if self.FinishedCallback then
                self:FinishedCallback(self)
            end
        end
    end
    SC.CoreOctree.Builder.__index = SC.CoreOctree.Builder

    --Global Table
    SC.CoreExplosions = {}

    --Local functions
    --Spawn an explosion somewhere on the hull of the ship
    local function HullExplosion(self, level)
	    if not IsValid(self) then return end

	    local trace = SC.GetRandomHullTrace(self)

	    if trace.Hit and IsValid(trace.Entity) and trace.Entity:GetProtector() == self then
		    --Setup Effect
		    local effect = {}
		    effect.Origin = trace.HitPos
		    effect.Normal = trace.HitNormal
		    effect.Entity = trace.Entity

		    --Small Explosion
		    if not level or level == 1 then
			    SC.CreateEffect("sc_subxpl_small0" .. math.random(1,3), effect)
		    --Medium Explosion
		    elseif level == 2 then
			    SC.CreateEffect("sc_subxpl_medium0" .. math.random(1,3), effect)
		    --Large Explosion
		    elseif level == 3 then
			    SC.CreateEffect("sc_subxpl_large0" .. math.random(1,3), effect)
		    end
	    end
    end

    --Global Functions
    --Fighter Death Sequence
    function SC.CoreExplosions.FighterDeath(self)
	    --Play a "Critial Hit"
	    local effect = {}
	    effect.Origin = self:GetPos()
	    effect.Entity = self

	    SC.CreateEffect("sc_subxpl_fightercrit", effect)

	    --Set Drag
	    SC.SetDrag(self.ConWeldTable, 1)

	    --Knock Around
	    local parent = self
	    if IsValid(self:GetParent()) then parent = self:GetParent() end
	    local physObj = parent:GetPhysicsObject()
	    if IsValid(physObj) then
		    physObj:AddAngleVelocity(VectorRand() * math.Rand(-90, 90))
	    end

	    --Drift for a bit, then pop
	    timer.Simple(2, function()
		    if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_fighter", effect)
		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end

    --Frigate Death Sequence
    function SC.CoreExplosions.FrigateDeath(self)
	    local duration = 3

	    --Initial Boom
	    HullExplosion(self, 1)

	    --Set Drag
	    SC.SetDrag(self.ConWeldTable, 50)

	    --Schedule increasing amounts of hullbooms until death
	    local seconds = 0
	    while seconds < duration do
		    local delay = ((duration - seconds) * 0.30) + math.Rand(-0.2, 0.2)

		    --Small Explosions at first
		    if seconds < (duration * 0.85) then
			    timer.Simple(seconds + delay, function()
				    HullExplosion(self, 1)
			    end)
		    --Medium Explosions near end
		    else
			    timer.Simple(seconds + delay, function()
				    HullExplosion(self, 2)
			    end)
		    end

		    seconds = seconds + delay
	    end

	    --Main Explosion
	    timer.Simple(duration, function()
		    if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_frigate", effect)
		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end

    --Cruiser Death Sequence
    function SC.CoreExplosions.CruiserDeath(self)
        local duration = 9

	    --Initial Booms
		for i = 0, 4 do
            timer.Simple(0.75 * i, function()
				HullExplosion(self, 1)
			end)

            timer.Simple(2 * i, function()
				HullExplosion(self, 1)
			end)

            timer.Simple(2.2 * i, function()
				HullExplosion(self, 1)
			end)

			timer.Simple(0.45 * i, function()
				HullExplosion(self, 2)
			end)
		end

        HullExplosion(self, 3)

        --Set Drag
        SC.SetDrag(self.ConWeldTable, 50)

        --Schedule increasing amounts of hullbooms until death
        local seconds = 0
        while seconds < duration do
            local delay = ((duration - seconds) * 0.30) + math.Rand(-0.2, 0.2)

            --Small Explosions at first
            if seconds < (duration * 0.85) then
                timer.Simple(seconds + delay, function()
                    HullExplosion(self, 1)
                end)
            --Medium Explosions near end
            else
                timer.Simple(seconds + delay, function()
                    HullExplosion(self, 2)
                end)
            end

            seconds = seconds + delay
        end

        --Main Explosion
        timer.Simple(duration, function()
            if not IsValid(self) then return end

            local effect = {}
            effect.Origin = self:LocalToWorld(self:GetBoxCenter())

            SC.CreateEffect("sc_shipsplosion_cruiser", effect)
            SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
                                            EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
                                            KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
                                            THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
            self:PostExplode()
        end)
    end

    --Capital Death Sequence
    function SC.CoreExplosions.CapitalDeath(self)
	    local duration = 15

	    --Initial Booms
		for i = 0, 2 do
			timer.Simple(0.75 * i, function()
				HullExplosion(self, 3)
			end)
		end

		--Main Death Sequence Effect and Sounds
		local effect = {}
		effect.Origin = self:GetBoxCenter()
		effect.Entity = self

		SC.CreateEffect("sc_subxpl_capitalcrit", effect)

	    --Set Drag
	    SC.SetDrag(self.ConWeldTable, 150)

	    --Intersperse some small to medium explosions for texture
	    local seconds = 4
	    while seconds < duration - 5 do
		    local delay = math.Rand(0.15, 1.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, math.random(1,2))
		    end)

		    seconds = seconds + delay
	    end

		--End the sequence with some larger explosions
	    seconds = duration - 5
	    while seconds < duration - 1 do
		    local delay = math.Rand(0.15, 0.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, math.random(2,3))
		    end)

		    seconds = seconds + delay
	    end

	    --Main Explosion
	    timer.Simple(duration, function()
		    if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_capital", effect)
		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end

    --Titan Death Sequence
    function SC.CoreExplosions.TitanDeath(self)
	    local duration = 30

	    --Initial Booms to get the party started
		for i = 0, 2 do
			timer.Simple(0.75 * i, function()
				HullExplosion(self, 3)
			end)
		end

		--Main Big Critical Boom and Sound Cues
		local effect = {}
		effect.Origin = self:GetBoxCenter()
		effect.Entity = self

		SC.CreateEffect("sc_subxpl_titancrit", effect)

		--Set Drag
		SC.SetDrag(self.ConWeldTable, 500)

		--Queue some big Hull Explosions for main death sequence
	    local seconds = 3
	    while seconds < 6 do
		    local delay = math.Rand(0.75, 1.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, 3)
		    end)

		    seconds = seconds + delay
	    end

		--Queue some big Hull Explosions at the end for maximum boom
	    seconds = duration - 5
	    while seconds < duration - 1.5 do
		    local delay = math.Rand(0.15, 0.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, 3)
		    end)

		    seconds = seconds + delay
	    end

		--Intersperse some medium explosions for texture
	    seconds = 7.5
	    while seconds < duration - 4 do
		    local delay = math.Rand(0.35, 1.5)

		    timer.Simple(seconds + delay, function()
			    HullExplosion(self, 2)
		    end)

		    seconds = seconds + delay
	    end

		--Queue Explosion Effect (Negative Delay to match sound effect)
		timer.Simple(duration - 2.75, function()
			if not IsValid(self) then return end

		    local effect = {}
		    effect.Origin = self:LocalToWorld(self:GetBoxCenter())

		    SC.CreateEffect("sc_shipsplosion_titan", effect)
		end)

	    --Main Explosion
	    timer.Simple(duration, function()
		    if not IsValid(self) then return end

		    SC.Explode(self:GetPos(), self.sigrad * 0.5, self.sigrad * 0.5, 10000, {EM=(self.Shield.Max/2),
											EXP=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/2),
											KIN=(self.Armor.Max/2) + (self.Hull.Max/2),
											THERM=(self.Hull.Max/2) + (self.Armor.Max/2) + (self.Shield.Max/4)}, self.Owner, self, self.ConWeldTable)
		    self:PostExplode()
	    end)
    end
end

--Saves a ton of space in the core file
function SC.MakeCoreAccessors(ENT, name)
    if not ENT or not name then return end

    if SERVER then
        ENT["Consume"..name] = function(self, amount, dontsync)
            self["Set"..name.."Amount"](self, math.Clamp(self[name]["Amount"] - amount, 0, self[name]["Max"], dontsync))
	        self[name]["Percent"] = self[name]["Amount"]/self[name]["Max"]
        end

        ENT["Supply"..name] = function(self, amount, dontsync)
            self["Set"..name.."Amount"](self, math.Clamp(self[name]["Amount"] + amount, 0, self[name]["Max"], dontsync))
	        self[name]["Percent"] = self[name]["Amount"]/self[name]["Max"]
        end
    end

    ENT["Get"..name.."Max"] = function(self)
        return self[name]["Max"]
    end

    local maxid
    ENT["Set"..name.."Max"] = function(self, value, dontsync)
        self[name]["Max"] = value

        if not dontsync then
            SC.NWAccessors.SyncValue(self, maxid)
        end
    end
    SC.NWAccessors.CreateSpecialNWAccessor(ENT, name.."Max", "number", ENT["Get"..name.."Max"], ENT["Set"..name.."Max"], 1)
    maxid = SC.NWAccessors.GetNWAccessorID(ENT, name.."Max")

    ENT["Get"..name.."Percent"] = function(self)
        return self[name]["Percent"]
    end

    ENT["Get"..name.."Amount"] = function(self)
        return self[name]["Amount"]
    end

    local amountid
    ENT["Set"..name.."Amount"] = function(self, value, dontsync)
        value = math.Clamp(value,0,self[name]["Max"])

	    self[name]["Amount"] = value
	    self[name]["Percent"] = value/self[name]["Max"]

        if not dontsync then
            SC.NWAccessors.SyncValue(self, amountid)
        end

	    return self[name]["Percent"]
    end
    SC.NWAccessors.CreateSpecialNWAccessor(ENT, name.."Amount", "number", ENT["Get"..name.."Amount"], ENT["Set"..name.."Amount"], 2)
    amountid = SC.NWAccessors.GetNWAccessorID(ENT, name.."Amount")
end

--Picks a point on the exterior bounding box and traces inwards to find a
--point on the hull, useful for things like death explosions, damage fires,
--and other such things.
--	Returns tracedata.
local maxHullTraceAttempts = 50	--Used to prevent infinite loops, but may cause inaccurate tracing in rare cases.
function SC.GetRandomHullTrace(self)
	if not IsValid(self) then return nil end
	if self:GetClass() ~= "ship_core" then
		if IsValid(self:GetProtector()) then
			self = self:GetProtector()
		else
			return nil
		end
	end

	local boxSize = (self:GetBoxMax() - self:GetBoxMin()):Length() + 10

	--Pick a random point along the bounding box of the ship
	math.randomseed(SysTime())
	local vec = VectorRand()
	vec = vec * boxSize
	vec.x = math.Clamp(vec.x, self:GetBoxMin().x, self:GetBoxMax().x)
	vec.y = math.Clamp(vec.y, self:GetBoxMin().y, self:GetBoxMax().y)
	vec.z = math.Clamp(vec.z, self:GetBoxMin().z, self:GetBoxMax().z)
	vec = vec + self:GetBoxCenter()
	vec = self:LocalToWorld(vec)

	local trace
	local attempts = 0
	while attempts < maxHullTraceAttempts and (trace == nil or not IsValid(trace.Entity) or not IsValid(trace.Entity:GetProtector()) or trace.Entity:GetProtector() ~= self) do
		attempts = attempts + 1

		--Aim at a random part of the ship
		local part
		while not IsValid(part) do
			part = table.Random(self.ConWeldTable)
		end

		--Trace from point to part
		local dir = (part:GetPos() - vec):GetNormalized()
		local tData = {
			start = vec,
			endpos = dir * 30000,
			ignoreworld = true,
			filter = function(e)
                if not IsValid(e) then return false end

				if e:GetProtector() == self then
					return true
				end

				return false
			end
		}
		trace = util.TraceLine(tData)
	end

	return trace
end

--Set Drag on a number of entities
function SC.SetDrag(parts, coef)
	if not parts then return end

	for k,v in pairs(parts) do
		if IsValid(v) then
			local physobj = v:GetPhysicsObject()
			if IsValid(physobj) then
				v.SB_Ignore = true
				physobj:EnableDrag(true)
				physobj:SetDragCoefficient(coef)
			end
		end
	end
end

function SC.GetWeldedEntities( ent, ResultTable )
	local ResultTable = ResultTable or {}

	if not IsValid(ent) then return end
	if IsValid( ResultTable[ ent ] ) then return end

	ResultTable[ ent ] = ent

	local ConTable = constraint.GetTable( ent )

	for k, con in ipairs( ConTable ) do

		for EntNum, Ent in pairs( con.Entity ) do
			if con.Type == "Weld" then
				SC.GetWeldedEntities(Ent.Entity, ResultTable)
			end
		end

	end

	return ResultTable
end

function SC.CalcPercent(Start, Amounts, Mult, Clamp, min, max)
    local Mult = Mult or 1
    local ret = 0

    for i, k in pairs(Amounts) do
        ret = ret + ((k/100 * Mult) * (1/i^1.45))
    end

    if Clamp then ret = math.Clamp(ret, min, max) end

    if Start == 0 then
        return ret
    end

    return Start + (Start * ret)
end

local MaxResist = 90
function SC.CalcResist(Start, Amounts)
    local ret = Start*100
    for i,k in pairs(Amounts) do
        ret = ret + (k * (0.5/i^1.2))
    end

    return math.Clamp(ret, -MaxResist, MaxResist)/100
end