AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_lsentity"
ENT.PrintName = "Space Combat 2 - Ship Module"
ENT.Author = "Lt.Brandon"
ENT.Purpose = "The solution to cancer."
ENT.Instructions = "Press large red button, observe giant lightning spears impaling you."

local base = scripted_ents.Get("base_lsentity")
hook.Add( "InitPostEntity", "base_scmodule_post_entity_init", function()
	base = scripted_ents.Get("base_lsentity")
end)

ENT.Spawnable = false
ENT.AdminOnly = false
ENT.PlayerToggleable = true
ENT.IsSCModule = true

function ENT:SharedInit()
    base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Enabled", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Cycling", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "CyclePercent", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "CycleActivationCost", "table", {})
    SC.NWAccessors.CreateNWAccessor(self, "CycleDuration", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "HasCycle", "bool", true)
    SC.NWAccessors.CreateNWAccessor(self, "PassiveBonuses", "table", {})
    SC.NWAccessors.CreateNWAccessor(self, "ModuleName", "cachedstring", "FAIL")
    SC.NWAccessors.CreateNWAccessor(self, "HideOverlay", "bool", false)
end

function ENT:GetBonusText()
    local Bonuses = self:GetPassiveBonuses()

    if next(Bonuses) then
        local Text = "Passive Bonuses:\n"
        for i,k in pairs(Bonuses) do
            Text = Text..i..": "..k.."\n"
        end

        return Text
    end
end

function ENT:GetActivationCostText()
    local Cost = self:GetCycleActivationCost()

    if next(Cost) then
        local Text = "Activation Cost:\n"
        for i,k in pairs(Cost) do
            Text = Text..i..": "..k.."\n"
        end

        return Text
    end
end

function ENT:GetFittingText()
    local Fitting = self:GetFitting()
    local Text = "\n\n-Fitting-"

    if Fitting.CPU > 0 then Text = Text.."\nCPU: "..Fitting.CPU end
    if Fitting.PG > 0 then Text = Text.."\nPG: "..Fitting.PG end
    if Fitting.Slot ~= "None" then
        if Fitting.NumSlots and Fitting.NumSlots ~= 1 then
            Text = Text.."\nSlots Needed: "..Fitting.NumSlots
        end

        Text = Text.."\nSlot: "..Fitting.Slot
    end
	if table.Count(Fitting.Classes) > 0 then
        Text = Text.."\nRestricted To Classes:"

        for i,k in pairs(Fitting.Classes) do
            Text = Text.."\n    - "..k
        end
    end

    if Text == "\n\n-Fitting-" then
		Text = ""
	end

    Text = Text.."\n\nLink Status: "..Fitting.Status

    return Text
end

if CLIENT then
    function ENT:Think()
        if not self:BeingLookedAtByLocalPlayer() then return end

        if self:GetHideOverlay() then
            self:SetOverlayText("")
            return
        end

        local Text = self:GetModuleName().."\n\n"
        local BonusText = self:GetBonusText()
        local ActivationText = self:GetActivationCostText()
        local FittingText = self:GetFittingText()

        if self:GetHasCycle() then
            Text = Text.."Status: "
            if self:GetCycling() then
                Text = Text.."Cycling\n"
            else
                Text = Text.."Off\n"
            end
        end

        if BonusText then
            Text = Text.."\n"..BonusText
        end

        if self:GetHasCycle() then
            if ActivationText then
                Text = Text..ActivationText
            end

            Text = Text.."\nDuration: "..sc_ds(self:GetCycleDuration())
            Text = Text.."\nCycle: "..sc_ds(self:GetCyclePercent() * 100) .. "%"
        end

        Text = Text..FittingText

        self:SetOverlayText(Text)
    end
else
    function ENT:Initialize()
        base.Initialize(self)

        self.SoundFilter = RecipientFilter(false)
        self:UpdateSoundFilter()
    end

    function ENT:Setup()
        base.Setup(self)

        self:UpdateSoundFilter()
        self:SetTurnOnSound("ambient/machines/thumper_startup1.wav")
        self:SetTurnOffSound("vehicles/apc/apc_shutdown.wav")
        self:SetLoopSound("ambient/machines/combine_shield_loop3.wav")
        self:SetEnableSound("buttons/button16.wav")
    end

    function ENT:SetEnableSound(path)
        self.EnableSound = CreateSound(self, Sound(path), self.SoundFilter)
        self.EnableSound_path = path
    end

    function ENT:SetTurnOnSound(path)
        self.TurnOnSound = CreateSound(self, Sound(path), self.SoundFilter)
        self.TurnOnSound_path = path
    end

    function ENT:SetTurnOffSound(path)
        self.TurnOffSound = CreateSound(self, Sound(path), self.SoundFilter)
        self.TurnOffSound_path = path
    end

    function ENT:SetLoopSound(path)
        self.LoopSound = CreateSound(self, Sound(path), self.SoundFilter)
        self.LoopSound_path = path
    end

    function ENT:SoundPlay(which)
        if self.Muted then return end

        if self[which] then
            local setfunc = self["Set" .. which]
            local path = self[which .. "_path"]

            setfunc(self, path)

            self[which]:Play()
        end
    end

    function ENT:SoundStop(which)
        if self[which] then
            self[which]:Stop()
        end
    end

    function ENT:SetMuted(b)
        self.Muted = b

        if b then
            self:SoundStop("TurnOnSound")
            self:SoundStop("TurnOffSound")
            self:SoundStop("LoopSound")
            self:SoundStop("EnableSound")
        elseif self:GetCycling() then
            self:SoundPlay("LoopSound")
        end
    end

    function ENT:UpdateSoundFilter()
        local Owner = SC.GetEntityOwner(self)
        local Protector = self:GetProtector()

        if not self.SoundFilter then
            self.SoundFilter = RecipientFilter(false)
        end

        self.SoundFilter:RemoveAllPlayers()

        if IsValid(Protector) then
            if Protector.IsCoreEnt then
                local ManagedPlayers = Protector:GetManagedPlayers()
                for _, Player in pairs(ManagedPlayers) do
                    if IsValid(Player) and Player:GetPos():Distance(self:GetPos()) < 1024 then
                        self.SoundFilter:AddPlayer(Player)
                    end
                end
            end
        end

        if IsValid(Owner) and Owner:GetPos():Distance(self:GetPos()) < 1024 then
            self.SoundFilter:AddPlayer(Owner)
        end
    end

    function ENT:OnRemove()
        base.OnRemove(self)
        self:SetMuted(true)
    end


    function ENT:GetWirePorts()
        return {"On", "Mute", "Hide overlay"}, {"Cycle Percent", "On"}
    end

    function ENT:CanEnable()
        return IsValid(self:GetProtector()) and self:GetProtector():CheckClass(self:GetFitting()) and self:GetProtector():CheckFitting(self:GetFitting())
    end

    function ENT:OnModuleEnabled()
        -- Override this to add functionality on module enabled
    end

    function ENT:OnModuleDisabled()
        -- Override this to add functionality on module disabled
    end

    function ENT:PreStartCycle()
        -- Called before the entity checks if it can cycle
    end

    function ENT:OnStartedCycling()
        -- Called when the entity first starts cycling. This is different from OnCycleStarted, which is called at the start of every cycle.
    end

    function ENT:OnCycleStarted()
        -- Called when the module starts a cycle
    end

    function ENT:OnCycleUpdated(TimeRemaining)
        -- Called when a module's cycle is updated
    end

    function ENT:OnCycleFinished()
        -- Called when the module finishes it's cycle.
    end

    function ENT:OnStoppedCycling()
        -- Called when the entity stops cycling. This is different from OnCycledFinished, which calls at the end of every cycle.
    end

    function ENT:OnCoreUpdated()
        -- Called when the core finishes recalculating. Commonly used to apply bonuses to modules.
    end

    function ENT:EnableModule()
        self:UpdateSoundFilter()

		if self:CanEnable() then
            local Fitting = self:GetFitting()
            self:GetProtector():ConsumeFitting(Fitting)
            self:SetFitting(Fitting)

            self:SetEnabled(true)
            self:OnModuleEnabled()

            self:SoundPlay("EnableSound")

            return true
		else
            self:DisableModule(true)
            return false
		end
    end

    function ENT:DisableModule(DisableModifierUpdate)
        local Core = self:GetProtector()
        local Fitting = self:GetFitting()
        if IsValid(Core) then
            local HasFitting, HasSlot, HasCPU, HasPG = Core:CheckFitting(Fitting)

            if not Core:CheckClass(Fitting) then
                Fitting.Status = "Offline - Wrong Class"
            elseif not HasFitting then
                local first = true
                Fitting.Status = "Offline - Not enough "

                if not HasSlot then
                    Fitting.Status = Fitting.Status.."Slots"
                    first = false
                end

                if not HasCPU then
                    if first then
                        Fitting.Status = Fitting.Status.."CPU"
                        first = false
                    else
                        Fitting.Status = Fitting.Status.."/CPU"
                    end
                end

                if not HasPG then
                    if first then
                        Fitting.Status = Fitting.Status.."PG"
                        first = false
                    else
                        Fitting.Status = Fitting.Status.."/PG"
                    end
                end
            else
                Fitting.Status = "Offline"
            end

            if not DisableModifierUpdate then
                Core:UpdateModifiers()
            end
        else
            Fitting.Status = "Offline"
        end

        self:SetFitting(Fitting)
        self:SetEnabled(false)
    end

    function ENT:Think()
        if not self:GetProtector() then self:GetFitting().Status = "Offline" end

        if self:GetCycling() then
            self:UpdateCycle()
        elseif self.SC_Active and self:CanCycle() then
            self:StartCycle()
        end

        self:NextThink(CurTime() + math.min(1, self:GetCycleDuration() / 3))
        return true
    end

    function ENT:CanCycle()
        return self:GetHasCycle() and IsValid(self:GetProtector()) and self:GetFitting().CanRun and self:HasResources(self:GetCycleActivationCost())
    end

    function ENT:StartCycle(StartedByEndCycle)
        if self:GetCycling() then return false end

        self:UpdateSoundFilter()

        self:PreStartCycle()

        if not self:CanCycle() then return false end

        --Set cycle as started
        self:SetCycling(true)
        self.CycleEnd = CurTime() + self:GetCycleDuration()
        self:ConsumeResources(self:GetCycleActivationCost())

        if not StartedByEndCycle then
            self:OnStartedCycling()
            self:SoundPlay("TurnOnSound")
            self:SoundPlay("LoopSound")

            if WireLib then
                WireLib.TriggerOutput(self, "On", 1)
            end
        end

        self:OnCycleStarted()

        return true
    end

    function ENT:UpdateCycle()
        if self:GetCycling() then
            local TimeRemaining = self.CycleEnd - CurTime()

            if TimeRemaining < (FrameTime() * 2) then
                self:EndCycle()
                if WireLib then
                    WireLib.TriggerOutput(self, "Cycle Percent", 0)
                end
            else
                self:SetCyclePercent(1 - TimeRemaining / self:GetCycleDuration())
                self:OnCycleUpdated(TimeRemaining)
                if WireLib then
                    WireLib.TriggerOutput(self, "Cycle Percent", self:GetCyclePercent())
                end
            end
        end
    end

    function ENT:EndCycle()
        if self:GetCycling() then
            self:SetCycling(false)
            self:SetCyclePercent(0)
            self:OnCycleFinished()
            self.CycleEnd = 0

            if self.SC_Active and self:CanCycle() then
                self:StartCycle(true)
            else
                self:OnStoppedCycling()
                self:SoundPlay("TurnOffSound")
                self:SoundStop("LoopSound")
                if WireLib then
                    WireLib.TriggerOutput(self, "On", 0)
                end
            end

            return true
        end

        return false
    end

    function ENT:TriggerInput(Input, Value)
        if Input == "On" and self:GetHasCycle() then
            if Value ~= 0 then
                if self:GetFitting().CanRun then
                    self.SC_Active = true
                    self:StartCycle()
                end
            else
                self.SC_Active = false
            end
        elseif Input == "Mute" then
            self:SetMuted(Value ~= 0)
        elseif Input == "Hide overlay" then
            self:SetHideOverlay(Value ~= 0)
        end
    end

    -- TODO: Cleanup code, looks like ass
    function ENT:Use(ply)
        if not IsValid(self:GetPlayer()) and not ply:IsAdmin() then return end

        if IsValid(self:GetPlayer()) then --Only protect if the person is here
            if not ply:IsAdmin() and (ply ~= self:GetPlayer() and self:GetPlayer():GetInfoNum( "sc_generators_use_protect", 1 ) ~= 0 and self:GetPlayer().CPPIGetFriends) then
                local friends = self:GetPlayer():CPPIGetFriends()
                if friends then
                    local ok = false
                    for i=1,#friends do
                        if friends[i] == ply then
                            ok = true
                            break
                        end
                    end
                    if not ok then return end
                end
            end
        end

        if self.PlayerToggleable then
            self:TriggerInput("On", self.SC_Active and 0 or 1)
        end
    end
end