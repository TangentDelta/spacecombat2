e2function string entity:getFaction()
	if this then
		if this:GetClass() == "player" then
            return team.GetName(this:Team())
        elseif this.CPPIGetOwner then
            local Owner = this:CPPIGetOwner()
            if Owner:GetClass() == "player" then
                return team.GetName(Owner:Team())
            else
                return ""
            end
		else
			return ""
		end
	end
end