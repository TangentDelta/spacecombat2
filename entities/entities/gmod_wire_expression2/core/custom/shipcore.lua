
--[[***********************************************************
*************          Duneds core-reading extension          ************
***********************************************************]]--

e2function number entity:getShieldAmount()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetShieldAmount()
		else
			return 0
		end
	end
end

e2function number entity:getArmorAmount()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetArmorAmount()
		else
			return 0
		end
	end
end

e2function number entity:getHullAmount()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetHullAmount()
		else
			return 0
		end
	end
end

e2function number entity:getShieldPercent()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetShieldPercent()
		else
			return 0
		end
	end
end

e2function number entity:getArmorPercent()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetArmorPercent()
		else
			return 0
		end
	end
end

e2function number entity:getHullPercent()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetHullPercent()
		else
			return 0
		end
	end
end

e2function number entity:getShieldMax()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetShieldMax()
		else
			return 0
		end
	end
end

e2function number entity:getArmorMax()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetArmorMax()
		else
			return 0
		end
	end
end

e2function number entity:getHullMax()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetHullMax()
		else
			return 0
		end
	end
end

--[[***********************************************************
*************          Fehas core-reading extension          ************
***********************************************************]]--

e2function number entity:getCapAmount()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetCapAmount()
		else
			return 0
		end
	end
end

e2function number entity:getCapPercent()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetCapPercent()
		else
			return 0
		end
	end
end

e2function number entity:getCapMax()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetCapMax()
		else
			return 0
		end
	end
end


--[[***********************************************************
*************          Steeveeo's Core Extensions          ************
***********************************************************]]--


e2function entity entity:getCoreEnt()
	if not IsValid(this) then return nil end
	if this:IsPlayer() or this:IsWorld() then return nil end

	return this:GetProtector() or NULL
end

e2function entity entity:sc2ShipGyropod()
	if not IsValid(this) then return nil end
	if this:IsPlayer() or this:IsWorld() then return nil end

	local Core = this:GetProtector()

	if IsValid(Core) then
		return (Core:GetGyropod() or NULL)
	else
		return nil
	end
end

e2function vector entity:sc2ShipCenter()
	if not IsValid(this) then return end
	if this:GetClass() ~= "ship_core" then return end

	return this:GetBoxCenter()
end

e2function vector entity:sc2ShipForward()
	if not IsValid(this) then return end
	if this:GetClass() ~= "ship_core" then return end

	return this:GetShipForward()
end

e2function vector entity:sc2ShipRight()
	if not IsValid(this) then return end
	if this:GetClass() ~= "ship_core" then return end

	return this:GetShipRight()
end

e2function vector entity:sc2ShipUp()
	if not IsValid(this) then return end
	if this:GetClass() ~= "ship_core" then return end

	return this:GetShipUp()
end

e2function string entity:getShipName()
	if not IsValid(this) then return "" end

	local core = this
	if this:GetClass() ~= "ship_core" then
		if IsValid(this:GetProtector()) then
			core = this:GetProtector()
		else
			return ""
		end
	end

	return core:GetShipName()
end

e2function void entity:setShipName(string name)
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "ship_core" then return end

	this:SetShipName(name)
end

--ADMIN ONLY FROM HERE DOWN--

e2function number entity:setHullAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:SetHullAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:setArmorAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:SetArmorAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:setShieldAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:SetShieldAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:setCapAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:SetCapAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:combatImmune(number active)
	if (!IsValid(this)) then return -1 end

	local owner = self.player

	if (owner:IsAdmin() and !this:IsPlayer()) then
		this.SC_Immune = tobool(active)
		return active
	end

	return -1
end

--[[**********************************************************
*************Lt.Brandon's core/weapon-reading extension************
***********************************************************]]--
local E2TABLE = {n = {}, s = {}, stypes = {}, ntypes = {}, size = 0, istable = true}
e2function number entity:getSigRad()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetNodeRadius()
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShieldRechargeTime()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetShieldRechargeTime()
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShieldRecharge()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:CalculateRecharge(this.Shield.Max, this.Shield.Amount, this:GetShieldRechargeTime())
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShieldRechargeMax()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:CalculateRecharge(this.Shield.Max, this.Shield.Max * 0.25, this:GetShieldRechargeTime())
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShipMass()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this.total_mass
		else
			return -1
		end
	end
	return -2
end

e2function string entity:getCoreClass()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetShipClass()
		else
			return "Unknown/Not a core"
		end
	end
	return "Invalid Entity"
end

e2function number entity:getStationCore()
	if IsValid(this) then
		if string.find(this:GetClass(), "ship_core") then
			return (1 and this:GetIsStation()) or 0
		else
			return -1
		end
	end

	return -2
end

e2function number entity:isLifeSupportEnabled()
	if IsValid(this) then
        if this:GetClass() == "ship_core" then
            local Enabled = this:GetLifeSupportEnabled()
            return Enabled and 1 or 0
		else
			return -1
		end
	end
	return -2
end

e2function number entity:enableLifeSupport()
	if IsValid(this) then
        if this:GetClass() == "ship_core" then
            this:EnableLifeSupport()
            return 1
		else
			return -1
		end
	end
	return -2
end

e2function number entity:disableLifeSupport()
	if IsValid(this) then
        if this:GetClass() == "ship_core" then
            this:DisableLifeSupport()
            return 1
		else
			return -1
		end
	end
	return -2
end

--weapon functions
e2function table entity:getTurretWeapons()
	if IsValid(this) then
		if this:GetClass() == "sc_turret" then
			local tab = table.Copy(E2TABLE)

			for i,k in pairs(this.Weapons) do
				tab.n[i] = k
				tab.ntypes[i] = "e"
				tab.size = tab.size + 1
			end

			return tab
		end
	end

	return table.Copy(E2TABLE)
end

e2function string entity:getWeaponName()
	if IsValid(this) then
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.PrintName
		else
			return "Unknown/Not a weapon"
		end
	end
	return "Invalid Entity"
end

e2function string entity:getWeaponClass()
	if IsValid(this) then
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.Data.GUN.CLASS
		else
			return "Unknown/Not a weapon"
		end
	end
	return "Invalid Entity"
end

e2function number entity:getShotsRemaining()
	if IsValid(this) then
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.ShotsRemaining
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getIsReloading()
	if IsValid(this) then
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.IsReloading and 1 or 0
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getIsFiring()
	if IsValid(this) then
		if string.find(this:GetClass(), "sc_weapon*") then
			return this:GetShooting() and 1 or 0
		else
			return -1
		end
	end
	return -2
end

--#region SC2 Turret Data

---Get the number of launchers on a turret
e2function number entity:getTurretLauncherCount()
	if IsValid(this) and this.IsSCTurret then
		return #this.Weapons
	end
end

---Get the type of a launcher in a turret
e2function string entity:getTurretLauncherType(number LauncherIndex)
	if IsValid(this) and this.IsSCTurret and this.Weapons[LauncherIndex] then
		return this.Weapons[LauncherIndex].LauncherType:GetName()
	end

	return "Invalid"
end

---Get the projectile recipe name of a launcher in a turret
e2function string entity:getTurretLauncherRecipe(number LauncherIndex)
	if IsValid(this) and this.IsSCTurret and this.Weapons[LauncherIndex] then
		return this.Weapons[LauncherIndex].ProjectileRecipe:GetName()
	end

	return "Invalid"
end

---Get the type of a launcher in a weapon module
e2function string entity:getWeaponLauncherType()
	if IsValid(this) and this.IsSCWeapon then
		return this.Launcher.LauncherType:GetName()
	end

	return "Invalid"
end

---Get the projectile recipe name of a launcher in a turret
e2function string entity:getWeaponLauncherRecipe()
	if IsValid(this) and this.IsSCWeapon then
		return this.Launcher.ProjectileRecipe:GetName()
	end

	return "Invalid"
end

--#endregion

--[[**********************************************************
*************Hobnob's weapon-reading extension************
***********************************************************]]--

e2function number entity:getProjectileVel()
	if IsValid(this) then
		if string.find(this:GetClass(), "sc_weapon*") then
			if not this.Data then return -1 end
			if not this.Data.PROJECTILE then return -1 end
			return this.Data.PROJECTILE.VELOCITY or -1
		else
			return -1
		end
	end
	return -2
end


--[[****************************************************heh***
*************Lambda's weapon-reading extension****************
***********************************************************]]--

e2function vector entity:getWeaponDirection()
	if IsValid(this) then
		if string.find(this:GetClass(), "sc_weapon*") then
			if not this.Data or not this.Data.GUN then return Vector(0,0,0) end
			if not this.Data.GUN.FIRINGANGLE or this.Data.GUN.FIRINGANGLE == 1 then
				return Vector(1,0,0)
			elseif this.Data.GUN.FIRINGANGLE == 2 then
				return Vector(-1,0,0)
			elseif this.Data.GUN.FIRINGANGLE == 3 then
				return Vector(0,1,0)
			elseif this.Data.GUN.FIRINGANGLE == 4 then
				return Vector(0,-1,0)
			elseif this.Data.GUN.FIRINGANGLE == 5 then
				return Vector(0,0,-1)
			elseif this.Data.GUN.FIRINGANGLE == 6 then
				return Vector(0,0,1)
			end
		else
			return Vector(0,0,0)
		end
	end
	return Vector(0,0,0)
end

--[[**********************************************************
***************Kanzuke's repair-reading extension*************
****Why are we still following this author heading format?****
***********************************************************]]--

e2function string entity:getRepairType()
	if IsValid(this) then
		if this:GetClass() == "sc_module_booster" then
			return this:GetType()
		else
			return "Unknown/Not a repair"
		end
	end
	return "Invalid Entity"
end

e2function string entity:getRepairName()
	if IsValid(this) then
		if this:GetClass() == "sc_module_booster" then
			return this:GetModuleName()
		else
			return "Unknown/Not a repair"
		end
	end
	return "Invalid Entity"
end

e2function number entity:getRepairAmount()
	if IsValid(this) then
		if this:GetClass() == "sc_module_booster" then
			return this:GetBoostAmount()
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getRepairCost()
	if IsValid(this) then
		if this:GetClass() == "sc_module_booster" then
			if not this.Boost then return -1 end
			if not this.Boost.CAP then return -1 end
			return math.Round(this.Boost.CAP) or -1
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getRepairDuration()
	if IsValid(this) then
		if this:GetClass() == "sc_module_booster" then
			return this:GetCycleDuration()
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getIsCycling()
	if IsValid(this) then
		if this:GetClass() == "sc_module_booster" then
			return this:GetCycling()
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getCyclePercent()
	if IsValid(this) then
		if this:GetClass() == "sc_module_booster" then
			return this:GetCyclePercent()
		else
			return -1
		end
	end
	return -2
end

--[[**********************************************************
**************Kanzuke's turret painting extension*************
**i'm different*******************************************]]--

--
--
--
-- Disabled until further notice, these are broken as of 9/29/2017. TODO - Fix this. - Lt.Brandon
--
--
--

--[[
e2function void entity:turretDisableInheritance()
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "sc_turret" then return end

	this.NextMatCheck = CurTime() + 1e30
end

e2function void entity:turretEnableInheritance()
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "sc_turret" then return end

	this.NextMatCheck = CurTime() + 1
end

e2function number entity:turretHoloCount()
	if not IsValid(this) then return -2 end
	if this:GetClass() ~= "sc_turret" then return -1 end

	local Count = 0
	for i,h in ipairs(this.Holograms) do
		if h:GetColor().a ~= 0 then
			Count = Count + 1
		end
	end

	return Count
end

local function TranslateIndex(ent,index)
	local Count = 0
	for i,h in ipairs(ent.Holograms) do
		if h:GetColor().a ~= 0 then
			Count = Count + 1
			if Count == index then return i end
		end
	end
end

local function ColorClamp(c)
	c.r = math.Clamp(c.r,0,255)
	c.g = math.Clamp(c.g,0,255)
	c.b = math.Clamp(c.b,0,255)
	c.a = 255
	return c
end

e2function vector entity:turretHoloColor(index)
	if not IsValid(this) then return {-2,-2,-2} end
	if this:GetClass() ~= "sc_turret" then return {-1,-1,-1} end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return {-1,-1,-1} end
	local c = H:GetColor()
	return { c.r, c.g, c.b }
end

e2function void entity:turretHoloColor(index, vector color)
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "sc_turret" then return end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return end

	H:SetColor(ColorClamp(Color(color[1],color[2],color[3])))
end

e2function string entity:turretHoloMaterial(index)
	if not IsValid(this) then return "" end
	if this:GetClass() ~= "sc_turret" then return "" end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return "" end
	return H:GetMaterial()
end

e2function void entity:turretHoloMaterial(index, string material)
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "sc_turret" then return end
	if string.lower(material) == "pp/copy" then return end -- Apparently this material is bad?

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return end
	H:SetMaterial(material)
end

e2function array entity:turretHoloMaterials(index)
	if not IsValid(this) then return {} end
	if this:GetClass() ~= "sc_turret" then return {} end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return {} end
	return H:GetMaterials()
end

e2function string entity:turretHoloSubMaterial(index, matindex)
	if not IsValid(this) then return "" end
	if this:GetClass() ~= "sc_turret" then return "" end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return "" end
	return H:GetSubMaterial(matindex-1) or ""
end

e2function void entity:turretHoloSubMaterial(index, matindex, string material)
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "sc_turret" then return end
	if string.lower(material) == "pp/copy" then return end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return end
	H:SetSubMaterial(matindex-1,material)
end

e2function number entity:turretHoloSkin(index)
	if not IsValid(this) then return 0 end
	if this:GetClass() ~= "sc_turret" then return 0 end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return 0 end
	return H:GetSkin()
end

e2function number entity:turretHoloSkinCount(index)
	if not IsValid(this) then return 0 end
	if this:GetClass() ~= "sc_turret" then return 0 end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return 0 end
	return H:SkinCount()
end

e2function void entity:turretHoloSkin(index, skin)
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "sc_turret" then return end

	local H = this.Holograms[TranslateIndex(this,index)]
	if not IsValid(H) then return end
	H:SetSkin(skin)
end
]]--