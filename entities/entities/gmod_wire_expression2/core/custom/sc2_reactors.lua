--[[
    These functions will work on any class derived from sc_module_reactor

    Function List:
        n sc2ReactorGetSizeForClass(s:Class, n:IsAuxiliary)
        n sc2ReactorGetCoolantCount()
        n sc2ReactorGetReactionCount(s:ReactionType)
        t sc2ReactorGetCoolantInfo(n:CoolantID)
        t sc2ReactorGetReactionInfo(s:ReactionType, n:ReactionID)
        a sc2ReactorGetReactorTypes()

        n e:isSC2Reactor()
        n e:isSC2ReactorAlarmDisabled()
        n e:isSC2ReactorAuxiliary()
        n e:sc2ReactorMaxTemperature()
        n e:sc2ReactorTemperature()
        n e:sc2ReactorPGBonus()
        n e:sc2ReactorSize()
        n e:sc2ReactorReactionID()
        n e:sc2ReactorCoolantID()
        s e:sc2ReactorCoolantName()
        s e:sc2ReactorReactionName()
        s e:sc2ReactorType()
]]

-- Global functions
e2function number sc2ReactorGetSizeForClass(string Class, IsAuxiliary)
    return (SC.Reactors.GetReactorSizeForClass(Class) or 1) * (IsAuxiliary == 1 and 0.25 or 1)
end

e2function number sc2ReactorGetCoolantCount()
    return table.Count(SC.Reactors.GetCoolantTypes())
end

e2function number sc2ReactorGetReactionCount(string ReactionType)
    return table.Count(SC.Reactors.GetReactionTypes(ReactionType))
end

e2function table sc2ReactorGetCoolantInfo(CoolantID)
    local CoolantInfo = SC.Reactors.GetCoolantInfo(CoolantID)
    local Out = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}
    if not CoolantInfo then return Out end

    Out.s = {
        CoolantName = CoolantInfo.CoolantName,
        OutResource = CoolantInfo.OutResource,
        HeatDissipation = CoolantInfo.HeatDissipation,
        AmountNeeded = CoolantInfo.AmountNeeded
    }

    Out.stypes = {
        CoolantName = "s",
        OutResource = "s",
        HeatDissipation = "n",
        AmountNeeded = "n"
    }

    return Out
end

e2function table sc2ReactorGetReactionInfo(string ReactionType, ReactionID)
    local ReactionInfo = SC.Reactors.GetReactionInfo(ReactionType, ReactionID)
    local Out = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}
    if not ReactionInfo then return Out end

    local Inputs = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}
    for i,k in pairs(ReactionInfo.Inputs) do
        if type(i) == "string" and type(k) == "number" then
            Inputs.s[i] = k
            Inputs.stypes[i] = "n"
            Inputs.size = Inputs.size + 1
        end
    end

    local Outputs = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}
    for i,k in pairs(ReactionInfo.Outputs) do
        if type(i) == "string" and type(k) == "number" then
            Outputs.s[i] = k
            Outputs.stypes[i] = "n"
            Outputs.size = Outputs.size + 1
        end
    end

    Out.s = {
        DisplayName = ReactionInfo.DisplayName,
        ReactionType = ReactionInfo.ReactionType,
        EfficientTemperature = ReactionInfo.EfficientTemperature,
        PowerVariable = ReactionInfo.PowerVariable,
        CycleTime = ReactionInfo.CycleTime,
        Inputs = Inputs,
        Outputs = Outputs
    }

    Out.stypes = {
        DisplayName = "s",
        ReactionType = "s",
        EfficientTemperature = "n",
        PowerVariable = "n",
        CycleTime = "n",
        Inputs = "t",
        Outputs = "t"
    }

    Out.size = 7

    return Out
end

e2function array sc2ReactorGetReactorTypes()
    local Out = {}
    for i, k in pairs(SC.Reactors.GetReactorTypes()) do
        if type(k) == "string" then
            table.insert(Out, k)
        end
    end
    return Out
end

-- Entity Functions
e2function number entity:isSC2Reactor()
	if IsValid(this) and this.IsSCReactor then
		return 1
    end

    return 0
end

e2function number entity:isSC2ReactorAlarmDisabled()
	if IsValid(this) and this.IsSCReactor and this.DisableAlarm then
		return 1
    end

    return 0
end

e2function number entity:isSC2ReactorAuxiliary()
	if IsValid(this) and this.IsSCReactor and this:GetIsAuxiliary() then
		return 1
    end

    return 0
end

e2function number entity:sc2ReactorMaxTemperature()
	if IsValid(this) and this.IsSCReactor then
		return this.MaxTemperature
    end

    return 0
end

e2function number entity:sc2ReactorTemperature()
	if IsValid(this) and this.IsSCReactor then
		return this:GetTemperature()
    end

    return 0
end

e2function number entity:sc2ReactorPGBonus()
	if IsValid(this) and this.IsSCReactor then
		return this:GetPGBonus()
    end

    return 0
end

e2function number entity:sc2ReactorSize()
	if IsValid(this) and this.IsSCReactor then
		return this:GetReactorSize()
    end

    return 0
end

e2function number entity:sc2ReactorReactionID()
	if IsValid(this) and this.IsSCReactor then
		return this:GetReactorReaction()
    end

    return 0
end

e2function number entity:sc2ReactorCoolantID()
	if IsValid(this) and this.IsSCReactor then
		return this:GetReactorCoolant()
    end

    return 0
end

e2function string entity:sc2ReactorReactionName()
	if IsValid(this) and this.IsSCReactor and SC.Reactors.GetReactorTypes()[this:GetReactorType()] then
		return SC.Reactors.GetReactionInfo(SC.Reactors.GetReactorTypes()[this:GetReactorType()], this:GetReactorReaction()).DisplayName
    end

    return ""
end

e2function string entity:sc2ReactorCoolantName()
	if IsValid(this) and this.IsSCReactor then
		return SC.Reactors.GetCoolantInfo(this:GetReactorCoolant()).CoolantName
    end

    return ""
end

e2function string entity:sc2ReactorType()
	if IsValid(this) and this.IsSCReactor then
		return SC.Reactors.GetReactorTypes()[this:GetReactorType()] or ""
    end

    return ""
end