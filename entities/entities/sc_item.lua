AddCSLuaFile()
DEFINE_BASECLASS("base_scentity")
ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName = "Item"
ENT.Author = "Lt.Brandon"
ENT.Purpose = "Physics go brrr."
ENT.Instructions = "...hit me?"
ENT.Spawnable = false
ENT.AdminOnly = false
local base = scripted_ents.Get("base_scentity")

hook.Add("InitPostEntity", "sc_item_post_entity_init", function()
    base = scripted_ents.Get("base_scentity")
end)

function ENT:SharedInit()
    base.SharedInit(self)
end

if CLIENT then return end

function ENT:Initialize()
    self:PhysicsInit(SOLID_VPHYSICS)
    self:SetMoveType(MOVETYPE_VPHYSICS)
    self:SetSolid(SOLID_VPHYSICS)
    self:SetUseType(SIMPLE_USE)
    self.Unconstrainable = true
    self.CanTool = function() return false end

    self:SharedInit()
end

function ENT:Use(Activator)
    -- HACK: I don't want to add logic for the burger elsewhere yet, remove this later
    self:Remove()

    if Activator:IsPlayer() then
        Activator:SetHealth(100)
        Activator:SetArmor(100)
        Activator:EmitSound("vo/npc/male01/question06.wav")
    end
end

function ENT:ApplyDamage(Damage, Attacker, Inflictor, HitData)
    -- TODO: Implement dropping resource items on damage
    return false
end