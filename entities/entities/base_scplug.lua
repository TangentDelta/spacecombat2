--[[
    SC Plug

    Author: Hobnob
--]]

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName = "SC Plug"
ENT.Author = "Hobnob"

local base = scripted_ents.Get("base_scentity")
hook.Add( "InitPostEntity", "base_scplug_post_entity_init", function()
	base = scripted_ents.Get("base_scentity")
end)

if SERVER then
    -- Gets The Base Entity, normally the one that spawned the plug
    function ENT:GetBase()
        return self.BaseEnt
    end

    -- Sets the Base Entity, normally the one that spawned the plug
    function ENT:SetBase(BaseEnt)
        self.BaseEnt = BaseEnt
    end

    -- Checks if entity is a valid socket
    function ENT:IsValidSocket(Ent)
        return false
    end

    -- Connects two sockets together
    function ENT:ConnectToSocket(Ent, Force)
        if not self:IsValidSocket(Ent) or not self:CanConnect() then return false end
        self:SetConnected(true)
        self.ConnectedSocket = Ent

        return true
    end

    -- Disconnects from a socket
    function ENT:DisconnectFromSocket()
        self:SetConnected(false)
        self:AddConnectionDelay(5)
        self.ConnectedSocket = nil
    end

    -- Is Plug connected to a socket
    function ENT:IsConnected()
        return self.Connected
    end

    -- Checks if the plug can currently connect to something
    function ENT:CanConnect()
        return not self:IsConnected() and (CurTime() > (self.LastConnectionDelay or 0))
    end

    -- Internal Function: Decide if we're connected or not
    function ENT:SetConnected(Connected)
        self.Connected = Connected
    end

    -- Adds a delay to reconnecting, prevents plugs from being stuck in their socket
    function ENT:AddConnectionDelay(Delay)
        self.LastConnectionDelay = CurTime() + (Delay or 0)
    end

    -- Gets the trace direction used in FindSocket
    function ENT:GetTraceDirection()
        return self:GetForward()
    end

    -- Gets the plug socket position offset
    function ENT:GetOffset()
        return Vector()
    end

    -- Gets the connected pump
    function ENT:GetConnectedSocket()
        if self:IsConnected() then
            return self.ConnectedSocket
        end
    end

    -- Sets the connected pump
    function ENT:SetConnectedSocket(Ent)
        self.ConnectedSocket = Ent
    end

    -- Fires a Trace to look for a socket to connect to
    function ENT:FindSocket()
        local TraceResult = util.TraceLine({
                                            start = self:GetPos(),
                                            endpos = self:GetPos() + self:GetTraceDirection()*50,
                                            filter = {self} })
        local HitEnt = TraceResult.Entity

        -- Checks that the base entity and hit entity are both linked pumps that are not on the same grid
        self:ConnectToSocket(HitEnt)
    end

    -- Think Function, run every second
    function ENT:Think()
        -- makes the tick function run every second
        self:NextThink( CurTime() + 1 )
        -- must return true
        return true
    end

    -- Called when plug is used, this disconnects from the socket if connected.
    function ENT:Use()
        if self:IsConnected() then
            self:DisconnectFromSocket()
        end
    end

    function ENT:OnRemove()
		base.OnRemove( self )

        -- Disconnect ourselves
        self:DisconnectFromSocket()
	end
end
