--[[
	  Minable Terrestrial Rock Entity

	  Author: Steeveeo
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_mineableentity"

ENT.PrintName = "Ore-Rich Rock"
ENT.Author = "Steeveeo"
ENT.Contact = "http://diaspora-community.com"
ENT.Purpose = "I got a rock..."
ENT.Instructions = ""

ENT.Spawnable = false
ENT.AdminSpawnable = true