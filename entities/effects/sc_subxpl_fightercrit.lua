--[[*********************************************
	Fighter Death Pre-Explosion and Fire Trail

	Author: Steeveeo
**********************************************]]--

util.PrecacheSound( "shipsplosion/sc_fighterdeath.mp3" )

local function particleThink(particle)
	local ent = particle.Ent
	if not IsValid(ent) then return end

	particle:SetPos(ent:LocalToWorld(particle.Pos))

	particle:SetNextThink(CurTime())
end

function EFFECT:Init( data )

	self.Sound = "shipsplosion/sc_fighterdeath.mp3"

	self.Normal = data:GetNormal()
	self.Parent = data:GetEntity()

    if not IsValid(self.Parent) then
        return
    end

	self.Pos = self.Parent:WorldToLocal(data:GetOrigin())
	self.LifeTime = 2
	self.Emitter = ParticleEmitter(self.Parent:LocalToWorld(self.Pos))

	--Debris
	for i=1, 25 do
		local velocity = VectorRand() * math.Rand(150, 700)
		local size = math.random(10, 30)

		local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(4, 6))
		p:SetVelocity(velocity)
		p:SetAirResistance(50)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(size)
		p:SetEndSize(size)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end

	--Sparks
	local velDir = self.Parent:GetVelocity():GetNormalized()
	for i=1, 35 do
		local velocity = (velDir + (VectorRand() * 0.45)):GetNormalized() * (math.Rand(500, 750))

		local p = self.Emitter:Add("effects/shipsplosion/sparks_002", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(1, 2))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartLength(math.Rand(300, 500))
		p:SetEndLength(p:GetStartLength() + math.Rand(700, 1000))
		p:SetStartSize(math.Rand(200, 250))
		p:SetEndSize(p:GetStartSize() + math.Rand(150, 175))
		p:SetRoll(math.Rand(0, 360))
		p:SetColor(math.random(200, 255), math.random(184, 220), math.random(64, 164))
	end

	--Fire Main
	for i=1, 50 do
		local velocity = VectorRand() * math.Rand(150, 1000)

		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(2, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(100, 225))
		p:SetEndSize(math.random(250, 400))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end

	--Fire Highlights
	for i=1, 25 do
		local velocity = VectorRand() * math.Rand(150, 1000)

		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(2, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(100, 225))
		p:SetEndSize(math.random(250, 400))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(96, 96, 255)
	end

	--Flare
	for i=1, 3 do
		local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.25, 0.45))
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(5000)
		p:SetEndSize(0)
		p:SetColor(220, 200, 255)
	end

	--Fire Trail
	timer.Create("sc_fightercrit" .. SysTime(), 0.05, self.LifeTime * 20, function()
		if !IsValid(self.Parent) then return end

		--Fire Main
		for i=1, 3 do
			local velocity = VectorRand() * math.Rand(250, 350)

			local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Parent:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(1, 2))
			p:SetVelocity(velocity)
			p:SetAirResistance(75)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(25, 50))
			p:SetEndSize(math.random(250, 400))
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
		end

		--Fire Highlights
		local velocity = VectorRand() * math.Rand(250, 350)

		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(1, 2))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(25, 50))
		p:SetEndSize(math.random(250, 400))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(96, 96, 255)

		--Flare
		local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.15, 0.25))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(1250)
		p:SetEndSize(1500)
		p:SetColor(255, 220, 200)
		p:SetThinkFunction(particleThink)
		p:SetNextThink(CurTime())
		p.Ent = self.Parent
		p.Pos = self.Pos
	end)

	--Play Sound
	math.randomseed(FrameTime())
	sound.Play(self.Sound, self.Parent:LocalToWorld(self.Pos), 511)
	sound.Play(self.Sound, self.Parent:LocalToWorld(self.Pos), 511)
end

function EFFECT:Think( )
    if not IsValid(self.Parent) then
        return false
    end

  	self.LifeTime = self.LifeTime - FrameTime()
	if self.LifeTime <= -3 then --Give the smoke a chance to spawn
		self.Emitter:Finish()
		return false
	end

	return true
end


function EFFECT:Render( )
	return false
end
