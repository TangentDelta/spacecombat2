--[[*********************************************
	Scalable Blaster Launch Effect
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Shocks
	for i=1, 30 do
		local velocity = (self.Normal + (VectorRand() * 0.15)):GetNormalized() * (math.Rand(0, 2500) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(0.1, 0.45))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(math.random(15, 45) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(220, 255, 255)
	end
	
	--Plasma Stream
	for i=1, 15 do
		local velocity = self.Normal * (math.Rand(0, 1250) * self.Scale)
		
		local p = self.Emitter:Add("sprites/physbeam", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.50))
		p:SetVelocity(velocity)
		p:SetAirResistance(500)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(150, 250) * self.Scale)
		p:SetEndSize(0)
		p:SetStartLength(math.random(0, 350) * self.Scale)
		p:SetEndLength(math.random(500, 750) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(144, 144, 255)
	end
	
	--Flare
	for i=1, 2 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001", self.Pos)
		p:SetDieTime(math.Rand(0.3, 0.45))
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(750 * self.Scale)
		p:SetEndSize(0)
		p:SetColor(200, 220, 255)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
  	return false
end

function EFFECT:Render( )
	return false
end