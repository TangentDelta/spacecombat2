--[[*********************************************
	Big Bertha Cannon Charge Effect
	
	Buzzy green ball of doom.
	
	Author: Steeveeo
**********************************************]]--

local function parentedParticle(p)
	if p.OffsetSpeed then
		local offsetSpeed = p.OffsetSpeed * FrameTime()
		p.Offset = p.Offset + offsetSpeed
	end
	
	p:SetPos(p.Parent:LocalToWorld(p.Offset))
	p:SetNextThink(CurTime())
end


local shockRingDist = 2500
local shaftSparkDist = 1500


shockRingDist = shockRingDist * shockRingDist
shaftSparkDist = shaftSparkDist * shaftSparkDist


function EFFECT:Init( data )
	self.Scale = data:GetScale()
	self.Parent = data:GetEntity()
	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	self.PlanarEmitter = ParticleEmitter(self.Pos, true)
	
	self.NextFlareEmit = CurTime()
	self.FlareEmitDelay = 0.1
	
	self.NextSparkEmit = CurTime()
	self.SparkEmitDelay = 0.05
	
	self.NextShockEmit = CurTime()
	self.ShockEmitDelay = 0.1
	
	self.NextSuckyLineEmit = CurTime()
	self.SuckyLineEmitDelay = 0.1
end


function EFFECT:Think( )
	//Cancelled/Failed?
	if not IsValid(self.Parent) or self.Parent:GetChargeLevel() <= 0 then
		//TODO: Blow up the bubble?
		
		return false
	end
	
	local dist = LocalPlayer():GetPos():DistToSqr(self.Parent:LocalToWorld(self.Pos))
	local chargeLevel = self.Parent:GetChargeLevel()
	
	--Shakey flare
	if CurTime() > self.NextFlareEmit then
		local size = 400 * chargeLevel
		
		for i=1, 3 do
			local p = self.Emitter:Add("sprites/light_ignorez", self.Parent:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(0.1, 0.15))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(size + math.Rand(-size * 0.5, 100) * self.Scale)
			p:SetColor(255, 255, 255)
			p.Offset = self.Pos
			p.Parent = self.Parent
			p:SetThinkFunction(parentedParticle)
			p:SetNextThink(CurTime())
		end
		
		for i=1, 4 do
			local p = self.Emitter:Add("effects/flares/light-rays_001", self.Parent:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(0.1, 0.25))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(size * 2 + math.Rand(-size * 0.5, 100) * self.Scale)
			p:SetColor(100, 255, 100)
			p.Offset = self.Pos
			p.Parent = self.Parent
			p:SetThinkFunction(parentedParticle)
			p:SetNextThink(CurTime())
		end
	
		self.NextFlareEmit = CurTime() + self.FlareEmitDelay
	end
	
	--Sparky shaft stuff
	if CurTime() > self.NextSparkEmit and self.Parent:GetChargeLevel() > 0.5 and dist < shaftSparkDist then
		for i=1, 25 * self.Parent:GetChargeLevel() do
			local mins = self.Parent:OBBMins()
			local maxs = self.Parent:OBBMaxs()
			local pos = Vector(math.Rand(mins.x, maxs.x), math.Rand(mins.y, maxs.y), math.Rand(mins.z, maxs.z))
			local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Parent:LocalToWorld(pos))
			p:SetDieTime(math.Rand(0.2, 0.5))
			p:SetStartAlpha(0)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(15, 20) * self.Scale * self.Parent:GetChargeLevel())
			p:SetEndSize(0)
			p:SetRoll(math.random(0, 360))
			p:SetRollDelta(math.Rand(-1, 1))
			p:SetColor(200, 255, 200)
			p.Offset = pos
			p.Parent = self.Parent
			p.OffsetSpeed = self.Pos - pos
			p:SetThinkFunction(parentedParticle)
			p:SetNextThink(CurTime())
		end
		
		self.NextSparkEmit = CurTime() + self.SparkEmitDelay
	end
	
	--Sucky line things of doom
	if CurTime() > self.NextSuckyLineEmit and self.Parent:GetChargeLevel() > 0.35 then
		for i=1, 30 * self.Parent:GetChargeLevel() do
			local p = self.Emitter:Add("effects/shipsplosion/sparks_00" .. math.random(1,4), self.Parent:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(0.3, 0.65))
			p:SetVelocity(VectorRand())
			p:SetStartAlpha(0)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(50, 75) * self.Parent:GetChargeLevel() * self.Scale)
			p:SetEndSize(0)
			p:SetStartLength(math.random(125, 500) * self.Parent:GetChargeLevel() * self.Scale)
			p:SetEndLength(0)
			p:SetColor(200, 255, 200)
			p.Offset = self.Pos
			p.Parent = self.Parent
			p:SetThinkFunction(parentedParticle)
			p:SetNextThink(CurTime())
		end
		
		self.NextSuckyLineEmit = CurTime() + self.SuckyLineEmitDelay
	end
	
	--Shaft Shock
	if CurTime() > self.NextShockEmit and dist < shockRingDist then
		local ang = self.Normal:Angle()
		local revAng = (-self.Normal):Angle()
		for i=1, 4 do
			local mins = self.Parent:OBBMins()
			local maxs = self.Parent:OBBMaxs()
			local pos = maxs * -self.Normal
			local p = self.PlanarEmitter:Add("effects/shipsplosion/shockwave_001", self.Parent:LocalToWorld(pos))
			p:SetDieTime(1)
			p:SetStartAlpha(0)
			p:SetEndAlpha(255)
			p:SetStartSize((math.random(20, 75) * self.Scale) * self.Parent:GetChargeLevel())
			p:SetEndSize(math.random(5, 10) * self.Scale)
			p:SetColor(200, 255, 200)
			p.Offset = pos
			p.Parent = self.Parent
			p.OffsetSpeed = self.Pos - pos
			p:SetThinkFunction(parentedParticle)
			p:SetNextThink(CurTime())
			
			if i % 2 == 0 then
				p:SetAngles(self.Parent:LocalToWorldAngles(ang))
			else
				p:SetAngles(self.Parent:LocalToWorldAngles(revAng))
			end
		end
		
		self.NextShockEmit = CurTime() + self.ShockEmitDelay
	end

  	return true
end

function EFFECT:Render( )
	return false
end