local matRefraction	= Material( "refract_ring" )

local tMats = {}

tMats.Glow1 = Material("sprites/light_glow02")
--tMats.Glow1 = Material("models/roller/rollermine_glow")
tMats.Glow2 = Material("sprites/yellowflare")
tMats.Glow3 = Material("sprites/redglow2")

for _,mat in pairs(tMats) do

	mat:SetMaterialInt("$spriterendermode",9)
	mat:SetMaterialInt("$ignorez",1)
	mat:SetMaterialInt("$illumfactor",8)
	
end
--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )

	self.Position = data:GetOrigin()
	self.Position.z = self.Position.z + 4
	self.TimeLeft = CurTime() + 6
	self.GAlpha = 254
	self.GSize = 25
	self.CloudHeight = data:GetScale()
	
	self.Refract = 0
	self.Size = 12.5
	if render.GetDXLevel() <= 81 then
		matRefraction = Material( "effects/strider_pinch_dudv" )
	end
	self.rad = 90
	self.SplodeDist = 350
	self.BlastSpeed = 1500
	self.lastThink = 0
	self.MinSplodeTime = CurTime() + self.CloudHeight/self.BlastSpeed
	self.MaxSplodeTime = CurTime() + 3
	self.GroundPos = self.Position - Vector(0,0,self.CloudHeight)
	
	local Pos = self.Position
	
	
	self.smokeparticles = {}
	self.Emitter = ParticleEmitter( Pos )
	self.vecang = Vector(0,0,0)


	
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )

	
	

	local Pos = self.Position
	local timeleft = self.TimeLeft - CurTime()
	if timeleft > 0 then 
		local ftime = FrameTime()
		
		self.GAlpha = self.GAlpha - 10.5*ftime
		self.GSize = self.GSize - 0.12*timeleft*ftime
		
		self.Size = self.Size + 1200*ftime
		self.Refract = self.Refract + 1.3*ftime
		
		--shock ring
		if (self.Size < 8000) then
			local spawndist = self.Size
			local NumPuffs = spawndist / 20
			
				
			local ang = self.vecang:Angle()
			for i=1,50 do
					
		
					local particle = self.Emitter:Add( "particles/flamelet"..math.random(1,5), self.Position )
					particle:SetVelocity(Vector(math.random(-1800,1800),math.random(-1800,1800),math.random(-1800,1800)))
					particle:SetDieTime( math.Rand(1,5) )
					particle:SetStartLength(2000)
					particle:SetEndLength(1)
					particle:SetStartAlpha( math.Rand(230, 250) )
					particle:SetStartSize( math.Rand(100,200) )
					particle:SetEndSize( 1 )
					particle:SetRoll( 90 )
					particle:SetColor(255,255,255)
					particle:SetRollDelta( math.random( -1, 1 ) )
			
			end
			--for i=1,40 do
			--		ang:RotateAroundAxis(ang:Forward(), (360/40))
			--		local newang = ang:Up()
			--		local spawnpos = (Pos + (newang * spawndist))
			--		local particle = self.Emitter:Add( "particles/flamelet"..math.random(1,5), spawnpos)
			--		particle:SetVelocity(vecang*math.Rand(2,3))
			--		particle:SetVelocity(Vector(0, 0, 0))
			--		particle:SetDieTime( 6 )
			--		particle:SetStartAlpha( 255 )
			--		particle:SetStartSize( 2000 )
			--		particle:SetEndSize( 1000 )
			--		particle:SetEndAlpha(0)
			--		particle:SetRoll( 90 )
			--		particle:SetColor(255,255,255)
			--		particle:SetRollDelta( math.random( -1, 1 ) )
			--
			--end	
		end
		
		return true
	else
		self.Emitter:Finish()
		return false	
	end

end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )

local startpos = self.Position

	--Base glow
	render.SetMaterial(tMats.Glow1)
	render.DrawSprite(startpos, 250*self.GSize,250*self.GSize,Color(255, math.random(200,255), 255,self.GAlpha))


	end



