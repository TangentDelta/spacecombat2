/**********************************************
	Scalable Artillery Hit Effect
	
	Author: Steeveeo
***********************************************/

util.PrecacheSound( "ambient/explosions/explode_1.wav" )
util.PrecacheSound( "ambient/explosions/explode_3.wav" )
util.PrecacheSound( "ambient/explosions/explode_4.wav" )

function EFFECT:Init( data )

	self.Sounds = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_3.wav",
		"ambient/explosions/explode_4.wav"
	}
	
	//Particle Settings
	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Emitter = ParticleEmitter(self.Pos)
	
	//Smoke Plume
	for i=1, 25 do
		local velocity = (self.Normal + (VectorRand() * 0.65)):GetNormalized() * (math.Rand(125, 1500) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(1, 2) * self.Scale, 0.5, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(200)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(125, 250) * self.Scale)
		p:SetEndSize(math.random(125, 250) * self.Scale)
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(0,128)
		p:SetColor(grey, grey, grey)
		p:SetGravity(self.Normal * math.Rand(100, 500) * self.Scale)
	end
	
	//Fire Plume
	for i=1, 25 do
		local velocity = (self.Normal + (VectorRand() * math.Rand(0.1,1.0))):GetNormalized() * (math.Rand(300, 2000) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.2, 0.3) * self.Scale, 0.1, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(100, 150) * self.Scale)
		p:SetEndSize(math.random(250, 300) * self.Scale)
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(255, 225, 196)
	end
	
	//Flare
	for i=1, 5 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.2, 0.4) * self.Scale, 0.05, 0.35))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(1250, 1500) * self.Scale)
		p:SetEndSize(math.random(500, 750) * self.Scale)
		p:SetColor(255, 225, 196)
	end
	
	//Play Sound
	math.randomseed(SysTime())
	sound.Play( self.Sounds[math.random(1,#self.Sounds)], self.Pos)
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
  	return false
end


function EFFECT:Render( )
	return false
end
