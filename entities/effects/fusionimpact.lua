local matRefraction	= Material( "refract_ring" )

local tMats = {}

tMats.Glow1 = Material("sprites/light_glow02")
tMats.Glow2 = Material("sprites/yellowflare")
tMats.Glow3 = Material("sprites/redglow2")

for _,mat in pairs(tMats) do

	mat:SetMaterialInt("$spriterendermode",9)
	mat:SetMaterialInt("$ignorez",1)
	mat:SetMaterialInt("$illumfactor",8)
	
end

--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )

	self.Position = data:GetOrigin()
	self.Scale = data:GetScale()
	self.Size = 12.5
	
	local Pos = self.Position
	local Scale = self.Scale
	self.Emitter = ParticleEmitter( Pos )
	for i=1,5 do
		
		local vecang = VectorRand()*8
		local spawnpos = Pos + 256*vecang
			for k=1,5 do
			local particle = self.Emitter:Add( "particles/flamelet"..math.random(1,4), Pos)
			particle:SetVelocity(Vector(0,0,0))
			particle:SetDieTime( math.Rand( 1, 2 ) )
			particle:SetStartAlpha( math.Rand(230, 250) )
			particle:SetEndAlpha(0)
			particle:SetStartSize(k * Scale )
			particle:SetEndSize(k * Scale/math.Rand(1,2))
			end
			for k=1,10 do
			local particle = self.Emitter:Add( "effects/energysplash", Pos)
			particle:SetVelocity(Vector(math.Rand(-250,250),math.Rand(-250,250),math.Rand(-250,250)))
			particle:SetDieTime( math.Rand( 1, 2 ) )
			particle:SetStartLength(200)
			particle:SetEndLength(1000)
			particle:SetStartAlpha( 255 )
			particle:SetEndAlpha(255)
			particle:SetStartSize(100 )
			particle:SetColor(200,200,255)
			particle:SetEndSize(1)
			end
			
	end
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
self.Emitter:Finish()
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )

	end



