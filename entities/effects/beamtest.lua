local bMats = {}

bMats.Glow23 = GAMEMODE.MaterialFromVMT(
"bluelaser",
[["UnlitTwoTexture"

{
	"$baseTexture"  "effects/bluelaser1"
	"$Texture2" "effects/bluelaser1_smoke"
	"$nocull" "1"	
	"$nodecal" "1"
	"$additive" "1"
	"$no_fullbright" 1
	"$model" "1"
	"$nofog" "1"


	"Proxies"
	{
		"TextureScroll"
		{
			"texturescrollvar" "$texture2transform"
			"texturescrollrate" 1
			"texturescrollangle" 0
		}

		"Sine"
		{
			"sineperiod"	.06
			"sinemin"	".3"
			"sinemax"	".5"
			"resultVar"	"$alpha"
		}

	}



}]]
)
bMats.Glow24 = Material("effects/blueflare1")
bMats.Glow24:SetMaterialInt("$selfillum", 10)
bMats.Glow24:SetMaterialInt("$illumfactor",8)
local lMats = {}
lMats.Glow13 = Material("effects/blueblacklargebeam")
lMats.Glow13:SetMaterialInt("$vertexalpha", 1)
lMats.Glow13:SetMaterialInt("$vertexcolor", 1)
lMats.Glow13:SetMaterialInt("$additive", 1)
lMats.Glow13:SetMaterialInt("$nocull", 1)
lMats.Glow13:SetMaterialInt("$selfillum", 10)
lMats.Glow13:SetMaterialInt("$illumfactor",8)
lMats.Glow23 = GAMEMODE.MaterialFromVMT(
	"sc_blue_beam02",
	[["UnLitGeneric"
	{
		"$basetexture"		"effects/blueblacklargebeam"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
		"$illumfactor" 8
	}]]
)
--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )

	self.StartOffset= data:GetStart():Length()
	self.rad 		= 16
	self.ColLerp	= data:GetScale()
	self.ent		= data:GetEntity()
	self.AimDir		= data:GetNormal()
	self.EndOffset 	= self.ent:GetRange()
	self.col 		= Color(Lerp(self.ColLerp, 0, 255), Lerp(self.ColLerp, 255, 0), Lerp(self.ColLerp, 0, 255), 255)

	self.StartPos = self.ent:GetPos() + (self.AimDir * self.StartOffset)
	self.MaxRange = self.EndOffset
	self.LastHitRange = self.MaxRange
	self.EndOffset = Lerp(0.01, 0, self.MaxRange)
	
	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.ent:GetPos() + (self.AimDir * self.EndOffset)
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit then
		self.EndPos = tr.HitPos
	else
		self.EndPos = self.ent:GetPos() + (self.AimDir * self.EndOffset)
	end
	
	self:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	if not IsValid(self.ent) then return false end
	
	if self.Dying then
		self.StartOffset = Lerp(0.02, self.StartOffset, self.EndOffset)
		self.StartPos = self.ent:GetPos() + (self.AimDir * self.StartOffset)
		self.EndOffset = math.Min(Lerp(0.02, self.EndOffset, self.MaxRange), self.LastHitRange)
		self.EndPos = self.ent:GetPos() + (self.AimDir * self.EndOffset)
	else
		self.StartPos = self.ent:GetPos() + (self.AimDir * self.StartOffset)
		self.EndOffset = math.Min(Lerp(0.02, self.EndOffset, self.MaxRange), self.LastHitRange)
		
		local tracedata = {}
		tracedata.start = self.StartPos
		tracedata.endpos = self.ent:GetPos() + (self.AimDir * self.EndOffset)
		local tr = util.TraceLine(tracedata)
		
		if tr.Hit then
			self.EndPos = tr.HitPos
			self.LastHitRange = tr.HitPos:Distance(self.StartPos)
		else
			self.EndPos = self.ent:GetPos() + (self.AimDir * self.EndOffset)
			self.LastHitRange = self.MaxRange
		end
	end
	
	self:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
	if not self.ent:GetShooting() and not self.Dying then
		timer.Simple(1, function() 
			self.Dead = true
		end)
		
		self.Dying = true
	end
	
	if self.Dead then
		return false
	end
	
	return true
end


--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	local size = math.random(200,400)
   	local EndPoz = (self.StartPos - self.EndPos):Angle():Forward() * 50
   	
	render.SetMaterial( bMats.Glow24 )
	
	if not self.Dying then
	   	render.DrawSprite(self.StartPos, size , size , self.col) 
	   	render.DrawSprite(self.StartPos, size/2 , size/2 , Color(255, 255, 255, 255))
	   	render.DrawSprite(self.StartPos, size , size , self.col) 
	   	render.DrawSprite(self.StartPos, size/2 , size/2 , Color(255, 255, 255, 255))
	   	render.DrawSprite(self.StartPos, size , size , self.col) 
	   	render.DrawSprite(self.StartPos, size/2 , size/2 , Color(255, 255, 255, 255))
	end
   	
   	render.DrawSprite(self.EndPos + EndPoz, size*2 , size*2, self.col) 
   	render.DrawSprite(self.EndPos + EndPoz, size, size , Color(255, 255, 255, 255))
   	render.DrawSprite(self.EndPos + EndPoz, size*2, size*2, self.col) 
   	render.DrawSprite(self.EndPos + EndPoz, size, size , Color(255, 255, 255, 255)) 
   	render.DrawSprite(self.EndPos + EndPoz, size*2 , size*2, self.col) 
   	render.DrawSprite(self.EndPos + EndPoz, size, size , Color(255, 255, 255, 255)) 	
	
	render.SetMaterial( bMats.Glow23 )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100, 110, 0, Color( 0, 255, 0, 200 ) )
  
	render.SetMaterial( bMats.Glow24 )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100, 0.5, 0.5, self.col )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100, 0.5, 0.5, self.col )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100, 0.5, 0.5, self.col )
   	render.DrawBeam( self.EndPos, self.StartPos, 50, 0.5, 0.5, Color( 255, 255, 255, 255 ) )	
   	render.DrawBeam( self.EndPos, self.StartPos, 50, 0.5, 0.5, Color( 255, 255, 255, 255 ) )	
   	render.DrawBeam( self.EndPos, self.StartPos, 50, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
   	render.DrawBeam( self.EndPos, self.StartPos, 50, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
				 
end
