/**********************************************
	Capital Class Kill Confirm and Sound Cues
	
	Author: Steeveeo
***********************************************/

local sound_CritExplode = Sound("shipsplosion/sc_explode_04_near.mp3")

local sound_Slowdown_Near = Sound("shipsplosion/sc_slowdown_loud.mp3")
local sound_Slowdown_Far = Sound("shipsplosion/sc_slowdown_normalized.mp3")

local sound_MetalGroan = Sound("shipsplosion/sc_preexplode_groan.mp3")
local sound_Thunder = Sound("shipsplosion/sc_preexplode_rumble.mp3")

function EFFECT:Init( data )
	
	//Particle Settings
	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Core = data:GetEntity()
	self.Scale = data:GetScale() or 1
	self.DieTime = CurTime() + 21
	self.Emitter = ParticleEmitter(LocalPlayer():GetPos())
	self.Trails = {}
	
	if not IsValid(self.Core) then return end
	
	--self.Dist = LocalPlayer():GetPos():Distance(self.Pos) --Note: I know it seems inefficient to do this more than once, but this is a long sequence, and players tend to move when big things go boom
	self.FarDist = GetConVar("sc_shipDeathSoundRadius_Max"):GetFloat()
	self.MidDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_MidFactor"):GetFloat()
	self.NearDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_NearFactor"):GetFloat()
	
	--Queue Big Boom
	timer.Simple(15, function()
		--Debris
		for i=1, 45 do
			local velocity = VectorRand() * math.Rand(1000, 5500)
			local size = math.random(75, 150)
			
			local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Core:LocalToWorld(self.Pos))
			p:SetDieTime(10)
			p:SetVelocity(velocity)
			p:SetAirResistance(75)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(size)
			p:SetEndSize(size)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
			local grey = math.random(64,196)
			p:SetColor(grey, grey, grey)
		end
			
		--Sparks, Sparks Everywhere!
		for i=1, 45 do
			local velocity = VectorRand() * math.Rand(0, 1500)
			
			local p = self.Emitter:Add("effects/shipsplosion/sparks_001", self.Core:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(3, 4))
			p:SetVelocity(velocity)
			p:SetAirResistance(75)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartLength(math.Rand(750, 1200))
			p:SetEndLength(p:GetStartLength() + math.Rand(1000, 1250))
			p:SetStartSize(math.Rand(1000, 1500))
			p:SetEndSize(p:GetStartSize() + math.Rand(750, 1250))
			p:SetRoll(math.Rand(0, 360))
			p:SetColor(math.random(200, 255), math.random(184, 220), math.random(64, 164))
		end
		
		--Flare Core
		for i=1, 2 do
			local p = self.Emitter:Add("sprites/light_ignorez", self.Core:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(2, 4))
			p:SetStartAlpha(128)
			p:SetEndAlpha(0)
			p:SetStartSize(10000)
			p:SetEndSize(3500)
			p:SetColor(255, 225, 196)
		end
		
		--Flare Expanding
		for i=1, 2 do
			local p = self.Emitter:Add("effects/flares/halo-flare_001_ignorez", self.Core:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(1, 2))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(math.random(22500, 30000))
			p:SetColor(255, 225, 196)
		end
			
		--Shake Screen
		if GetConVar("sc_doScreenShakeFX"):GetBool() then
			util.ScreenShake(LocalPlayer():GetPos(), 10, 255, 1, 4096)
		end
		
		--Play Sound
		local dist = LocalPlayer():GetPos():Distance(self.Core:LocalToWorld(self.Pos))
		if dist < self.FarDist then
			LocalPlayer():EmitSound(sound_CritExplode, 511, 125, 1)
			LocalPlayer():EmitSound(sound_CritExplode, 511, 125, 1)
		end
	end)
	
	--Queue Slowdown
	timer.Simple(2.5, function()
		local dist = LocalPlayer():GetPos():Distance(self.Core:LocalToWorld(self.Pos))
		if dist < self.NearDist then
			LocalPlayer():EmitSound(sound_Slowdown_Near, 265, 100, 1)
		elseif dist < self.FarDist then
			LocalPlayer():EmitSound(sound_Slowdown_Far, 265, 100, 1)
		end
	end)
	
	--Queue Metallic Groans
	timer.Simple(1.5, function()
		local dist = LocalPlayer():GetPos():Distance(self.Core:LocalToWorld(self.Pos))
		if dist < self.MidDist then
			LocalPlayer():EmitSound(sound_MetalGroan, 511, 25, 1)
		end
	end)
	
	--Queue Thunder
	timer.Simple(3, function()
		local dist = LocalPlayer():GetPos():Distance(self.Core:LocalToWorld(self.Pos))
		if dist < self.FarDist then
			LocalPlayer():EmitSound(sound_Thunder, 511, 50, 1)
		end
	end)
end

function EFFECT:Think( )
  	if CurTime() > self.DieTime then
		self.Emitter:Finish()
		return false
	else
		return true
	end
end


function EFFECT:Render( )
	return false
end
