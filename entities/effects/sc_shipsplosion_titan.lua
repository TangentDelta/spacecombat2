--[[*********************************************
	Titan Main Death Explosion
	
	Author: Steeveeo
**********************************************]]--

local sound_MainBoom = Sound("shipsplosion/sc_titandeath.mp3")

function EFFECT:Init( data )
	
	--Set up Effect Settings
	self.Position = data:GetOrigin()
	self.Direction = VectorRand() --data:GetNormal()
	self.Lifetime = 14
	self.Duration = 0
	self.Dead = false

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Shockwave Settings
	self.Shock = {}
	self.Shock.Active = false
	self.Shock.Mat = Material("effects/shipsplosion/shockwave_001")
	self.Shock.Mat:SetInt("$overbrightfactor",1)
	self.Shock.Color = Color(128, 128, 144)
	self.Shock.CurSize = 0
	self.Shock.MaxSize = 35000
	self.Shock.StartAlpha = 255
	self.Shock.Alpha = self.Shock.StartAlpha
	self.Shock.FadeStart = 6
	self.Shock.FadeTime = 6
	self.Shock.Rotation = math.Rand(0, 360)
	self.Shock.RotationSpeed = 10
	self.Shock.RotationDrag = 2
	self.Shock.RotationMinSpeed = 0.75
	self.Shock.GrowExp = 0.65
	self.Shock.Passes = 1
		
	--Play Sound
	local dist = LocalPlayer():GetPos():Distance(self.Pos)
	if dist < GetConVar("sc_shipDeathSoundRadius_Max"):GetFloat() then
		LocalPlayer():EmitSound(sound_MainBoom, 511, 100, 1)
		LocalPlayer():EmitSound(sound_MainBoom, 511, 100, 1)
		LocalPlayer():EmitSound(sound_MainBoom, 511, 100, 1)
	end
	
	--Delay explosion to match main sound
	timer.Simple(2.75, function()
		self.Shock.Active = true
	
		--Fire
		for i=1, 175 do
			math.randomseed(i * FrameTime())
			local velocity = VectorRand() * math.Rand(1200, 15000)
			
			local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
			p:SetDieTime(math.Rand(10, 15))
			p:SetVelocity(velocity)
			p:SetAirResistance(150)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(1000, 1500))
			p:SetEndSize(math.random(1500, 2000))
			p:SetRoll(math.random(-360, 360))
			p:SetRollDelta(math.Rand(-0.01, 0.01))
		end
		
		--Fire Highlights
		for i=1, 100 do
			math.randomseed(i * FrameTime())
			local velocity = VectorRand() * math.Rand(1200, 15000)
			
			local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Pos)
			p:SetDieTime(math.Rand(10, 15))
			p:SetVelocity(velocity)
			p:SetAirResistance(150)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(1000, 1500))
			p:SetEndSize(math.random(1500, 2000))
			p:SetRoll(math.random(-360, 360))
			p:SetRollDelta(math.Rand(-0.01, 0.01))
			p:SetColor(96, 96, 255)
		end
		
		--Fire Ring
		local ang = self.Direction:Angle()
		local spinAng = ang:Up()
		local ringParticles = 75
		for i=1, ringParticles do
			ang:RotateAroundAxis(spinAng, 360 / ringParticles)
			local velocity = ang:Forward() * math.Rand(7500, 10000) + (VectorRand() * math.Rand(500, 1250))
			
			local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
			p:SetDieTime(math.Rand(10, 15))
			p:SetVelocity(velocity)
			p:SetAirResistance(60)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(1000, 1500))
			p:SetEndSize(math.random(1500, 2000))
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.01, 0.01))
		end
		
		--Fire Ring Highlights
		local ang = self.Direction:Angle()
		local spinAng = ang:Up()
		local ringParticles = 75
		for i=1, ringParticles do
			ang:RotateAroundAxis(spinAng, 360 / ringParticles)
			local velocity = ang:Forward() * math.Rand(7500, 10000) + (VectorRand() * math.Rand(500, 1250))
			
			local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Pos)
			p:SetDieTime(math.Rand(10, 15))
			p:SetVelocity(velocity)
			p:SetAirResistance(60)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(1000, 1500))
			p:SetEndSize(math.random(1500, 2000))
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.01, 0.01))
			p:SetColor(96, 96, 255)
		end
		
		--Debris
		for i=1, 250 do
			local velocity = VectorRand() * math.Rand(1500, 7500)
			local size = math.random(75, 150)
			
			local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Pos)
			p:SetDieTime(math.Rand(20, 40))
			p:SetVelocity(velocity)
			p:SetAirResistance(25)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(size)
			p:SetEndSize(size)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
			local grey = math.random(64,196)
			p:SetColor(grey, grey, grey)
		end
		
		--Flare Small
		for i=1, 25 do
			local velocity = VectorRand() * math.Rand(1500, 5000)
			
			local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
			p:SetDieTime(math.Rand(5, 10))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetVelocity(velocity)
			p:SetAirResistance(50)
			p:SetStartSize(8000)
			p:SetEndSize(15000)
			p:SetColor(220, 220, 255)
		end
		
		--Flare Rays Small Horiz
		for i=1, 3 do
			local velocity = VectorRand() * math.Rand(1000, 1500)
			
			local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Pos)
			p:SetDieTime(math.Rand(3, 6))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetVelocity(velocity)
			p:SetAirResistance(50)
			p:SetStartSize(20000)
			p:SetEndSize(25000)
			p:SetColor(255, 220, 220)
		end
		
		--Flare Rays Small Vert
		for i=1, 3 do
			local velocity = VectorRand() * math.Rand(1000, 1500)
			
			local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Pos)
			p:SetDieTime(math.Rand(3, 6))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetVelocity(velocity)
			p:SetAirResistance(50)
			p:SetStartSize(20000)
			p:SetEndSize(25000)
			p:SetColor(255, 220, 220)
			p:SetRoll(-90)
		end
		
		--Flare Big
		for i=1, 5 do
			local p = self.Emitter:Add("effects/flares/halo-flare_001_ignorez", self.Pos)
			p:SetDieTime(math.Rand(2, 3))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(100000)
			p:SetEndSize(100000)
			p:SetColor(255, 220, 200)
		end
		
		--Flash Flicker
		for i=1, 15 do
			local velocity = VectorRand() * math.Rand(5000, 7500)
			
			local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
			p:SetDieTime(math.Rand(0.1, 0.35))
			p:SetVelocity(velocity)
			p:SetStartAlpha(255)
			p:SetEndAlpha(255)
			p:SetStartSize(5000)
			p:SetEndSize(25000)
			p:SetColor(220, 220, 255)
			p:SetRoll(math.Rand(0, 360))
		end
		
		--Shake Screen
		if GetConVar("sc_doScreenShakeFX"):GetBool() then
			util.ScreenShake(LocalPlayer():GetPos(), 20, 0.1, 0.25, 4096)
			util.ScreenShake(LocalPlayer():GetPos(), 8, 0.1, 8, 4096)
		end
		
		--Spawn shocky bits randomly until sound ends
		timer.Create("TitanBoom" .. CurTime(), 0.01, 1300, function()
			--Clean up Emitter
			if IsValid(self) and not self.Dead then
				if self.Duration > 5 then
					--Random Chance based on time after explosion
					math.randomseed(CurTime())
					local rand = math.random(1, 10)
					local range = 10 - (10 * (self.Duration / self.Lifetime))
					
					if rand < range then
						for i = 1, math.random(1, 2 * range) do
							timer.Simple(math.Rand(0, 1), function()
								local pos = self.Pos + (VectorRand() * 4500)
								local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), pos)
								p:SetDieTime(math.Rand(0.025, 0.1))
								p:SetStartAlpha(255)
								p:SetEndAlpha(255)
								p:SetStartSize(math.Rand(250, 1250))
								p:SetEndSize(p:GetStartSize() + math.Rand(150, 250))
								p:SetColor(220, 220, 255)
								p:SetRoll(math.Rand(0, 360))
								
								local size = p:GetStartSize()
								local p2 = self.Emitter:Add("sprites/light_ignorez", pos)
								p2:SetDieTime(math.Rand(0.025, 0.1))
								p2:SetStartAlpha(16)
								p2:SetEndAlpha(0)
								p2:SetStartSize((size * 15))
								p2:SetEndSize(p:GetStartSize() + math.random(75, 125))
								p2:SetColor(200, 220, 255)
							end)
						end
					end
				end
			end
		end) --End Shocky Bits
	end) --End Delay
end


--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	if self.Shock.Active then
		if self.Duration < self.Lifetime then 
			local frameTime = FrameTime()
			
			-------------
			-- Shockwave
			-------------
			
			--Grow
			local growRate = math.abs(self.Shock.MaxSize - self.Shock.CurSize) * self.Shock.GrowExp
			self.Shock.CurSize = self.Shock.CurSize + (growRate * frameTime)
			
			--Spin
			self.Shock.RotationSpeed = math.Max(self.Shock.RotationSpeed - (self.Shock.RotationDrag * frameTime), self.Shock.RotationMinSpeed)
			self.Shock.Rotation = self.Shock.Rotation + (self.Shock.RotationSpeed * frameTime)
			
			--Fade Out
			if self.Duration > self.Shock.FadeStart then
				local alphaPerSec = self.Shock.StartAlpha / self.Shock.FadeTime
				local alphaStep = alphaPerSec * frameTime
				
				self.Shock.Alpha = math.Clamp(self.Shock.Alpha - alphaStep, 0, 255)
			end
			
			--Increment Effect Timer
			self.Duration = self.Duration + frameTime
		else
			self.Dead = true
			self.Emitter:Finish()
			
			return false
		end
	end
	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	
	--Shockwave
	if self.Shock.Alpha > 0 then
		render.SetMaterial(self.Shock.Mat)
		self.Shock.Color.a = self.Shock.Alpha
		
		for i = 1, self.Shock.Passes do
			render.DrawQuadEasy( self.Position, self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, self.Shock.Rotation )
			render.DrawQuadEasy( self.Position, -self.Direction, self.Shock.CurSize, self.Shock.CurSize, self.Shock.Color, -self.Shock.Rotation )
		end
	end
end

