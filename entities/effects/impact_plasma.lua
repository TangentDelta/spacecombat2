-- Revamping this effect to be a bit more dramatic without being outright laggy. -Steeveeo (4/07/14)
function EFFECT:Init( data )
	local pos, dir, nrm = data:GetOrigin(), data:GetNormal() or Vector(0,0,0), data:GetStart()
	
	self.Pos = pos
	self.Color = Color(0, 255, 0, 255)
	self.Emitter = ParticleEmitter( pos )
	self.GroundEmitter = ParticleEmitter( pos, true )
	
	local n = 5
	for i=1,n do
		local vec = (dir + (VectorRand() * 0.5)):GetNormalized() --( VectorRand() * 0.30 + nrm * 0.50 + dir * 0.20 ):GetNormalized()
		local p = self.Emitter:Add( "effects/shipsplosion/fire_001", pos + dir * -35 )
		p:SetVelocity( vec * ( 50 + math.random(15) ) )
		p:SetAirResistance( 500 )
		p:SetDieTime( math.Rand(0.1,0.5) )
		p:SetStartLength( 40+math.random(30) )
		p:SetEndLength( 200+math.random(150) )
		p:SetStartAlpha( 255 )
		p:SetEndAlpha( 0 )
		p:SetStartSize( 20+math.random(5) )
		p:SetEndSize( 140+math.random(80) )
		p:SetColor(math.random(0,162), 255, math.random(0,162))
	end
	
	--Normal Splash
	for i=1,5 do
		local splash = self.GroundEmitter:Add("effects/shipsplosion/shockwave_001", pos + dir * 2)
		splash:SetDieTime( 0.25 )
		splash:SetStartSize( 0 )
		splash:SetEndSize( 75 )
		splash:SetStartAlpha( 255 )
		splash:SetEndAlpha( 0 )
		splash:SetColor(math.random(0,196), 255, math.random(0,196))
		splash:SetAngles(dir:Angle())
	end
	
	self.Emitter:Finish()
	self.GroundEmitter:Finish()
end

function EFFECT:Think()
	return false
end

function EFFECT:Render()
	return false
end
