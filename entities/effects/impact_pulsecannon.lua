--[[*********************************************
	Scalable Pulse Cannon Impact Effect
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	self.EmitterNorm = ParticleEmitter(self.Pos, true)
	self.Scale = data:GetScale()
	
	--Energy Ring
	local dir = self.Normal:Angle():Right()
	local ang = dir:Angle()
	for i=1, 10 do
		ang:RotateAroundAxis(self.Normal, 36)
		local velocity = ang:Forward() * (math.Rand(250, 275) * self.Scale) + (self.Normal * 10)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.Pos)
		p:SetDieTime(math.Rand(0.3, 0.65))
		p:SetVelocity(velocity)
		p:SetAirResistance(150 * (self.Scale))
		p:SetStartLength(math.random(50, 75) * self.Scale)
		p:SetEndLength(math.random(50, 75) * self.Scale)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(50, 75) * self.Scale)
		p:SetEndSize(math.random(50, 75) * self.Scale)
		p:SetColor(math.random(144,196), math.random(144,196), 255)
	end
	
	--Normal Splash
	for i=1, 5 do
		local splash = self.EmitterNorm:Add("effects/shipsplosion/smoke_003add", self.Pos + self.Normal * 2)
		splash:SetDieTime(math.Rand(0.3, 1))
		splash:SetStartSize(75 * self.Scale)
		splash:SetEndSize(0)
		splash:SetStartAlpha(255)
		splash:SetEndAlpha(0)
		splash:SetColor(math.random(144,255), math.random(144,255), 255)
		splash:SetRoll(math.random(0, 360))
		splash:SetAngles(self.Normal:Angle())
	end
	
	--Normal Shock
	for i=1, 2 do
		local splash = self.EmitterNorm:Add("effects/shipsplosion/shockwave_001", self.Pos + self.Normal * 2)
		splash:SetDieTime(0.35)
		splash:SetStartSize(0)
		splash:SetEndSize(150 * self.Scale)
		splash:SetStartAlpha(255)
		splash:SetEndAlpha(0)
		splash:SetColor(math.random(144,255), math.random(144,255), 255)
		splash:SetAngles(self.Normal:Angle())
	end
	
	--Flare
	for i=1, 2 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.2, 0.5))
		p:SetStartAlpha(255 * self.Scale)
		p:SetEndAlpha(0)
		p:SetStartSize(100 * self.Scale)
		p:SetEndSize(100 * self.Scale)
		p:SetColor(200, 225, 255)
	end
	
	--Play Sound
	math.randomseed(FrameTime())
	sound.Play("ship_weapons/wpn_plasma_blaster_hit.wav", self.Pos, 65, math.random(75, 255))
	
	self.Emitter:Finish()
	self.EmitterNorm:Finish()
end

function EFFECT:Think()
  	return false
end

function EFFECT:Render()
	return false
end
