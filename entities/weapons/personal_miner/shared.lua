SWEP.PrintName = "Personal Mining Device"
SWEP.Author = "Fuhrball"
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = "Left Click to mine rocks."

SWEP.Spawnable = true
SWEP.AdminSpawnable = false

SWEP.ViewModel = "models/weapons/v_IRifle.mdl"
SWEP.WorldModel = "models/weapons/w_IRifle.mdl"
SWEP.Slot = 3
SWEP.Base = "weapon_base"

SWEP.HoldType = "ar2"

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Category = "SB3 Mining"