SWEP.PrintName			= "Logistigoggles" -- This will be shown in the spawn menu, and in the weapon selection menu
SWEP.Author			= "TangentDelta" -- These two options will be shown when you have the weapon highlighted in the weapon selection menu
SWEP.Instructions		= "Place goggles on face and see the logistics!"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.Slot			    = 5
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= false

SWEP.ViewModel			= "models/weapons/c_arms_animations.mdl"
SWEP.WorldModel			= "models/props_junk/PopCan01a.mdl"
SWEP.UseHands           = true
SWEP.HoldType           = "normal"

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
    self:SetWeaponHoldType(self.HoldType)
end



function SWEP:PrimaryAttack()

end

function SWEP:SecondaryAttack()

end

if SERVER then return end

SWEP.WepSelectIcon = surface.GetTextureID( "vgui/gmod_camera" )

function SWEP:DrawHUD()
    --Insert logistics debugging data here
    --Need to network the nodes first
end