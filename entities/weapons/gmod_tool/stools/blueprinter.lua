TOOL.Category = "Construction"
TOOL.Name = "Blueprinter"

TOOL.Information = {
    {
        name = "left"
    },
    {
        name = "right"
    }
}

cleanup.Register("duplicates")
local LocalPos = vector_origin
local LocalAng = angle_zero

function Blueprinter_SpawnDupe(SpawnCenter, SpawnAngle, Owner, Dupe)
    --
    -- Spawn them all at our chosen positions
    --
    LocalPos = SpawnCenter
    LocalAng = SpawnAngle
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)
    DisablePropCreateEffect = true
    local Ents = duplicator.Paste(Owner or game.GetWorld(), Dupe.Entities, {})
    DisablePropCreateEffect = nil
    -- Find special entities
    local ShipCore
    local Gyropod

    for i, k in pairs(Ents) do
        -- Check for special entities
        if k.IsCoreEnt then
            ShipCore = k
        elseif k.IsSCGyropod then
            Gyropod = k
            Gyropod.ShouldAutoParent = false
        end

        -- Freeze any physics objects
        local PhysObj = k:GetPhysicsObject()

        if IsValid(PhysObj) then
            PhysObj:EnableMotion(false)
            PhysObj:Sleep()
        end

        -- Re-create physics object for prop dynamics, otherwise parenting breaks
        if k:GetClass() == "prop_dynamic" then
            k:PhysicsInitStatic(SOLID_VPHYSICS)
        end

        -- Ship parts should only transmit if the parent does
        if k ~= Gyropod then
            k:SetTransmitWithParent(true)
        end

        -- Prevent the entity from being interacted with
        k.SB_Ignore = true
        k.Untouchable = true
        k.Unconstrainable = true
        k.PhysgunDisabled = true
        k.CanTool = function()
            return false
        end

        -- Ships cannot be unfrozen
        k:SetUnFreezable(false)

        if Owner == nil then
            -- Owner is nil, set the owner to world
            if NADMOD then --If we're using NADMOD PP, then use its function
                NADMOD.SetOwnerWorld(k)
            elseif CPPI then --If we're using SPP, then use its function
                k:SetNWString("Owner", "World")
            else --Well fuck it, lets just use our own!
                k.Owner = game.GetWorld()
            end
        else
            -- Otherwise set the owner to the player, who isn't nil
            k.Owner = Owner

            if k.CPPISetOwner then
                k:CPPISetOwner(Owner)
            end
        end
    end

    -- Now that all the special entities are found, loop over everything again and apply constraints
    for i, k in pairs(Ents) do
        if k ~= Gyropod then
            --reweld everything
            if util.IsValidPhysicsObject(k, 0) then
                constraint.Weld(k, Gyropod, 0, 0, 0, false, true)
            end

            --parent that shit up
            if not k:IsVehicle() then
                k:SetParent(Gyropod)
            end
        end
    end

    LocalPos = vector_origin
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)

    hook.Call("AdvDupe_FinishPasting", nil, {
        {
            EntityList = Dupe.Entities,
            CreatedEntities = Ents,
            ConstraintList = {},
            CreatedConstraints = {},
            HitPos = SpawnCenter,
            Player = Owner
        }
    }, 1)

    return Ents, ShipCore, Gyropod
end

--
-- PASTE
--
function TOOL:LeftClick(trace)
    if CLIENT then return true end
    if not self:GetOwner():IsAdmin() then return true end

    --
    -- Get the copied dupe. We store it on the player so it will still exist if they die and respawn.
    --
    local AsJSON = file.Read("sc_ship_dupe.json")
    if not AsJSON then return false end

    local dupe = util.JSONToTable(AsJSON)
    if not dupe then return false end
    --
    -- We want to spawn it flush on thr ground. So get the point that we hit
    -- and take away the mins.z of the bounding box of the dupe.
    --
    local SpawnCenter = trace.HitPos
    SpawnCenter.z = SpawnCenter.z - dupe.Mins.z

    local Ents = Blueprinter_SpawnDupe(SpawnCenter, angle_zero, self:GetOwner(), dupe)

    --
    -- Create one undo for the whole creation
    --
    undo.Create("Duplicator")

    for k, ent in pairs(Ents) do
        undo.AddEntity(ent)
    end

    for k, ent in pairs(Ents) do
        self:GetOwner():AddCleanup("duplicates", ent)
    end

    undo.SetPlayer(self:GetOwner())
    undo.Finish()

    return true
end

local function FilterEntityTable(tab)
    local varType

    for k, v in pairs(tab) do
        varType = TypeID(v)

        if varType == 5 then
            tab[k] = FilterEntityTable(tab[k])
        elseif varType == 6 or varType == 9 then
            tab[k] = nil
        end
    end

    return tab
end

--
-- Copy
--
local function SaveEnt(data, ent)
    --
    -- Merge the entities actual table with the table we're saving
    -- this is terrible behaviour - but it's what we've always done.
    --
    if ent.PreEntityCopy then
        ent:PreEntityCopy()
    end

    local EntityClass = duplicator.FindEntityClass(ent:GetClass())
    local EntTable = ent:GetTable()

    -- Fix dumb entities that actually relied on the behavior of copying the entity table
    -- I'm looking at you, wiremod.
    if EntityClass then
        local VarType

        for iNumber, Key in pairs(EntityClass.Args) do
            -- Translate keys from old system
            if not (Key == "Pos" or Key == "Model" or Key == "Ang" or Key == "Angle" or Key == "ang" or Key == "angle" or Key == "pos" or Key == "position" or Key == "model") then
                VarType = TypeID(EntTable[Key])

                if VarType == 5 then
                    data[Key] = FilterEntityTable(table.Copy(EntTable[Key]))
                elseif not (VarType == 9 or VarType == 6) then
                    data[Key] = EntTable[Key]
                end
            end
        end
    end

    if ent.EntityMods then
        data.EntityMods = table.Copy(ent.EntityMods)
    end

    if ent.PostEntityCopy then
        ent:PostEntityCopy()
    end

    --
    -- Set so me generic variables that pretty much all entities
    -- would like to save.
    --
    data.Pos = ent:GetPos()
    data.Angle = ent:GetAngles()
    data.Class = ent:GetClass()
    data.Model = ent:GetModel()
    data.Skin = ent:GetSkin()
    data.Mins, data.Maxs = ent:GetCollisionBounds()
    --data.ColGroup = ent:GetCollisionGroup()
    data.Name = ent:GetName()
    data.Pos, data.Angle = WorldToLocal(data.Pos, data.Angle, LocalPos, LocalAng)
    data.ModelScale = ent:GetModelScale()

    if data.ModelScale == 1 then
        data.ModelScale = nil
    end

    if data.Name == "" then
        data.Name = nil
    end

    if data.Skin == 0 then
        data.Skin = nil
    end

    -- Force all physics props to be prop dynamic
    if data.Class == "prop_physics" then
        data.Class = "prop_dynamic_override"
    end

    -- Allow the entity to override the class
    -- (this is a hack for the jeep, since it's real class is different from the one it reports as)
    if ent.ClassOverride then
        data.Class = ent.ClassOverride
    end

    if data.EntityMods and data.EntityMods.colour then
        if data.EntityMods.colour.Color then
            local C = data.EntityMods.colour.Color
            if C.r == 255 and C.g == 255 and C.b == 255 and C.a == 255 then
                data.EntityMods.colour.Color = nil
            end
        end

        if data.EntityMods.colour.RenderFX == 0 then
            data.EntityMods.colour.RenderFX = nil
        end

        if data.EntityMods.colour.RenderMode == 0 then
            data.EntityMods.colour.RenderMode = nil
        end

        if table.Count(data.EntityMods.colour) == 0 then
            data.EntityMods.colour = nil
        end
    end

    --
    -- Store networks vars/DT vars (assigned using SetupDataTables)
    --
    if ent.GetNetworkVars then
        data.DT = ent:GetNetworkVars()
    end

    -- Make this function on your SENT if you want to modify the
    -- returned table specifically for your entity.
    if ent.OnEntityCopyTableFinish then
        ent:OnEntityCopyTableFinish(data)
    end

    --
    -- Exclude this crap
    --
    for k, v in pairs(data) do
        if isfunction(v) then
            data[k] = nil
        end
    end

    data.OnDieFunctions = nil
    data.AutomaticFrameAdvance = nil
    data.BaseClass = nil
end

local function CopyEntTable(Ent)
    local output = {}
    SaveEnt(output, Ent)

    return output
end

local function Copy(Ent, AddToTable)
    local Ents = {}
    local Constraints = {}
    duplicator.GetAllConstrainedEntitiesAndConstraints(Ent, Ents, Constraints)
    local EntTables = {}
    local ShipInfo

    if AddToTable ~= nil then
        EntTables = AddToTable.Entities or {}
    end

    for k, v in pairs(Ents) do
        if v.IsCoreEnt then
            ShipInfo = {
                Class = v:GetShipClass(),
                SubClass = v:GetShipSubClass(),
                SigRad = v:GetSigRad(),
                MaxCapacitor = v:GetCapMax(),
                MaxShield = v:GetShieldMax(),
                MaxArmor = v:GetArmorMax(),
                NPCShipTags = {
                    ["Default"] = true
                },
                PowerLevel = 0
            }
        end
        EntTables[k] = CopyEntTable(v)
    end

    local mins, maxs = duplicator.WorkoutSize(EntTables)

    return {
        Entities = EntTables,
        Mins = mins,
        Maxs = maxs,
        ShipInfo = ShipInfo
    }
end

function TOOL:RightClick(trace)
    if not IsValid(trace.Entity) then return false end
    if CLIENT then return true end
    if not self:GetOwner():IsAdmin() then return true end

    --
    -- Set the position to our local position (so we can paste relative to our `hold`)
    --
    LocalPos = trace.HitPos
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)
    local Dupe = Copy(trace.Entity)
    local AsJSON = util.TableToJSON(Dupe)

    file.Write("sc_ship_dupe.json", AsJSON)

    LocalPos = vector_origin
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)

    if not Dupe then return false end
    return true
end