// This file defines a bunch of stand-in functions for working on the menus outside of gmod
// If it's included in gmod nothing in this file is used.

function GetCoreHealthData()
{
    return {
        Hull: {
            Amount: 50000,
            MaxAmount: 50000
        },

        Armor: {
            Amount: 50000,
            MaxAmount: 50000
        },

        Shield: {
            Amount: 35000,
            MaxAmount: 50000
        }
    };
}

function niceNumber(uglyNumber) {
    var niceNumber = parseFloat((uglyNumber).toPrecision(2));
    var order = Math.floor(Math.log10(niceNumber));

    var suffix = '';
    if (order >= 12) {
        niceNumber = niceNumber / Math.pow(10, 12);
        suffix = 'T';
    } else if (order >= 9) {
        niceNumber = niceNumber / Math.pow(10, 9);
        suffix = 'B';
    } else if (order >= 6) {
        niceNumber = niceNumber / Math.pow(10, 6);
        suffix = 'M';
    } else if (order >= 3) {
        niceNumber = niceNumber / Math.pow(10, 3);
        suffix = 'K';
    } else if (order <= -3) {
        niceNumber = niceNumber / Math.pow(10, order);
        suffix = ' × 10<sup>' + order + '<\sup>';
    }

    return niceNumber + suffix;
}

const spacecombat = {
    UpdateInventoryData: function(Callback) {
        Callback({
            ["global-storage"]: {
                Panel: {
                    ID: "global-storage",
                    ShowNumberedList: true,
                },
                Resources: [
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    },
                    {
                        Name: "Hydrogen",
                        Amount: Math.random() * 10000,
                        NiceAmount: niceNumber(Math.random() * 10000),
                        TotalVolume: 100,
                        TotalMass: 100
                    }
                ]
            }
        });
    }
};