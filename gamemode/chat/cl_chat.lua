local ChosenChatLevel = "All"
local NumberOfBodies = 0
local HtmlTemplate = [[
<html>
    <head>
        <style type="text/css">
            #messages {
                padding: 5px;
                width: 95%;
                padding-bottom: 0px;
                display:break-word;
            }
            .message {
                padding: 1px;
            }
            .username
            {
                text-decoration: none;
            }
            html
            {
                color: white;
                margin-top: 0px;
                font-family:'Arial';
                font-weight:bold;
                text-shadow: 0em 0em 0.3125em rgba(0,0,0,1),
                0.3125em 0.1875em 0.4375em rgba(15,0,0,0.7),
                -0.3125em -0.1875em 0.4375em rgba(0,15,15,0.7);
                font-size:1em;
            }
            body
            {
                overflow: hidden;
            }
            div
            {
                display:break-word;
            }
            superbold {
                font-family:'Impact';
                font-weight:normal;
            }
            codeinline {
                font-family:'Consolas';
                font-weight:normal;
                text-shadow: 0;
                background-color: rgba(46,49,54,0.6);
            }
            codeblock {
                font-family:'Consolas';
                font-weight:normal;
                font-size: 0.75em;
                line-height: 0.875em;
                text-shadow: 0;
                display: inline-block;
                padding: 0.1875em 0.3125em 0.1875em 0.3125em;
                overflow-x:auto;
                box-sizing: border-box;
                margin-left: 60px;
                background-color: rgba(46,49,54,0.5);
                border: 2px;
                border-style: solid;
                border-radius: 5px;
                border-color: rgba(20,21,25,0.8);
                border-spacing: 10px;
                white-space: pre-wrap;
                word-wrap: break-word;
            }
            a:link    {color:orange; background-color:transparent; text-decoration:none}
            a:visited {color:purple; background-color:transparent; text-decoration:none}
            a:hover   {color:red; background-color:transparent; text-decoration:underline}
            a:active  {color:yellow; background-color:transparent; text-decoration:underline}
        </style>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    </head>
    <body>
        <div id="messages"></div>

        <script type="text/javascript">
            var isOpen = false;
            var timeout = null;
            var timeoutWillClear = [];
            var maxMessages = 2500;

            function fadeMessageTimeout(messageElem, millis)
            {
                if (millis === 0) { // We are scheduled to fade out this message now
                    if (isOpen === false) { // Chat is closed, but messages may not have faded away
                        if (timeout === null) { // Previous messages have already faded away, we can too.
                            $(messageElem).animate({ opacity: 0 }, 1000);
                        }
                        else { // The rest of the chat is going to fade out soon after us, add this message to that timeout
                            timeoutWillClear.push(messageElem);
                        }
                    }
                    // else { Chat is open, so this message will be cleared by it's timeout, do not fade. }
                }
                else { // Schedule the fade in 'millis' milliseconds
                    setTimeout( function() { fadeMessageTimeout(messageElem,0) },millis);
                }
            }

            function handleMessage(message)
            {
                var isAtBottom=((window.innerHeight+window.scrollY)>=document.body.offsetHeight);
                var messParts=message.split("</span>");
                var add=messParts[0]+"</span>";
                messParts.splice(0,1);
                add+=messParts.join("");


                add = add.replace(/http[s]?:\/\/[\w\.-]+(:[0-9]+)?(\/\s|\/[^\r\n\t\f \"\<\>]+)?/gi, function replaceUrls(link)
                {
                    var escapedLink = encodeURI(link);
                    escapedLink = escapedLink.replace(/[\.]$/, "");
                    escapedLink = escapedLink.replace(/(http|https):\/\//gi, function screwGarry(link)
                    {
                        return link.toLowerCase();
                    });

                    var ret = "<a href=\"javascript:game.openURL('"+escapedLink+"')\">"+escapedLink+"</a>";
                    return ret;
                });
                add = add.replace(/fttp/gi,"http")

                var messageElem=document.createElement('div');
                messageElem.className="message";
                messageElem.innerHTML=add;
                var messagesContainer=document.getElementById("messages");

                // Check if we've reached the message limit.
                var overflow = Math.max(0,(messagesContainer.childNodes.length + 1)-maxMessages)
                if(overflow > 0) { // Remove the oldest message
                    messagesContainer.removeChild(messagesContainer.firstChild)
                }

                messagesContainer.appendChild(messageElem);
                if (isOpen === true) { // Chat is open, fadeChatOut() will handle hiding this message
                    $(messageElem).stop(true,true);
                    $(messageElem).css('opacity', 1);
                }
                else{
                    fadeMessageTimeout(messageElem, 10000);
                }
                if(isAtBottom){document.body.scrollTop=document.body.scrollHeight;}
            }

            function fadeChatOut()
            {
                document.body.style.overflow = 'hidden';
                if (timeout !== null) { clearTimeout(timeout); }
                timeoutWillClear = Array.prototype.slice.call( document.getElementById("messages").childNodes, 0 ); // Store all the currently visible messages in an Array,
                // so we don't accidentally hide newly arrived messages when the timeout triggers in 10 seconds.
                // I don't know what the fuck this function actually does, only that it works better than iterating did.
                isOpen = false;
                timeout = setTimeout(function() {
                    for (var i = 0; i < timeoutWillClear.length; i++) {
                        var child = timeoutWillClear[i];
                        $(child).stop(true, true);
                        $(child).animate({ opacity: 0 }, 1000);
                    }
                    timeout = null;
                    timeoutWillClear = [];
                }, 10000);
            }

            function fadeChatIn()
            {
                document.body.style.overflow = 'auto';
                isOpen = true;
                if (timeout !== null) {
                    clearTimeout(timeout);
                    timeout = null;
                    timeoutWillClear = [];
                }
                $(".message").stop(true, true);
                $(".message").animate({ opacity: 1 }, 200)
                document.body.scrollTop=document.body.scrollHeight;
            }
        </script>
    </body>
</html>
]]

local patternchars = { "`","*","_","~",":" }
local function escapeFurther(s) for _,c in ipairs({ "*","_","~",":" }) do s = string.Replace(s,c,"\\"..c) end return s end
-- Pretty Cool Markdown
local patterns = {
    { "```([^`]+)```", [[<br><codeblock>%1</codeblock>]], "```", "\\`\\`\\`", function(s) return string.gsub(escapeFurther(s),"^<br><codeblock><br />","<br><codeblock>",1) end}, -- Code Block
    { "`([^`]+)`", [[<codeinline>%1</codeinline>]], "`", "\\`", function(s) return string.gsub(escapeFurther(s),"[\\]+\\`","\\`") end}, -- Inline Code
    { "%*%*%*([^%*\\][^%*\\]-)%*%*%*", [[<em><superbold>%1</superbold></em>]], "***", "\\*\\*\\*" }, -- Bold Italics
    { "%*%*([^%*\\][^%*\\]-)%*%*", [[<superbold>%1</superbold>]], "**", "\\*\\*" }, -- Bold
    { "%*([^%*\\][^%*\\]-)%*", [[<em>%1</em>]], "*", "\\*" }, -- Italics
    { "~~([^~\\][^~\\]-)~~", [[<strike>%1</strike>]], "~~", "\\~\\~" }, -- Strikethrough
    { "__([^_\\][^_\\]-)__", [[<u>%1</u>]], "__", "\\_\\_" }, -- Underline
    { "^ _([^_\\][^_\\]-)_ $", [[ <em>%1</em> ]], "_", "\\_" }, -- Italics Again, but only for /me. You have two other ways to do inline italics, and neither of them are snakecase.
    { "([%s%c])/([^_\\][^_\\]-)/([%s%c])", [[%1<em>%2</em>%3]], "_", "\\_" }, -- Fine Brandon, you get your /thing/
    { ":([%w_]+):", [[<img src="fttps://steamcommunity-a.akamaihd.net/economy/emoticon/%1" title="%1" height="16" width="16">]], ":", "\\:", nil, function(s) return not string.match(s,"[0-9]",1) end} -- Steam Emotes
}

local function markDown(text)
	text = " "..text.." "
	for i = 1, #patterns do
		local p = patterns[i]
		local search = text
		local result = "" -- Rebuild the string by splitting on tokens that match this markdown tag
		while true do -- Could be any number of patterns in one message, just hopefully not infinity otherwise rip.
			local j,k,g1,g2,g3 = string.find(search,p[1])
			if not j then break end
			if g2 then -- So we can still do /italics/ for Brandon
				if search[j] == "\\" then
					local replace = string.Replace(string.Replace(string.Replace(string.sub(search,j+1,k-1),p[3],p[4]),"%1",""),"%3",g3)
					result = result..string.sub(search,1,j-1)..replace
					search = string.sub(search,k-1,#search)
				else
					local replace = string.Replace(string.Replace(string.Replace(p[2],"%2",g2),"%1",g1),"%3",g3)
					result = result..string.sub(search,1,j-1)..replace
					search = string.sub(search,k-1,#search)
				end
			elseif search[j-1] == "\\" then -- This markdown tag was escaped, escape all it's constituent characters so they don't get markdown'd
				local replace = string.Replace(string.sub(search,j,k),p[3],p[4])
				if p[5] then replace = p[5](replace) end
				result = result..string.sub(search,1,j-2)..replace
				search = string.sub(search,k+1,#search)
			else -- 99% of cases go here
				local bounds = j > 1 and k < #search - 1
				if p[6] == nil or
					(p[6] and not bounds or
					(bounds and p[6](search[j-1]) and p[6](search[k+1])) )
				then
					local replace = string.Replace(p[2],"%1",g1)
					if p[5] then replace = p[5](replace) end
					result = result..string.sub(search,1,j-1)..replace
					search = string.sub(search,k+1,#search)
				else
					result = result..string.sub(search,1,k)
					search = string.sub(search,k+1,#search)
				end
			end
		end
		text = result..search
	end
	for _,c in ipairs(patternchars) do text = string.Replace(text,"\\"..c,c) end -- De-escape any markdown chars
	return string.gsub(text,"^ (.*) $", "%1")
end

local function AddChatBody(name)
    local cx, cy = ChatVGUI:GetPos()
    local body = vgui.Create("DHTML")
    body:SetHTML(HtmlTemplate)
    body:SetAllowLua(true)
    body:AddFunction("game", "openURL", function(str)
        gui.OpenURL(str)
    end)

    body.html = HtmlTemplate

    body:SetPos(cx + 5, cy + 18)
    body:SetSize(ScrW() * .5 - 5, 182 +(ScrH() * 0.05))
    body:SetVisible(false)

    ChatVGUI.Bodies[name] = body

    local OptBut = vgui.Create("DColorButton", ChatVGUI)
    OptBut:SetPos(8 + (62 * NumberOfBodies), 2)
    OptBut:SetSize(60, 20)
    OptBut:SetColor(Color(60, 60, 60, 255))
    OptBut:SetText(name)
    OptBut:SetContentAlignment(5)
    OptBut:SetFont("DefaultBold")
    OptBut.LastFlash = 0
    OptBut.IsFlashed = false
    OptBut.ShouldFlash = false
    OptBut.Nam = name
    OptBut.DoClick = function(pnl)
        for i, k in pairs(ChatVGUI.Bodies) do
            k:SetVisible(false)

            if not k.OptBut.ShouldFlash then
                k.OptBut:SetColor(Color(60, 60, 60, 255))
            end
        end

        pnl:SetColor(Color(90, 90, 90, 255))
        pnl.ShouldFlash = false
        ChosenChatLevel = OptBut.Nam
        ChatVGUI.Bodies[ChosenChatLevel]:QueueJavascript([[fadeChatIn()]])
        ChatVGUI.Bodies[pnl.Nam]:SetVisible(true)
        ChatVGUI.Bodies[pnl.Nam]:SetZPos(1000)
    end

    ChatVGUI.Bodies[name].OptBut = OptBut
    NumberOfBodies = NumberOfBodies + 1

    return ChatVGUI.Bodies[name]
end

-- Panel based blur function by Chessnut from NutScript
local blurmat = Material("pp/blurscreen")
local function blur(panel, layers, density, alpha)
    -- Its a scientifically proven fact that blur improves a script
    local x, y = panel:LocalToScreen(0, 0)
    surface.SetDrawColor(255, 255, 255, alpha)
    surface.SetMaterial(blurmat)

    for i = 1, 3 do
        blurmat:SetFloat("$blur", (i / layers) * density)
        blurmat:Recompute()

        render.UpdateScreenEffectTexture()
        surface.DrawTexturedRect(-x, -y, ScrW(), ScrH())
    end
end

local function CreateChatGUI()
    ChatVGUI = vgui.Create("DPanel")
    ChatVGUI:SetPos(40, ScrH() - 365 - (ScrH() * 0.05))
    ChatVGUI:SetSize(ScrW() * .5, 200 + (ScrH() * 0.05))
    ChatVGUI:SetBackgroundColor(Color(70, 70, 70, 110))
    ChatVGUI:SetVisible(false)
    ChatVGUI.Bodies = { }

    local OldPaint = ChatVGUI.Paint
    ChatVGUI.Paint = function(self, w, h)
        blur(self, 7, 20, 150)
        OldPaint(self, w, h)
    end

    AddChatBody("All")
    AddChatBody("Faction")
    AddChatBody("Private")
    AddChatBody("Notices")
    if LocalPlayer():IsAdmin() then
        AddChatBody("Admin")
        ChatVGUI.Bodies["Admin"]:SetVisible(false)
    end

    ChatVGUI.Bodies["All"]:SetVisible(true)
    ChatVGUI.Bodies["All"].OptBut:SetColor(Color(90,90,90))

    ChatVGUI.CloseLink = vgui.Create("DButton", ChatVGUI)
    ChatVGUI.CloseLink:SetPos(ScrW() * .5 - 15, 2)
    ChatVGUI.CloseLink:SetText("X")
    ChatVGUI.CloseLink:SetSize(15, 15)
    ChatVGUI.CloseLink:SetVisible(true)
    ChatVGUI.CloseLink.DoClick = function()
        chat.Close()
    end

    local tx, ty = ChatVGUI:GetPos()
    ChatVGUI.ChatTextEntry = vgui.Create("DTextEntry", ChatVGUI)
    ChatVGUI.ChatTextEntry:SetPos(tx, ty + 200 + (ScrH() * 0.05))
    ChatVGUI.ChatTextEntry:SetSize(ScrW() * .5, 15)
    ChatVGUI.ChatTextEntry:SetVisible(true)
    ChatVGUI.ChatTextEntry:SetEnabled(true)
    ChatVGUI.ChatTextEntry:SetHistoryEnabled(true)
    ChatVGUI.ChatTextEntry:SetKeyboardInputEnabled(true)
    ChatVGUI.ChatTextEntry:SetMouseInputEnabled(true)
    ChatVGUI.ChatTextEntry:SetAllowNonAsciiCharacters(true)
    ChatVGUI.ChatTextEntry:SetUpdateOnType(true)
    ChatVGUI.ChatTextEntry:SetMultiline(true)

    function ChatVGUI.ChatTextEntry:OnKeyCodeTyped(code)
        if code == KEY_ESCAPE then
            self:SetHeight(15)
            self:SetVerticalScrollbarEnabled(false)

            chat.Close()
            gui.HideGameUI()
            return true
        end

        if code == input.GetKeyCode(input.LookupBinding("toggleconsole")) then
            gui.HideGameUI()
            return false
        end

        if code == KEY_ENTER then
            if self.IsAutocompleting then
                local function CloseMenu()
                    local tab = self:GetAutoComplete(self:GetText())
                    if not tab or #tab <= 1 then
                        self.HistoryPos = 1
                        self.IsAutocompleting = false

                        if IsValid(self.Menu) then
                            self.Menu:Remove()
                        end
                    end
                end

                if IsValid(self.SelectedItem) then
                    local txt = self.SelectedItem:GetText()
                    timer.Simple(0, function()
                        if not IsValid(self) then return end
                        self:SetText(txt)
                        self:SetCaretPos(txt:len())
                        self:OnTextChanged()
                        CloseMenu()
                    end)
                    return true
                else
                    CloseMenu()
                    return true
                end
            elseif input.IsKeyDown(KEY_LSHIFT) or input.IsKeyDown(KEY_RSHIFT) then
                self:SetValue(self:GetValue().."\n")
                return true
            else
                if IsValid(self.Menu) then
                    self.Menu:Remove()
                end

                self:FocusNext()
                self:OnEnter()
                self.HistoryPos = 1
                self.IsAutocompleting = false
                return true
            end
        end

        if self.m_bHistory then
            if code == KEY_UP then
                self.HistoryPos = self.HistoryPos - 1
                self:UpdateFromHistory()
                return true
            elseif code == KEY_DOWN then
                self.HistoryPos = self.HistoryPos + 1
                self:UpdateFromHistory()
                return true
            end
        end

        if code == KEY_TAB then
            if not self.IsAutocompleting then
                self.HistoryPos = 1
                self.IsAutocompleting = true

                local tab = self:GetAutoComplete(self:GetText())
                if tab and #tab > 0 then
                    self:OpenAutoComplete(tab)

                    if IsValid(self.Menu) then
                        self:UpdateFromMenu()
                    end
                else
                    self.IsAutocompleting = false
                end
            else
                self.HistoryPos = 1
                self.IsAutocompleting = false

                if IsValid(self.Menu) then
                    self.Menu:Remove()
                end
            end

            return true
        end
    end

    function ChatVGUI.ChatTextEntry:OnTextChanged(noMenuRemoval)
        self.HistoryPos = 0
        self:UpdateConvarValue()

        if IsValid(self.Menu) and not noMenuRemoval then
            self.Menu:Remove()
        end

        if self.IsAutocompleting then
            local tab = self:GetAutoComplete(self:GetValue())
            if tab then
                self:OpenAutoComplete(tab)
            end
        end

        local Value = self:GetValue()
        hook.Run("ChatTextChanged", Value)

        local Lines = string.Explode("\n", Value)
        self:SetHeight(15 * math.min(#Lines, 5))

        if #Lines > 5 then
            self:SetVerticalScrollbarEnabled(true)
        else
            self:SetVerticalScrollbarEnabled(false)
        end

        self:OnChange()
    end

    function ChatVGUI.ChatTextEntry:UpdateFromMenu()
        local pos = self.HistoryPos
        local num = self.Menu:ChildCount()

        self.Menu:ClearHighlights()

        if pos < 1 then pos = num end
        if pos > num then pos = 1 end

        local item = self.Menu:GetChild(pos)
        if not item then return end

        self.Menu:HighlightItem(item)
        self.SelectedItem = item
        self.HistoryPos = pos
    end

    function ChatVGUI.ChatTextEntry:GetAutoComplete(Text)
        if self.IsAutocompleting then
            local Autocomplete = {}
            hook.Run("ChatAutoComplete", Autocomplete, Text)

            local Return = {}
            for i,k in pairs(Autocomplete) do
                if Text == "" or string.StartWith(string.lower(k), string.lower(Text)) then
                    table.insert(Return, k)
                end
            end

            return Return
        end
    end

    function ChatVGUI.ChatTextEntry:OnEnter()
        local Value = string.Trim(self:GetValue(), "\n")
        Value = string.Trim(Value, "\t")
        Value = string.Trim(Value, " ")

        if Value ~= "" then
            net.Start("PlayerSay")
            net.WriteString(Value)
            net.WriteString(ChosenChatLevel)
            net.WriteBit(self.IsTeam)
            net.SendToServer()

            self:AddHistory(Value)
        end

        self:SetHeight(15)
        self:SetVerticalScrollbarEnabled(false)

        chat.Close()
    end
end
hook.Remove("InitPostEntity", "LoadChat")
hook.Add("InitPostEntity", "LoadChat", CreateChatGUI)

local function msgCreateChatVGUI(isTeam)
    if hook.Run("StartChat", isTeam) then
        return
    end

    if not ChatVGUI then CreateChatGUI() end

    ChatVGUI:SetVisible(true)
    ChatVGUI.Bodies[ChosenChatLevel]:QueueJavascript([[fadeChatIn()]])
    ChatVGUI.ChatTextEntry.IsTeam = isTeam or false
    ChatVGUI.ChatTextEntry:MakePopup()
    ChatVGUI.ChatTextEntry:SetText("")
    ChatVGUI.ChatTextEntry:SetHeight(15)
    ChatVGUI.ChatTextEntry:SetVerticalScrollbarEnabled(false)
    gui.EnableScreenClicker(true)
end

local function OpenChat(ply, bind, pressed)
    if ply == LocalPlayer() then
        if (bind == "messagemode" or bind == "messagemode2") and pressed then
            msgCreateChatVGUI(bind == "messagemode2")
            return true
        end
    end
end
hook.Remove("PlayerBindPress", "ChatBind")
hook.Add("PlayerBindPress", "ChatBind", OpenChat) -- Check binds

function chat.Open(mode)
    msgCreateChatVGUI(mode ~= 1)
end

function chat.Close()
    ChatVGUI.Bodies[ChosenChatLevel]:QueueJavascript([[fadeChatOut()]])
    ChatVGUI.ChatTextEntry:SetText("")
    ChatVGUI:SetVisible(false)
    gui.EnableScreenClicker(false)
    hook.Run("FinishChat")
    hook.Run("ChatTextChanged", "")
end

function chat.GetChatBoxPos()
    if not IsValid(ChatVGUI) then return 0, 0 end
    return ChatVGUI:GetPos()
end

function chat.GetChatBoxSize()
    if not IsValid(ChatVGUI) then return 0, 0 end
    return ChatVGUI:GetSize()
end

if not oldaddtext then oldaddtext = chat.AddText end
function chat.AddText2(tab, ...)
    -- Sometimes this hasn't loaded yet when we want to put things into the chatbox, so lets give it a chance to load.
    if not IsValid(ChatVGUI) then
        local varargs = { ...}
        timer.Simple(1, function()
            -- Create it if it doesn't exist, with some checks to make sure this isn't done before the game is fully initialized.
            if IsValid(LocalPlayer()) and LocalPlayer().IsAdmin and not ChatVGUI then CreateChatGUI() end

            -- Bleh have to unpack this because it dislikes being used outside of the function its used in originally.
            chat.AddText2(tab, unpack(varargs))
        end)
        return
    end

    local body = ChatVGUI.Bodies[tab]
    local insert = ""

    local colorChangeCount = 0

    for i, k in pairs({...}) do
        local typ = string.lower(tostring(type(k)))
        if typ == "player" then
            local col = team.GetColor(k:Team())
            local rankColor = "rgba("..col.r..","..col.g..","..col.b..","..col.a..")"

            insert = insert..[[<span class="username" style="color:]]..rankColor..[[">]] ..(k:Nick())..[[</span>]]
        elseif typ == "table" then
            if k.r and k.g and k.b then
                local color = "rgba("..k.r..","..k.g..","..k.b..","..k.a..")"
                if colorChangeCount > 0 then insert = insert..[[</span>]] end
                insert = insert..[[<span class="username" style="color:]]..color..[[">]]

                colorChangeCount = colorChangeCount + 1
            end
        else
            local strToInsert = tostring(k)

            strToInsert = string.gsub(string.gsub(strToInsert, "<", "&lt"), ">", "&gt")
            strToInsert = string.gsub(strToInsert, "\n", "<br />")
            strToInsert = markDown(strToInsert)

            insert = insert..strToInsert
        end
    end

    if colorChangeCount > 0 then insert = insert..[[</span>]] end

    if insert ~= "" and body ~= nil then
        insert = string.gsub(insert, "\\", "\\\\")
        insert = string.gsub(insert, "\"", "\\\"")
        insert = string.gsub(insert, "\'", "\\'")
        insert = string.gsub(insert, ":", "\\:")
        insert = string.gsub(insert, "/", "\\/")

        body:QueueJavascript([[handleMessage("]]..insert..[[")]])
        chat.PlaySound()
    end

    if tab ~= ChosenChatLevel and ChosenChatLevel ~= "All" then
        ChatVGUI.Bodies[tab].OptBut.ShouldFlash = true
    end

    oldaddtext(...)
end

function chat.AddText(...)
    chat.AddText2("All", ...)
end

net.Receive("AddToChatBox", function()
    local tab = net.ReadString()
    local data = net.ReadTable()

    chat.AddText2(tab, unpack(data))

    if tab ~= "All" then chat.AddText2("All", unpack(data)) end
end)

local function ChatBoxThink()
    if ChatVGUI and ChatVGUI:IsVisible() then
        for k, n in pairs(ChatVGUI.Bodies) do
            if n.OptBut.ShouldFlash then
                if CurTime() - n.OptBut.LastFlash > .4 then
                    local color = Color(60, 60, 60, 255)

                    if not n.OptBut.IsFlashed then
                        color = Color(30, 30, 190, 255)
                        n.OptBut.IsFlashed = true
                    else
                        n.OptBut.IsFlashed = false
                    end

                    n.OptBut:SetColor(color)
                    n.OptBut.LastFlash = CurTime()
                end
            end
        end
    end
end
timer.Remove("ChatBoxThink")
timer.Create("ChatBoxThink", 0.4, 0, ChatBoxThink)

hook.Remove("StartChat", "start_chatting")
hook.Add("StartChat", "start_chatting", function()
    RunConsoleCommand("start_chatting")
end)

hook.Remove("FinishChat", "stop_chatting")
hook.Add("FinishChat",  "stop_chatting", function()
    RunConsoleCommand("stop_chatting")
end)