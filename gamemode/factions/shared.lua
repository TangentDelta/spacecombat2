AddCSLuaFile()

local GM = GM
local team = team

GM.Factions = {
    {
        Name = "Civilian",
        ShortName = "CIV",
        Description = "",
        Color = Color(100, 255, 100, 255),
        Image = "gui/factions/faction_civ_1.png"
    },
    {
        Name = "Bereavement Advocates",
        ShortName = "BA",
        Description = [[
    The Bereavement Advocates are not native to our universe - they arrived here by a grave misfortune, and now pose the greatest threat this galaxy has ever seen.

    The B.A are a motley alliance of various subfactions, each with their own methodlogy and technology and all united under the BA name. Catapulted to our universe via a mishap with an ADARI prisoner, they now seek to repair their grand flagship, the Eversor, with the resources and even harvested populations of our worlds in order to enable it to return them to their home galaxy.

    The Bereavement Advocates show no mercy to this galaxy's citizens; all factions are merely obstructions to their grand harvest. Swift and brutal in combat, utilizing whatever of their own alien technology they brought with them from their home, they are a force that only a galaxy united can hope to stand against.
        ]],
        Color = Color(255, 0, 0, 255),
        Image = "gui/factions/faction_ba_1.png"
    },
    {
        Name = "Spectral Directorate",
        ShortName = "SD",
        Description = [[
    The Spectral Directorate (Commonly referred to as "The Directorate", or "SD") is a secretive organization that prefers subterfuge, propaganda, and information control over direct conflict.  Its public directive is to cultivate lesser factions, and disrupt major factions through political, financial, and technological influence. A galaxy where one group holds all the power is useless to the Directorate.

    Tendrils of the Directorate extend throughout the galaxy, carefully nudging events to align with their underlying plans.  What those plans are, however, is an enigma to those outside its inner sanctum, and any unexplained SD activity is likely in pursuit of this mystery.  Benevolence one minute, malevolence the next, if there's Spectral activity in your sector, best to keep your head down.
        ]],
        Color = Color(186, 85, 211, 255),
        Image = "gui/factions/faction_sd_1.png",
        PlayerClass = "player_spectral"
    },
    {
        Name = "Advanced Development and Research Institutes",
        ShortName = "ADARI",
        Description = [[
    The decrepit remains of a once large and prosperous conglomerate of scientific groups, the Advanced Development and Research Institutes (More commonly known as the ADARI) seek to continue their legacy of (mostly) peaceful scientific advancement. While they are small and lacking resources they are easily the most technologically advanced group in the known universe barring perhaps the highly secretive Spectral Directorate. Their origins are largely unknown, however, they suddenly appeared approximately 25 years before the BA Incursion.

    Rumors in the area around the systems now inhabited by the ADARI say that they suddenly appeared without warning in a fashion similar to the BA, speaking a completely unknown language and wielding technologies that seemed like magic to the local inhabitants. However, unlike the havoc brought by the appearance of the Bereavement Advocates, the ADARI have peacefully maintained the scientific growth of themselves and their new neighbors.
    ]],
        Color = Color(0, 148, 255, 255),
        Image = "gui/factions/faction_adari_1.png"
    },
    {
        Name = "Transhumanist Collective",
        ShortName = "THC",
        Description = [[
    A relatively small group that has recently started gaining an abnormal amount of traction in a galaxy being watched by the Spectral Directorate, the Transhumanist Collective wishes for all races to become one with technology. Many of their members are peaceful members of society that only wish for the advancement of all races, while others are simply infatuated with cybernetics and mechanical advancement.

    However, not all within the Collective are of the same opinion, and some of the more extremist members have become twisted with a cultish idolization of the recently discovered PN crystals. These crystals are capable of self-reproduction by feeding upon other materials, however, they are also known to cause a strange madness within many lifeforms to further their expansion across continents, worlds, and solar systems. When used in a properly contained environment they can be used to provide massive amounts of energy relatively easily, however, they are not easily tamed, and hardly understood.
        ]],
        Color = Color(0, 127, 70, 255),
        Image = "gui/factions/faction_thc_1.png"
    },
}

GM.FactionIndicies = {}
for Index, Data in pairs(GM.Factions) do
    GM.FactionIndicies[Data.Name] = Index
end

function GM:CreateTeams()
    for Index, Data in pairs(GM.Factions) do
        team.SetUp(Index, Data.Name, Data.Color, false)
    end
end

if SERVER then
    include("init.lua")
end

include("cl_init.lua")
