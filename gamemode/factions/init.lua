local GM = GM
local team = team

util.AddNetworkString("SC.RequestPlayerCreation")
util.AddNetworkString("SC.PlayerCreationCompleted")

function GM.PlayerCreationCompleted(Player, Name, Faction, Info, Success)
    hook.Run("SC.OnPlayerCreationCompleted", Player, Name, Faction, Info, Success)

    net.Start("SC.PlayerCreationCompleted")
    net.WriteBool(Success or false)
    net.WriteString(Name or "New Character")
    net.WriteString(Faction or "Civilian")
    net.WriteTable(Info or {})
    net.Send(Player)
end

function GM.OnPlayerCreationRequest(Player, Name, Faction, Info)
    -- Name and Info are ignored for now
    -- TODO: Hook up Name and Info when careers are added

    local FactionIndex = GM.FactionIndicies[Faction]
    if FactionIndex == nil then
        GM.PlayerCreationCompleted(Player, Name, Faction, Info, false)
    end

    Player:SetTeam(FactionIndex)

    if GM.Factions[FactionIndex].PlayerClass then
        player_manager.SetPlayerClass(Player, GM.Factions[FactionIndex].PlayerClass)
    end

    Player:Spawn()

    GM.PlayerCreationCompleted(Player, Name, Faction, Info, true)
end

net.Receive("SC.RequestPlayerCreation", function(len, ply)
    local Name = net.ReadString()
    local Faction = net.ReadString()
    local Info = net.ReadTable()

    GM.OnPlayerCreationRequest(ply, Name, Faction, Info)
end)