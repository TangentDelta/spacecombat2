local GM = GM

hook.Remove("SC.Config.Register", "SC.LoadSC2ClientConfig")
hook.Add("SC.Config.Register", "SC.LoadSC2ClientConfig", function()
    local Config = GM.Config

    Config:Register({
        File = "spacecombat_cl",
        Section = "Debug",
        Key = "EnableDebugMode",
        Default = false,
        ConVar = "sc_debug_cl",
        OnChanged = function(File, Section, Key, NewValue)
            SC.Debug = NewValue
        end
    })

    Config:Register({
        File = "spacecombat_cl",
        Section = "Debug",
        Key = "DebugPrintLevel",
        Default = 3,
        ConVar = "sc_debuglevel_cl",
        OnChanged = function(File, Section, Key, NewValue)
            SC.DebugLevel = NewValue
        end
    })
end)