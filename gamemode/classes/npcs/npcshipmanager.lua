local GM = GM

--#region Global Ship Manager Stuff

--Reload the global NPC ship manager if it exists
hook.Remove("OnReloaded", "SC.NPCShipManager.OnReloaded")
hook.Add("OnReloaded", "SC.NPCShipManager.OnReloaded", function()
    local CurrentManager = GAMEMODE.CurrentNPCShipManager

    if IsValid(CurrentManager) then
        for _,Ship in pairs(CurrentManager:GetShips()) do
            Ship:Remove()
        end

        GAMEMODE.CurrentNPCShipManager = nil

        local NewManager = GM:GetNPCShipManager()
    end
end)

---@return NPCShipManager #The current NPC ship manager
function GM:GetNPCShipManager()
    if not IsValid(GAMEMODE.CurrentNPCShipManager) then
        GAMEMODE.CurrentNPCShipManager = GAMEMODE.class.getClass("NPCShipManager"):new()
    end

    return GAMEMODE.CurrentNPCShipManager
end

--#endregion
--#region NPC Ship Manager Class
--#region General Class Stuff

---Custom targeter filter table
local NPCShipTargeterFilters = {
    {
        DisplayName = "Mining",
        Classes = {
            "mining_asteroid"
        },
        CheckFaction = false,
        CheckEnvironment = true,    --Should we check to see if the target is in an environment
        InEnvironment = false       --If we're checking if a target is in an environment, what state should it be in?
    },
    {
        DisplayName = "Salvage",
        Classes = {
            "sc_wreck"
        },
        CheckFaction = false,
        CheckEnvironment = true,    --Should we check to see if the target is in an environment
        InEnvironment = false       --If we're checking if a target is in an environment, what state should it be in?
    },
    {
        DisplayName = "Combat",
        Classes = {
            "ship_core"
        },
        CheckFaction = true,
        IgnoreOwnedEntities = false,
        CheckEnvironment = true,    --Should we check to see if the target is in an environment
        InEnvironment = false       --If we're checking if a target is in an environment, what state should it be in?
    }
}

local ShipSizes = {
    Drone = 1,
    Fighter = 2,
    Frigate = 3,
    Cruiser = 4,
    Battlecruiser = 5,
    Battleship = 6,
    Dreadnaught = 7,
    Titan = 8
}

---@class NPCShipManager
---@field Ships table #NPC ships managed by this manager
---@field ShipTemplates table #Templates of ships that can be spawned as NPC ships
---@field NextShipID number #The next ID number to be assigned to a ship
---@field TotalShips number #How many NPC ships are currently alive and active
---@field MaxShips number #The maximum number of ships that can be spawned on the map
---@field NextThinkTime number #The next time, in seconds, that the manager should think
---@field DoSpawnShips boolean #A flag indicating that NPC ships should spawn
---@field DoProcessShips boolean #A flag indicating if NPC ships should think
---@field CoreLookup table #A table of NPC ships indexed by their ship core entity ID
---@field FactionRelations table #A table of factions and what their relations are with eachother
---@field FactionBehaviors table #A table of the likelyhood for a faction to perform a task
---@field WeightedFactionTable table #A table used to randomly pick a faction weighed by `Rarity`
---@field MapHasSpace boolean #A flag indicating that this map contains a "space" environment
---@field Planets table #A table of all planets on the map
---@field TransportPlanets table #A table of planets that the transport activity can pick
---@field HostilePlanets table #A table of planets that are hostile and we should avoid at all costs!
local C = GM.LCS.class({
    Ships = {},
    ShipTemplates = {},
    NextShipID = 1,
    TotalShips = 0,
    MaxShips = 5,
    NextThinkTime = 0,
    DoSpawnShips = true,
    DoProcessShips = true,

    CoreLookup = {},

    FactionRelations = {},
    FactionBehaviors = {},
    WeightedFactionTable = {},

    MapHasSpace = true,
    Planets = nil,
    TransportPlanets = nil,
    HostilePlanets = nil
})

function C:init()
    self.NextThinkTime = CurTime() + 10
    self:LoadShipTemplates()
    self:PopulateFactionRelations()
    self:PopulateFactionBehaviors()
end

function C:IsValid()
    return true
end
--#endregion
--#region NPC Ship Templates

---Load the NPC ship templates from files into the `ShipTemplates` table
function C:LoadShipTemplates()
    self.ShipTemplates = {}

    local Files = file.Find(SC.DataFolder .. "/npcships/*", "DATA")
    for _,File in pairs(Files) do
        local AsJSON = file.Read(SC.DataFolder .. "/npcships/" .. File)
        if AsJSON then
            local DupeData = util.JSONToTable(AsJSON)
            if DupeData then
                SC.Error(string.format("[NPC Ship Manager]Loaded NPC ship template: %s", File), 4)

                DupeData.FileName = string.match(File, "(.+)%..+$")

                local MaxVec = DupeData.Maxs
                local MinVec = DupeData.Mins
                DupeData.ShipInfo.SigRad = math.max(math.abs(MaxVec.X) + math.abs(MinVec.X),
                math.abs(MaxVec.Y) + math.abs(MinVec.Y),
                math.abs(MaxVec.Z) + math.abs(MinVec.Z))

                table.insert(self.ShipTemplates, DupeData)
            else
                SC.Error(string.format("[NPC Ship Manager]Failed to load NPC ship template data for file: %s", File), 5)
            end
        else
            SC.Error(string.format("[NPC Ship Manager]Failed to load NPC ship template file: %s", File), 5)
        end
    end
end

---Finds and returns a random template filtered by a filtering function
---@param Filter function #A function that is passed a template and returns true if the template matches
function C:GetFilteredTemplate(Filter)
    table.Shuffle(self.ShipTemplates)
    for _, Template in pairs(self.ShipTemplates) do
        if Filter(Template) then
            return Template
        end
    end
end

---Finds and returns a template for the specified activity for a faction
---@param Faction number #The team index of the faction to find a template for
---@param Activity string #The name of the activity to find a template for
function C:GetTemplateForFactionActivity(Faction, Activity)
    local FactionData = self.FactionBehaviors[Faction]
    local FactionName = team.GetName(Faction)
    local WeightedTemplates = {}

    for TemplateIndex, Template in pairs(self.ShipTemplates) do
        if Template.ShipInfo then
            local ShipInfo = Template.ShipInfo
            if (ShipInfo.PowerLevel or 0) <= FactionData.Power then
                local TagMatch = ShipInfo.NPCShipTags[Activity]
                local FactionMatch = true

                if ShipInfo.FactionWhitelist and (not ShipInfo.FactionWhitelist[FactionName]) then
                    FactionMatch = false
                end

                if TagMatch and FactionMatch then
                    for I = 1, (FactionData.Power - (ShipInfo.PowerLevel or 0)) + 1 do
                        table.insert(WeightedTemplates, TemplateIndex)
                    end
                end
            end
        end
    end

    if #WeightedTemplates > 0 then
        return self.ShipTemplates[table.Random(WeightedTemplates)]
    end
end

--#endregion
--#region NPC Ship Management

---@return number #The next unused ship ID
function C:GetNextAvailableShipID()
    local I = 1
    while self.Ships[I] do
        I = I + 1
    end

    return I
end

---Register a new NPC ship with this manager
---@param Ship NPCShip #The NPC ship object to register with this manager
function C:RegisterShip(Ship)
    self.Ships[self.NextShipID] = Ship
    Ship:SetID(self.NextShipID)

    self.NextShipID = self:GetNextAvailableShipID()
    self.TotalShips = self.TotalShips + 1
end

---Unregister and remove an NPC ship from this manager
---@param Ship NPCShip #The NPC ship object to unregister from this manager
function C:UnregisterShip(Ship)
    self.NextShipID = Ship:GetID()

    --Clear the reference to the ship in the CoreIndex->NPCShip table
    for CoreIndex, NPCShip in pairs(self.Ships) do
        if NPCShip:GetID() == self.NextShipID then
            self.CoreLookup[CoreIndex] = nil
        end
    end

    Ship:SetID(0)
    self.Ships[self.NextShipID] = nil
    self.TotalShips = self.TotalShips - 1
end

---Returns a table of all of the NPC ship objects
---@return table Ships #The table of ships
function C:GetShips()
    return self.Ships
end

---Removes all ships that this manager has created
function C:RemoveAllShips()
    for _, Ship in pairs(self:GetShips()) do
        Ship:Remove()
    end
end

---Sets all ships to be enabled or disabled
---@param Enabled boolean #The state to set the ships to
function C:SetShipsEnabled(Enabled)
    self.DoProcessShips = Enabled

    -- TODO: Turn on/off the firing groups of the targeters too
    for ShipIndex, Ship in ipairs(self.Ships) do
        if IsValid(Ship) then
            if Enabled then
                Ship:GetGyropod():TriggerInput("Active", 1)
            else
                Ship:GetGyropod():TriggerInput("Active", 0)
            end
        end
    end
end

---Associates a core's entity ID with an NPC ship
---@param NPCShipID number #The unique ID of the NPC ship to associate the core to
---@param Core entity #The core to associate to the NPC ship ID
function C:AddShipCoreAssociation(NPCShipID, Core)
    if IsValid(Core) then
        self.CoreLookup[Core:EntIndex()] = self.Ships[NPCShipID]
    end
end

---Returns the NPC ship object associated with a core entity
---@param Core entity
---@return NPCShip
function C:GetShipByCore(Core)
    return self.CoreLookup[Core:EntIndex()]
end

--#endregion
--#region NPC Ship Creation

---Spawn a test
function C:SpawnTest()

    self:SpawnNPCShipOfFactionAndActivity(2, "Combat")
    self:SpawnNPCShipOfFactionAndActivity(3, "Combat")

end

---Spawn a specific template for a faction
---Ignores faction restrictions!
---@param Faction number #The index of the faction to spawn the ship for
---@param TemplateFileName string #The name of the template (wihout its extension) to spawn
function C:SpawnTemplate(Faction, TemplateFileName)
    local FoundTemplate = nil
    for _, Template in ipairs(self.ShipTemplates) do
        if Template.FileName == TemplateFileName then
            FoundTemplate = Template
            break
        end
    end

    if not FoundTemplate then
        print(string.format("No template was found for requested file name \"%s\"!", TemplateFileName))
        return
    end

    local Activity = table.Random(table.GetKeys(FoundTemplate.ShipInfo.NPCShipTags))

    self:SpawnNPCShipOfFactionAndActivity(Faction, Activity, FoundTemplate)
end

---Spawn a new NPC ship at the given coordinates
---@param Pos vector #The world position where the NPC ship will spawn at
---@param Class string #The class of ship to spawn
---@param Template table #The template of ship to spawn
---@return NPCShip NewNPCShip #The newly-created NPC ship
function C:SpawnNPCShip(Pos, Template)
    local NewNPCShip = GAMEMODE.class.getClass("NPCShip"):new()
    local Ents, Core, Gyropod = Blueprinter_SpawnDupe(Pos, angle_zero, nil, Template)

    NewNPCShip:SetEntities(Ents)
    NewNPCShip:SetCore(Core)
    NewNPCShip:SetGyropod(Gyropod)
    NewNPCShip:SetShipClass(ShipSizes[Template.ShipInfo.Class])

    if Template.ShipInfo then
        local SigRadDist = Template.ShipInfo.SigRad
        NewNPCShip:SetTargetDistance(1000)
        NewNPCShip:SetStoppingDistance(SigRadDist)
    end

    if IsValid(Core) then
        Core.Fitting.PG_R = 1000
        Core.Fitting.CPU_R = 1000
    end

    return NewNPCShip
end

--#endregion
--#region Activities

---Spawn a targeter if the template doesn't have one
---TODO: Make this more generic and spawn other SC entities
---@param NewNPCShip NPCShip #The NPC ship to spawn the targeter for
---@return entity Targeter #The targeter that was created
function C:SpawnTargeterForShip(NewNPCShip)
    local Gyropod = NewNPCShip:GetGyropod()

    local Targeter = ents.Create("sc_module_targeter")
    Targeter:SetAngles(Gyropod:GetAngles())
    Targeter:SetPos(Gyropod:LocalToWorld(Vector(20, 0, 0)))
    Targeter:SetModel("models/maxofs2d/cube_tool.mdl")
    Targeter:Spawn()

    --Ownership crap
    Targeter:SetPlayer(game.GetWorld())
    if NADMOD then --If we're using NADMOD PP, then use its function
        NADMOD.SetOwnerWorld(Targeter)
    elseif CPPI then --If we're using SPP, then use its function
        Targeter:SetNWString("Owner", "World")
    else --Well fuck it, lets just use our own!
        Targeter.Owner = game.GetWorld()
    end

    --Make the targeter behave as if it were spawned by the blueprinter spawner
    local PhysObj = Targeter:GetPhysicsObject()

    if IsValid(PhysObj) then
        PhysObj:EnableMotion(false)
        PhysObj:Sleep()
    end

    Targeter:SetTransmitWithParent(true)

    -- Prevent the entity from being interacted with
    Targeter.SB_Ignore = true
    Targeter.Untouchable = true
    Targeter.Unconstrainable = true
    Targeter.PhysgunDisabled = true
    Targeter.CanTool = function()
        return false
    end

    -- Ships cannot be unfrozen
    Targeter:SetUnFreezable(false)

    --reweld everything
    if util.IsValidPhysicsObject(Targeter, 0) then
        constraint.Weld(Targeter, Gyropod, 0, 0, 0, false, true)
    end

    --parent that shit up
    Targeter:SetParent(Gyropod)

    return Targeter
end

---Run through the list of activities and spawn NPC ships to complete them
function C:ProcessNPCActivities()
    local Faction = self:PickWeightedFaction()
    local Activity = self:PickWeightedActivityForFaction(Faction)
    self:SpawnNPCShipOfFactionAndActivity(Faction, Activity)
end

---Spawn an NPC ship for a faction, with an activity
---@param Faction number #The team ID of the faction to spawn the ship for
---@param Activity string #The name of the activity to assign to the ship
function C:SpawnNPCShipOfFactionAndActivity(Faction, Activity, TemplateOverride)

    local Template = TemplateOverride
    if not Template then
        Template = self:GetTemplateForFactionActivity(Faction, Activity)
    end

    if not Template then
        SC.Error(string.format("No template found for faction %s and activity %s!", team.GetName(Faction), Activity), 5)
        return
    end

    local ShipSpawnPos = self:FindValidSpawnPoint(Template)
    if not ShipSpawnPos then return end --Couldn't find a valid spawn location? Don't spawn the ship.
    local NewNPCShip = self:SpawnNPCShip(ShipSpawnPos, Template)
    --Pre ship creation

    NewNPCShip:SetFaction(Faction)
    NewNPCShip:SetRadius(Template.ShipInfo.SigRad)

    --Create targeter if it doesn't exist
    local Targeter
    local HaveWeapons = false
    local HaveRefinery = false
    for _, Ent in pairs(NewNPCShip:GetEntities()) do
        if Ent.IsSCTargeter then
            Targeter = Ent
        elseif Ent.IsSCWeapon or Ent.IsSCTurret then
            HaveWeapons = true
        elseif Ent:GetClass() == "sc_refinery" then
            NewNPCShip:AddRefinery(Ent)
            HaveRefinery = true
        end
    end

    --Spawn a targeter for the ship if it doesn't have one
    if not Targeter then
        Targeter = self:SpawnTargeterForShip(NewNPCShip)
    end

    --Set up the targeter with some initial settings
    NewNPCShip:SetTargeter(Targeter)
    Targeter.TargetFilters = NPCShipTargeterFilters
    Targeter.SC_Active = true

    --Handle combat ships
    if HaveWeapons then
        NewNPCShip:AddActivity("Combat")
        NewNPCShip:SetPrimaryActivity("Combat")
    end
    NewNPCShip:SetHasWeapons(HaveWeapons)

    --Post ship creation

    --Special activity cases for mining and transport
    if Activity == "Mining" then
        NewNPCShip:AddActivity("Mining")
        NewNPCShip:SetPrimaryActivity("Mining")
        if HaveRefinery then
            NewNPCShip:AddActivity("Refining")
        end
    elseif Activity == "Transport" then
        NewNPCShip:AddActivity("Transport")
        NewNPCShip:SetPrimaryActivity("Transport")
    end

    SC.Error(string.format("[NPC Ship Manager]Spawned ship Template: %s Faction %s Activity: %s", Template.FileName, team.GetName(Faction), Activity), 4)
end

--#endregion
--#region NPC Ship Factions

---Populate the faction relation table with some initial values
---This manual data entry is very WIP and should be changed to something more automatic at some point
function C:PopulateFactionRelations()
    local CIV = GAMEMODE.FactionIndicies["Civilian"]
    local BA = GAMEMODE.FactionIndicies["Bereavement Advocates"]
    local SD = GAMEMODE.FactionIndicies["Spectral Directorate"]
    local ADARI = GAMEMODE.FactionIndicies["Advanced Development and Research Institutes"]
    local THC = GAMEMODE.FactionIndicies["Transhumanist Collective"]

    for FactionName, FactionIndex in pairs(GAMEMODE.FactionIndicies) do
        self:AddBaseFactionRelation(FactionIndex)
    end


    --Civilian Relations
    self.FactionRelations[CIV][BA] = -100   --The civilians will avoid the BA but not go out of their way to kill them
    self.FactionRelations[CIV][ADARI] = -100  --Avoid the crazy science people
    self.FactionRelations[CIV][THC] = -100  --They do crazy science things

    --BA Relations
    self.FactionRelations[BA][CIV] = -1000000  --The BA hate everyone and everything.
    self.FactionRelations[BA][SD] = -1000000
    self.FactionRelations[BA][ADARI] = -1000000  --The only way to apease them
    self.FactionRelations[BA][THC] = -1000000    --is with your blood

    --SD Relations
    self.FactionRelations[SD][BA] = -1000000

    --ADARI Relations
    self.FactionRelations[ADARI][CIV] = 1000     --The ADARI are friendly and want to help the citizens
    self.FactionRelations[ADARI][BA] = -100   --They won't seek to actively engate the BA, but will avoid them
    self.FactionRelations[ADARI][THC] = 500 --Science people do science things

    --Collective Relations
    self.FactionRelations[THC][BA] = -500
end

---Populate the faction behavior table with its initial values
---This is also very WIP and should be replaced with something else
function C:PopulateFactionBehaviors()

    --TODO: Add behavior definitions with activities
    --DefineBehavior("Mining", ...)
    --TODO: Give the factions weighted chances to perform behaviors, with chances to raise/lower a behavior's weight given certain events
    --IE, a chance to increase the weight of a "Combat" behavior if a ship gets blown up
    -- the chance to decrease the weight of a behavior over time
    -- etc...

    local CIV = GAMEMODE.FactionIndicies["Civilian"]
    local BA = GAMEMODE.FactionIndicies["Bereavement Advocates"]
    local SD = GAMEMODE.FactionIndicies["Spectral Directorate"]
    local ADARI = GAMEMODE.FactionIndicies["Advanced Development and Research Institutes"]
    local THC = GAMEMODE.FactionIndicies["Transhumanist Collective"]


    ---@class NPCFactionBehavior
    ---@field Rarity number #A weight of how common this faction is. The higher the number, the more numerous they will be
    ---@field Power number #How powerful this faction is. A higher number means more higher-tier ships will be used
    ---@field Activities table #A table of the weighted activities that this faction will perform
    self.FactionBehaviors[CIV] = {
        Rarity = 3,
        Power = 3,

        Activities = {
            Mining = 1,
            Transport = 0
        }
    }

    self.FactionBehaviors[BA] = {
        Rarity = 2,
        Power = 7,

        Activities = {
            Combat = 1  --Blood blood blood blood blood blood
        }
    }

    self.FactionBehaviors[SD] = {
        Rarity = 1,
        Power = 10,

        Activities = {
            Combat = 1,
            Transport = 0
        }
    }

    self.FactionBehaviors[ADARI] = {
        Rarity = 3,
        Power = 7,

        Activities = {
            Mining = 4,
            Transport = 0,
            Combat = 1
        }
    }

    self.FactionBehaviors[THC] = {
        Rarity = 3,
        Power = 7,

        Activities = {
            Mining = 4,
            Transport = 0,
            Combat = 1
        }
    }

    --Fill the weighted table of factions with quantiites of that faction
    --There is probably a much better way to do this, but I am tired and need to test this
    for FactionIndex, Data in pairs(self.FactionBehaviors) do
        for I = 1, Data.Rarity do
            table.insert(self.WeightedFactionTable, FactionIndex)
        end

        Data.WeightedActivities = {}

        for ActivityName, Weight in pairs(Data.Activities) do
            for I = 1, Weight do
                table.insert(Data.WeightedActivities, ActivityName)
            end
        end
    end
end


function C:AddBaseFactionRelation(NewFactionIndex)
    self.FactionRelations[NewFactionIndex] = {}
    self.FactionBehaviors[NewFactionIndex] = {}

    for FactionIndex, Relations in pairs(self.FactionRelations) do
        Relations[NewFactionIndex] = 0
        self.FactionRelations[NewFactionIndex][FactionIndex] = 0
    end
end

---Get a faction from the table of factions, weighed by the faction's `Rarity` field
---@return number FactionID #The ID of the random faction
function C:PickWeightedFaction()
    return table.Random(self.WeightedFactionTable)
end

---Get the name of an activity for this faction, weighed by the activity's weights
---@param Faction number #The team ID of the faction to get an activity for
function C:PickWeightedActivityForFaction(Faction)
    return table.Random(self.FactionBehaviors[Faction].WeightedActivities)
end

---Return the relation that faction A has to faction B
---@param FactionA number
---@param FactionB number
function C:GetFactionRelation(FactionA, FactionB)
    return self.FactionRelations[FactionA][FactionB]
end


--#endregion
--#region Things In Space

---Populate the `TransportPlanets` table with planets that won't destroy the transport ships
function C:PopulateTransportPlanets()
    self.Planets = {}
    self.TransportPlanets = {}
    self.HostilePlanets = {}
    self.MapHasSpace = not GAMEMODE:GetSpace():GetHabitable()

    for _, Env in ipairs(GM.AtmosphereTable) do
        local EnvEntry = {
            Pos = Env:GetCelestial():getEntity():GetPos(),
            Radius = Env:GetRadius(),
            Environment = Env
        }

        table.insert(self.Planets, EnvEntry)

        if Env:GetPressure() <= 2 then
            table.insert(self.TransportPlanets, self.Planets[#self.Planets])
        else
            table.insert(self.HostilePlanets, self.Planets[#self.Planets])
        end
    end
end

---Get the list of planets that are valid for transport activity ships to travel to
---@return table TransportPlanets
function C:GetTransportPlanets()
    if not self.TransportPlanets then
        self:PopulateTransportPlanets()
    end

    return self.TransportPlanets
end

---Get the list of planets that are hostile
---@return table HostilePlanets
function C:GetHostilePlanets()
    if not self.HostilePlanets then
        self:PopulateTransportPlanets()
    end

    return self.HostilePlanets
end

---Get the list of planets that are just planets
---@return table Planets
function C:GetPlanets()
    if not self.Planets then
        self:PopulateTransportPlanets()
    end

    return self.Planets
end

---Find a point on the map that the ship template can spawn in
---@param Template table #The ship template to find a spawn point for
function C:FindValidSpawnPoint(Template)
    if not self.Planets then
        self:PopulateTransportPlanets()
    end

    local Range = 14000
    local Mins = Template.Mins
    local Maxs = Template.Maxs
    local Radius = Template.ShipInfo.SigRad    --Used to find a safe distance from a planet

    local TraceData = {
        start = nil,
        endpos = nil,
        mins = Mins,
        maxs = Maxs,
        ignoreworld = false,
        filter = {}
    }

    for SpawnAttempt = 1, 20 do
        local Pos = Vector(math.random(-Range, Range), math.random(-Range, Range), math.random(-Range, Range))
        for _, Env in ipairs(self.Planets) do
            local Distance = Pos:Distance(Env.Pos)
            if Distance > Radius + (Env.Radius * 1.5) then
                TraceData.start = Pos
                TraceData.endpos = Pos
                local TR = util.TraceHull(TraceData)
                if not TR.Hit then
                    return Pos
                end
            end
        end
    end

    SC.Error("No valid spawn locations found after 20 attempts!", 4)
end

--#endregion
--#region Global Hooks

---Think that occurs once per game tick
function C:Think()

    if self.DoProcessShips then
        --Think all of the ships once per tick
        for ShipID, Ship in pairs(self.Ships) do
            if IsValid(Ship) then
                Ship:Think()
            end
        end
    end

    if CurTime() > self.NextThinkTime then
        self.NextThinkTime = CurTime() + 1

        --Spawn ships if needed
        if self.DoSpawnShips and (player.GetCount() > 0) and self.MapHasSpace and (self.TotalShips < self.MaxShips) then
            self:ProcessNPCActivities()
        end

        if self.DoProcessShips then
            --Make the ships slow think
            for ShipID, Ship in pairs(self.Ships) do
                if IsValid(Ship) then
                    Ship:SlowThink()
                end
            end
        end
    end
end

hook.Remove("Think", "SC.NPCShipManager.Think")
hook.Add("Think", "SC.NPCShipManager.Think", function()
    local Manager = GM:GetNPCShipManager()
    if IsValid(Manager) then
        Manager:Think()
    end
end)

hook.Remove("SC.ShouldApplyDamage", "SC.NPCShipManager.ShouldApplyDamage")
hook.Add("SC.ShouldApplyDamage", "SC.NPCShipManager.ShouldApplyDamage", function(self, Damage, Attacker, Inflictor, HitData)
    if self:IsProtected() then
        Core = self:GetProtector()

        local NPCShip = GAMEMODE:GetNPCShipManager():GetShipByCore(Core)
        if NPCShip then
            NPCShip:ApplyDamage(Damage, Attacker, Inflictor, HitData)
        end
    end
end)


--#endregion

GM.class.registerClass("NPCShipManager", C)
--#endregion