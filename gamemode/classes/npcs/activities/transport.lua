
if CLIENT then return end

local GM = GM

---@class NPCShip_Activity_Transport: NPCShip_Activity_Base #A transport NPC ship activity
---@field Itinerary table #The transporation itinerary assigned to this ship
---@field TargetPlanet table #The current planet that the ship is trying to navigate to
---@field LastTargetPlanet table #The last planet that we visited
---@field WaitingTime number #The time, in seconds, to start moving again and move to the next target planet
---@field Waiting boolean #A flag indicating that the ship is waiting some time before movbing onto its next planet
local C = GM.class.getClass("NPCShip_Activity_Base"):extends({
    Itinierary = nil,
    TargetPlanet = nil,
    LastTargetPlanet = nil,
    Waiting = false
})

local BaseClass = C:getClass()

--#region General Class Stuff
function C:init(NPCShip)
    BaseClass.init(self, NPCShip)

    self.Itinerary = {}
    self:GenerateItinerary()
end

--#endregion
--#region Member variables


--#endregion
--#region Callback Overrides

---Called when the ship arrives at a planet
function C:OnArrivedAtLastWaypoint()
    --TODO: Add some code to make the ship level out at its destination
    self.Waiting = true
    self.WaitingTime = CurTime() + math.random(5, 15)  --Wait around for a bit
end
--#endregion
--#region Itinerary Management

---Generate the itinerary that the ship will follow
function C:GenerateItinerary()
    local Manager = GM:GetNPCShipManager()
    local Planets = Manager:GetTransportPlanets()
    local PlanetCount = table.Count(Planets)

    if PlanetCount < 2 then    --Sad map is sad :(
        if PlanetCount == 1 then
            self.Itinerary = { Planets[1] }
        end

        return
    end

    local ItineraryLength = math.random(3, 10)  --How many planets to add to the itinerary
    local LastPlanetIndex = 0  --The last planet that was added to the itinerary

    for I = 1, ItineraryLength do
        local RandomPlanetIndex = math.random(1, PlanetCount)
        if RandomPlanetIndex == LastPlanetIndex then
            RandomPlanetIndex = RandomPlanetIndex + 1
            if RandomPlanetIndex > PlanetCount then RandomPlanetIndex = 1 end
        end

        table.insert(self.Itinerary, Planets[RandomPlanetIndex])
        LastPlanetIndex = RandomPlanetIndex
    end
end

---Generate a set of waypoints to navigate this ship to `TargetPlanet`
function C:SetCourseForTargetPlanet()
    local Ship = self.NPCShip
    local PlanetEntity = self.TargetPlanet:GetCelestial():getEntity()
    local PlanetPos = PlanetEntity:GetPos()
    local PlanetRadius = self.TargetPlanet:GetRadius()

    --Fly up and out of the planet that we are in right now
    if self.LastTargetPlanet then
        local LastPlanetPos = self.LastTargetPlanet:GetCelestial():getEntity():GetPos()
        Ship:AddWaypoint(LastPlanetPos + Vector(0,0,self.LastTargetPlanet:GetRadius()))
    end

    --Fly into the top of the target planet's atmosphere
    Ship:AddWaypoint(PlanetPos + Vector(0,0,PlanetRadius))

    --and down to some random point
    local RandomHeight = (math.random() * 0.2) + 0.2
    local RandomHeightSin = math.sin(RandomHeight * math.pi)
    local RandomPoint = Vector(
        PlanetRadius * math.sin(math.random() * math.pi * 2) * RandomHeightSin,
        PlanetRadius * math.cos(math.random() * math.pi * 2) * RandomHeightSin,
        PlanetRadius * RandomHeight
    )
    Ship:AddWaypoint(PlanetPos + RandomPoint)

    Ship:SetTargetMovePos(nil)
end

function C:TargetNextPlanet()
    self.LastTargetPlanet = self.TargetPlanet
    self.TargetPlanet = table.remove(self.Itinerary)
    self:SetCourseForTargetPlanet()
end

--#endregion
--#region Thinking

---
function C:SlowThink()
    if self.Waiting then
        if CurTime() < self.WaitingTime then
            return
        end

        self.Waiting = false
        self:TargetNextPlanet()
    end

    if #self.Itinerary == 0 then
        self.NPCShip:SetActivity("Leave")
        return
    end

    if not self.TargetPlanet then
        self:TargetNextPlanet()
    end
end
--#endregion

GM.class.registerClass("NPCShip_Activity_Transport", C)