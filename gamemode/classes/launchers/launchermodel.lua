local GM = GM
local LauncherModels = {}

GM.Launchers = GM.Launchers or {}
GM.Launchers.Models = {}
GM.Launchers.Models.LoadedModels = LauncherModels

if SERVER then
    util.AddNetworkString("SC.LauncherModelsLoad")

    net.Receive("SC.LauncherModelsLoad", function(Length, Client)
        net.Start("SC.LauncherModelsLoad")
        net.WriteUInt(table.Count(LauncherModels), 16)
        for _,Data in pairs(LauncherModels) do
            Data:WriteCreationPacket()
        end
        net.Send(Client)
    end)
else
    net.Receive("SC.LauncherModelsLoad", function()
        local NumberOfComponents = net.ReadUInt(16)
        SC.Print("Got "..NumberOfComponents.." serialized Launcher Models from the server!", 4)
        for I = 1, NumberOfComponents do
            local Data = GM.class.new("LauncherModel")
            Data:ReadCreationPacket()
            LauncherModels[Data:GetModel()] = Data

            SC.Print("Loaded Model Data "..Data:GetModel(), 4)
        end


        SC.Print("Finished loading Launcher Models", 4)
        hook.Run("SC.LauncherModelsLoaded")
    end)
end

function GM.Launchers.Models.ReloadLauncherModels()
    -- Server loads all files from disk for launcher models
    if SERVER then
        -- Get all the files
        local Files = file.Find(SC.DataFolder.."/launchers/models/*", "DATA")

        -- For each file load the data into a new launcher model class
        for _,File in pairs(Files) do
            local NewLauncherModel = GM.class.new("LauncherModel")
            if NewLauncherModel:LoadFromINI(File) then
                LauncherModels[NewLauncherModel:GetModel()] = NewLauncherModel
            else
                SC.Error("Failed to load launcher model from file "..File, 5)
            end
        end

        hook.Run("SC.LauncherModelsLoaded")

    -- Clients request data from the server for launcher models
    else
        -- Reset the existing data until we get the new stuff
        LauncherModels = {}
        GM.Launchers.Models.LoadedModels = LauncherModels

        -- Request new data from the server
        net.Start("SC.LauncherModelsLoad")
        net.SendToServer()
    end
end

hook.Remove("SC.LauncherUpgradeDataLoaded", "SC.LoadLauncherModels")
hook.Add("SC.LauncherUpgradeDataLoaded", "SC.LoadLauncherModels", function()
    GM.Launchers.Models.ReloadLauncherModels()
end)

local C = GM.LCS.class({
    -- What class should be used for the launcher
    Model = "LauncherModel",

    -- A table of positions that a weapon could fire from using this model
    FiringPositions = {},

    -- The direction vector that the weapon fires from in local space
    FiringDirection = Angle(1, 0, 0)
})

function C:init()
    self.FiringPositions = {}
end

function C:GetModel()
    return self.Model
end

function C:GetFiringPosition(Index)
    return self.FiringPositions[Index]
end

function C:GetNextFiringPosition(LastIndex)
    local NextIndex, NextPos = next(self.FiringPositions, LastIndex)
    if not NextPos then
        NextIndex = 1
        NextPos = self:GetFiringPosition(1)
    end
    return NextIndex, NextPos
end

function C:GetFiringPositions()
    return table.Copy(self.FiringPositions)
end

function C:GetFiringDirection()
    return self.FiringDirection
end

function C:LoadFromINI(FileName)
    local Success, Data = GM.util.LoadINIFile(SC.DataFolder.."/launchers/models/"..FileName)

    -- Load all the data into the class if we got any data back
    if Success then
        self:DeSerialize(Data)
        return true
    end

    return false
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Model)
    net.WriteAngle(self.FiringDirection)
    net.WriteUInt(table.Count(self.FiringPositions), 8)
    for i, k in pairs(self.FiringPositions) do
        net.WriteVector(k)
    end
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Model = net.ReadString()
    self.FiringDirection = net.ReadAngle()
    self.FiringPositions = {}
    local NumPositions = net.ReadUInt(8)
    for I=1, NumPositions do
        table.insert(self.FiringPositions, net.ReadVector())
    end
end

function C:Serialize()
    return {
        Configuration = {
            Model = self.Model,
            FiringDirection = self.FiringDirection,
        },
        FiringPositions = self.FiringPositions,
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Model = Data.Configuration.Model or "error.mdl"
        self.FiringDirection = Data.Configuration.FiringDirection or Angle(0, 0, 0)
    end

    self.FiringPositions = Data.FiringPositions or {}
end

GM.class.registerClass("LauncherModel", C)