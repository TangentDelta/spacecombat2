local GM = GM

local C = GM.class.getClass("Launcher"):extends({
    LogisticsNode = nil,
    TaskQueue = nil,    --A queue of tasks to assign to new drones
    MaxDrones = 0,
    MaxQueueSize = 0,
    AvailableDrones = 0,
    DroneSize = 1000,
    Drones = {}
})

local BaseClass = C:getClass()

function C:IsProjectileRecipeCompatible(Recipe)
    return BaseClass.IsProjectileRecipeCompatible(self, Recipe)
end

function C:SetLauncherType(Type)
    local LauncherData = BaseClass.SetLauncherType(self, Type)
    if LauncherData then

    end
end

function C:init()
    --TODO: Remove these when Brandon exposes them in the launcher data
    self.HasMuzzleFlash = false
    self.FireSound = "sound/weapons/physcannon/energy_bounce2.wav"

    if SERVER then
        --Set up the logistics node associated with this port
        self.LogisticsNode = GM.class.getClass("LogisticsNode"):new("DRONEPORT")
        self.LogisticsNode:SetIsCourier(true)   --The drone port can act as a courier, and a logistics node
        self.LogisticsNode:SetParent(self)
        self.LogisticsNode:SetStorage(self:GetCore())

        --Drone status stuff
        self.MaxDrones = 10
        self.AvailableDrones = self.MaxDrones
        self.MaxQueueSize = self.MaxDrones
        self.TaskQueue = {}

        --Called by the logistics manager when it wants to give us a task
        self.LogisticsNode.AddCourierTask = function(Courier, SupplyNode, DemandNode, ResourceName, ResourceAmount)
            table.insert(self.TaskQueue, {
                SupplyNode = SupplyNode,
                DemandNode = DemandNode,
                ResourceName = ResourceName,
                Amount = ResourceAmount
            })
        end

        --Returns true if this courier is available for service
        self.LogisticsNode.GetCourierIsAvailable = function(Node)
            return table.Count(self.TaskQueue) < self.MaxQueueSize
        end

        --Returns how much of a resource we can fit in a single drone
        self.LogisticsNode.GetMaxCarryableAmount = function(Node, ResourceName)
            local Resource = GAMEMODE:GetResourceFromName(ResourceName)
            if not Resource then return 0 end
            local ResourceSize = Resource:GetSize()
            return math.floor(self.DroneSize / ResourceSize)
        end
    end

    BaseClass.init(self)
end

function C:CanFire()
    local CanFire = BaseClass.CanFire(self)
    return CanFire and (#self.TaskQueue > 0) and (self.AvailableDrones > 0)
end

function C:SetOwner(NewOwner)
    self.LogisticsNode:SetOwner(NewOwner)
    BaseClass.SetOwner(self, NewOwner)
end

function C:SetCore(Core)
    self.LogisticsNode:SetStorage(Core)
    BaseClass.SetCore(self, Core)
end

--[[
========================
    Drone Management
========================
]]
function C:AddDrone(NewDrone)
    --Configure the drone's controller
    local DroneControl = NewDrone:GetTargetingComponent()
    DroneControl:SetHome(self)
    DroneControl:SetStorageSize(self.DroneSize)

    --Courier stuff
    local Task = table.remove(self.TaskQueue, 1)    --Get the task to assign to this drone
    DroneControl:SetTask(Task)

    --Keep track of the drone
    self.Drones[NewDrone:GetID()] = NewDrone

    self.AvailableDrones = self.AvailableDrones - 1
end

--Called by the drone control component when the drone is removed
function C:RemoveDrone(Drone)
    self.Drones[Drone:GetID()] = nil

    self.AvailableDrones = self.AvailableDrones + 1
end

--[[
==========================
    Internal Callbacks
==========================
]]
function C:PreProjectileFired(Projectile)
    Projectile.ShouldSendNeUpdates = true

    BaseClass.PreProjectileFired(self, Projectile)
end

function C:PostProjectileFired(Projectile)
    self:AddDrone(Projectile)

    BaseClass.PostProjectileFired(self, Projectile)
end

function C:OnRemoved()
    for DroneID, Drone in pairs(self.Drones) do
        Drone:Remove()
        self.Drones[DroneID] = nil
    end

    GM:GetLogisticsManager():UnregisterNode(self.LogisticsNode)

    BaseClass.OnRemoved(self)
end

--[[
====================================
    Entity Compatibility Methods
====================================
]]
function C:GetPos()
    return self:GetWorldPosition()
end

function C:GetProtector()
    return self:GetCore()
end

--[[
============================
    Base Class Overrides
============================
]]
function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.LogisticsNode = self.LogisticsNode:Serialize()
    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    if Data.LogisticsNode then
        self.LogisticsNode:DeSerialize(Data.LogisticsNode)
    end
end

GM.class.registerClass("Launcher_Droneport", C)