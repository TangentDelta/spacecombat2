local GM = GM
local Color = Color
local math = math
local surface = surface
local slice = GM.util.tableSlice
local smooth = GM.util.smoother
local ColorBlack = Color(0, 0, 0, 0)

local C = GM.class.getClass("HudComponent"):extends({
	value = 0,
	maxvalue = 0,
	width = 0, -- Smallest of width/height will determine radius, so circle doesn't go out of bounds
	height = 0,
	radius = 0, -- Radius will be set during init, based on the above
	color = Color(150,150,255,255),
	backgroundColor = Color(150,255,150,255),
    centerColor = Color(0, 0, 0, 0),
	fill = true, -- Fill represents the style, whether it should have val color from top all the way clockwise to currvalue, or if it should be like a pip, either side of the value.
	pipSize = 5, -- Pcnt of the whole circle is the pip, value lies at the mid point of the pip
	target = 0,
	smoothFactor = 0.15
})

function C:init(x,y,w,h,val,maxval,col,bgcol,fill,ccol)
	self:super('init', x, y)

	self:setWidth(w)
	self:setHeight(h)

	self:setValue(val)
	self:setMaxValue(maxval)

	self:setColor(col)
	self:setBackgroundColor(bgcol)

    if ccol ~= nil then
        self:setCenterColor(ccol)
    end

	if fill ~= nil then
		self:setFill(fill)
	end

	-- Initial radius setting stuff
	local r = self:calcRadius()
	self:setRadius( r )

	self:setWidth( r*2 )
	self:setHeight( r*2 )

end

-- Get/Set of Width & Height is from this class
function C:getWidth()
	return self.width
end

function C:setWidth(w)
	self.width = w
	return
end

function C:getHeight()
	return self.height
end

function C:setHeight(h)
	self.height = h
	return
end

function C:getColor()
	return self.color
end

function C:setColor(c)
	self.color = c
	return
end

function C:getCenterColor()
	return self.centerColor
end

function C:setCenterColor(c)
	self.centerColor = c
	return
end

function C:getBackgroundColor()
	return self.backgroundColor
end

function C:setBackgroundColor(c)
	self.backgroundColor = c
	return
end

function C:getValue(v)
	return self.value
end

function C:setValue(v)
	self.value = v
	return
end

function C:getMaxValue()
	return self.maxvalue
end

function C:setMaxValue(v)
	self.maxvalue = v
	return
end

function C:getTarget()
	return self.target
end

function C:setTarget(t)
	self.target = math.Clamp(t, 0, self:getMaxValue() )
	return
end

function C:getSmoothFactor()
	return self.smoothFactor
end

function C:setSmoothFactor(s)
	self.smoothFactor = s
	return
end

function C:isFill()
	return self.fill
end

function C:setFill(bool)
	self.fill = bool
	return
end

function C:getPipSize()
	return self.pipSize
end

function C:setPipSize(p)
	self.pipSize = p
	return
end

function C:getRadius()
	return self.radius
end

function C:setRadius( r )
	self.radius = r
	return
end

function C:calcRadius()
	return ((self:getWidth()<self:getHeight()) and self:getWidth()/2 or self:getHeight()/2)
end

local function colorNegate(clr)
	local clr = table.Copy(clr)
	if type(clr) ~= "table" or not clr.a or not clr.r or not clr.g or not clr.b then error("Expected table ( clr ), got "..type(clr)) return clr end

	--Get negative of the clr
	clr.r = 255 - clr.r
	clr.g = 255 - clr.g
	clr.b = 255 - clr.b
	clr.a = 255
	return clr
end

-- Returns whether c is to the left of the line from a to b.
-- Copy pasted from EGP
local function counterclockwise( a, b, c )
	local area = (a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y)
	return area > 0
end

local function renderBackgroundCircle(self,update)
	if not self.renderBackgroundCircle_poly or update then
		local circ = {}
		for i=1, 360, 13 do
			table.insert(circ, {
				x = self:getX() + math.cos( math.rad(i) ) * self:getRadius(),
				y = self:getY() + math.sin(math.rad(i)) * self:getRadius()
			})
		end
		self.renderBackgroundCircle_poly = circ
	end

	local circ = self.renderBackgroundCircle_poly
	render.CullMode(counterclockwise(circ[1],circ[2],circ[3]) and MATERIAL_CULLMODE_CCW or MATERIAL_CULLMODE_CW)
	surface.SetTexture(0)
	surface.SetDrawColor(self:getBackgroundColor())
	surface.DrawPoly(circ)
	render.CullMode(MATERIAL_CULLMODE_CCW)
end

local function renderFillIndicator(self,update)
	-- This is a fill indicator which will be value color up to the current value, examples include FPS

	if not self.renderFillIndicator_poly or update then
		local circ = {}

		if math.Round(self:getValue(),2) < self:getMaxValue() then
			circ[1] = {x = self:getX(), y = self:getY()}
		end

		for i=0, 359, 13 do
			if i/359 <= self:getValue()/self:getMaxValue() then
				circ[#circ+1] = {
					x = self:getX() + math.cos( math.rad(i-90) ) * self:getRadius()*(99/100),
					y = self:getY() + math.sin( math.rad(i-90) ) * self:getRadius()*(99/100)
				}
			else
				break
			end
		end

		if math.Round(self:getValue(),2) < self:getMaxValue() then
			circ[#circ+1] = {x = self:getX(), y = self:getY()}
		end

		self.renderFillIndicator_poly = circ
	end

	local circ = self.renderFillIndicator_poly
	render.CullMode(counterclockwise(circ[1],circ[2],circ[3] or {x=0,y=0}) and MATERIAL_CULLMODE_CCW or MATERIAL_CULLMODE_CW)
	surface.SetTexture(0)
	surface.SetDrawColor( self:getColor() )
	surface.DrawPoly( circ )
	render.CullMode(MATERIAL_CULLMODE_CCW)
end

local function renderNonFillIndicator(self,update)
	-- This is just a segement indicator, the ceter being the current value, examples clock.

	if not self.renderNonFillIndicator_poly or update then
		local circ = {{x= self:getX(), y= self:getY()}}

		local ppDeg = 360 / self:getMaxValue() -- point per degree. Cuts 360 degrees up to find how many degrees per point
		local transVal = ppDeg * (self:getValue()) -- transformed value. degrees/point * points = degrees :D

		for i=transVal - (((self:getPipSize()-1)/2)*ppDeg), transVal + (((self:getPipSize()-1)/2)*ppDeg), 4 do -- Go 2 points either side for the segment. Total coverage of pipSize points
			circ[#circ+1] = {
				x = self:getX() + math.cos( math.rad(i-90) ) * (self:getRadius()*(99/100)),
				y = self:getY() + math.sin( math.rad(i-90) ) * self:getRadius()*(99/100)
			}
		end

		circ[#circ+1] = {x = self:getX(), y = self:getY()}
		self.renderNonFillIndicator_poly = circ
	end

	local circ = self.renderNonFillIndicator_poly
	render.CullMode(counterclockwise(circ[1],circ[2],circ[3]) and MATERIAL_CULLMODE_CCW or MATERIAL_CULLMODE_CW)
	surface.SetTexture(0)
	surface.SetDrawColor( self:getColor() )
	surface.DrawPoly( circ )
	render.CullMode(MATERIAL_CULLMODE_CCW)
end

local function renderCoverCircle(self,update)

	if not self.renderCoverFillIndicator_poly or update then
		local circ = {}
		for i=1, 360, 13 do
			table.insert(circ, {
				x = self:getX() + math.cos( math.rad(i-90) ) * (self:getRadius()*(3/5)),
				y = self:getY() + math.sin(math.rad(i-90)) * (self:getRadius()*(3/5))
			})
		end
		self.renderCoverFillIndicator_poly = circ
	end

	local circ = self.renderCoverFillIndicator_poly
	render.CullMode(counterclockwise(circ[1],circ[2],circ[3]) and MATERIAL_CULLMODE_CCW or MATERIAL_CULLMODE_CW)
	surface.SetTexture(0)
    if self:getCenterColor() ~= ColorBlack then
        surface.SetDrawColor( self:getCenterColor() )
    else
	    surface.SetDrawColor( colorNegate( self:getBackgroundColor() ) )
    end
	surface.DrawPoly(circ)
	render.CullMode(MATERIAL_CULLMODE_CCW)
end

function C:think()
	if math.abs(self:getTarget() - self:getValue()) >= (0.015 * self:getMaxValue()) then
		self:setValue( smooth(self:getTarget(), self:getValue(), self:getSmoothFactor()) )
	end
end

function C:render()
	self:super('render') -- Call back to HudComponent

    local update = false
    local updateMain = false
	if self:getValue() ~= self.oldval then update = true self.oldval = self:getValue() end
	if self:getMaxValue() ~= self.oldmaxval then update = true self.oldmaxval = self:getMaxValue() end
	if self:getRadius() ~= self.oldradius then update = true self.oldradius = self:getRadius() end

	-- Think will be called from the top Super class' render func.
	-- This will end up setting the radius for us as defined above
	if self:isVisible() then
		renderBackgroundCircle(self,updateMain) -- Background circle first

		if self:isFill() then -- Check what type of indicator we want. Whether we want it to fill from top -> value or just have a segment filled near the value
			renderFillIndicator(self,update or updateMain)
		else
			renderNonFillIndicator(self,update or updateMain)
		end

		renderCoverCircle(self,updateMain) -- Center cover circle to hide things
	end

end

GM.class.registerClass("HudRadialIndicator", C)

