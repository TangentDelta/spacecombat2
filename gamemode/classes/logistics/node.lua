local GM = GM

---@class LogisticsNode
local C = GM.LCS.class({
    Storage = nil,      --The resource container/ship core/resource node that this logistics node is attached to
    Owner = nil,       --Who owns this node
    Supply = {},        --A list of resources supplied by this node
    Demand = {},        --A list of resources demanded by this node
    Inbound = {},       --A list of resources that are inbound to this node
    Outbound = {},      --A list of resources that are outbound from this node
    ID = 0,             --The node's ID in the global node table
    Type = "",          --What type this node is
    Label = "Unnamed",         --The user-assigned label on this node

    IsCourier = false,  --Does this node also act as a courier
    SupplyRange = 5000  --How far out this courier node can supply resources
})

function C:init(Type)
    self.Type = Type
    GAMEMODE:GetLogisticsManager():RegisterNode(self)
end

--[[
=================================
    Member Variable Accessors
=================================
]]

--ID

function C:SetID(NewID)
    self.ID = NewID
end

function C:GetID()
    return self.ID
end

--Owner

function C:SetOwner(NewOwner)
    self.Owner = NewOwner
end

function C:GetOwner()
    return self.Owner
end

--Type

function C:GetType()
    return self.Type
end

--Label

function C:SetLabel(NewLabel)
    self.Label = NewLabel or "Unnamed"
end

function C:GetLabel()
    return self.Label
end

--IsCourier

function C:SetIsCourier(IsCourier)
    self.IsCourier = IsCourier
end

function C:GetIsCourier()
    return self.IsCourier
end

--Parent

function C:SetParent(Parent)
    self.Parent = Parent
end

function C:GetParent()
    return self.Parent
end

--Storage

function C:SetStorage(Storage)
    self.Storage = Storage
end

function C:GetStorage()
    return self.Storage
end

--SupplyRange
function C:SetSupplyRange(SupplyRange)
    self.SupplyRange = SupplyRange
end

function C:GetSupplyRange()
    return self.SupplyRange
end

--[[
===================================
    Generic Member Table Access
===================================
]]

--Add/update an entry in a resource talbe
local function AddTableEntry(Tbl, EntryName, ResourceName, ResourceQuantity, ResourcePriority)
    if EntryName and ResourceName then
        if not Tbl[EntryName] then Tbl[EntryName] = {} end
        local Entry = Tbl[EntryName]
        Entry[ResourceName] = {
            Quantity = ResourceQuantity or 0,
            Priority = ResourcePriority or 5
        }
    end
end

--Add/update a list of entries in a resoure table
local function AddTableEntries(Tbl, List)
    for _, NewEntry in pairs(List) do
        AddTableEntry(Tbl, NewEntry.EntryName, NewEntry.ResourceName, NewEntry.Quantity, NewEntry.Priority)
    end
end

--Remove an entry from a resource table, or a specific resource from a resource table's entry
local function RemoveTableEntry(Tbl, EntryName, ResourceName)
    if ResourceName and Tbl[EntryName] then
        Tbl[EntryName][ResourceName] = nil
    else
        Tbl[EntryName] = nil
    end
end

--Formats a resource table into a flat table indexed by resource name
local function FormatTableToResources(Tbl)
    local NewTbl = {}
    for EntryName, Entry in pairs(Tbl) do
        for ResourceName, ResourceEntry in pairs(Entry) do
            if not NewTbl[ResourceName] then
                NewTbl[ResourceName] = {
                    Quantity = ResourceEntry.Quantity,
                    Priority = ResourceEntry.Priority
                }
            else
                local NewEntry = NewTbl[ResourceName]
                if ResourceEntry.Quantity > NewEntry.Quantity then NewEntry.Quantity = ResourceEntry.Quantity end
                if ResourceEntry.Priority > NewEntry.Priority then NewEntry.Priority = ResourceEntry.Priority end
            end
        end
    end

    return NewTbl
end

--[[
==========================================
    Demand/Supply Addition/Subtraction
==========================================
]]

--Add a demand to the table of demands
function C:AddDemand(DemandName, ResourceName, ResourceQuantity, ResourcePriority)
    AddTableEntry(self.Demand, DemandName, ResourceName, ResourceQuantity, ResourcePriority)
end

--Add a list of demands to the demand table
function C:AddDemands(DemandList)
    AddTableEntries(self.Demand, DemandList)
end

--Remove a demand entry by name, or an individual demand from a demand entry
function C:RemoveDemand(DemandName, ResourceName)
     --Delete the demand, or the entire demand group
    RemoveTableEntry(self.Demand, DemandName, ResourceName)
end

--
--  Supply
--

--Add a Supply to the table of Supplies
function C:AddSupply(SupplyName, ResourceName, ResourceQuantity, ResourcePriority)
    AddTableEntry(self.Supply, SupplyName, ResourceName, ResourceQuantity, ResourcePriority)
end

--Add a list of Supplies to the Supply table
function C:AddSupplys(SupplyList)
    AddTableEntries(self.Supply, SupplyList)
end

--Remove a Supply entry by name, or an individual Supply from a Supply entry
function C:RemoveSupply(SupplyName, ResourceName)
    --Delete the supply, or the entire supply group
    RemoveTableEntry(self.Supply, SupplyName, ResourceName)
end

--[[
=========================================
    Resource Table Information Access
=========================================
]]

--
--  Raw table access
--

function C:GetDemand()
    return self.Demand
end

function C:GetSupply()
    return self.Supply
end

--
--  Formated table access
--

--Returns all resources demanded by this node
function C:GetDemandResources()
    return FormatTableToResources(self.Demand)
end

--Returns all resources supplied by this node
function C:GetSupplyResources()
    return FormatTableToResources(self.Supply)
end

--Returns a table of resources, and their quantities, required to fulfill this node's demands
function C:GetRequiredResources()
    if not self.Storage then return {} end

    local RequiredResources = {}
    local DemandResources = self:GetDemandResources()
    for ResName, DemandEntry in pairs(DemandResources) do
        local Quantity = DemandEntry.Quantity
        if Quantity == 0 then Quantity = self.Storage:GetMaxAmount(ResName) end     --If the quantity requested is 0, treat it as "fill to the max"
        local Needed = Quantity - (self:GetAmount(ResName) + self:GetInboundAmount(ResName))
        if Needed > 0 then
            RequiredResources[ResName] = {
                Quantity = math.floor(Needed),
                Priority = DemandEntry.Priority
            }
        end
    end
    return RequiredResources
end

--Returns a table of resources, and their quantities, that are available for this node to supply
function C:GetAvailableResources()
    local AvailableResources = {}
    local SupplyResources = self:GetSupplyResources()
    for ResName, SupplyEntry in pairs(SupplyResources) do
        local Available = (self:GetAmount(ResName) - SupplyEntry.Quantity) - self:GetOutboundAmount(ResNAme)
        if Available > 0 then
            AvailableResources[ResName] = {
                Quantity = math.floor(Available),
                Priority = SupplyEntry.Priority
            }
        end
    end
    return AvailableResources
end

--Returns how much of a resource is required to fulfill this node's demand for it
function C:GetRequiredResource(ResourceName)
    local DemandEntry = self:GetDemandResources()[ResourceName]
    if not DemandEntry then return 0 end
    local Quantity = DemandEntry.Quantity
    if Quantity == 0 then Quantity = self.Storage:GetMaxAmount(ResourceName) end     --If the quantity requested is 0, treat it as "fill to the max"
    local Needed = Quantity - (self:GetAmount(ResourceName) + self:GetInboundAmount(ResourceName))
    return math.max(math.floor(Needed), 0)
end

--Returns how much of a resource is available for this node to supply
function C:GetAvailableResource(ResourceName)
    local SupplyEntry = self:GetSupplyResources()[ResourceName]
    if not SupplyEntry then return 0 end
    local Available = (self:GetAmount(ResourceName) - SupplyEntry.Quantity) - self:GetOutboundAmount(ResourceName)
    return math.max(math.floor(Available), 0)
end

--[[
============================================
    Inbound/Outbound Resource Management
============================================
]]
--Add onto the amount of resources outbound from this node
function C:AddOutbound(ResourceName, ResourceQuantity)
    self.Outbound[ResourceName] = (self.Outbound[ResourceName] or 0) + ResourceQuantity
end

--Add onto the amount of resources inbound to this node
function C:AddInbound(ResourceName, ResourceQuantity)
    self.Inbound[ResourceName] = (self.Inbound[ResourceName] or 0) + ResourceQuantity
end

--Returns how much of a resource is inbound to this node
--A courier is carrying this resource to this node
function C:GetInboundAmount(ResourceName)
    return self.Inbound[ResourceName] or 0
end

--Returns how much of a resource is outbound from this node
--A courier is on its way to collect this much resource
function C:GetOutboundAmount(ResourceName)
    return self.Outbound[ResourceName] or 0
end

function C:SubtractInbound(ResourceName, ResourceAmount)
    self.Inbound[ResourceName] = math.max((self.Inbound[ResourceName] or 0) - ResourceAmount, 0)
end

function C:SubtractOutbound(ResourceName, ResourceAmount)
    self.Outbound[ResourceName] = math.max((self.Outbound[ResourceName] or 0) - ResourceAmount, 0)
end

--[[
=========================
    Resource Transfer
=========================
]]
--Functions used to abstract the interface between the outside world and our storage

--Used by a courier when it arrives at a demanding node to supply its requested resource
function C:CourierSupplyResource(ResourceName, ResourceAmount)
    if not self.Storage then return false end

    self:SubtractInbound(ResourceName, ResourceAmount)
    return self.Storage:SupplyResource(ResourceName, ResourceAmount)
end

--Used by a courier when it arrives at a supplying node to consume its requested resource
function C:CourierConsumeResource(ResourceName, ResourceAmount)
    if not self.Storage then return false end

    self:SubtractOutbound(ResourceName, ResourceAmount)
    return self.Storage:ConsumeResource(ResourceName, ResourceAmount)
end

function C:GetAmount(ResourceName)
    if not self.Storage then return 0 end

    return self.Storage:GetAmount(ResourceName)
end

function C:GetMaxAmount(ResourceName)
    if not self.Storage then return 0 end

    return self.Storage:GetMaxAmount(ResourceName)
end

--[[
=====================
    Entity Methods
=====================
]]

function C:GetPos()
    return self.Parent:GetPos()
end

function C:GetProtector()
    return self.Parent:GetProtector()
end

function C:IsValid()
    return (self.ID ~= 0) and IsValid(self.Owner)
end

--[[
====================================
    Node Table Member Management
====================================
]]

--Returns a table of nodes that are compatible with this one
--Right now, just returns a table of nodes owned by our owner
function C:GetCompatibleNodes()
    local CompatibleNodes = {}
    for NodeID, Node in pairs(GM:GetLogisticsManager():GetNodes()) do
        if IsValid(Node) then
            local NotSelf = (Node ~= self) and (Node:GetStorage() ~= self.Storage)
            local InRange = self:GetParent():GetPos():Distance(Node:GetParent():GetPos()) < self.SupplyRange
            local SameOwner = self:GetOwner() == Node:GetOwner()

            if NotSelf and InRange and SameOwner then
                table.insert(CompatibleNodes, Node)
            end
        end
    end

    return CompatibleNodes
end

--Returns true if a player is allowed to modify this node
--Right now just checks if the player is the owner
function C:IsPlayerAllowedToModify(Ply)
    return self:GetOwner() == Ply
end

--[[
=====================================
    Serialization/Deserialization
=====================================
]]
function C:Serialize()
    local Data = {
        Demand = self:GetDemand(),
        Supply = self:GetSupply(),
        Label = self:GetLabel()
    }

    return Data
end

function C:DeSerialize(Data)
    self:SetLabel(Data.Label)

    if Data.Demand then
        self.Demand = {}
        for GroupName, Group in pairs(Data.Demand) do
            for ResName, Entry in pairs(Group) do
                self:AddDemand(GroupName, ResName, Entry.Quantity, Entry.Priority)
            end
        end

        self.Supply = {}
        for GroupName, Group in pairs(Data.Supply) do
            for ResName, Entry in pairs(Group) do
                self:AddSupply(GroupName, ResName, Entry.Quantity, Entry.Priority)
            end
        end
    end
end

GM.class.registerClass("LogisticsNode", C)
