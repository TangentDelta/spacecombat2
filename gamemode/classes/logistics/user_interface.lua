AddCSLuaFile()

---@alias player entity
--#region Server-Side Code
if SERVER then
    --#region Networking

    ---Send a complete table of the logistics manager entries that are relavent to player `Ply`
    ---@param Ply player #The player to send the update to
    function SendLMMUIUpdate(Ply)
        net.Start("SC.LogisticsManagerUIUpdate")
        local MatchingNodes = GAMEMODE:GetLogisticsManager():GetNodesForPlayer(Ply)

        net.WriteUInt(table.Count(MatchingNodes), 16)

        for NodeID, Node in pairs(MatchingNodes) do
            net.WriteUInt(NodeID, 16)   --Node ID
            SC.WriteNetworkedString(Node:GetType())
            SC.WriteNetworkedString(Node:GetLabel())

            local ShipName = Node:GetProtector() and Node:GetProtector():GetShipName() or "None"
            SC.WriteNetworkedString(ShipName)

            net.WriteVector(Node:GetPos())

            --TODO: Send all demand and supply groups instead of just default
            --TODO: Add a GetAllowed[Demand/Supply]Groups(Ply) function to the node
            --      This will allow factions or players to set permissions per group

            --Send the list of demands
            local NodeDemand = Node:GetDemand()["default"] or {}
            local DemandCount = table.Count(NodeDemand)
            net.WriteUInt(DemandCount, 16)
            if DemandCount > 0 then
                for ResName, Demand in pairs(NodeDemand) do
                    SC.WriteNetworkedString(ResName)
                    net.WriteUInt(Demand.Quantity, 32)
                    net.WriteInt(Demand.Priority, 8)
                end
            end

            --Send the list of supplies
            local NodeSupply = Node:GetSupply()["default"] or {}
            local SupplyCount = table.Count(NodeSupply)
            net.WriteUInt(SupplyCount, 16)
            if SupplyCount > 0 then
                for ResName, Supply in pairs(NodeSupply) do
                    SC.WriteNetworkedString(ResName)
                    net.WriteUInt(Supply.Quantity, 32)
                    net.WriteInt(Supply.Priority, 8)
                end
            end
        end

        net.Send(Ply)
    end

    ---Read a demand group into a node
    ---@param Node LogisticsNode #The node to read the demand group data into
    ---@param DemandGroup string #The name of the demand group to read into
    function ReadDemandGroup(Node, DemandGroup)
        Node:RemoveDemand(DemandGroup)  -- Clear the demand group of the node
        local DemandCount = net.ReadUInt(16)

        if DemandCount > 0 then
            for I = 1, DemandCount do
                local ResName = SC.ReadNetworkedString()
                local ResQuantity = net.ReadUInt(32)
                local ResPriority = net.ReadInt(8)

                Node:AddDemand(DemandGroup, ResName, ResQuantity, ResPriority)
            end
        end
    end

    ---Read a supply group into a node
    ---@param Node LogisticsNode #The node to read the supply group data into
    ---@param SupplyGroup string #The name of the supply group to read the data into
    function ReadSupplyGroup(Node, SupplyGroup)
        Node:RemoveSupply(SupplyGroup)  -- Clear the supply group of the node
        local SupplyCount = net.ReadUInt(16)

        if SupplyCount > 0 then
            for I = 1, SupplyCount do
                local ResName = SC.ReadNetworkedString()
                local ResQuantity = net.ReadUInt(32)
                local ResPriority = net.ReadInt(8)

                Node:AddSupply(SupplyGroup, ResName, ResQuantity, ResPriority)
            end
        end
    end

    ---Read a an update of multuple nodes sent from a player
    ---@param Ply player #The player to read the update from
    function ReadMultipleNodeUpdate(Ply)
        local NodeCount = net.ReadUInt(16)  -- Read how many nodes need to be updated
        for I = 1, NodeCount do
            local NodeID = net.ReadUInt(16)
            local Node = GAMEMODE:GetLogisticsManager():GetNodes()[NodeID]
            if not Node:IsPlayerAllowedToModify(Ply) then return end   -- Deny players from updating nodes that they don't own

            -- Read general node information
            Node:SetLabel(SC.ReadNetworkedString()) -- Update the node's label

            -- Read demands
            local DemandGroup = SC.ReadNetworkedString()
            ReadDemandGroup(Node, DemandGroup)

            -- Read supplies
            local SupplyGroup = SC.ReadNetworkedString()
            ReadSupplyGroup(Node, SupplyGroup)
        end
    end

    net.Receive("SC.LogisticsManagerUIUpdate", function(Len, Ply)
        local DoSendUpdate = net.ReadBool()

        if DoSendUpdate then
            SendLMMUIUpdate(Ply)
        else
            local DoMultipleNodeUpdate = net.ReadBool()
            if DoMultipleNodeUpdate then
                ReadMultipleNodeUpdate(Ply)
            end
        end
    end)

    --#endregion
    --#region Hooks

    hook.Add("ShowTeam", "SC.LogisticsManager.UI.ShowTeam", function(Ply)
        net.Start("SC.OpenLogisticsManagerUI")
        net.Send(Ply)
    end)
    --#endregion
end
--#endregion
--#region Client-Side Code
if CLIENT then
    --#region Resource Selection Menu Data

    -- Resource selection filters
    local ResourceFilters = {
        ["Energy"] = "ResourceType == Energy",
        ["Liquid"] = "ResourceType == Liquid",
        ["Gas"] = "ResourceType == Gas",
        ["Cargo"] = "ResourceType == Cargo",
        ["Ammo"] = "ResourceType == Ammo",
        ["Capacitors"] = "ResourceName contains Charge"
    }
    --#endregion

    -- Data
    local Nodes = {}    -- The client-side node data
    local Presets = {}  -- User-created presets
    local SelectedNodes = {}    -- A table of references to nodes that are currently selected by the UI
    local NodeMap = {}  -- Map the position of a node in the node list to a node in the node table
    -- Derma UI things
    local LMFrame = nil -- The main frame of the UI
    local RightClickMenu = nil  -- A menu that pops up when right-click on certain elements
    local EditResourceMenu = nil
    local PresetMenu = nil  -- The frame of the preset selection/management menu
    local NodeEditorPanel = nil -- The panel used by the node editor
    -- HUD variables
    local HoverNodePosition = nil   -- The position of the node that the player is currently hovering their mouse cursor over

    --#region Networking

    ---Send a request to the server for a logistics node data update
    local function SendRequestForLMMUIUpdate()
        net.Start("SC.LogisticsManagerUIUpdate")
        net.WriteBool(true) -- Set flag indicating that this is a request for update rather than a new update to be applied
        net.SendToServer()
    end

    ---Write the contents of a node update
    ---@param Node table #The node to write the update for
    local function WriteNodeUpdate(Node)
        -- Write out general node information
        net.WriteUInt(Node.NodeID, 16)
        SC.WriteNetworkedString(Node.Label)

        -- Write out the demands
        --TODO: Allow the demand group to be edited
        SC.WriteNetworkedString("default")  --The demand group that these updated demands belong to
        net.WriteUInt(#Node.Demand, 16) --The number of demands in this group
        for _, Demand in ipairs(Node.Demand) do
            SC.WriteNetworkedString(Demand.ResourceName)
            net.WriteUInt(Demand.Quantity, 32)
            net.WriteInt(Demand.Priority, 8)
        end

        --
        --Write out the supplies
        --
        SC.WriteNetworkedString("default")
        net.WriteUInt(#Node.Supply, 16)
        for _, Supply in ipairs(Node.Supply) do
            SC.WriteNetworkedString(Supply.ResourceName)
            net.WriteUInt(Supply.Quantity, 32)
            net.WriteInt(Supply.Priority, 8)
        end
    end

    ---Send updates for multiple nodes
    ---@param Nodes table #A list of nodes to send updates for
    local function SendNodeUpdates(UpdatedNodes)
        if table.Count(UpdatedNodes) == 0 then return end  -- Why are you trying to do an update with 0 nodes?
        net.Start("SC.LogisticsManagerUIUpdate")
        net.WriteBool(false) -- Reset flag indicating that this is a new update and not a request
        net.WriteBool(true) -- Set flag indicating that this is a multi-node update

        net.WriteUInt(table.Count(UpdatedNodes), 16)   -- How many nodes are being updated

        for NodeID, Node in pairs(UpdatedNodes) do
            WriteNodeUpdate(Node)
        end

        net.SendToServer()
    end

    --#endregion
    --#region User Interface Element Updating
    local function UpdateLMMUINodeEntry(LineNumber)
        if not IsValid(LMFrame) then return end

        local Node = NodeMap[LineNumber]
        local NodeList = LMFrame.NodeList
        NodeList:GetLine(LineNumber):SetColumnText(1, Node.ShipName)
        NodeList:GetLine(LineNumber):SetColumnText(2, Node.Type)
        NodeList:GetLine(LineNumber):SetColumnText(3, Node.Label)
    end

    --#endregion
    --#region Heads-Up Display

    ---Draw a box around the node currently being hovered over by the player
    local function DrawNodeHoverBox()
        if not HoverNodePosition then return end
        local ScreenBoxPos = HoverNodePosition:ToScreen()
        surface.SetDrawColor(Color(0, 58, 0))   -- A dark green background shadow color
        surface.DrawOutlinedRect(ScreenBoxPos.x - 21, ScreenBoxPos.y - 21, 40, 40)
        surface.SetDrawColor(Color(0, 255, 0))  -- A bright green to highlight the node
        surface.DrawOutlinedRect(ScreenBoxPos.x - 20, ScreenBoxPos.y - 20, 40, 40)
    end

    ---Draw a box around every node that is currently selected by the player
    local function DrawNodeSelectedBox()
        if table.Count(SelectedNodes) == 0 then return end

        for NodeID, Node in pairs(SelectedNodes) do
            local ScreenBoxPos = Node.Pos:ToScreen()
            surface.SetDrawColor(Color(0, 58, 0))   -- A dark green background shadow color
            surface.DrawOutlinedRect(ScreenBoxPos.x - 21, ScreenBoxPos.y - 21, 40, 40)
            surface.SetDrawColor(Color(0, 99, 0))   -- A lighter (but still dark) green indicating which nodes are selected
            surface.DrawOutlinedRect(ScreenBoxPos.x - 20, ScreenBoxPos.y - 20, 40, 40)
        end
    end

    ---Paint the HUD
    local function LMMUIHUDPaint()
        DrawNodeSelectedBox()
        DrawNodeHoverBox()  -- Draw the node hover box last so that it renders on top of any selected node boxes
    end

    --#endregion
    --#region Misc. Local Node Table Routines

    ---Find an entry in the supply/demand table with a given name
    ---@param Tbl table #The table to search through
    ---@param Name string #The name of the entry to find
    ---@return integer EntryIndex #The index of the entry, if found
    local function FindEntryWithName(Tbl, Name)
        for EntryIndex, Entry in ipairs(Tbl) do
            if Entry.ResourceName == Name then
                return EntryIndex
            end
        end
    end

    ---Apply a preset to a node
    ---@param Node table #The node to apply the preset to
    ---@param Preset table #The preset to copy the data from
    local function ApplyPreset(Node, Preset)
        Node.Demand = {}
        Node.Supply = {}
        for ResName, Demand in pairs(Preset.Demand) do
            table.insert(Node.Demand, {
                ResourceName = ResName,
                Quantity = Demand.Quantity or 0,
                Priority = Demand.Priority or 5
            })
        end

        for ResName, Supply in pairs(Preset.Supply) do
            table.insert(Node.Supply, {
                ResourceName = ResName,
                Quantity = Supply.Quantity or 0,
                Priority = Supply.Priority or 5
            })
        end
    end

    ---Create a new preset from a node
    ---@param PresetName string #The name of the new preset
    local function CreatePreset(PresetName)
        Presets[PresetName] = {
            Demand = table.Copy(NodeEditorPanel.Demands),
            Supply = table.Copy(NodeEditorPanel.Supplies)
        }
    end

    --#endregion
    --#region Derma Frame Creation

    ---Create and display the preset window
    local function OpenPresetMenu()
        if IsValid(PresetMenu) then return end

        local SelectedPreset = nil

        -- Create the preset menu frame
        PresetMenu = vgui.Create("DFrame")
        PresetMenu:SetSize(512, 128)
        PresetMenu:Center()
        PresetMenu:SetTitle("Preset Menu")

        -- Predefining some elements
        local PresetDeleteButton
        local PresetApplyButton

        -- Upper preset management panel
        local PresetManagement = vgui.Create("DPanel", PresetMenu)
        PresetManagement:Dock(TOP)

        -- Preset selection help text
        local PresetManagementHelpText = vgui.Create("DLabel", PresetManagement)
        PresetManagementHelpText:SetText("Select Preset: ")
        PresetManagementHelpText:SizeToContents()
        PresetManagementHelpText:DockMargin(2, 0, 0, 2)
        PresetManagementHelpText:Dock(LEFT)

        -- Preset selection combo
        local PresetSelection = vgui.Create("DComboBox", PresetManagement)
        PresetSelection:SetValue("Presets")
        PresetSelection.OnSelect = function(Panel, Index, Value)
            SelectedPreset = Value
            local ValidSelection = Value and Presets[Value]
            PresetDeleteButton:SetEnabled(ValidSelection)
            PresetApplyButton:SetEnabled(ValidSelection)
        end
        for PresetName, _ in pairs(Presets) do
            PresetSelection:AddChoice(PresetName)
        end
        PresetSelection:SetWidth(256)
        PresetSelection:Dock(LEFT)

        -- Apply preset button
        PresetApplyButton = vgui.Create("DButton", PresetManagement)
        PresetApplyButton:SetText("Apply Preset")
        PresetApplyButton.DoClick = function(_)
            local Preset = Presets[SelectedPreset]
            -- Apply the preset to all selected nodes
            for NodeID, Node in pairs(SelectedNodes) do
                ApplyPreset(Node, Preset)
            end

            SendNodeUpdates(SelectedNodes)  -- Send the node updates to the server
            NodeEditorPanel:UpdateEntries() -- Update the entries in the supply+demand list
        end
        PresetApplyButton:SetEnabled(false)
        PresetApplyButton:SizeToContents()
        PresetApplyButton:Dock(RIGHT)

        -- Delete preset button
        PresetDeleteButton = vgui.Create("DButton", PresetManagement)
        PresetDeleteButton:SetText("Delete Preset")
        PresetDeleteButton.DoClick = function(_)
            Presets[SelectedPreset] = nil
            PresetSelection:Clear()
            for PresetName, _ in pairs(Presets) do
                PresetSelection:AddChoice(PresetName)
            end
        end
        PresetDeleteButton:SetEnabled(false)
        PresetDeleteButton:SizeToContents()
        PresetDeleteButton:Dock(RIGHT)

        -- Lower preset creation panel
        local PresetCreation = vgui.Create("DPanel", PresetMenu)
        PresetCreation:Dock(TOP)

        -- Preset creation help text
        local PresetHelpText = vgui.Create("DLabel", PresetCreation)
        PresetHelpText:SetText("Preset Name: ")
        PresetHelpText:SizeToContents()
        PresetHelpText:DockMargin(2, 0, 0, 2)
        PresetHelpText:Dock(LEFT)

        --Preset creation text input
        local PresetCreationName = vgui.Create("DTextEntry", PresetCreation)
        if table.Count(SelectedNodes) > 1 then
            PresetCreationName:SetPlaceholderText("Please select only one node to create a preset!")
            PresetCreationName:SetEnabled(false)
        else
            PresetCreationName:SetPlaceholderText("Type a name and press enter to create a preset")
        end

        PresetCreationName.OnEnter = function(Panel, Text)
            CreatePreset(Text)
            PresetSelection:AddChoice(Text)
        end
        PresetCreationName:Dock(FILL)

        PresetMenu:MakePopup()
    end

    --Creates and displays a new node editor window
    local function FillNodeEditorPanel(EditorPanel)
        -- Create some initial variables used internally
        EditorPanel.Demands = {}
        EditorPanel.Supplies = {}

        --#region Node Editor List Creation

        -- Create the supply+demand entry list
        local EntryList = vgui.Create("DListView", EditorPanel)
        EditorPanel.EntryList = EntryList
        local ColumnResource = EntryList:AddColumn("Resource")
        local ColumnQuantity = EntryList:AddColumn("Quantity")
        local ColumnPriority = EntryList:AddColumn("Priority")
        local ColumnType = EntryList:AddColumn("Type")

        -- Resize the columns
        ColumnQuantity:SetFixedWidth(100)
        ColumnPriority:SetFixedWidth(100)
        ColumnType:SetFixedWidth(80)


        EntryList.OnMousePressed = function(Panel, Code)
            if Code == MOUSE_RIGHT then
                if table.Count(SelectedNodes) > 0 then
                    EditorPanel:OpenEntryListMenu()
                end
            end
        end

        EntryList:SetWidth(512)
        EntryList:Dock(FILL)

        --#endregion

        --#region Node Editor Line Right-Click Functions

        ---Right-click on a line in the list
        ---@param Panel dpanel #The line that was right-clicked on
        local function OpenLineMenu(Panel)
            if IsValid(RightClickMenu) then return end

            local ResName = Panel:GetValue(1)
            local EntryType = Panel:GetValue(4)
            local EntryIsSupply = EntryType == "Supply"
            local SelectedLines = EntryList:GetSelected()

            RightClickMenu = vgui.Create( "DMenu" )

            local EditEntryOption = RightClickMenu:AddOption( "Edit " .. EntryType, function()
                EditorPanel:OpenEditLine(EntryIsSupply, ResName)
            end)

            local DeleteEntryOption = RightClickMenu:AddOption( "Delete " .. EntryType, function()
                local ModifiedNodes = {}    -- A table of nodes that were modified by this operation

                for _, Line in ipairs(SelectedLines) do
                    local LineResName = Line:GetValue(1)
                    if Line:GetValue(4) == EntryType then
                        for NodeID, Node in pairs(SelectedNodes) do
                            local NodeTable = EntryIsSupply and Node.Supply or Node.Demand  -- Get the table in the node that we are going to edit
                            local EntryIndex = FindEntryWithName(NodeTable, LineResName)
                            if EntryIndex then
                                table.insert(ModifiedNodes, Node)   -- Add the node being modified to a list of nodes that were modified by this operation
                                table.remove(NodeTable, EntryIndex)  -- Remove this resource from the table
                            end
                        end
                    end
                end

                SendNodeUpdates(ModifiedNodes)
                EditorPanel:UpdateEntries()
            end)

            local AddDemandOption = RightClickMenu:AddOption( "Add Demand", function()
                EditorPanel:OpenEditLine(false)
            end)

            local AddSupplyOption = RightClickMenu:AddOption( "Add Supply", function()
                EditorPanel:OpenEditLine(true)
            end)

            EditEntryOption:SetIcon( "icon16/database_edit.png" )
            DeleteEntryOption:SetIcon( "icon16/database_delete.png" )
            AddDemandOption:SetIcon( "icon16/database_add.png" )
            AddSupplyOption:SetIcon( "icon16/database_add.png" )

            RightClickMenu:Open()
        end

        --#endregion
        --#region Node Editor List Update

        -- Update the supply+demand entry list from the nodes
        function EditorPanel:UpdateEntries()
            self.EntryList:Clear()

            ---Update the list of demands and supplies from the list of selected nodes
            EditorPanel.Demands = {}
            EditorPanel.Supplies = {}

            for NodeID, Node in pairs(SelectedNodes) do
                -- Populate the list of demands
                for _, Demand in pairs(Node.Demand) do
                    local ExistingDemandEntry = EditorPanel.Demands[Demand.ResourceName]

                    if not ExistingDemandEntry then
                        -- If there hasn't been a resource demanded with this name yet, create an entry for it
                        EditorPanel.Demands[Demand.ResourceName] = {
                            Quantity = Demand.Quantity,
                            Priority = Demand.Priority
                        }
                    else
                        -- If it does exist, check to see if it is different
                        -- Values that are different are set to nil to indicate "multiple values"

                        -- Check for differing quantity
                        if Demand.Quantity ~= ExistingDemandEntry.Quantity then
                            ExistingDemandEntry.Quantity = nil
                        end
                        -- Check for differing priority
                        if Demand.Priority ~= ExistingDemandEntry.Priority then
                            ExistingDemandEntry.Priority = nil
                        end
                    end
                end

                -- Populate the list of supplies
                for _, Supply in pairs(Node.Supply) do
                    local ExistingSupplyEntry = EditorPanel.Supplies[Supply.ResourceName]

                    if not ExistingSupplyEntry then
                        -- If there hasn't been a resource demanded with this name yet, create an entry for it
                        EditorPanel.Supplies[Supply.ResourceName] = {
                            Quantity = Supply.Quantity,
                            Priority = Supply.Priority
                        }
                    else
                        -- If it does exist, check to see if it is different
                        -- Values that are different are set to nil to indicate "multiple values"

                        -- Check for differing quantity
                        if Supply.Quantity ~= ExistingSupplyEntry.Quantity then
                        ExistingSupplyEntry.Quantity = nil
                        end
                        -- Check for differing priority
                        if Supply.Priority ~= ExistingSupplyEntry.Priority then
                        ExistingSupplyEntry.Priority = nil
                        end
                    end
                end
            end

            -- Now create the actual demand lines in the list
            for ResourceName, Data in pairs(EditorPanel.Demands) do
                local Quantity = Data.Quantity or "..." -- If a field is nil, replace it with the string "Multiple Values"
                if Quantity == 0 then Quantity = "Unlimited" end    -- A quantity of 0 indicates "unlimited" demand

                local Priority = Data.Priority or "..." -- Do the same thing with priority

                -- Create the line
                local NewLine = self.EntryList:AddLine(ResourceName, Quantity, Priority, "Demand")
                NewLine.OnRightClick = OpenLineMenu
                NewLine:SetTooltip(ResourceName)
            end

            -- Do the same for the supplies
            for ResourceName, Data in pairs(EditorPanel.Supplies) do
                local Quantity = Data.Quantity or "..." -- If a field is nil, replace it with the string "Multiple Values"
                if Quantity == 0 then Quantity = "Unlimited" end    -- A quantity of 0 indicates "unlimited" supply quantity

                local Priority = Data.Priority or "..." -- Do the same thing with priority

                -- Create the line
                local NewLine = self.EntryList:AddLine(ResourceName, Quantity, Priority, "Supply")
                NewLine.OnRightClick = OpenLineMenu
                NewLine:SetTooltip(ResourceName)
            end
        end
        --#endregion

        ---Open up the window used to add or edit a supply or demand
        ---@param DoSupply boolean #If true, a supply table is to be edited
        ---@param ResourceToEdit string #The name of the resource to edit. If nil, indicates that a new entry is to be created.
        function EditorPanel:OpenEditLine(DoSupply, ResourceToEdit)
            if IsValid(EditResourceMenu) then return end    -- Return if the user is already editing a line

            local EditType = DoSupply and "Supply" or "Demand"

            local ResourceNames = {[ResourceToEdit or "Hydrogen"] = true}
            local ResourceQuantity = 0
            local ResourcePriority = 5

            local ResourceTable = DoSupply and EditorPanel.Supplies or EditorPanel.Demands

            -- Identify which entry to modify if we're modifying an entry
            if ResourceToEdit then
                local Entry = ResourceTable[ResourceToEdit]
                if Entry then
                    -- If the value of a field is nil, load a defaul value
                    ResourceQuantity = Entry.Quantity or 0
                    ResourcePriority = Entry.Priority or 5
                end
            end


            -- Create the EditResourceMenu frame
            EditResourceMenu = vgui.Create("DFrame")
            EditResourceMenu:Center()
            EditResourceMenu:SetSize(320, 256)
            if ResourceToEdit then
                EditResourceMenu:SetTitle("Editing " .. EditType)
            else
                EditResourceMenu:SetTitle("Add A " .. EditType)
            end
            EditResourceMenu:MakePopup()

            -- Create the panels that will house the inputs
            local PanelResourceName = vgui.Create("DPanel", EditResourceMenu)
            PanelResourceName:DockPadding(2, 2, 2, 2)
            PanelResourceName:Dock(TOP)

            local PanelResourceQuantity = vgui.Create("DPanel", EditResourceMenu)
            PanelResourceQuantity:DockPadding(2, 2, 2, 2)
            PanelResourceQuantity:Dock(TOP)

            local PanelResourceUnlimited = vgui.Create("DPanel", EditResourceMenu)
            PanelResourceUnlimited:DockPadding(2, 2, 2, 2)
            PanelResourceUnlimited:Dock(TOP)

            local PanelResourcePriority = vgui.Create("DPanel", EditResourceMenu)
            PanelResourcePriority:DockPadding(2, 2, 2, 2)
            PanelResourcePriority:Dock(TOP)

            local PanelButtons = vgui.Create("DPanel", EditResourceMenu)
            PanelButtons:SetPaintBackground(false)
            PanelButtons:DockPadding(2, 2, 2, 2)
            PanelButtons:Dock(TOP)

            -- Populate the resource name panel
            local ResourceNameText = vgui.Create("DLabel", PanelResourceName)
            ResourceNameText:SetText("Resource Name")
            ResourceNameText:SizeToContents()
            ResourceNameText:Dock(LEFT)

            local ResourceNameButton = vgui.Create("DButton", PanelResourceName)
            ResourceNameButton:SetWidth(128)
            ResourceNameButton.DoClick = function()
                -- Get the list of resource names
                local ResourcesToDisplay = {}
                local ResourcesSelected = {}
                for ResName, _ in pairs(GAMEMODE:GetResourceTypes()) do
                    table.insert(ResourcesToDisplay, ResName)  -- Add the name of this resource to the list of resources to show
                    -- If this resource is selected, add its index to the list of lines to select
                    if ResourceNames[ResName] then
                        table.insert(ResourcesSelected, #ResourcesToDisplay)
                    end
                end
                -- Create the derma elements for the resource selection pop-up menu
                local ResourceSelectionFrame = vgui.Create("DFrame")
                ResourceSelectionFrame:SetSize(256,512)
                ResourceSelectionFrame:MakePopup()
                ResourceSelectionFrame:Center()

                local AcceptButton = vgui.Create("DButton", ResourceSelectionFrame)
                AcceptButton:SetText("Accept")
                AcceptButton:Dock(BOTTOM)

                local Search = vgui.Create("SearchPanel", ResourceSelectionFrame)
                Search:Dock(FILL)

                Search:SetDataVGUIElement("SearchElement_Resource")
                Search:SetData(ResourcesToDisplay)
                Search:SetFilters(ResourceFilters)
                Search:SetMultiselect(true)
                Search:SetSelectedElements(ResourcesSelected)

                AcceptButton.DoClick = function()
                    local SelectedData = Search:GetSelectedData()

                    if #SelectedData == 1 then
                        local SelectedResource = SelectedData[1]
                        ResourceNames = {[SelectedResource] = true}

                        -- Update the button's text for a single item selected
                        ResourceNameButton:SetText(SelectedResource)
                        ResourceNameButton:SetTooltip(SelectedResource)
                    elseif #SelectedData > 1 then
                        -- Populate the table of resource names that are selected
                        ResourceNames = {}
                        for _, ResName in ipairs(Search:GetSelectedData()) do
                            ResourceNames[ResName] = true
                        end

                        -- Update the button's text for multiple items selected
                        ResourceNameButton:SetText(string.format("%d Items", #SelectedData))
                        ResourceNameButton:SetTooltip(table.concat(SelectedData, "\n"))
                    end

                    -- If nothing was selected (how?), fall through to here and just close the menu with no changes

                    ResourceSelectionFrame:Close()
                end
            end

            ResourceNameButton:SetText(ResourceToEdit or "Hydrogen")
            ResourceNameButton:SetTooltip(ResourceToEdit or "Hydrogen")
            ResourceNameButton:Dock(RIGHT)

            -- Populate the reosurce quantity panel
            local ResourceQuantityText = vgui.Create("DLabel", PanelResourceQuantity)
            ResourceQuantityText:SetText("Resource Quantity")
            ResourceQuantityText:SizeToContents()
            ResourceQuantityText:Dock(LEFT)

            local ResourceQuantityInput = vgui.Create("DNumberWang", PanelResourceQuantity)
            ResourceQuantityInput:SetWidth(128)
            ResourceQuantityInput:SetMin(0)
            ResourceQuantityInput:SetMax(4294967296)
            ResourceQuantityInput:SetDecimals(0)
            ResourceQuantityInput:SetValue(ResourceQuantity)
            ResourceQuantityInput.OnValueChanged = function(Panel, Value)
                ResourceQuantity = Value
            end
            ResourceQuantityInput:Dock(RIGHT)

            -- Populate the resource unlimited panel
            local ResourceUnlimitedText = vgui.Create("DLabel", PanelResourceUnlimited)
            ResourceUnlimitedText:SetText("Unlimited " .. EditType)
            ResourceUnlimitedText:SizeToContents()
            ResourceUnlimitedText:Dock(LEFT)

            local ResourceUnlimitedInput = vgui.Create("DCheckBox", PanelResourceUnlimited)
            ResourceUnlimitedInput:SetSize(20,20)
            ResourceUnlimitedInput.OnChange = function(Panel, State)
                if State then
                    ResourceQuantity = 0
                    ResourceQuantityInput:SetEnabled(false)
                else
                    ResourceQuantityInput:SetEnabled(true)
                    ResourceQuantity = ResourceQuantityInput:GetValue()
                end
            end
            ResourceUnlimitedInput:SetChecked(ResourceQuantity == 0)
            ResourceUnlimitedInput:OnChange(ResourceQuantity == 0)
            ResourceUnlimitedInput:Dock(RIGHT)

            -- Populate the resource priority panel
            local ResourcePriorityText = vgui.Create("DLabel", PanelResourcePriority)
            ResourcePriorityText:SetText(EditType .. " Priority")
            ResourcePriorityText:SizeToContents()
            ResourcePriorityText:Dock(LEFT)

            local ResourcePriorityInput = vgui.Create("DNumberWang", PanelResourcePriority)
            ResourcePriorityInput:SetWidth(128)
            ResourcePriorityInput:SetMin(-20)
            ResourcePriorityInput:SetMax(20)
            ResourcePriorityInput:SetDecimals(0)
            ResourcePriorityInput:SetValue(ResourcePriority)
            ResourcePriorityInput.OnValueChanged = function(Panel, Value)
                ResourcePriority = Value
            end
            ResourcePriorityInput:Dock(RIGHT)

            -- Button
            local AcceptButton = vgui.Create("DButton", PanelButtons)
            AcceptButton:Dock(RIGHT)
            AcceptButton:SetText("Accept")
            AcceptButton.DoClick = function()
                -- Apply the new data to the selected nodes
                for NodeID, Node in pairs(SelectedNodes) do
                    local NodeResourceTable = DoSupply and Node.Supply or Node.Demand
                    for ResourceName, _ in pairs(ResourceNames) do
                        local EntryIndex = FindEntryWithName(NodeResourceTable, ResourceName)
                        if EntryIndex then
                            local Entry = NodeResourceTable[EntryIndex]
                            Entry.Quantity = ResourceQuantity
                            Entry.Priority = ResourcePriority
                        else
                            table.insert(NodeResourceTable, {
                                ResourceName = ResourceName,
                                Quantity = ResourceQuantity,
                                Priority = ResourcePriority
                            })
                        end
                    end
                end

                -- Network the changes to the server
                SendNodeUpdates(SelectedNodes)

                -- Refresh the list of entries
                self:UpdateEntries()

                -- Close the menu
                EditResourceMenu:Close()
            end
        end

        --#region Node Editor List Body Right-Click Functions

        ---Open the demand list right-click menu
        function EditorPanel:OpenEntryListMenu()
            if IsValid(RightClickMenu) then return end

            RightClickMenu = vgui.Create( "DMenu" )

            local AddDemandOption = RightClickMenu:AddOption( "Add Demand", function()
                self:OpenEditLine(false)
            end)

            local AddSupplyOption = RightClickMenu:AddOption( "Add Supply", function()
                self:OpenEditLine(true)
            end)

            AddDemandOption:SetIcon( "icon16/database_add.png" )
            AddSupplyOption:SetIcon( "icon16/database_add.png" )

            RightClickMenu:Open()
        end

        --#endregion
        --#region Node Editor Final Init

        EditorPanel:UpdateEntries()

        --#endregion
    end

    ---Create and display the logistics management window
    local function OpenLMMUI()
        if not IsValid(LMFrame) then
            -- Reset frame variables
            HoverBoxPos = nil
            SelectedNodes = {}

            -- Create the window frame
            LMFrame = vgui.Create("DFrame")
            --LMFrame:SetSize(768,256)
            LMFrame:SetSize(768,256)
            LMFrame:Center()
            LMFrame:SetTitle("Logistics Management")
            LMFrame:MakePopup()

            --#region Node Selection Panel

            --#region Tools Panel
            -- Create the tools panel
            local ToolsPanel = vgui.Create("DPanel", LMFrame)
            ToolsPanel:SetPaintBackground(false)
            ToolsPanel:Dock(TOP)

            -- Left-side help text
            local HelpText = vgui.Create("DLabel", ToolsPanel)
            HelpText:SetText("Hold ctrl to select multiple nodes")
            HelpText:SizeToContents()
            HelpText:Dock(LEFT)

            -- Node name change help text
            local NameHelp = vgui.Create("DLabel", ToolsPanel)
            NameHelp:SetText("Change Node Name: ")
            NameHelp:SizeToContents()
            NameHelp:DockMargin(100, 0, 0, 2)
            NameHelp:Dock(LEFT)

            -- Node name change name entry box
            local EditableName = vgui.Create("DTextEntry", ToolsPanel)
            ToolsPanel.EditableName = EditableName
            EditableName:SetValue("No nodes selected")
            EditableName.OnEnter = function(Panel, Text)
                for NodeIndex, Node in pairs(SelectedNodes) do
                    Node.Label = Text
                end

                SendNodeUpdates(SelectedNodes)
            end
            EditableName:SetEnabled(false)
            EditableName:SetWidth(128)
            EditableName:Dock(LEFT)

            LMFrame.EditableName = EditableName

            -- Presets menu button
            local ButtonPresets = vgui.Create("DButton", ToolsPanel)
            ToolsPanel.ButtonPresets = ButtonPresets
            ButtonPresets:SetText("Presets")
            ButtonPresets.DoClick = function(_)
                OpenPresetMenu()
            end
            ButtonPresets:SetEnabled(false)
            ButtonPresets:SizeToContents()
            ButtonPresets:Dock(RIGHT)

            --#endregion

            -- Create the node selection panel
            local NodeSelectionPanel = vgui.Create("DPanel", LMFrame)
            NodeSelectionPanel:SetSize(256,256)
            NodeSelectionPanel:SetPaintBackground(false)
            NodeSelectionPanel:Dock(FILL)

            -- Create the node list
            local NodeList = vgui.Create("DListView", NodeSelectionPanel)
            LMFrame.NodeList = NodeList
            NodeList:AddColumn("Ship")
            NodeList:AddColumn("Label")
            NodeList:Dock(FILL)

            NodeList.OnRowSelected = function(Panel, RowIndex, Row)
                -- Enable all of the elements if no nodes were previously selected
                if table.Count(SelectedNodes) == 0 then
                    -- Show and enable the top tools
                    ButtonPresets:SetEnabled(true)  -- Enable the preset menu button
                    EditableName:SetEnabled(true)   -- Enable the node name change text input
                end

                SelectedNodes = {}  -- Clear the selected node reference table
                local SelectedNodeCount = 1
                local Node = NodeMap[RowIndex]
                SelectedNodes[Node.NodeID] = Node

                for _, Line in ipairs(NodeList:GetSelected()) do
                    local LineID = Line:GetID()
                    if LineID ~= RowIndex then
                        Node = NodeMap[LineID] -- Get the node that was selected

                        SelectedNodes[Node.NodeID] = Node
                        SelectedNodeCount = SelectedNodeCount + 1
                    end
                end

                if SelectedNodeCount == 1 then
                    LMFrame.EditableName:SetValue(Node.Label)
                else
                    LMFrame.EditableName:SetValue("Multiple Nodes")
                end

                NodeEditorPanel:UpdateEntries()
            end

            --#endregion
            --#region Node Editor Panel

            -- Create the node editor panel
            NodeEditorPanel = vgui.Create("DPanel", LMFrame)
            NodeEditorPanel:SetSize(512,256)
            NodeEditorPanel:SetPaintBackground(false)
            NodeEditorPanel:Dock(RIGHT)

            -- Fill the panel in
            FillNodeEditorPanel(NodeEditorPanel)

            --#endregion

            --HUD Painting hook
            hook.Add("HUDPaint", "SC.LogisticsManager.UI.HUDPaint", LMMUIHUDPaint)

            --Wrap things up if the manager is closed
            LMFrame.OnClose = function(Frame)
                hook.Remove("HUDPaint", "SC.LogisticsManager.UI.HUDPaint")

                NodeEditorPanel = nil
            end

            SendRequestForLMMUIUpdate()
        end
    end

    ---Populates the node list with node entries
    local function CreateLMMUINodeList()
        NodeMap = {}    -- Clear the node map
        local OldSelectedNodes = SelectedNodes    -- Save the previous list of selected nodes to dereference any old/removed nodes
        SelectedNodes = {}  -- Clear the list of selected nodes
        local NodeList = LMFrame.NodeList
        for NodeIndex, Node in pairs(Nodes) do
            local NewLine = NodeList:AddLine(Node.ShipName, Node.Label)
            local LineID = NewLine:GetID()

            -- Select the line if it was selected previously
            if OldSelectedNodes[NodeIndex] then
                NewLine:SetSelected(true)
                SelectedNodes[NodeIndex] = Node
            end

            --Populate the node map
            NodeMap[LineID] = Node

            -- Handle mouse hover stuff
            NewLine.OnCursorEntered = function(Panel)
                HoverNodePosition = Node.Pos
            end

            NewLine.OnCursorExited = function(panel)
                HoverNodePosition = nil
            end
        end
    end

    --#endregion
    --#region Client-Side Receive Networking

    --Receive an update to the Logistics Manager Management UI from the server
    local function ReceiveLMMUIUpdate()
        Nodes = {}
        local NodeCount = net.ReadUInt(16)
        if NodeCount == 0 then return end

        for LineIndex = 1, NodeCount do
            Nodes[LineIndex] = {}
            local NewNode = Nodes[LineIndex]

            NewNode.NodeID = net.ReadUInt(16)
            --Populate some general node data
            NewNode.Type = SC.ReadNetworkedString("", function(ID, String)
                NewNode.Type = String
                UpdateLMMUINodeEntry(LineIndex)
            end)
            NewNode.Label = SC.ReadNetworkedString("", function(ID, String)
                NewNode.Label = String
                UpdateLMMUINodeEntry(LineIndex)
            end)
            NewNode.ShipName = SC.ReadNetworkedString("", function(ID, String)
                NewNode.ShipName = String
                UpdateLMMUINodeEntry(LineIndex)
            end)

            NewNode.Pos = net.ReadVector()

            --Populate the demand table
            NewNode.Demand = {}
            local DemandCount = net.ReadUInt(16)
            if DemandCount > 0 then
                for DemandIndex = 1, DemandCount do
                    NewNode.Demand[DemandIndex] = {}

                    local ResName = SC.ReadNetworkedString("", function(ID, String)
                        NewNode.Demand[DemandIndex].ResourceName = String
                        UpdateLMMUINodeEntry(LineIndex)
                    end)

                    NewNode.Demand[DemandIndex].ResourceName = ResName
                    NewNode.Demand[DemandIndex].Quantity = net.ReadUInt(32)
                    NewNode.Demand[DemandIndex].Priority = net.ReadInt(8)
                end
            end

            --Populate the supply table
            NewNode.Supply = {}
            local SupplyCount = net.ReadUInt(16)
            if SupplyCount > 0 then
                for SupplyIndex = 1, SupplyCount do
                    NewNode.Supply[SupplyIndex] = {}

                    local ResName = SC.ReadNetworkedString("", function(ID, String)
                        NewNode.Supply[SupplyIndex].ResourceName = String
                        UpdateLMMUINodeEntry(LineIndex)
                    end)

                    NewNode.Supply[SupplyIndex].ResourceName = ResName
                    NewNode.Supply[SupplyIndex].Quantity = net.ReadUInt(32)
                    NewNode.Supply[SupplyIndex].Priority = net.ReadInt(8)
                end
            end
        end

        CreateLMMUINodeList()
    end

    net.Receive("SC.OpenLogisticsManagerUI", function(Len)
        OpenLMMUI()
    end)

    net.Receive("SC.LogisticsManagerUIUpdate", function(Len)
        ReceiveLMMUIUpdate()
    end)

    --#endregion
end
--#endregion