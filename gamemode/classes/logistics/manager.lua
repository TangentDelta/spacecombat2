local GM = GM

include("user_interface.lua")

--Reload the global logistics manager if it exists
hook.Remove("OnReloaded", "SC.LogisticsManager.OnReloaded")
hook.Add("OnReloaded", "SC.LogisticsManager.OnReloaded", function()
    local CurrentManager = GAMEMODE.CurrentLogisticsManager

    if IsValid(CurrentManager) then
        local OldNodes = CurrentManager:GetNodes()
        GAMEMODE.CurrentLogisticsManager = nil

        local NewManager = GM:GetLogisticsManager()

        for _,Node in pairs(OldNodes) do
            NewManager:RegisterNode(Node)
        end
    end
end)

--Returns the gamemode's global logistics manager
--The manager is created if it doesn't exist yet
function GM:GetLogisticsManager()
    if not IsValid(GAMEMODE.CurrentLogisticsManager) then
        GAMEMODE.CurrentLogisticsManager = GAMEMODE.class.getClass("LogisticsManager"):new()
    end

    return GAMEMODE.CurrentLogisticsManager
end



--[[
======================
    Start of Class
======================
]]

local C = GM.LCS.class({
    Nodes = {},
    CourierVisitations = {},   --A table of nodes visited by a courier. Used for round-robin distribution
    NextNodeID = 1,
    NextThinkTime = 0
})

function C:init()

end

function C:IsValid()
    return true
end


--[[
=======================
    Node Management
=======================
]]
function C:GetNextAvailableNodeID()
    local I = 1
    while true do
        if not self.Nodes[I] then return I end
        I = I + 1
    end
end

function C:RegisterNode(Node)
    Node:SetID(self.NextNodeID)
    self.Nodes[self.NextNodeID] = Node
    self.CourierVisitations[self.NextNodeID] = {}
    self.NextNodeID = self:GetNextAvailableNodeID()
end

function C:UnregisterNode(Node)
    self.NextNodeID = Node:GetID()
    Node:SetID(0)
    self.Nodes[self.NextNodeID] = nil
    self.CourierVisitations[self.NextNodeID]  = nil
end

function C:GetNodes()
    return self.Nodes
end

--Returns a list of nodes that are owned by a player
--TODO: Add nodes that belong to the player's faction, when we get around to that
function C:GetNodesForPlayer(Ply)
    local Nodes = {}
    for NodeID, Node in pairs(self.Nodes) do
        if Node:GetOwner() == Ply then
            Nodes[NodeID] = Node
        end
    end

    return Nodes
end

--Returns a list of sets of nodes that can interconnect with eachother
--TODO: Needs to be extended to support player-to-player connections and connections between different node types
function C:GetNodeGroups()
    local NodeGroups = {}

    for NodeID, Node in pairs(self.Nodes) do
        if IsValid(Node) then   --Don't process tasks for nodes that have embraced the void
            local NodeOwner = Node:GetOwner()
            local GroupKey = tostring(NodeOwner:EntIndex()) .. Node:GetType()
            if not NodeGroups[GroupKey] then
                NodeGroups[GroupKey] = {}
            end

            table.insert(NodeGroups[GroupKey], Node)
        end
    end

    return NodeGroups
end

--[[
=======================
    Logistics Logic
=======================
]]

--Return with a table of resources that the suppliers in this group can supply
--This should probably get added to a monolithic processing loop
function C:GetSuppliableResourceList(Group)
    local SuppliableResources = {}
    for NodeID, Node in pairs(Group) do
        local Resources = Node:GetAvailableResources()
        for ResName, _ in pairs(Resources) do
            SuppliableResources[ResName] = true
        end
    end

    return SuppliableResources
end

--Attempt to dispatch a task to Node, using NodeGroup as the sample set
function C:DispatchTasksToCourierNodeFromGroup(CourierNode, NodeGroup)
    local NodeResources = CourierNode:GetAvailableResources()  --A table of resources that this node has available to supply
    local PriorityDemandTable = {}  --A table of demands levels, and each node with its demands in it
    local HighestPriority = 0   --The highest priority demand that we encountered while building the PriorityDemandTable table
    local CourierID = CourierNode:GetID()
    local CourierVisitation = self.CourierVisitations[CourierID]

    --Go through every single node in the group
    for _, Node in pairs(NodeGroup) do
        --Go through the node's demands
        for ResName,Demand in pairs(Node:GetRequiredResources()) do
            --Can we actually supply this demand?
            if NodeResources[ResName] then
                --Pull some data from the demand
                local DemandPriority = Demand.Priority  --The demand's priority

                local FulfillDemand = true     --A flag indicating that we need to fulfill this demand
                --For now, this flag is always set. When more logic is added, this will be used for more complex behaviors

                --Does this demand need to get fulfilled?
                if FulfillDemand then
                    local NodeID = Node:GetID()
                    --Update the highest priority
                    if DemandPriority > HighestPriority then HighestPriority = DemandPriority end
                    --Create some table entries if they do not exist yet
                    if not PriorityDemandTable[DemandPriority] then PriorityDemandTable[DemandPriority] = {} end
                    if not PriorityDemandTable[DemandPriority][ResName] then PriorityDemandTable[DemandPriority][ResName] = {} end
                    --Add this demand to the list of demands that need to be fulfilled
                    PriorityDemandTable[DemandPriority][ResName][NodeID] = Demand.Quantity
                    --Add entries to the courier visitation table if it has none yet
                    if not CourierVisitation[ResName] then CourierVisitation[ResName] = {} end
                end
            end
        end
    end

    --We now have a list of nodes that have unmet demands, sorted by priority.
    --The higher the priority number, the higher the priority of the demand.

    --Now work throug the list of demands and try to find one that we can fulfill
    for I = HighestPriority,0,-1 do
        --Are there demands at this level? Don't want to iterate a nil table entry...
        local DemandTable = PriorityDemandTable[I]
        if DemandTable then
            for ResName, ResourceDemands in pairs(DemandTable) do
                local CourierResourceVisitation = CourierVisitation[ResName]
                local DoProcessNodeDemands = true
                local LoopEscape = 100
                while DoProcessNodeDemands and (LoopEscape > 0) do
                    LoopEscape = LoopEscape - 1
                    DoProcessNodeDemands = false
                    local DoResetCourierVisitation = true
                    for NodeID, DemandQuantity in pairs(ResourceDemands) do
                        local DemandNode = self.Nodes[NodeID]

                        local QuantityFromSupply = CourierNode:GetAvailableResource(ResName)
                        local QuantityCarryable = CourierNode:GetMaxCarryableAmount(ResName)
                        local QuantityRequired = DemandNode:GetRequiredResource(ResName)
                        local QuantityTransferrable = math.min(QuantityFromSupply, QuantityCarryable, QuantityRequired)
                        if QuantityTransferrable > 0 then
                            if not CourierResourceVisitation[NodeID] then
                                CourierResourceVisitation[NodeID] = true

                                CourierNode:AddOutbound(ResName, QuantityTransferrable)
                                DemandNode:AddInbound(ResName, QuantityTransferrable)

                                CourierNode:AddCourierTask(CourierNode, DemandNode, ResName, QuantityTransferrable)

                                if not CourierNode:GetCourierIsAvailable() then return end  --Courier can't accept any more tasks, we're done.
                            end

                            DoProcessNodeDemands = true
                        else
                            DoResetCourierVisitation = false
                        end
                    end

                    if DoResetCourierVisitation then
                        for NodeID, _ in pairs(CourierResourceVisitation) do
                            CourierResourceVisitation[NodeID] = false
                        end
                    end
                end

                if LoopEscape <= 0 then
                    SC.Error("Warning: Infinite loop in logistics demand processing loop!", 4)
                    SC.Error(string.format("Courier: %d Resource: %s", CourierID, ResName), 4)
                    for NodeID, VisitationState in pairs(CourierResourceVisitation) do
                        SC.Error(string.format("%d: %s", NodeID, VisitationState), 4)
                    end
                end
            end
        end
    end
end

function C:DispatchLogisticsTasks()
    for NodeID, Node in pairs(self.Nodes) do
        if IsValid(Node) then
            if Node:GetIsCourier() then
                if Node:GetCourierIsAvailable() then    --Don't go any further if the courier isn't available
                    local NodeGroup = Node:GetCompatibleNodes()
                    --The node itself is a courier, use it as the origin point for logistics calculations
                    self:DispatchTasksToCourierNodeFromGroup(Node, NodeGroup)
                end
            else
                --Process things here for nodes that are not couriers
                --Eventually, have couriers dispatched to these nodes
            end
        end
    end
end


function C:Think()
    if CurTime() > self.NextThinkTime then
        self:DispatchLogisticsTasks()   --Dispatch tasks to all node groups
        self.NextThinkTime = CurTime() + 1
    end
end

if SERVER then
    hook.Remove("Think", "SC.LogisticsManager.Think")
    hook.Add("Think", "SC.LogisticsManager.Think", function()
        local Manager = GM:GetLogisticsManager()
        if IsValid(Manager) then
            Manager:Think()
        end
    end)
end

GM.class.registerClass("LogisticsManager", C)