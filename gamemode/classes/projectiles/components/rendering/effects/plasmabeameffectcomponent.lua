local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    Length = 5000,
    CurLength = 0,
    Velocity = -6000
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("PlasmaBeamEffectComponent")

local bGlow1
local bGlow2
local bGlow3
local lGlow2
local lGlow3

if CLIENT then
    bGlow1 = GM.MaterialFromVMT(
        "sc_blue_ball01",
        [["UnLitGeneric"
        {
            "$basetexture"		"sprites/bluelight1"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
   )
    bGlow2 = GM.MaterialFromVMT(
        "sc_blue_ball02",
        [["UnLitGeneric"
        {
            "$basetexture"		"effects/blueflare1"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
   )
    bGlow3 = GM.MaterialFromVMT(
        "sc_blue_ball03",
        [["UnLitGeneric"
        {
            "$basetexture"		"effects/blueflare1"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
   )
    lGlow2 = GM.MaterialFromVMT(
        "sc_blue_beam02",
        [["UnLitGeneric"
        {
            "$basetexture"		"sprites/laserbeam"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
   )
    lGlow3 = GM.MaterialFromVMT(
        "sc_blue_beam03",
        [["UnLitGeneric"
        {
            "$basetexture"		"sprites/laserbeam"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
   )
end

function C:init(Parent, NewColor)
	BaseClass.init(self, Parent)

    self:SetColor(NewColor or Color(24, 108, 240, 255))
end


function C:SetScale(NewScale)
    BaseClass.SetScale(self, NewScale)
    self.Length = NewScale * 5000
end

function C:Think()
        --self.CurTrailLength = math.Clamp(self.CurTrailLength + (self.Velocity * FrameTime()), 0, self.TrailLength)
        self.CurLength = math.Clamp(self.CurLength + (-self.Velocity * FrameTime()), 0, self.Length)
end

function C:Draw()
    local Parent = self:GetParent()
	local Start = math.random(750,1000) * self:GetScale()
	local End = math.random(750,1000) * self:GetScale()
	render.SetMaterial(bGlow1)
    render.DrawBeam((Parent:GetPos() - Parent:GetAngles():Forward()*self.CurLength), Parent:GetPos(), 300 * self:GetScale(), ((2*CurTime())-(0.0005*self:GetScale())), 2*CurTime(), self:GetColor())

	render.SetMaterial(bGlow2)
    render.DrawSprite(Parent:GetPos(), Start, Start, self:GetColor())

	render.SetMaterial(bGlow3)
    render.DrawSprite(Parent:GetPos(), Start/2, Start/2, Color(255, 255, 255, 255))

	render.SetMaterial(bGlow2)
    render.DrawSprite((Parent:GetPos() - Parent:GetAngles():Forward()*self.CurLength), End, End, self:GetColor())

    render.SetMaterial(bGlow3)
    render.DrawSprite((Parent:GetPos() - Parent:GetAngles():Forward()*self.CurLength), End/2, End/2, Color(255, 255, 255, 255))

    render.SetMaterial(lGlow2)
    render.DrawBeam((Parent:GetPos() - Parent:GetAngles():Forward()*self.CurLength), Parent:GetPos(), 188 * self:GetScale(), ((2*CurTime())-(0.001*self:GetScale())), 2*CurTime(), self:GetColor())

    render.SetMaterial(lGlow3)
    render.DrawBeam((Parent:GetPos() - Parent:GetAngles():Forward()*self.CurLength), Parent:GetPos(), 70 * self:GetScale(), ((2*CurTime())-(0.001*self:GetScale())), 2*CurTime(), Color(255, 255, 255, 255))

    render.SetMaterial(lGlow3)
    render.DrawBeam((Parent:GetPos() - Parent:GetAngles():Forward()*self.CurLength), Parent:GetPos(), 128 * self:GetScale(), ((2*CurTime())-(0.001*self:GetScale())), 2*CurTime(), Color(255, 255, 255, 155))
end

function C:GetComponentClass()
    return "PlasmaBeamEffectComponent"
end

GM.class.registerClass("PlasmaBeamEffectComponent", C)