local C = GM.class.getClass("ProjectileComponent"):extends({
    Type = "FAIL",
    Amount = 0
})

local BaseClass = C:getClass()

function C:init(Type, Amount)
    self.Type = Type or "Shield"
    self.Amount = Amount or 0

    BaseClass.init(self)
end

function C:ApplyBoost(Target, HitData)
    if IsValid(Target) then
        if self.Type == "Shield" then
            Target:SupplyShield(self.Amount)
        elseif self.Type == "Armor" then
            Target:SupplyArmor(self.Amount)
        elseif self.Type == "Hull" then
            Target:SupplyHull(self.Amount)
        elseif self.Type == "Capacitor" then
            Target:SupplyCap(self.Amount)
        end
    end
end

function C:OnProjectileEvent(Event, Info)
    if Event == "Collision" then
        self:ApplyBoost(Info.HitEntity, Info)
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return false
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "RemoteBoosterComponent"
end

GM.class.registerClass("RemoteBoosterComponent", C)