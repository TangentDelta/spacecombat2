local C = GM.class.getClass("DamageComponent"):extends({
    Triggered = false,
    DamageTicks = 10
})

local BaseClass = C:getClass()

function C:init(DamageTicks, Damage)
    self.DamageTicks = DamageTicks
    BaseClass.init(self, Damage)
end

function C:DealDamage(Target, HitData)
    local DamagePerTick = self:GetDamage()
    local Owner = self:GetParent():GetOwner()
    timer.Create("SC.DoTComponentDamage"..tostring(self:GetParent():GetID()), 1, self.DamageTicks, function()
        if IsValid(Target) then
            Target:ApplyDamage(DamagePerTick, Owner, nil, HitData)
        end
    end)
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)

    Data.DamageTicks = self.DamageTicks

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.DamageTicks = Data.DamageTicks or 10
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "DoTComponent"
end

GM.class.registerClass("DoTComponent", C)