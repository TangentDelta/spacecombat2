local GM = GM

--Enums
local STATE_IDLE = 0
local STATE_MOVETODEMAND = 1
local STATE_MOVETOSUPPLY = 2
local STATE_RETURNHOME = 3

local C = GM.class.getClass("DroneControlComponent"):extends({
    Task = nil,
    State = STATE_IDLE,
    StorageSize = 1000,
    Range = 5000,
    Storage = nil
})

local BaseClass = C:getClass()
local ZeroVector = Vector(0, 0, 0)
local ZeroAngle = Angle(0, 0, 0)

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("LogisticsDroneControlComponent")

function C:init()
    BaseClass.init(self)
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "LogisticsDroneControlComponent"
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.StorageSize = self.StorageSize
    Data.Range = self.Range
    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    self.StorageSize = Data.StorageSize or 0
    self.Range = Data.Range or 0
end

--Stop constructing the class at this point if on client
if CLIENT then
    GM.class.registerClass("LogisticsDroneControlComponent", C)
    return
end

--[[
===================================
    Behavior Callback Overrides
===================================
]]

--Called every time we arrive at the target
function C:OnArrivedAtTarget()
    if not self.Task then return end    --Why are we here with no task???

    local SupplyNode = self.Task.SupplyNode
    local DemandNode = self.Task.DemandNode

    if self.State == STATE_MOVETOSUPPLY then
        --Arrived at supplier

        local PullSuccessful = self:PullFromSupply()
        if PullSuccessful then
            --Fly off to the demand node
            self:SetTarget(DemandNode)
            self.State = STATE_MOVETODEMAND
        else
            --The logistics manager was a jerk and sent us to a node with no suppliable resource
            --Go home and sulk

            self:SetTargetToHome()
            self.State = STATE_RETURNHOME
        end

    elseif self.State == STATE_MOVETODEMAND then
        --Arrived at demander

        local PushSuccessful = self:PushToDemand()

        if PushSuccessful then
            --Fly back home
            self:SetTargetToHome()
            self.State = STATE_RETURNHOME
        else
            --The demander somehow ended up with more of the resource than we were sent to supply to it
            if DemandNode ~= SupplyNode then
                --Create a new task for ourself to return the resource to the supplying node and return whatever we took
                self.Task.DemandNode = SupplyNode
                self:SetTarget(DemandNode)
            else
                --We already tried returning the resource and it didn't fit!
                --Go home and dump the resource into the void
                self:SetTargetToHome()
                self.State = STATE_RETURNHOME
            end
        end
    elseif self.State == STATE_RETURNHOME then
        --Arrived at home
        --This is handled in the OnArrivedAtHome callback
    end
end

--Called when we return home
function C:OnArrivedAtHome()
    if self.State == STATE_RETURNHOME then
        --We're home! Delete our parent.
        self:GetParent():Remove()
    end
end

--[[
=================================
    Member Variable Accessors
=================================
]]
function C:SetTask(Task)
    self.Task = Task
end

function C:GetTask()
    return self.Task
end

function C:SetStorageSize(NewStorageSize)
    self.StorageSize = NewStorageSize
    self:ResizeStorage()
end

--[[
==============
    Events
==============
]]
function C:OnProjectileEvent(Event, Info)
    if Event == "Removed" then
        if IsValid(self:GetHome()) then
            self:GetHome():RemoveDrone(self:GetParent())
        end
        return
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

--[[
==================================
    Node Resource Manipulation
==================================
]]
function C:PullFromSupply()
    local ResName = self.Task.ResourceName
    local ResAmount = self.Task.Amount
    local SupplyNode = self.Task.SupplyNode


    --Find out how much we can pull
    local SupplierAmount = SupplyNode:GetAmount(ResName)
    local TransferAmount = math.min(SupplierAmount, ResAmount)

    --Can't transfer anything. Return false.
    if not (TransferAmount > 0) then
        return false
    end

    --Pull the resource from the supplying node into our storage
    SupplyNode:CourierConsumeResource(ResName, TransferAmount) --Pull while updaing the outbound table
    self:SupplyResource(ResName, TransferAmount)

    return true
end

function C:PushToDemand()
    local ResName = self.Task.ResourceName
    local ResAmount = self.Task.Amount
    local DemandNode = self.Task.DemandNode

    --Find out how much we can push
    local DemanderAmount = math.max(DemandNode:GetMaxAmount(ResName) - DemandNode:GetAmount(ResName), 0)
    local TransferAmount = math.min(DemanderAmount, ResAmount)

    --Can't transfer anything. Return false.
    if not (TransferAmount > 0) then
        return false
    end

    --Push the resource from our storage into the demanding node
    DemandNode:CourierSupplyResource(ResName, TransferAmount)
    self:ConsumeResource(ResName, TransferAmount)

    return true
end

--[[
==========================
    Storage Management
==========================
]]
function C:ResizeStorage()
    if not self.Storage then
        self.Storage = {}
        self.Storage["Energy"] = GAMEMODE:NewStorage("Energy", self.StorageSize * 100, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Cargo"] = GAMEMODE:NewStorage("Cargo", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Ammo"] = GAMEMODE:NewStorage("Ammo", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Liquid"] = GAMEMODE:NewStorage("Liquid", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Gas"] = GAMEMODE:NewStorage("Gas", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
    else
        self.Storage["Energy"]:Resize(self.StorageSize * 100, true)
        self.Storage["Cargo"]:Resize(self.StorageSize, true)
        self.Storage["Ammo"]:Resize(self.StorageSize, true)
        self.Storage["Liquid"]:Resize(self.StorageSize, true)
        self.Storage["Gas"]:Resize(self.StorageSize, true)
    end
end

function C:GetStorageOfType(StorageType)
    return self.Storage[StorageType]
end

function C:SupplyResource(ResourceName, ResourceAmount)
    local Resource = GAMEMODE:GetResourceFromName(ResourceName)
    local ResourceType = Resource:GetType()
    return self:GetStorageOfType(ResourceType):SupplyResource(ResourceName, ResourceAmount)
end

function C:ConsumeResource(ResourceName, ResourceAmount)
    local Resource = GAMEMODE:GetResourceFromName(ResourceName)
    local ResourceType = Resource:GetType()
    return self:GetStorageOfType(ResourceType):ConsumeResource(ResourceName, ResourceAmount)
end

function C:Think()
    if self.State == STATE_IDLE then
        if self.Task then
            if self.Task.SupplyNode:GetParent() == self:GetHome() then  --We belong to a drone port. Transfer the resources immediately
                local PullSuccessful = self:PullFromSupply()
                if PullSuccessful then
                    self:SetTarget(self.Task.DemandNode)
                    self.State = STATE_MOVETODEMAND
                else
                    self:SetTargetToHome()
                    self.State = STATE_RETURNHOME
                end
            else
                self:SetTarget(self.Task.SupplyNode)
                self.State = STATE_MOVETOSUPPLY
            end
        end
    end

    --Go home and delete ourself if the target becomes invalid
    if not IsValid(self:GetTarget()) then
        self:SetTargetToHome()
        self.State = STATE_RETURNHOME
    end

    --TODO: More checks to make sure that the supplier still has the supplies we need
    --  and that the demander still requires the supplies that we are providing

    BaseClass.Think(self)
end

GM.class.registerClass("LogisticsDroneControlComponent", C)

