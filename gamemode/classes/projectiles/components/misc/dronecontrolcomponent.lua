local GM = GM

local C = GM.class.getClass("ProjectileComponent"):extends({
    Home = nil,
    Target = nil,
    Arrived = false,
    NextTargetPosUpdateTime = 0,
    LastTargetPos = Vector(0,0,0),
    TargetPos = Vector(0,0,0),
    TargetVelocity = Vector(0,0,0),

    TurnRate = 0.1,
    Speed = 2000,
    AccelerationDistance = 1000
})

local ZeroVector = Vector(0, 0, 0)
local ZeroAngle = Angle(0, 0, 0)
local BaseClass = C:getClass()


-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("DroneControlComponent")

function C:init()
    self.LastTargetPos = ZeroVector
    BaseClass.init(self)
end

--[[
==========================
    Internal Callbacks
==========================
]]
--These get overriden by the drone behavior class
--Do not override these externally!

--Called when the drone sets home as its target
function C:OnReturnToHome()

end

--Called when the drone starts moving towards a target
function C:OnMovingToTarget()

end

--Called when the drone arrives at the target
function C:OnArrivedAtTarget()

end

--Called when the drone arrives at home
function C:OnArrivedAtHome()

end

--[[
==========================
    External Callbacks
==========================
]]
--Callbacks that are meant to be overridden externally

--Called when the drone arrives at the target
function C:OnArrivedAtTargetCallback()

end

--Called when the drone arrives at home
function C:OnArrivedAtHomeCallback()

end

--[[
==============
    Events
==============
]]
function C:OnProjectileEvent(Event, Info)
    if Event == "SetHomeEntity" and IsValid(Info.Home) then
        self.Home = Info.Home
    elseif CLIENT and (Event == "SetTargetPosition") then
        self.TargetPos = Info.Target
    elseif CLIENT and (Event == "SetTargetVelocity") then
        self.TargetVelocity = Info.TargetVelocity
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

--[[
=======================================
    Member Variable Setting/Getting
=======================================
]]

function C:SetTarget(NewTarget)
    if IsValid(NewTarget) then
        local TargetPos = NewTarget:GetPos()
        self.LastTargetPos = TargetPos
        self:GetParent():FireNetworkedEvent("SetTargetPosition", {Target = TargetPos})
        self.Target = NewTarget
    end
end

function C:GetTarget()
    return self.Target
end

function C:SetHome(NewHome)
    self.Home = NewHome
end

function C:GetHome()
    return self.Home
end

function C:GetStorage()
    return self.Storage
end

function C:SetStorage(Storage)
    self.Storage = Storage
end

function C:SetParent(NewParent)
    BaseClass.SetParent(self, NewParent)

    if self:GetParent() ~= nil then
        self:GetParent():SetTargetingComponent(self)
    end
end

--[[
=====================
    Misc. Methods
=====================
]]
function C:SetTargetToHome()
    if IsValid(self.Home) then
        self:SetTarget(self.Home)
        self:OnReturnToHome()
    else
        --No home!
        if SERVER then
            local Parent = self:GetParent()
            Parent:Remove()
        end
    end
end

function C:ProcessMovement()
    local Parent = self:GetParent()
    local MovementComponent = Parent:GetMovementComponent()
    local TargetPos = self.TargetPos
    local MyPos = Parent:GetPos()
    local TargetDistance = MyPos:Distance(TargetPos)

    if TargetDistance > 100 then
        if SERVER and self.Arrived then
            self:OnMovingToTarget()
            self.Arrived = false
        end
        local Forward = Parent:GetAngles():Forward()
        local DirToTarget = MyPos - TargetPos
        DirToTarget = DirToTarget / DirToTarget:Length()
        local Dot = (2 - (Forward:Dot(DirToTarget) + 1))

        local Velocity

        if Dot > 1.9 then
            Velocity = -DirToTarget * (self.Speed * math.Clamp(TargetDistance / self.AccelerationDistance, 0, 1))
        else
            Velocity = MovementComponent:GetVelocity() * 0.5
        end

        MovementComponent:SetVelocity(Velocity)

        --TODO: Fix this to use something better
        Parent:SetAngles(((-DirToTarget * self.TurnRate) + Forward):Angle())

    else
        --Arrived at the target
        if SERVER and not self.ArrivedAtTarget then
            local Target = self.Target

            self:OnArrivedAtTarget()
            self:OnArrivedAtTargetCallback()

            if Target == self.Home then
                self:OnArrivedAtHomeCallback()  --Call the external callback first since the internal one might delete the parent
                self:OnArrivedAtHome()
            end

            self.Arrived = true
        end
    end
end

function C:Think()
    if SERVER then
        --Poof out of existence if our home is destroyed
        if not IsValid(self.Home) then self:GetParent():Remove() return end
        --Network the target position to the client
        local Target = self.Target
        if IsValid(Target) then
            local TargetPos = Target:GetPos()
            if TargetPos ~= self.LastTargetPos then
                self.TargetVelocity = (TargetPos - self.LastTargetPos) / FrameTime()

                if CurTime() > self.NextTargetPosUpdateTime then
                    self:GetParent():FireNetworkedEvent("SetTargetPosition", {Target = TargetPos})
                    self:GetParent():FireNetworkedEvent("SetTargetVelocity", {TargetVelocity = self.TargetVelocity})
                    self.NextTargetPosUpdateTime = CurTime() + 1
                end

                self.LastTargetPos = TargetPos
            end

            self.TargetPos = TargetPos
        else
            self:SetTargetToHome()
        end
    end

    --Process the drone's movement
    self:ProcessMovement()

    --Parent got deleted by one of the callbacks. Return early so that the baseclass doesn't generate errors
    if not IsValid(Parent) then return end

    BaseClass.Think(self)
end

--[[
======================
    Member Methods
======================
]]

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return true
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return false
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return true
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "DroneControlComponent"
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteVector(self.TargetPos, false)
    net.WriteFloat(self.TurnRate)
    net.WriteFloat(self.Speed)
    net.WriteFloat(self.AccelerationDistance)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self.TargetPos = net.ReadVector()
    self.TurnRate = net.ReadFloat()
    self.Speed = net.ReadFloat()
    self.AccelerationDistance = net.ReadFloat()
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.TurnRate = self.TurnRate
    Data.Speed = self.Speed
    Data.AccelerationDistance = self.AccelerationDistance

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    self.TurnRate = Data.TurnRate or 0
    self.Speed = Data.Speed or 0
    self.AccelerationDistance = Data.AccelerationDistance or 1
end

GM.class.registerClass("DroneControlComponent", C)