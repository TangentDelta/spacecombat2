local C = GM.class.getClass("MovementComponent"):extends({
    TargetPos = vector_origin,
    TurnRate = 0,
    Speed = 0,
    AccelerationTime = 0,
    LateralThrust = false,
    LateralAccelerationTime = 0.2,
    WaverPos = vector_origin,
    TargetWaverPos = vector_origin,
    WaverRate = 0.1,
    WaverAmount = 200,
    NextWaverUpdate = 0,
    NextNetUpdate = 0
})

local BaseClass = C:getClass()

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("HomingMovementComponent")
local ZeroAngle = Angle(0, 0, 0)

function C:init(TurnRate, Speed, AccelerationTime, LateralThrust, Target)
    BaseClass.init(self)

    self.TargetPos = Target or vector_origin
    self.TurnRate = TurnRate or 0
    self.Speed = Speed or 0
    self.AccelerationTime = AccelerationTime or 1
    self.LateralThrust = LateralThrust or false
end

function C:OnProjectileEvent(Event, Info)
    if Event == "SetTargetPosition" then
        self.TargetPos = Info.Target or self.TargetPos or vector_origin
        return
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return SERVER or (CLIENT and self.ClientsidePrediction)
end

-- Homing movement requires network updates for wavering and when clientside prediction is disabled
function C:WantsNetworkUpdate()
    return not self.ClientsidePrediction or self.WaverAmount > 0.01
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return false
end

function C:Think()
    local Parent = self:GetParent()
    if not Parent then return end
    if CLIENT and not self.ClientsidePrediction then return end

    local TargetPos = self.TargetPos

    if self.WaverAmount > 0.01 and self.TargetPos ~= vector_origin then
        if SERVER and CurTime() > self.NextWaverUpdate then
            local WaverAmount = math.Remap(Parent:GetPos():Distance(TargetPos), 2000, 10000, self.WaverAmount, self.WaverAmount * 10)
            self.TargetWaverPos = VectorRand(-WaverAmount, WaverAmount)
            self.NextWaverUpdate = CurTime() + (1 / (self.WaverRate * 20))
        end

        self.WaverPos = LerpVector(self.WaverRate, self.WaverPos, self.TargetWaverPos)

        TargetPos = TargetPos + self.WaverPos
    end

    local Forward = Parent:GetAngles():Forward()
    local DirToTarget = TargetPos ~= vector_origin and (Parent:GetPos() - TargetPos) or Forward
    DirToTarget = DirToTarget / DirToTarget:Length()
    local Dot = (2 - (Forward:Dot(DirToTarget) + 1))
    local mod = math.pow(math.Clamp(self.TurnRate * Dot, 0, 1), 0.1)

    local RelativeVelocity = WorldToLocal(self:GetVelocity(), ZeroAngle, vector_origin, Parent:GetAngles())
    local XAcceleration = self.Speed / self.AccelerationTime
    local Acceleration = Vector(RelativeVelocity.x < self.Speed and math.min(XAcceleration * Dot, XAcceleration) or 0, 0, 0)

    if self.LateralThrust then
        local MaxLateral = self.Speed / self.LateralAccelerationTime
        Acceleration.Y = math.Clamp(-RelativeVelocity.Y, -MaxLateral, MaxLateral)
        Acceleration.Z = math.Clamp(-RelativeVelocity.Z, -MaxLateral, MaxLateral)
    end

    self:SetRelativeAcceleration(Acceleration)
    Parent:SetAngles((DirToTarget * -mod + Forward * (1-mod)):Angle())

    BaseClass.Think(self)
end

-- Because of the way this class works we do NOT want to sync anything from the server regularly!
function C:ReadNetworkUpdate()
    local HasNetUpdate = net.ReadBool()
    if HasNetUpdate or not self.ClientsidePrediction then
        BaseClass.ReadNetworkUpdate(self)

        self.TargetWaverPos = net.ReadVector()
    end
end

function C:SendNetworkUpdate()
    local SendUpdate = CurTime() > self.NextNetUpdate
    net.WriteBool(SendUpdate)
    if SendUpdate or not self.ClientsidePrediction then
        -- Wavering projectiles need to sync just after the waver target updates
        if self.WaverAmount > 0.01 then
            self.NextNetUpdate = self.NextWaverUpdate + 0.01
        else
            self.NextNetUpdate = CurTime() + 1
        end

        BaseClass.SendNetworkUpdate(self)

        net.WriteVector(self.TargetWaverPos, false)
    end
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return true
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "HomingMovementComponent"
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteVector(self.TargetPos, false)
    net.WriteFloat(self.TurnRate)
    net.WriteFloat(self.Speed)
    net.WriteFloat(self.AccelerationTime)
    net.WriteBool(self.LateralThrust)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self.TargetPos = net.ReadVector()
    self.TurnRate = net.ReadFloat()
    self.Speed = net.ReadFloat()
    self.AccelerationTime = net.ReadFloat()
    self.LateralThrust = net.ReadBool()
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.Target = self.TargetPos
    Data.TurnRate = self.TurnRate
    Data.Speed = self.Speed
    Data.AccelerationTime = self.AccelerationTime
    Data.LateralThrust = self.LateralThrust

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    self.TargetPos = Data.Target or Vector(0,0,0)
    self.TurnRate = Data.TurnRate or 0
    self.Speed = Data.Speed or 0
    self.AccelerationTime = Data.AccelerationTime or 1
    self.LateralThrust = Data.LateralThrust or false
end

GM.class.registerClass("HomingMovementComponent", C)