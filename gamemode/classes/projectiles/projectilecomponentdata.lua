local GM = GM
local ProjectileComponentData = {}

GM.Projectiles = GM.Projectiles or {}
GM.Projectiles.Components = {}
GM.Projectiles.Components.LoadedComponents = ProjectileComponentData

if SERVER then
    util.AddNetworkString("SC.ProjectileComponentDataLoad")

    net.Receive("SC.ProjectileComponentDataLoad", function(Length, Client)
        net.Start("SC.ProjectileComponentDataLoad")
        net.WriteUInt(table.Count(ProjectileComponentData), 16)
        for _,Data in pairs(ProjectileComponentData) do
            Data:WriteCreationPacket()
        end
        net.Send(Client)
    end)
else
    net.Receive("SC.ProjectileComponentDataLoad", function()
        local NumberOfComponents = net.ReadUInt(16)
        SC.Print("Got "..NumberOfComponents.." serialized Projectile Components from the server!", 4)
        for I = 1, NumberOfComponents do
            local Data = GM.class.new("ProjectileComponentData")
            Data:ReadCreationPacket()
            ProjectileComponentData[Data:GetName()] = Data

            SC.Print("Loaded Projectile Component "..Data:GetName(), 4)
        end


        SC.Print("Finished loading Projetile Components", 4)
        hook.Run("SC.ProjectileComponentDataLoaded")
    end)
end

function GM.Projectiles.Components.ReloadProjectileComponentData()
    -- Server loads all files from disk for projectile Components
    if SERVER then
        -- Get all the files
        local Files = file.Find(SC.DataFolder.."/projectiles/components/*", "DATA")

        -- For each file load the data into a new projectile Component class
        for _,File in pairs(Files) do
            local NewProjectileComponentData = GM.class.new("ProjectileComponentData")
            if NewProjectileComponentData:LoadFromINI(File) then
                ProjectileComponentData[NewProjectileComponentData:GetName()] = NewProjectileComponentData
            else
                SC.Error("Failed to load projectile component from file "..File, 5)
            end
        end

        hook.Run("SC.ProjectileComponentDataLoaded")

    -- Clients request data from the server for projectile Components
    else
        -- Reset the existing data until we get the new stuff
        ProjectileComponentData = {}
        GM.Projectiles.Components.LoadedComponents = ProjectileComponentData

        -- Request new data from the server
        net.Start("SC.ProjectileComponentDataLoad")
        net.SendToServer()
    end
end

if SERVER then
    hook.Remove("SC.Content.PostConfig", "SC.LoadProjectileComponentData")
    hook.Add("SC.Content.PostConfig", "SC.LoadProjectileComponentData", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end)

    concommand.Add("sc_reloadweapons", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end, nil, nil, FCVAR_CHEAT)
else
    hook.Remove("SC.Content.PostContentMounted", "SC.LoadProjectileComponentData")
    hook.Add("SC.Content.PostContentMounted", "SC.LoadProjectileComponentData", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end)

    concommand.Add("sc_reloadweapons_cl", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end, nil, nil, FCVAR_CHEAT)
end

local C = GM.LCS.class({
    -- Name of the component
    Name = "Component",

    -- Description of the component displayed in the projectile creation interface
    Description = "",

    -- What class should be created when spawning the projectile
    Class = "ProjectileComponent",

    -- What family of components does this component belong to
    -- Used to determine what components can be placed in a slot
    Family = "Component",

    -- A list of factions that can use the component in projectiles
    Factions = {},

    -- A list of resources required to produce a projectile with this component
    ResourceCost = {},

    -- If the component should be hidden from the projectile creation tool
    Hidden = false,

    -- The amount of mass added to the projectile by the component
    Mass = 0,

    -- A table of data passed to the component class when it is created
    ClassData = {},

    -- A list of options available for this component in the projectile creation tool
    ToolOptions = {},
})

function C:init()
    -- Does nothing by default, add functionality here
end

function C:GetName()
    return self.Name
end

function C:GetDescription()
    return self.Description
end

function C:GetClass()
    return self.Class
end

function C:GetFamily()
    return self.Family
end

function C:IsHidden()
    return self.Hidden
end

function C:GetMass()
    return self.Mass
end

function C:GetFactions()
    return table.Copy(self.Factions)
end

function C:GetResourceCost()
    return table.Copy(self.ResourceCost)
end

function C:LoadFromINI(FileName)
    local Success, Data = GM.util.LoadINIFile(SC.DataFolder.."/projectiles/components/"..FileName)

    -- Load all the data into the class if we got any data back
    if Success then
        if self:ValidateINIData(Data) then
            self:DeSerialize(Data)
            return true
        else
            SC.Error(Format("Projectile component file %s has invalid data, it will not be loaded!", FileName), 5)
            return false
        end
    end

    return false
end

function C:ValidateINIData(Data)
    -- Check if it has a configuration table
    if not istable(Data.Configuration) then
        SC.Error("No configuration table provided!", 5)
        return false
    end

    -- Make sure everything is actually in that table
    if not isstring(Data.Configuration.Name) then
        SC.Error("No name provided!", 5)
        return false
    end

    if not isstring(Data.Configuration.Description) then
        SC.Error("No description provided!", 5)
        return false
    end

    if not isstring(Data.Configuration.Class) then
        SC.Error("No class provided!", 5)
        return false
    end

    if not isstring(Data.Configuration.Family) then
        SC.Error("No family provided!", 5)
        return false
    end

    -- Check that all factions exist
    if istable(Data.Configuration.Factions) then
        local FactionList = team.GetAllTeams()
        for _, FactionName in ipairs(Data.Configuration.Factions) do
            local FoundFaction = false
            for _, FactionData in ipairs(FactionList) do
                if FactionName == FactionData.Name then
                    FoundFaction = true
                    break
                end
            end

            if not FoundFaction then
                SC.Error(Format("Faction %s does not exist!", 5))
                return false
            end
        end
    end

    -- Make sure it has a valid class
    do
        local TestComponent = GM.class.new(Data.Configuration.Class)
        if not TestComponent then
            SC.Error("Component class does not exist!", 5)
        end
    end

    -- Make sure any resources exist
    if istable(Data.ResourceCost) then
        for Name, Amount in pairs(Data.ResourceCost) do
            if not GM:GetResourceFromName(Name) then
                SC.Error(Format("Resource %s does not exist!"), 5)
                return false
            end

            if Amount < 1 then
                SC.Error(Format("Resource cost for %s is less than 1!"), 5)
                return false
            end
        end
    end

    -- Check default tool options
    if istable(Data.ToolOptions) then
        -- Temporarily set the tool options table for verification
        self.ToolOptions = Data.ToolOptions

        local DefaultOptions = {}
        for Name, Info in pairs(Data.ToolOptions) do
            DefaultOptions[Name] = Info.Default
        end

        if not self:ValidateToolOptions(DefaultOptions) then
            SC.Error("Invalid default tool options found!", 5)
            return false
        end

        self.ToolOptions = {}
    end

    return true
end

function C:ValidateToolOptions(Options)
    -- Make sure the options are a table
    if not Options or not type(Options) == "table" then
        SC.Error("Tool Options table was invalid!", 5)
        return false
    end

    -- Loop over all the options and make sure everything matches the tool options set in the data
    for Name, Value in pairs(Options) do
        -- If the option isn't supposed to be here then the data isn't valid
        local ToolOptions = self.ToolOptions[Name]
        if not ToolOptions then
            SC.Error(Format("No data for tool option %s!", Name), 5)
            return false
        end

        -- Check to make sure the data is of the right type
        if ToolOptions.Type == "Number" then
            -- Make sure it's a number
            if type(Value) ~= "number" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)
                return false
            end

            -- Make sure it's not too low
            if ToolOptions.Min and Value < ToolOptions.Min then
                SC.Error(Format("Value below minimum for option %s!", Name), 5)
                return false
            end

            -- Make sure it's not too high
            if ToolOptions.Max and Value > ToolOptions.Max then
                SC.Error(Format("Value above maximum for option %s!", Name), 5)
                return false
            end
        elseif ToolOptions.Type == "Color" then
            -- Make sure it's a color
            if type(Value) ~= "table" or not Value.r or not Value.g or not Value.b then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)
                return false
            end
        elseif ToolOptions.Type == "String" then
            -- Make sure it's a string
            if type(Value) ~= "string" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)
                return false
            end
        elseif ToolOptions.Type == "Vector" then
            -- Make sure it's a vector
            if type(Value) ~= "Vector" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)
                return false
            end
        elseif ToolOptions.Type == "Angle" then
            -- Make sure it's a angle
            if type(Value) ~= "Angle" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)
                return false
            end
        end
    end

    return true
end

function C:GetComponentData(Options, Modifiers)
    -- If the options we got from the tool aren't valid them don't return anything
    if not self:ValidateToolOptions(Options) then
        SC.Error("Unable to create component from data, invalid tool options provided", 5)
        return false, {}
    end

    -- This table has the basic data needed to create a component
    local Data = {
        ComponentClass = self.Class,
        Modifiers = table.Copy(Modifiers or {}),
        Mass = self.Mass
    }

    -- Add any special data needed by our component class
    for Name, Value in pairs(self.ClassData) do
        Data[Name] = Value
    end

    -- Set the values of any configured data
    for Name, Info in pairs(self.ToolOptions) do
        Data[Name] = Options[Name] or Info.Default
    end

    return true, Data
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Name)
    net.WriteString(self.Description)
    net.WriteString(self.Class)
    net.WriteString(self.Family)
    net.WriteTable(self.Factions)
    net.WriteBool(self.Hidden)
    net.WriteFloat(self.Mass)
    net.WriteTable(self.ClassData)
    net.WriteTable(self.ToolOptions)
    net.WriteTable(self.ResourceCost)
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Name = net.ReadString()
    self.Description = net.ReadString()
    self.Class = net.ReadString()
    self.Family = net.ReadString()
    self.Factions = net.ReadTable()
    self.Hidden = net.ReadBool()
    self.Mass = net.ReadFloat()
    self.ClassData = net.ReadTable()
    self.ToolOptions = net.ReadTable()
    self.ResourceCost = net.ReadTable()
end

function C:Serialize()
    return {
        Configuration = {
            Name = self.Name,
            Description = self.Description,
            Class = self.Class,
            Family = self.Family,
            Factions = self.Factions,
            Hidden = self.Hidden,
            Mass = self.Mass,
        },

        ClassData = self.ClassData,
        ToolOptions = self.ToolOptions,
        ResourceCost = self.ResourceCost,
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Name = Data.Configuration.Name or "Component"
        self.Description = Data.Configuration.Description or ""
        self.Class = Data.Configuration.Class or "ProjectileComponent"
        self.Family = Data.Configuration.Family or "Component"
        self.Factions = Data.Configuration.Factions or {}
        self.Hidden = Data.Configuration.Hidden or false
        self.Mass = Data.Configuration.Mass or 0
    end

    self.ClassData = Data.ClassData or {}
    self.ToolOptions = Data.ToolOptions or {}
    self.ResourceCost = Data.ResourceCost or {}
end

GM.class.registerClass("ProjectileComponentData", C)