--[[=====================
	Timed Map Rotator
		By Steeveeo
--===================]]--
AddCSLuaFile()

local GM = GM
GM.MapRotate = {}
local MapRotate = GM.MapRotate

local mapRotate_Enabled = false

--Settings
if not ConVarExists("maprotate_drawSimple") then
	CreateConVar("maprotate_drawSimple", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end
local drawSimple = GetConVarNumber("maprotate_drawSimple") ~= 0


--NextMap Variables
local mapTime = 9999
local maxHours = 4
local mapString = "sb_reallylongname_v99"

--VotePanel Variables
local votePanel_ShowPanel = false
local votePanel_InProgress = false
local votePanel_Duration = 60
local votePanel_StartTime = CurTime()
local votePanel_PercentYes = 0.45
local votePanel_PercentNo = 0.25
local votePanel_TargetTime = 240
local votePanel_TargetMap = "sb_nobody_started_a_vote_yet_v99"
local votePanel_VoteType = "Time"

--Time Dial Elements
local generated = false
local secClock, minClock, hrClock, dialHead  --Clock
local nextMapLabel, nextMapTime, nextMapText --Text

--VotePanel Elements
local votePanel_HeaderLabel, votePanel_ActionLabel, votePanel_TargetLabel	--Text Elements
local votePanel_TimeBar, votePanel_VoteBarYes, votePanel_VoteBarNo			--Bars

--Convert seconds to "HH:MM:SS"
local function timeToString(time)
	local format = string.FormattedTime(time)
	local out = "%02i:%02i:%02i"

	out = string.format(out, format.h, format.m, format.s)

	return out
end

--Networking
net.Receive("MapRotate_Resync", function()
	mapRotate_Enabled = net.ReadBool()
	mapTime = net.ReadInt(32)
	mapString = net.ReadString()
	maxHours = math.floor(net.ReadInt(16) / 60)

    if nextMapText then
        nextMapText:setText(mapString)
    end

    if nextMapTime then
        nextMapTime:setText(timeToString(mapTime))
    end

	local curHours = math.floor(mapTime / 3600)
	if curHours > maxHours then
		maxHours = curHours
	end
end)

net.Receive("MapRotate_UpdateTime", function()
	mapTime = net.ReadInt(32)

	local curHours = math.floor(mapTime / 3600)
	if curHours > maxHours then
		maxHours = curHours
	end

    if nextMapTime then
        nextMapTime:setText(timeToString(mapTime))
    end
end)

net.Receive("MapRotate_UpdateMap", function()
	mapString = net.ReadString()

    if nextMapText then
        nextMapText:setText(mapString)
    end
end)

net.Receive("MapRotate_MaxTime", function()
	maxHours = math.floor(net.ReadInt(16) / 60)
end)

net.Receive("MapRotate_VoteStart", function()
	votePanel_InProgress = true
	votePanel_ShowPanel = true

	votePanel_Duration = net.ReadInt(16)
	votePanel_StartTime = net.ReadFloat()
	local isMapVote = net.ReadBool()

	if isMapVote then
		votePanel_VoteType = "Map"
		votePanel_TargetMap = net.ReadString()

		votePanel_ActionLabel:setText("Set Map To:")
		votePanel_TargetLabel:setText(votePanel_TargetMap)
	else
		votePanel_VoteType = "Time"
		votePanel_TargetTime = timeToString(net.ReadInt(32))

		votePanel_ActionLabel:setText("Set Time To:")
		votePanel_TargetLabel:setText(tostring(votePanel_TargetTime))
	end

	votePanel_TimeBar:setMaxValue(votePanel_Duration)

	votePanel_PercentYes = 0
	votePanel_PercentNo = 0
end)

net.Receive("MapRotate_VoteSubmitted", function()
	votePanel_PercentYes = net.ReadFloat()
	votePanel_PercentNo = net.ReadFloat()
end)

net.Receive("MapRotate_VoteEnd", function()
	votePanel_InProgress = false

	timer.Simple(10, function()
		votePanel_ShowPanel = false
	end)
end)


--Accessor Functions (Read-only on client)
function GM.MapRotate.GetTime()
	return mapTime
end

function GM.MapRotate.GetMaxHours()
	return maxHours
end

function GM.MapRotate.GetMap()
	return mapString
end

function GM.MapRotate.GetVoteInProgress()
	return votePanel_InProgress
end

function GM.MapRotate.GetVoteDuration()
	return votePanel_Duration
end

function GM.MapRotate.GetVoteTimeRemaining()
	return votePanel_InProgress and (votePanel_Duration - (CurTime() - votePanel_StartTime)) or 0
end

function GM.MapRotate.GetVoteType()
	return votePanel_InProgress and votePanel_VoteType or "None"
end

function GM.MapRotate.GetVoteTargetMap()
	return votePanel_InProgress and votePanel_TargetMap or "sb_nobody_started_a_vote_yet_v99"
end

function GM.MapRotate.GetVoteTargetTime()
	return votePanel_InProgress and votePanel_TargetTime or -1
end


local function setup()
	if not generated then
        drawSimple = GetConVarNumber("maprotate_drawSimple") ~= 0

		---------------------
		-- MapRotator Rings
        ---------------------
        local RadialSize = SC2ScreenScale(100)
		if not drawSimple then
            -- Hours
			hrClock = GAMEMODE.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize, RadialSize, 0, 0, Color(128, 0, 0, 200), Color(50, 0, 0, 255), true, Color(0, 0, 0, 255))
			hrClock:setMaxValue( 12 )
			hrClock:setPos( math.Round(ScrW() - hrClock:getRadius() - 20), math.Round(hrClock:getHeight() / 2 + 10) )
			hrClock:setSmoothFactor(0.6)

			local think = hrClock.think
			hrClock.think = function(self)
				self:setMaxValue( maxHours )
				self:setTarget( tonumber( string.FormattedTime(mapTime).h ) )
				think(self)
			end

			-- Minutes
			minClock = GAMEMODE.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize * 0.85, RadialSize * 0.85, 0, 0, Color(128, 0, 0, 255), Color(50, 0, 0, 255), true, Color(0, 0, 0, 255))
			minClock:setMaxValue( 60 )
			minClock:setPos( hrClock:getPos() )
			minClock:setSmoothFactor(0.6)

			local think = minClock.think
			minClock.think = function(self)
				self:setTarget( tonumber( string.FormattedTime(mapTime).m ) )
				think(self)
			end

			-- Seconds
			secClock = GAMEMODE.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize * 0.7, RadialSize * 0.7, 0, 0, Color(128, 0, 0, 255), Color(50, 0, 0, 255), true, Color(0, 0, 0, 255))
			secClock:setMaxValue( 60 )
			secClock:setPos( hrClock:getPos() )
			secClock:setSmoothFactor(0.6)

			local think = secClock.think
			secClock.think = function(self)
				self:setTarget( tonumber( string.FormattedTime(mapTime).s ) )
				think(self)
			end
		else
			-- Placeholder Circle
			hrClock = GAMEMODE.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize, RadialSize, 0, 0, Color(128, 0, 0, 200), Color(50, 0, 0, 255), true, Color(0, 0, 0, 255))
			hrClock:setMaxValue( 60 )
			hrClock:setPos( math.Round(ScrW() - hrClock:getRadius() - 20), math.Round(hrClock:getHeight() / 2 + 10) )
		end

		-- Dial Head
		dialHead = GAMEMODE.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize * 0.55, RadialSize * 0.55, 0, 0, Color(0, 0, 0, 255), Color(0, 0, 0, 255), true, Color(0, 0, 0, 255))
		dialHead:setMaxValue( 60 )
		dialHead:setPos( hrClock:getPos(), math.Round(hrClock:getHeight() / 2 + 10) )

		--End of MapRotator Rings--

		-------------------
		-- Next Map Labels
		-------------------

		local labelX = math.Round(ScrW() - hrClock:getRadius() - 20) - 185
		local labelY = math.Round(hrClock:getHeight() / 6) + 3
		nextMapLabel = GAMEMODE.class.getClass("TextElement"):new(labelX, labelY, Color(144, 144, 144, 255), "Next Map:")
		nextMapText = GAMEMODE.class.getClass("TextElement"):new(labelX, labelY + 18, Color(144, 144, 144, 255), mapString)

		nextMapTime = GAMEMODE.class.getClass("TextElement"):new(hrClock:getX(), hrClock:getY() - 6, Color(144, 144, 144, 255), timeToString(mapTime))
		nextMapTime:setX(hrClock:getX() - (nextMapTime:getWidth() / 2))

		--End of Next Map Labels--

		--------------------
		-- VotePanel Labels
		--------------------

		--Text
		labelX = math.Round(ScrW() - hrClock:getRadius() - 11) - 185
		labelY = math.Round(hrClock:getHeight() / 6) + 53
		votePanel_HeaderLabel = GAMEMODE.class.getClass("TextElement"):new(labelX, labelY, Color(255, 255, 255, 255), "VOTE IN PROGRESS")
		votePanel_ActionLabel = GAMEMODE.class.getClass("TextElement"):new(labelX - 10, labelY + 24, Color(200, 200, 200, 255), "Set Map To:")
		votePanel_TargetLabel = GAMEMODE.class.getClass("TextElement"):new(labelX - 10, labelY + 38, Color(200, 200, 200, 255), "sb_reallylongname_v99")

		--Time Remaining
		votePanel_TimeBar = GAMEMODE.class.getClass("HudBarIndicator"):new(labelX - 12, labelY + 56, 136, 6, votePanel_Duration, votePanel_Duration, Color(64, 64, 64, 255), Color(0, 0, 0, 255), Color(0, 64, 255, 255), false)
		votePanel_TimeBar:setSmoothFactor(0.3)

		local think = votePanel_TimeBar.think
		votePanel_TimeBar.think = function(self)
			local timeRemaining = votePanel_Duration - (CurTime() - votePanel_StartTime)
			if not votePanel_InProgress then
				timeRemaining = 0
			end

			self:setTarget(timeRemaining)

			think(self)
		end

		--Opposing Bar Graph (Note, I don't actually know what this type of plot is called, but it fills in from both sides with "undecided" in the middle)
		votePanel_VoteBarYes = GAMEMODE.class.getClass("HudBarIndicator"):new(labelX - 12, labelY + 66, 136, 12, 0, 1, Color(64, 64, 64, 255), Color(0, 0, 0, 0), Color(0, 200, 0, 255), false)
		votePanel_VoteBarYes:setSmoothFactor(0.3)
		votePanel_VoteBarNo = GAMEMODE.class.getClass("HudBarIndicator"):new(labelX - 12, labelY + 66, 136, 12, 0, 1, Color(64, 64, 64, 255), Color(200, 0, 0, 255), Color(0, 0, 0, 255), false)
		votePanel_VoteBarNo:setSmoothFactor(0.3)

		local think = votePanel_VoteBarYes.think
		votePanel_VoteBarYes.think = function(self)
			self:setTarget(votePanel_PercentYes)

			think(self)
		end

		local think = votePanel_VoteBarNo.think
		votePanel_VoteBarNo.think = function(self)
			self:setTarget(1 - votePanel_PercentNo)

			think(self)
		end

		--End of VotePanel Labels--

		---
		--- Register components
		---

		if not drawSimple then
			GAMEMODE:registerHUDComponent("maprotate_seconds", secClock)
			GAMEMODE:registerHUDComponent("maprotate_minutes", minClock)
			GAMEMODE:registerHUDComponent("maprotate_dialhead", dialHead)
		end
		GAMEMODE:registerHUDComponent("maprotate_hours", hrClock)
		GAMEMODE:registerHUDComponent("maprotate_maptime", nextMapTime)
		GAMEMODE:registerHUDComponent("maprotate_maptext", nextMapText)

		---
		--- End Component Registration
		---

		generated = true
	end
end

local BackingColor = Color(32, 0, 0, 200)
local function Paint()
	if not mapRotate_Enabled then return end

	setup()

	if LocalPlayer():Alive() and GetConVarNumber("cl_drawhud") ~= 0 then
		----------
		-- Dials
		----------

		--Render Backing Box
		local x = math.Round(ScrW() - hrClock:getRadius() - 20) - 190
		local y = math.Round(hrClock:getHeight() / 6)
		draw.RoundedBox(3, x, y, 180, 50, BackingColor)

		--Render Dials
		hrClock:render()
		if not drawSimple then
			minClock:render()
			secClock:render()
		end
		dialHead:render()

		--Render Text
		nextMapLabel:render()
		nextMapText:render()
		nextMapTime:render()

		--------------
		-- VotePanel
		--------------
		if votePanel_ShowPanel then
			--Backing
			x = math.Round(ScrW() - hrClock:getRadius() - 20) - 190
			y = math.Round((hrClock:getHeight() / 6) + 50)
			draw.RoundedBox(3, x, y, 140, 110, Color(16, 0, 0, 230))

			--Time remaining for vote (less than 15 seconds makes the buttons flash)
			local timeRemaining = votePanel_Duration - (CurTime() - votePanel_StartTime)

			if votePanel_InProgress then
				--Yes "Button"
				local color = Vector(64, 200, 64)
				if timeRemaining < 15 then
					color = LerpVector(math.sin(CurTime() * 4), Vector(64, 200, 64), Vector(255, 255, 64))
				end

				x = math.Round(ScrW() - hrClock:getRadius() - 20) - 187
				y = math.Round((hrClock:getHeight() / 6) + 137)
				draw.RoundedBox(6, x, y, 65, 20, Color(color.x, color.y, color.z, 255))
				draw.DrawText("!voteyes", "SBHud", x + 32, y + 2, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER)

				--No "Button"
				local color = Vector(200, 64, 64)
				if timeRemaining < 15 then
					color = LerpVector(math.sin(CurTime() * 4), Vector(200, 64, 64), Vector(255, 255, 64))
				end

				x = math.Round(ScrW() - hrClock:getRadius() - 20) - 118
				y = math.Round((hrClock:getHeight() / 6) + 137)
				draw.RoundedBox(6, x, y, 65, 20, Color(color.x, color.y, color.z, 255))
				draw.DrawText("!voteno", "SBHud", x + 32, y + 2, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER)
			else
				--Vote Over label
				x = math.Round(ScrW() - hrClock:getRadius() - 20) - 152
				y = math.Round((hrClock:getHeight() / 6) + 137)
				draw.DrawText("- VOTING COMPLETE -", "SBHud", x + 32, y + 2, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER)
			end

			--Bars of Doom
			votePanel_TimeBar:render()
			votePanel_VoteBarNo:render()
			votePanel_VoteBarYes:render()

			--Render Text
			votePanel_HeaderLabel:render()
			votePanel_ActionLabel:render()
			votePanel_TargetLabel:render()
		end
	end

end
hook.Add("HUDPaint", "MapRotate_HUDElements", Paint)
