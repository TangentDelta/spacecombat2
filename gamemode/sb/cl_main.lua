local GM = GM
local chat = chat

include("sh_main.lua")
include("cl_convar.lua")
include("cl_fonts.lua")
include("cl_hud.lua")

function GM:lookedAt(ent)
	if LocalPlayer():GetEyeTrace().Entity ~= ent then return false end
	if EyePos():Distance( ent:GetPos()) > 256 then return false end

	return true
end

function GM:fuzzyLook(ent)

	if not GM:lookedAt(ent) then
		local startPos = LocalPlayer():GetEyeTrace().StartPos

		local v = ent:GetPos() - startPos
		v = v:GetNormalized()

		local a = LocalPlayer():GetEyeTrace().HitPos - LocalPlayer():GetEyeTrace().StartPos
		a = a:GetNormalized()

		return ( math.abs(a:DotProduct( v )) > 0.90 and EyePos():Distance( ent:GetPos() ) < 512 ) and true or false

	else
		return true
	end

end

function GM:OnEnterEnvironment(env, ent)
	if env:GetName() ~= "" then
		chat.AddText(Color(255,255,255), "Entering ", Color(100,255,100), env:GetName())
	end
end

function GM:OnLeaveEnvironment(env, ent)
	if env:GetName() ~= "" then
		chat.AddText(Color(255,255,255), "Entering ", Color(100,255,100), env:GetName())
	end
end

net.Receive("SpaceCombat2-UpdatePlayerGravity", function()
    local gravity = net.ReadFloat()
    if not IsValid(LocalPlayer()) then return end
    LocalPlayer():SetGravity( math.max( gravity, 0.000001 ) )
end)
