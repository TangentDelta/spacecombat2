local surface = surface

surface.CreateFont("SBHud",
{
    font		= "Share Tech Mono",
    size		= SC2ScreenScale(14),
    weight		= 400
})

surface.CreateFont("SBHudLarge",
{
    font		= "Share Tech Mono",
    size		= SC2ScreenScale(32),
    weight		= 400
})

surface.CreateFont("HUDCompass",
{
    font		= "Share Tech Mono",
    size		= SC2ScreenScale(24),
    antialias = true,
    shadow = true,
    outline = false
})

surface.CreateFont("HUDCompassUpper",
{
    font		= "Share Tech Mono",
    size		= SC2ScreenScale(20),
    antialias = true,
    shadow = true,
    outline = false
})

surface.CreateFont("ToolInfo",
{
    font		= "Consola Mono",
    size		= 14,
    weight		= 300
})

surface.CreateFont("SpawnMenuLarge",
{
    font		= "Source Code Pro",
    size		= 20,
})

surface.CreateFont("SpawnMenuMedium",
{
    font		= "Source Code Pro",
    size		= 16,
})

surface.CreateFont("SpawnMenuSmall",
{
    font		= "Source Code Pro",
    size		= 14,
})

surface.CreateFont( "suiscoreboardheader"  , { font = "coolvetica", size = 28, weight = 100, antialiasing = true})
surface.CreateFont( "suiscoreboardsubtitle", { font = "coolvetica", size = 20, weight = 100, antialiasing = true})
surface.CreateFont( "suiscoreboardlogotext", { font = "coolvetica", size = 75, weight = 100, antialiasing = true})
surface.CreateFont( "suiscoreboardsuisctext", { font = "coolvetica", size = 18, weight = 50, antialiasing = true})
surface.CreateFont( "suiscoreboardplayername", { font = "coolvetica", size = 18, weight = 75, antialiasing = true})
surface.CreateFont( "suiscoreboardplayerrank", { font = "coolvetica", size = 16, weight = 75, italic = true, antialiasing = true})
surface.CreateFont(  "suiscoreboardcardinfo", { font = "DefaultSmall", size = 12, weight = 0, antialiasing = true} )

local tblFonts = { }

tblFonts["Default"] = {
	font = "Tahoma",
	size = 13,
	weight = 500,
}

tblFonts["TabLarge"] = {
	font = "Tahoma",
	size = 13,
	weight = 700,
	shadow = true,
}

tblFonts["DefaultBold"] = {
	font = "Tahoma",
	size = 13,
	weight = 1000,
}

tblFonts["DefaultSmall"] = {
	font = "Tahoma",
	size = 11,
	weight = 0,
}

tblFonts["Trebuchet24"] = {
	font = "Trebuchet MS",
	size = 24,
	weight = 900,
}

for k,v in SortedPairs( tblFonts ) do
	surface.CreateFont( k, tblFonts[k] );

	--print( "Added font '"..k.."'" );
end