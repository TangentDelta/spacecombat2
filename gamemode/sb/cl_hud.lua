local GM = GM
local internal = GM.internal

local os = os
local tonumber = tonumber
local tostring = tostring
local table = table
local pairs = pairs
local error = error
local string = string
local Color = Color
local math = math

--Settings
if not ConVarExists("sc_hud_showTimeElement") then
	CreateConVar("sc_hud_showTimeElement", 1, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end
local drawTimeBar = GetConVarNumber("sc_hud_showTimeElement") ~= 0

--== HUD SYSTEM FUNCTIONS ==--
internal.hud = {}

function GM:registerHUDComponent(name, component)
	if not component.is_A or not component:is_A( GM.class.getClass("HudComponent") ) then error("Component is not a HudComponent. Failed to register, "..name) return end
	internal.hud[name] = component
end

function GM:getHudComponentByName(name)
	return internal.hud[name]
end

function GM:getHudComponentByClass(classname)
	local ret = {}
	for k, v in pairs(internal.hud) do
		if v.is_A and v:is_A(classname) then
		   table.insert(ret, v)
		end
	end
	return ret
end

--==========================--


-------------------------
--------------------------
-------------------------

-- Locals for the panels so we can reference them throughout cl_hud
local generated = false

local statPanel, health, health_txt, armor, armor_txt -- Stats Panel & Children
local lsBar_Temp, SuitBar_Air, SuitBar_Energy, SuitBar_Coolant, lsBar_Temptxt, SuitText_Air, SuitText_Energy, SuitText_Coolant -- Lifesupport Stuff
local atmoLabel, atmoWarning --Atmosphere "HOSTILE" warning labels
local secClock, minClock, hrClock, fpsInd -- Clock & FPS
local secLabel, minLabel, hrLabel, fpsLabel -- Some labels for Clock & FPS
local secN, minN, hrN, fpsN -- Numbers for the Clock & FPS
local ammoPanel, ammo, ammo_txt, alt -- Ammo Panel & Children
local infoPanel, info -- Information Panel & Children

local wepTable = {}

local CompassUpperMarkers = {}
CompassUpperMarkers[90] = "N"
CompassUpperMarkers[135] = "NW"
CompassUpperMarkers[180] = "W"
CompassUpperMarkers[-180] = "W"
CompassUpperMarkers[-135] = "SW"
CompassUpperMarkers[-90] = "S"
CompassUpperMarkers[-45] = "SE"
CompassUpperMarkers[0] = "E"
CompassUpperMarkers[45] = "NE"

for i=-180,180,15 do
    if not CompassUpperMarkers[i] then
        CompassUpperMarkers[i] = tostring(i + 180)
    end
end

local PitchUpperMarkers = {}
for i=-180,180,15 do
    if not PitchUpperMarkers[-i] then
        PitchUpperMarkers[-i] = tostring(i)
    end
end

local CompassLowerMarkers = {}
for i=-180,180,15 do
    if not CompassLowerMarkers[i] then
        CompassLowerMarkers[i] = "|"
    end
end

for i=-180,180,5 do
    if not CompassLowerMarkers[i] then
        CompassLowerMarkers[i] = "."
    end
end

local function calcAmmo()

	local ammo, maxAmmo, secondaryAmmo
	local wep = LocalPlayer():GetActiveWeapon()

	if wep:IsValid() then
		ammo = wep:Clip1()

		if ammo ~= nil and ammo > 0 then

			-- Add Wep max ammo if it doesn't exist. Otherwise update ammo amount if we have something bigger
			if not wepTable[ wep:GetClass() ] or ammo > wepTable[ wep:GetClass() ] then
				wepTable[ wep:GetClass() ] = ammo or 0
			end
		end

		secondaryAmmo = wep:GetSecondaryAmmoType() and LocalPlayer():GetAmmoCount( wep:GetSecondaryAmmoType()) or 0
		maxAmmo = wepTable[ wep:GetClass() ]
	end

	return ammo, maxAmmo, secondaryAmmo
end

local function genComponents()
	if not generated then

		---
		--- Stats Panel - Bottom Left
		---

		statPanel = GM.class.getClass("HudPanel"):new( 0, 0, 0, 0, Color(20, 20, 20, 150), true)
		statPanel:setPadding(10, 10)
		statPanel:setAutoSize(true)

		local think = statPanel.think
		statPanel.think = function(self)
			think(self)

			self:setPos(20, ScrH() - 20 - self:getHeight())
		end

        local WideBarWidth = SC2ScreenScale(130)
        local WideBarHeight = SC2ScreenScale(10)
		health = GM.class.getClass("HudBarIndicator"):new(0, 0, WideBarWidth, WideBarHeight, 0, 100, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), false)
		health_txt = GM.class.getClass("TextElement"):new(health:getX(), health:getY() + health:getHeight(), Color(255, 255, 255, 255), "HEALTH")
		health:setSmoothFactor(0.3)

		local think = health.think
		health.think = function(self)
			self:setTarget(LocalPlayer():Health())

			think(self)
		end

		local think = health_txt.think
		health_txt.think = function(self)
			think(self)

			self:setText(string.format("HEALTH: %s", math.Round(LocalPlayer():Health())) .. "%")
		end

		armor = GM.class.getClass("HudBarIndicator"):new(0, health_txt:getY() + health_txt:getHeight() + SC2ScreenScale(5), WideBarWidth, WideBarHeight, 0, 100, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), false)
		armor_txt = GM.class.getClass("TextElement"):new(armor:getX(), armor:getY() + armor:getHeight(), Color(255, 255, 255, 255), "ARMOR")
		armor:setSmoothFactor(0.3)

		local think = armor.think
		armor.think = function(self)
			self:setTarget(LocalPlayer():Armor())

			think(self)
		end

		local think = armor_txt.think
		armor_txt.think = function(self)
			think(self)

			self:setText(string.format("ARMOR: %s", math.Round(LocalPlayer():Armor())) .. "%" )
		end

        -- LS Bars
        local LSBarWidth = SC2ScreenScale(20)
        local LSBarHeight = SC2ScreenScale(80)
        local LSInitialBarSpacing = SC2ScreenScale(30)
        local LSBarSpacing = SC2ScreenScale(13)

		lsBar_Temp = GM.class.getClass("HudBarIndicator"):new(health:getWidth() + LSInitialBarSpacing, 0, LSBarWidth, LSBarHeight, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), true)
		lsBar_Temp:setSmoothFactor(0.3)

		lsBar_Temptxt = GM.class.getClass("TextElement"):new(lsBar_Temp:getX() + (lsBar_Temp:getWidth() / 2), lsBar_Temp:getY() + lsBar_Temp:getHeight(), Color(255, 255, 255, 255), "°K")
		lsBar_Temptxt:setXAlign(TEXT_ALIGN_CENTER)

		SuitBar_Air = GM.class.getClass("HudBarIndicator"):new(lsBar_Temp:getX() + lsBar_Temp:getWidth() + LSBarSpacing, 0, LSBarWidth, LSBarHeight, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(0, 144, 0, 255), true)
		SuitBar_Air:setSmoothFactor(0.3)

		SuitText_Air = GM.class.getClass("TextElement"):new(SuitBar_Air:getX() + (SuitBar_Air:getWidth() / 2), SuitBar_Air:getY() + SuitBar_Air:getHeight(), Color(255, 255, 255, 255), "AIR")
		SuitText_Air:setXAlign(TEXT_ALIGN_CENTER)

		SuitBar_Coolant = GM.class.getClass("HudBarIndicator"):new(SuitBar_Air:getX() + SuitBar_Air:getWidth() + LSBarSpacing, 0, LSBarWidth, LSBarHeight, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(128, 128, 0, 255), true)
		SuitBar_Coolant:setSmoothFactor(0.3)

		SuitText_Coolant = GM.class.getClass("TextElement"):new(SuitBar_Coolant:getX() + (SuitBar_Coolant:getWidth() / 2), SuitBar_Coolant:getY() + SuitBar_Coolant:getHeight(), Color(255, 255, 255, 255), "COOL")
		SuitText_Coolant:setXAlign(TEXT_ALIGN_CENTER)

		SuitBar_Energy = GM.class.getClass("HudBarIndicator"):new(SuitBar_Coolant:getX() + SuitBar_Coolant:getWidth() + LSBarSpacing, 0, LSBarWidth, LSBarHeight, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(0, 0, 255, 255), true)
		SuitBar_Energy:setSmoothFactor(0.3)

		SuitText_Energy = GM.class.getClass("TextElement"):new(SuitBar_Energy:getX() + (SuitBar_Energy:getWidth() / 2), SuitBar_Energy:getY() + SuitBar_Energy:getHeight(), Color(255, 255, 255, 255), "PWR")
		SuitText_Energy:setXAlign(TEXT_ALIGN_CENTER)

		local think = SuitBar_Air.think
		SuitBar_Air.think = function(self)
            local Storage = LocalPlayer():GetStorage()
            if Storage then
                self:setTarget(Storage:GetAmount("Oxygen") / Storage:GetMaxAmount("Oxygen"))
            end

			think(self)
		end

		local think = SuitBar_Coolant.think
		SuitBar_Coolant.think = function(self)
            local Storage = LocalPlayer():GetStorage()
            if Storage then
                self:setTarget(Storage:GetAmount("Water") / Storage:GetMaxAmount("Water"))
            end

			think(self)
		end

		local think = SuitBar_Energy.think
		SuitBar_Energy.think = function(self)
            local Storage = LocalPlayer():GetStorage()
            if Storage then
                self:setTarget(Storage:GetAmount("Energy") / Storage:GetMaxAmount("Energy"))
            end

			think(self)
		end

		-- End LS Bars

		-- Atmosphere Warning

		atmoLabel = GM.class.getClass("TextElement"):new(0, armor_txt:getY() + SC2ScreenScale(17), Color(255, 255, 255, 255), "ENVIRONMENT STATUS:")

		atmoWarning = GM.class.getClass("TextElement"):new(health:getX() + (health:getWidth() / 2), atmoLabel:getY() + SC2ScreenScale(15), Color(0, 255, 0, 255), "SAFE")
		atmoWarning:setFont("SBHudLarge")
		atmoWarning:setXAlign(TEXT_ALIGN_CENTER)

		local think = atmoWarning.think
		atmoWarning.think = function(self)
            if LocalPlayer():GetEnvironmentHostile() then
                atmoWarning:setText("HOSTILE")
                atmoWarning:setColor(Color(255, 0, 0, 255))
            else
                atmoWarning:setText("SAFE")
                atmoWarning:setColor(Color(0, 255, 0, 255))
            end
			think(self)
		end

		-- End Atmosphere Warning

		statPanel:addChild(health)
		statPanel:addChild(health_txt)
		statPanel:addChild(armor)
		statPanel:addChild(armor_txt)
		statPanel:addChild(lsBar_Temp)
		statPanel:addChild(lsBar_Temptxt)
		statPanel:addChild(SuitBar_Air)
		statPanel:addChild(SuitText_Air)
		statPanel:addChild(SuitBar_Coolant)
		statPanel:addChild(SuitText_Coolant)
		statPanel:addChild(SuitBar_Energy)
		statPanel:addChild(SuitText_Energy)

		statPanel:addChild(atmoLabel)
		statPanel:addChild(atmoWarning)


		---
		--- End of StatsPanel
		---

		---
		--- Clock & FPS - Bottom
		---
        if drawTimeBar then
            local RadialSize = SC2ScreenScale(60)
			-- Seconds
			secClock = GM.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize, RadialSize, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), false, Color(50, 0, 0, 255))
			secClock:setMaxValue(60) -- 60 seconds in a minute hurr durr
			secClock:setPos(math.Round(ScrW() / 2 + secClock:getRadius() + secClock:getRadius() * 2 + 5), ScrH() - math.Round(secClock:getHeight() / 2 + 10))
			secClock:setSmoothFactor(0.5)

			local think = secClock.think
			secClock.think = function(self)
				self:setTarget(tonumber(os.date("%S")))
				think(self)
			end

			-- Minutes
			minClock = GM.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize, RadialSize, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), false, Color(50, 0, 0, 255))
			minClock:setMaxValue(60) -- 60 seconds in a minute hurr durr
			minClock:setPos(secClock:getX() - secClock:getRadius() - minClock:getRadius() - 10, secClock:getY())
			minClock:setSmoothFactor(0.6)

			local think = minClock.think
			minClock.think = function(self)
				self:setTarget(tonumber(os.date("%M") + os.date("%S") / 60))
				think(self)
			end

			-- Hours
			hrClock = GM.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize, RadialSize, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), false, Color(50, 0, 0, 255))
			hrClock:setMaxValue(12)
			hrClock:setPipSize(1.8)
			hrClock:setPos(minClock:getX() - minClock:getRadius() - hrClock:getRadius() - 10, minClock:getY())
			hrClock:setSmoothFactor(0.6)

			local think = hrClock.think
			hrClock.think = function(self)
				self:setTarget(tonumber((os.date("%H") % 12) + (os.date("%M")/60)))
				think(self)
			end

			-- FPS
			fpsInd = GM.class.getClass("HudRadialIndicator"):new(0, 0, RadialSize, RadialSize, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), true, Color(50, 0, 0, 255))
			fpsInd:setMaxValue(math.max(GetConVarNumber("fps_max") or 300, 30))
            fpsInd:setPos(hrClock:getX() - hrClock:getRadius() - fpsInd:getRadius() - 10, hrClock:getY())
            fpsInd:setTarget(60)

            local think = fpsInd.think
            fpsInd.think = function(self)
				self:setTarget(math.ceil((self:getTarget() + 1 / RealFrameTime()) / 2))
				think(self)
			end

			---
			--- LABELS
			---

			secLabel = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "Seconds")
			minLabel = GM.class.getClass("TextElement"):new(0 ,0, Color(255, 255, 255, 255), "Minutes")
			hrLabel = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "Hours")
			fpsLabel = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "FPS")

			secN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")
			minN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")
			hrN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")
			fpsN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")

			secLabel:setPos(secClock:getX() - secLabel:getWidth() / 2, secClock:getY() - secClock:getRadius() - secLabel:getHeight())
			minLabel:setPos(minClock:getX() - minLabel:getWidth() / 2, minClock:getY() - minClock:getRadius() - minLabel:getHeight())
			hrLabel:setPos(hrClock:getX() - hrLabel:getWidth() / 2, hrClock:getY() - hrClock:getRadius() - hrLabel:getHeight())
			fpsLabel:setPos(fpsInd:getX() - fpsLabel:getWidth() / 2, fpsInd:getY() - fpsInd:getRadius() - fpsLabel:getHeight())

			secN:setPos(secClock:getX() - secN:getHeight() / 2, secClock:getY() - secN:getHeight() / 2)
			minN:setPos(minClock:getX() - minN:getWidth() / 2, minClock:getY() - minN:getHeight() / 2)
			hrN:setPos(hrClock:getX() - hrN:getWidth() / 2, hrClock:getY() - hrN:getHeight() / 2)

			local think = secN.think
			secN.think = function(self)
				local val = ""
				local time = math.floor(secClock:getValue())
				if time < 10 then
					val = "0" .. tostring(time)
				else
					val = tostring(time)
				end

				self:setText(val)
				--secN:setPos(secClock:getX() - secN:getHeight() / 2, secClock:getY() - secN:getHeight() / 2)
				think(self)
			end

			local think = minN.think
			minN.think = function(self)
				local val = ""
				local time = math.floor(minClock:getValue())
				if time < 10 then
					val = "0" .. tostring(time)
				else
					val = tostring(time)
				end

				self:setText(val)

				--minN:setPos(minClock:getX() - minN:getWidth() / 2, minClock:getY() - minN:getHeight() / 2)
				think(self)
			end

			local think = hrN.think
			hrN.think = function(self)
				local val = ""
				local time = math.floor(os.date("%H")) -- To display 24 hour time, whilst the dial is repeated by 12
				if time < 10 then
					val = "0" .. tostring(time)
				else
					val = tostring(time)
				end

				self:setText(val)

				--hrN:setPos(hrClock:getX() - hrN:getWidth() / 2, hrClock:getY() - hrN:getHeight() / 2)
				think(self)
			end

			local think = fpsN.think
			fpsN.think = function(self)
				self:setText(tostring(math.floor(fpsInd:getValue())))
				fpsN:setPos(fpsInd:getX() - fpsN:getWidth() / 2, fpsInd:getY() - fpsN:getHeight() / 2)
				think(self)
			end

			---
			--- END OF LABELS
			---

		end
		---
		--- End of FPS & Clock
		---

		---
		--- Ammo Panel - Bottom Right
		---

		ammoPanel = GM.class.getClass("HudPanel"):new(0, 0, 0, 0, Color(20, 20, 20, 150), true)
		ammoPanel:setPadding(10, 10)

		local think = ammoPanel.think
		ammoPanel.think = function(self)
			think(self)

			self:setPos(ScrW() - 30 - self:getWidth(), ScrH() - 30 - self:getHeight())

			local ammo, maxAmmo, _ = calcAmmo()
			if ammo and maxAmmo and maxAmmo > 0 then
				self:setVisible(true)
			else
				self:setVisible(false)
			end

		end

		ammo = GM.class.getClass("HudBarIndicator"):new(0, 0, 120, 10, 0, 100, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), false)
		ammo_txt = GM.class.getClass("TextElement"):new(ammo:getX(), ammo:getY() + ammo:getHeight(), Color(255, 255, 255, 255), "Ammo")
		ammo:setSmoothFactor(0.3)

		local think = ammo.think
		ammo.think = function(self)
			local ammo, maxAmmo, _ = calcAmmo()

			if ammo and ammo >= 0 and maxAmmo and maxAmmo > 0 then
				self:setMaxValue(maxAmmo)
				self:setTarget(ammo)
			end

			think(self)
		end

		local think = ammo_txt.think
		ammo_txt.think = function(self)
			think(self)

			local ammo, maxAmmo, _ = calcAmmo()
			self:setText( string.format("Ammo: %s/%s", ammo, maxAmmo) )
		end

		alt = GM.class.getClass("TextElement"):new(ammo_txt:getX(), ammo_txt:getY() + ammo_txt:getHeight(), Color(255, 255, 255, 255), "Alt")

		local think = alt.think
		alt.think = function(self)
			think(self)

			local _, _, secondaryAmmo = calcAmmo()
			self:setText(string.format("Alt: %s", secondaryAmmo))
		end

		ammoPanel:addChild(ammo):addChild(ammo_txt):addChild(alt)
		---
		---	End of Ammo Panel
		---

		---
		--- Register components
		---

		GM:registerHUDComponent("StatPanel", statPanel) -- Optional, allows external lua scripts to call up the HUD elements for hooking etc
		GM:registerHUDComponent("AmmoPanel", ammoPanel)

		if drawTimeBar then
			GM:registerHUDComponent("fpsInd", fpsInd)
			GM:registerHUDComponent("secClock", secClock)
			GM:registerHUDComponent("minClock", minClock)
			GM:registerHUDComponent("hrClock", hrClock)
		end

		-- Don't bother with clock as it's not in a frame, and won't be used for anything other than defined here.

		---
		--- End Component Registration
		---

		generated = true
	end
end

local function DrawGraph(X, Y, SizeX, SizeY, DataMin, DataMax, Data)
    surface.SetDrawColor(Color(80, 20, 0, 150))
    surface.DrawRect(X, Y, SizeX, SizeY)

    surface.SetDrawColor(Color(100, 10, 0, 255))
    surface.DrawOutlinedRect(X, Y, SizeX, SizeY)

    local NumPoints = #Data
    local PointSpacing = SizeX / (NumPoints - 1)
    local PointSize = SC2ScreenScale(4)
    local HalfPointSize = PointSize * 0.5
    local LineColor = Color(255, 0, 0, 120)
    local PointColor = Color(255, 150, 0, 180)
    for I=1, NumPoints do
        local PrevData
        if Data[I-1] then
            PrevData = Data[I-1]
        end

        local CurData = Data[I]

        local PointX = X + (PointSpacing * (I - 1)) - HalfPointSize
        local PointY = Y + (SizeY * (1 - CurData))

        if PrevData then
            local PrevPointX = X + (PointSpacing * (I - 2)) - HalfPointSize
            local PrevPointY = Y + (SizeY * (1 - PrevData))

            surface.SetDrawColor(LineColor)
            surface.DrawLine(PointX, PointY, PrevPointX, PrevPointY)
        end

        surface.SetDrawColor(PointColor)
        surface.DrawRect(PointX, PointY, PointSize, PointSize)
    end
end

local DefaultCapacitorGraphData = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
local CoreCapacitorData = table.Copy(DefaultCapacitorGraphData)
local CoreCapacitorDataTimer = 0
local function PaintShipHUD(Player, Protector)
    local ScreenWidth = ScrW()
    local ScreenHeight = ScrH()

    -- Temperature Gauge Tomfoolery
    ---		Graph assumes min 200K and max 400K and an 80 pixel bar, because a range of 25 degrees of habitability is a bit tiny (283 - 308K)
    surface.SetTexture(0)
    surface.SetDrawColor(Color(0, 196, 255, 255)) --Cold
    surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY(), lsBar_Temp:getWidth(), lsBar_Temp:getHeight())
    surface.SetTexture(0)
    surface.SetDrawColor(Color(255, 0, 0, 255)) --Hot
    surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY(), lsBar_Temp:getWidth(), 36)
    surface.SetTexture(0)
    surface.SetDrawColor(Color(0, 255, 0, 255)) --Goldilocks
    surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY() + 36, lsBar_Temp:getWidth(), 10)
    surface.SetTexture(0)
    surface.SetDrawColor(Color(0, 0, 0, 255)) --Goldilocks Highlight
    surface.DrawOutlinedRect(lsBar_Temp:getX(), lsBar_Temp:getY() + 35, lsBar_Temp:getWidth(), 12)

    -- Main Panel
    statPanel:render()

    --Temperature Indicator
    local adjustedTemp = math.Clamp(LocalPlayer():GetTemperature(), 100, 520) - 100
    surface.SetTexture(0)
    surface.SetDrawColor(Color(255, 255, 255, 255))
    surface.DrawRect(lsBar_Temp:getX() - 3, lsBar_Temp:getY() + (79 - (79 * (adjustedTemp / 420))), lsBar_Temp:getWidth() + 6, 2)

    -- Protector Status
    if Protector.IsCoreEnt then
        local BarSizeX = SC2ScreenScale(30)
        local BarSizeY = SC2ScreenScale(500)

        local BarX = (ScreenWidth * 0.5) - SC2ScreenScale(450) - (BarSizeX * 0.5)
        local BarY = ScreenHeight * 0.5 - BarSizeY * 0.5

        local HullPercent = Protector:GetHullPercent()
        local ArmorPercent = Protector:GetArmorPercent()
        local ShieldPercent = Protector:GetShieldPercent()

        local HullAmount = Protector:GetHullMax()
        local ArmorAmount = Protector:GetArmorMax()
        local ShieldAmount = Protector:GetShieldMax()
        local Total = HullAmount + ArmorAmount + ShieldAmount

        HullAmount = (HullAmount / Total) * HullPercent
        ArmorAmount = (ArmorAmount / Total) * ArmorPercent
        ShieldAmount = (ShieldAmount / Total) * ShieldPercent

        surface.SetDrawColor(Color(220, 50, 0, 255))
        surface.DrawRect(BarX, BarY, BarSizeX, math.ceil(BarSizeY * HullAmount))

        surface.SetDrawColor(Color(120, 120, 120, 255))
        surface.DrawRect(BarX, BarY + math.ceil(HullAmount * BarSizeY), BarSizeX, math.ceil(BarSizeY * ArmorAmount))

        surface.SetDrawColor(Color(0, 100, 255, 255))
        surface.DrawRect(BarX, BarY + math.ceil((HullAmount + ArmorAmount) * BarSizeY), BarSizeX, math.ceil(BarSizeY * ShieldAmount))

        surface.SetDrawColor(Color(0, 50, 150, 255))
        surface.DrawOutlinedRect(BarX, BarY, BarSizeX, BarSizeY)

        if RealTime() > CoreCapacitorDataTimer then
            if CoreCapacitorDataTimer < 1 then
                CoreCapacitorData = table.Copy(DefaultCapacitorGraphData)
            end

            local NewData = {}
            for I=2, 10 do
                NewData[I] = CoreCapacitorData[I-1]
            end
            CoreCapacitorData = NewData

            CoreCapacitorData[1] = Protector:GetCapPercent()
            CoreCapacitorDataTimer = RealTime() + 0.5
        end

        local GraphSizeY = SC2ScreenScale(50)
        local GraphSizeX = statPanel:getWidth()
        DrawGraph(statPanel:getX(), statPanel:getY() - (GraphSizeY + SC2ScreenScale(10)), GraphSizeX, GraphSizeY, 0, 100, CoreCapacitorData)
    end

    -- Gyropod stuff
    local Gyropod = Protector:GetGyropod()
    local Vehicle = Player:GetVehicle()
    if IsValid(Gyropod) and Gyropod:GetLinkedPod() == Vehicle then
        do -- Compass
            local LowerText = ""
            local UpperText = ""
            local Bearing = math.floor(Gyropod:GetAngles().y)
            local TextCenterX = ScrW() * 0.5
            local TextCenterY = SC2ScreenScale(100)
            for I=-38,38,1 do
                local X = math.NormalizeAngle(I + Bearing)

                LowerText = CompassLowerMarkers[X]
                UpperText = CompassUpperMarkers[X]

                local TextX = TextCenterX + I * SC2ScreenScale(10)
                if UpperText then
                    draw.DrawText(UpperText, "HUDCompassUpper", TextX, TextCenterY + SC2ScreenScale(2), Color(255, 80, 0, 255), TEXT_ALIGN_CENTER)
                end

                if LowerText then
                    draw.DrawText(LowerText, "HUDCompass", TextX, TextCenterY + SC2ScreenScale(24), Color(255, 180, 0, 255), TEXT_ALIGN_CENTER)
                end
            end

            draw.DrawText('^', "HUDCompass", TextCenterX, TextCenterY + SC2ScreenScale(64), Color(255, 180, 0, 255), TEXT_ALIGN_CENTER)
        end

        do -- Compass
            local LowerText = ""
            local UpperText = ""
            local Pitch = math.floor(Gyropod:GetAngles().x)
            local TextCenterX = ScrW() * 0.5 + SC2ScreenScale(450)
            local TextCenterY =  ScrH() * 0.5
            for I=-24,24,1 do
                local X = math.NormalizeAngle(I + Pitch)

                LowerText = CompassLowerMarkers[X]
                UpperText = PitchUpperMarkers[X]

                local TextY = TextCenterY + I * SC2ScreenScale(10)
                if UpperText then
                    surface.SetFont("HUDCompassUpper")
                    local w, h = surface.GetTextSize(UpperText)
                    draw.DrawText(UpperText, "HUDCompassUpper", TextCenterX + SC2ScreenScale(2), TextY - h * 0.5, Color(255, 80, 0, 255), TEXT_ALIGN_RIGHT)
                end

                if LowerText then
                    draw.RotatedText(LowerText, "HUDCompass", TextCenterX + SC2ScreenScale(24), TextY, Color(255, 180, 0, 255), -90)
                end
            end

            draw.RotatedText('^', "HUDCompass", TextCenterX - SC2ScreenScale(44), TextCenterY, Color(255, 180, 0, 255), 90);
        end
    end

    -- Targeter stuff
    local Targeter = Protector:GetTargeter()
    if IsValid(Targeter) then
        local BallColor = Color(220, 60, 20, 50)
        local CircleColor = Color(220, 60, 20, 255)
        local ShipColor = Color(0, 200, 255, 255)
        local TargetBallSize = 256
        local TargetBallHalfSize = (TargetBallSize * 0.5)
        local ShipDir = Protector:GetShipForward()
        local ShipUp = Protector:GetShipUp()
        local ShipRight = Protector:GetShipRight()
        local ViewPos = ShipDir * -80 + ShipUp * 40
        local ShipPos = Protector:GetPos()
        local ViewDir = (-ViewPos):GetNormalized()
        local ViewAngle = ViewDir:Angle()
        local TextAngle = ShipDir:Angle()
        TextAngle:RotateAroundAxis(TextAngle:Up(), -90)
        TextAngle:RotateAroundAxis(TextAngle:Forward(), 90)

        local TargetList = Targeter:GetTargetList()
        local CurrentTarget = Targeter:GetTarget()
        local TargetColor = Color(255, 0, 0, 255)
        local FactionColor = Color(120, 255, 0, 255)
        local OtherColor = Color(100, 100, 155, 255)
        local ForwardColor = Color(255, 0, 0, 255)
        local RightColor = Color(0, 255, 0, 255)
        local UpColor = Color(0, 0, 255, 255)
        local MyTeam = Player:Team()

        -- Draw target if available
        if IsValid(CurrentTarget) then
            surface.SetDrawColor(TargetColor)
            local ScreenData = CurrentTarget:GetPos():ToScreen()
            local BoxSize

            if CurrentTarget.IsCoreEnt then
                BoxSize = SC2ScreenScale(40)
            else
                BoxSize = SC2ScreenScale(20)
            end

            local HalfBoxSize = BoxSize * 0.5
            local BoxX = math.Clamp(ScreenData.x - HalfBoxSize, ScrW() * 0.05, ScrW() - ScrW() * 0.05)
            local BoxY = math.Clamp(ScreenData.y - HalfBoxSize, ScrH() * 0.05, ScrH() - ScrH() * 0.05)
            surface.DrawOutlinedRect(BoxX, BoxY, BoxSize, BoxSize)

            if CurrentTarget.IsCoreEnt then
                local CoreHealthBarHeight = SC2ScreenScale(10)
                local CoreHealthBarY = BoxY + BoxSize + SC2ScreenScale(5)

                local HullPercent = CurrentTarget:GetHullPercent()
                local ArmorPercent = CurrentTarget:GetArmorPercent()
                local ShieldPercent = CurrentTarget:GetShieldPercent()

                surface.SetDrawColor(Color(200, 100, 0, 255))
                surface.DrawRect(BoxX, CoreHealthBarY, BoxSize * HullPercent, CoreHealthBarHeight)

                surface.SetDrawColor(Color(120, 120, 120, 255))
                surface.DrawRect(BoxX, CoreHealthBarY, BoxSize * ArmorPercent, CoreHealthBarHeight)

                surface.SetDrawColor(Color(0, 100, 255, 255))
                surface.DrawRect(BoxX, CoreHealthBarY, BoxSize * ShieldPercent, CoreHealthBarHeight)

                surface.SetDrawColor(TargetColor)
                surface.DrawOutlinedRect(BoxX, CoreHealthBarY, BoxSize, CoreHealthBarHeight)
            end
        end

        -- Targeting Information
        local TargeterFilter = Targeter:GetTargetFilter()
        local FilterText = "Target Filter: " .. TargeterFilter.DisplayName
        draw.SimpleText(FilterText, "HUDCompassUpper", (ScreenWidth * 0.5) + TargetBallHalfSize, secClock:getY() - TargetBallHalfSize - 60, Color(255, 255, 255))

        -- Targeter Ball
        cam.Start3D(ViewPos,
            ViewAngle,
            75,
            (ScreenWidth * 0.5) - TargetBallHalfSize,
            secClock:getY() - TargetBallSize - 60,
            TargetBallSize, TargetBallSize
        )
            render.PushFilterMag(TEXFILTER.ANISOTROPIC)
            render.PushFilterMin(TEXFILTER.ANISOTROPIC)

            cam.Start3D2D(Vector(0, 10, 55), TextAngle, 0.5)
                draw.SimpleText("+Z", "HUDCompass")
            cam.End3D2D()

            cam.Start3D2D(Vector(0, 10, -55), TextAngle, 0.5)
                draw.SimpleText("-Z", "HUDCompass")
            cam.End3D2D()

            cam.Start3D2D(Vector(55, 0, 10), TextAngle, 0.5)
                draw.SimpleText("+X", "HUDCompass")
            cam.End3D2D()

            cam.Start3D2D(Vector(-55, 0, 10), TextAngle, 0.5)
                draw.SimpleText("-X", "HUDCompass")
            cam.End3D2D()

            cam.Start3D2D(Vector(10, 55, 0), TextAngle, 0.5)
                draw.SimpleText("+Y", "HUDCompass")
            cam.End3D2D()

            cam.Start3D2D(Vector(10, -55, 0), TextAngle, 0.5)
                draw.SimpleText("-Y", "HUDCompass")
            cam.End3D2D()

            render.PopFilterMag()
	        render.PopFilterMin()

            render.SetColorMaterial()
            render.DrawLine(ShipRight * -50, ShipRight * 50, RightColor, false)
            render.DrawLine(ShipDir * -50, ShipDir * 50, ForwardColor, false)
            render.DrawSphere(vector_origin, 4, 8, 8, ShipColor)
            render.DrawSphere(vector_origin, 50, 16, 16, BallColor)

            cam.Start3D2D(vector_origin, ShipDir:AngleEx(ShipDir:Cross(ShipRight)), 1)
                local CircleMatrix = Matrix()
                CircleMatrix:Translate(Vector(-50, -50, 0))

                cam.PushModelMatrix(CircleMatrix, true)
                    surface.DrawCircle(50, 50, 50, CircleColor)
                    surface.DrawCircle(50, 50, 40, CircleColor)
                    surface.DrawCircle(50, 50, 30, CircleColor)
                    surface.DrawCircle(50, 50, 20, CircleColor)
                cam.PopModelMatrix()
            cam.End3D2D()


            -- Draw all available targets
            for _, Target in ipairs(TargetList) do
                if IsValid(Target) and Target ~= CurrentTarget then
                    local BlipColor
                    local Owner = SC.GetEntityOwner(Target)
                    local TargetIsCore = Target.IsCoreEnt or false
                    if IsValid(Owner) then
                        if Owner:Team() == MyTeam then
                            BlipColor = FactionColor
                        else
                            BlipColor = TargetIsCore and team.GetColor(Owner:Team()) or OtherColor
                        end
                    else
                        BlipColor = OtherColor
                    end

                    local BlipRotation = (ShipPos - Target:GetPos()):GetNormalized()
                    if BlipRotation:Dot(ViewDir) < 0.75 then
                        local BlipPos = BlipRotation * -50
                        render.DrawBox(BlipPos, BlipRotation:Angle(), Vector(0, -1, -1), Vector(4, 1, 1), BlipColor)
                        local PlaneIntersection = util.IntersectRayWithPlane(BlipPos, ShipUp, vector_origin, ShipUp)
                        if not PlaneIntersection then
                            PlaneIntersection = util.IntersectRayWithPlane(BlipPos, -ShipUp, vector_origin, ShipUp)
                        end

                        if PlaneIntersection then
                            render.DrawLine(BlipPos, PlaneIntersection, Color(BlipColor.r, BlipColor.g, BlipColor.b, 40))
                        end
                    end
                end
            end

            -- Draw the current target seperately so it's on top
            if IsValid(CurrentTarget) then
                local BlipRotation = (ShipPos - CurrentTarget:GetPos()):GetNormalized()
                local BlipPos = BlipRotation * -50
                render.DrawBox(BlipPos, BlipRotation:Angle(), Vector(-1, -2.25, -2.25), Vector(6, 2.25, 2.25), TargetColor)

                local PlaneIntersection = util.IntersectRayWithPlane(BlipPos, ShipUp, vector_origin, ShipUp)
                if not PlaneIntersection then
                    PlaneIntersection = util.IntersectRayWithPlane(BlipPos, -ShipUp, vector_origin, ShipUp)
                end

                if PlaneIntersection then
                    render.DrawLine(BlipPos, PlaneIntersection, TargetColor)
                end
            end
        cam.End3D()
    end

    if drawTimeBar then
        -- Clock [ ORDER IMPORTANT ]
        secClock:render()
        minClock:render()
        hrClock:render()

        -- FPS meter
        fpsInd:render()

        -- Labels [ ORDER NOT IMPORTANT ]
        secLabel:render()
        minLabel:render()
        hrLabel:render()
        fpsLabel:render()

        -- Numbers [ ORDER NOT IMPORTANT ]
        secN:render()
        minN:render()
        hrN:render()
        fpsN:render()
    end
end

local function PaintPlayerHUD(Player)
    do -- Compass
        local LowerText = ""
        local UpperText = ""
        local Bearing = math.floor(Player:EyeAngles().y)
        local TextCenterX = ScrW() * 0.5
        local TextCenterY = SC2ScreenScale(100)
        for I=-38,38,1 do
            local X = math.NormalizeAngle(I + Bearing)

            LowerText = CompassLowerMarkers[X]
            UpperText = CompassUpperMarkers[X]

            local TextX = TextCenterX + I * SC2ScreenScale(10)
            if UpperText then
                draw.DrawText(UpperText, "HUDCompassUpper", TextX, TextCenterY + SC2ScreenScale(2), Color(255, 80, 0, 255), TEXT_ALIGN_CENTER);
            end

            if LowerText then
                draw.DrawText(LowerText, "HUDCompass", TextX, TextCenterY + SC2ScreenScale(24), Color(255, 180, 0, 255), TEXT_ALIGN_CENTER);
            end
        end

        draw.DrawText('^', "HUDCompass", TextCenterX, TextCenterY + SC2ScreenScale(64), Color(255, 180, 0, 255), TEXT_ALIGN_CENTER);
    end

    -----
    -- Player Status Panel
    -----

    -- Temperature Gauge Tomfoolery
    ---		Graph assumes min 200K and max 400K and an 80 pixel bar, because a range of 25 degrees of habitability is a bit tiny (283 - 308K)
    surface.SetTexture(0)
    surface.SetDrawColor(Color(0, 196, 255, 255)) --Cold
    surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY(), lsBar_Temp:getWidth(), lsBar_Temp:getHeight())
    surface.SetTexture(0)
    surface.SetDrawColor(Color(255, 0, 0, 255)) --Hot
    surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY(), lsBar_Temp:getWidth(), 36)
    surface.SetTexture(0)
    surface.SetDrawColor(Color(0, 255, 0, 255)) --Goldilocks
    surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY() + 36, lsBar_Temp:getWidth(), 10)
    surface.SetTexture(0)
    surface.SetDrawColor(Color(0, 0, 0, 255)) --Goldilocks Highlight
    surface.DrawOutlinedRect(lsBar_Temp:getX(), lsBar_Temp:getY() + 35, lsBar_Temp:getWidth(), 12)

    -- Main Panel
    statPanel:render()

    --Temperature Indicator
    local adjustedTemp = math.Clamp(LocalPlayer():GetTemperature(), 100, 520) - 100
    surface.SetTexture(0)
    surface.SetDrawColor(Color(255, 255, 255, 255))
    surface.DrawRect(lsBar_Temp:getX() - 3, lsBar_Temp:getY() + (79 - (79 * (adjustedTemp / 420))), lsBar_Temp:getWidth() + 6, 2)

    -- AmmoPanel
    ammoPanel:render()

    if drawTimeBar then
        -- Clock [ ORDER IMPORTANT ]
        secClock:render()
        minClock:render()
        hrClock:render()

        -- FPS meter
        fpsInd:render()

        -- Labels [ ORDER NOT IMPORTANT ]
        secLabel:render()
        minLabel:render()
        hrLabel:render()
        fpsLabel:render()

        -- Numbers [ ORDER NOT IMPORTANT ]
        secN:render()
        minN:render()
        hrN:render()
        fpsN:render()
    end
end

function GM:HUDPaint()
	--HUD Invalidation (Update if any numbers change)
	if drawTimeBar ~= (GetConVarNumber("sc_hud_showTimeElement") ~= 0) then
		generated = false
		drawTimeBar = GetConVarNumber("sc_hud_showTimeElement") ~= 0
	end

	genComponents()

    local Player = LocalPlayer()
	if Player:Alive() and GetConVarNumber("cl_drawhud") ~= 0 then
        local Protector = Player:GetCurrentProtector()
        if IsValid(Protector) and Protector.IsCoreEnt then
            PaintShipHUD(Player, Protector)
        else
            CoreCapacitorDataTimer = 0
            PaintPlayerHUD(Player)
        end

	    self:PaintWorldTips()

	    -- Draw all of the default stuff
	    self.BaseClass.HUDPaint(self)

	    --self:PaintNotes()
	end

end