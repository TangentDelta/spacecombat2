require("lip")
AddCSLuaFile()

local GM = GM
local RegisteringConfigs = false
local SupportedConfigDataTypes = {
    string = true,
    number = true,
    boolean = true,
    Color = true,
    Vector = true,
    Angle = true,
}

local Config = {
    LoadedValues = {}
}

GM.Config = Config

--[[
    Info is a table containing all of the data about a config value
    This is the format of the Info table:

    Info = {
        -- Required
        File = "FileName",
        Section = "SectionName",
        Key = "ConfigName",
        Default = "DefaultValue",

        -- Optional
        ConVar = "sc_configvalue",
        OnConVar = function(File, Section, Key, NewValue) end,
        OnChanged = function(File, Section, Key, NewValue) end
    }
]]
function Config:Register(Info)
    -- Can only be called during the registration hook
    -- This is a requirement to support lua reloading within the gamemode
    if not RegisteringConfigs then
        SC.Error(string.format("Configuration values must only be registered during the SC.Config.Register hook!\n%s", debug.traceback()), 5)
        return false
    end

    -- Check for required info
    if not Info or not Info.File or not Info.Section or not Info.Key or Info.Default == nil then
        SC.Error(string.format("Tried to register invalid configuration value!\n%s", debug.traceback()), 5)
        return false
    end

    local ValueType = type(Info.Default)

    -- Special exception to check for Color data, which is just a normal table
    if ValueType == "table" then
        local Value = Info.Default
        if Value.r and Value.g and Value.b and Value.a then
            ValueType = "Color"
        end
    end

    -- Raise an error if an invalid data type was supplied
    if not SupportedConfigDataTypes[ValueType] then
        SC.Error(string.format("Tried to register invalid configuration value! Type %s is not supported!\n%s", ValueType, debug.traceback()), 5)
        return false
    end

    -- If there is a console command, then create it now
    local ConVar
    if Info.ConVar then
        local ConVarValue
        if ValueType == "boolean" then
            ConVarValue = Info.Default and 1 or 0
        elseif ValueType == "Color" then
            ConVarValue = tostring(Info.Default:ToVector())
        else
            ConVarValue = tostring(Info.Default)
        end

        local ConVarFlags
        if SERVER then
            ConVarFlags = {FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED}
        else
            ConVarFlags = {FCVAR_USERINFO}
        end

        local ConVarCallbackId = string.format("SC.ConVarOnChangedCallback.%s.%s.%s", Info.File, Info.Section, Info.Key)
        ConVar = CreateConVar(Info.ConVar, ConVarValue, ConVarFlags, Info.ConVarHelpText)
        cvars.RemoveChangeCallback(Info.ConVar, ConVarCallbackId)
        cvars.AddChangeCallback(Info.ConVar, function(ConVarName, OldValue, NewValue)
            if NewValue == OldValue then
                return
            end

            local SetValue
            if ValueType == "number" then
                SetValue = tonumber(NewValue)
            elseif ValueType == "boolean" then
                SetValue = tonumber(NewValue) == 1
            elseif ValueType == "Vector" then
                SetValue = Vector(NewValue)
            elseif ValueType == "Color" then
                SetValue = Vector(NewValue):ToColor()
            elseif ValueType == "Angle" then
                SetValue = Angle(NewValue)
            else
                SetValue = NewValue
            end

            if Info.OnConVar then
                Info.OnConVar(Info.File, Info.Section, Info.Key, SetValue)
            end

            Config:Set(Info.File, Info.Section, Info.Key, SetValue)
        end, ConVarCallbackId)
    end

    -- Add the value to the table
    Config.LoadedValues[Info.File] = Config.LoadedValues[Info.File] or {}
    Config.LoadedValues[Info.File][Info.Section] = Config.LoadedValues[Info.File][Info.Section] or {}
    Config.LoadedValues[Info.File][Info.Section][Info.Key] = {
        Value = Info.Default,
        ValueType = ValueType,
        Default = Info.Default,
        ConVarName = Info.ConVar,
        ConVar = ConVar,
        OnConVar = Info.OnConVar,
        OnChanged = Info.OnChanged
    }

    return true
end

function Config:Get(File, Section, Key, Default)
    if not Config.LoadedValues[File] or not Config.LoadedValues[File][Section] then
        return Default
    end

    local Value = Config.LoadedValues[File][Section][Key].Value

    return Value ~= nil and Value or Default
end

function Config:Set(File, Section, Key, Value)
    if not Config.LoadedValues[File] or not Config.LoadedValues[File][Section] or not Config.LoadedValues[File][Section][Key] then
        SC.Error(string.format("Tried to set invalid configuration value %s in %s %s!\n%s", Key, File, Section, debug.traceback()), 5)
        return false
    end

    if Value == nil then
        SC.Error(string.format("Cannot set configuration value %s in %s %s to a nil value!\n%s", Key, File, Section, debug.traceback()), 5)
        return false
    end

    local ConfigData = Config.LoadedValues[File][Section][Key]
    if type(Value) ~= ConfigData.ValueType then
        SC.Error(string.format("Cannot set configuration value %s in %s %s to a different type!\n%s", Key, File, Section, debug.traceback()), 5)
        return false
    end

    ConfigData.Value = Value

    if ConfigData.ConVar then
        local ConVarValue
        local ValueType = ConfigData.ValueType
        if ValueType == "boolean" then
            ConVarValue = Value and 1 or 0
        elseif ValueType == "Color" then
            ConVarValue = tostring(Value:ToVector())
        else
            ConVarValue = tostring(Value)
        end

        ConfigData.ConVar:SetString(ConVarValue)
    end

    if ConfigData.OnChanged then
        ConfigData.OnChanged(File, Section, Key, Value)
    end

    return true
end

function Config:SaveToFile(File)
    if not Config.LoadedValues[File] then
        return false
    end

    local OutData = {}
    for Section, Keys in pairs(Config.LoadedValues[File]) do
        OutData[Section] = {}
        for Key, Data in pairs(Keys) do
            OutData[Section][Key] = Data.Value
        end
    end

    file.CreateDir(SC.DataFolder.."/config/")
    lip.save(SC.DataFolder.."/config/"..File..".txt", OutData)

    return true
end

function Config:ReloadFromFile(File)
    if not Config.LoadedValues[File] then
        return false
    end

    local FilePath = SC.DataFolder.."/config/"..File..".txt"
    if not file.Exists(FilePath, "DATA") then
        return false
    end

    local FileData = lip.load(FilePath)
    for Section, Keys in pairs(FileData) do
        local CurrentSection = Config.LoadedValues[File][Section]
        if CurrentSection then
            for Key, Value in pairs(Keys) do
                Config:Set(File, Section, Key, Value)
            end
        else
            SC.Error(string.format("Invalid config section %s in file %s!", Section, File), 5)
        end
    end
end

local function LoadAllFromFiles()
    -- HACK: ...this needs to be moved to a different hook to fix initialization issues, but for now, hax
    timer.Simple(0, function()
        -- Register config values
        RegisteringConfigs = true
        hook.Run("SC.Config.Register")
        RegisteringConfigs = false

        -- Load the config values from file
        -- This will also save out any new values
        for File, Sections in pairs(Config.LoadedValues) do
            Config:ReloadFromFile(File)
            Config:SaveToFile(File)
        end

        hook.Run("SC.Config.PostLoad")
    end)
end

hook.Remove("InitPostEntity", "SC.Config.Init")
hook.Add("InitPostEntity", "SC.Config.Init", LoadAllFromFiles)
hook.Remove("OnReloaded", "SC.Config.Reload")
hook.Add("OnReloaded", "SC.Config.Reload", LoadAllFromFiles)