-- Derive from drive_base (see lua/drive/drive_base.lua)
AddCSLuaFile()
DEFINE_BASECLASS("drive_base")

DRIVETEST_PLATFORM = NULL
DRIVETEST_SCALE = 1
DRIVETEST_PLAYERMASS = 6.33 * DRIVETEST_SCALE
DRIVETEST_GRAVITY = -33.07 * DRIVETEST_SCALE
DRIVETEST_TERMINALVELOCITY = 3500 * DRIVETEST_SCALE
DRIVETEST_GROUNDSTICKDISTANCE = 2.0 * DRIVETEST_SCALE
DRIVETEST_MAXSTEPDISTANCE = 18 * DRIVETEST_SCALE
DRIVETEST_SWIMSPEED = 152 * DRIVETEST_SCALE
DRIVETEST_SWIMSPEEDFAST = 256 * DRIVETEST_SCALE
DRIVETEST_SWIMSPEEDSLOW = 120 * DRIVETEST_SCALE
DRIVETEST_CROUCHSPEED = 63.33 * DRIVETEST_SCALE
DRIVETEST_WALKINGSPEED = 150 * DRIVETEST_SCALE
DRIVETEST_RUNNINGSPEED = 200 * DRIVETEST_SCALE
DRIVETEST_SPRINTINGSPEED = 400 * DRIVETEST_SCALE
DRIVETEST_PLAYERHEIGHT = 73 * DRIVETEST_SCALE
DRIVETEST_CROUCHEDPLAYERHEIGHT = 37 * DRIVETEST_SCALE
DRIVETEST_PLAYEREYEHEIGHT = 64 * DRIVETEST_SCALE
DRIVETEST_CROUCHEDPLAYEREYEHEIGHT = 28 * DRIVETEST_SCALE
DRIVETEST_JUMPPOWER = 8.5 * DRIVETEST_SCALE
DRIVETEST_USETRACEDISTANCE = 82 * DRIVETEST_SCALE
DRIVETEST_AIRSPEEDMODIFIER = 0.065
DRIVETEST_MAXWALKABLESLOPE = 45
DRIVETEST_PLAYERINWATER = false
DRIVETEST_PLAYERINAIR = false
DRIVETEST_PLAYERONGROUND = false
DRIVETEST_PLAYERJUMP = false
DRIVETEST_PLAYERJUMPBOOST = false
DRIVETEST_UPDIRECTION = Vector(0, 0, 1)
DRIVETEST_RIGHTDIRECTION = Vector(0, 1, 0)
DRIVETEST_FORWARDDIRECTION = Vector(1, 0, 0)

-- Only contains anything that is different from the default friction
DRIVETEST_FRICTION = {
    default = 0.8,
    slipperymetal = 0.1,
    mud = 0.6,
    slipperyslime = 0.1,
    slime = 0.9,
    glass = 0.5,
    gravel = 0.4,
    snow = 0.35,
    ice = 0.1,
    gmod_ice = 0.01,
    gmod_bouncy = 0.5
}

function DRIVETEST_SETSCALE(Scale)
    DRIVETEST_SCALE = Scale
    DRIVETEST_PLAYERMASS = 6.33 * DRIVETEST_SCALE
    DRIVETEST_GRAVITY = -33.07 * DRIVETEST_SCALE
    DRIVETEST_TERMINALVELOCITY = 3500 * DRIVETEST_SCALE
    DRIVETEST_GROUNDSTICKDISTANCE = 2.0 * DRIVETEST_SCALE
    DRIVETEST_MAXSTEPDISTANCE = 18 * DRIVETEST_SCALE
    DRIVETEST_SWIMSPEED = 152 * DRIVETEST_SCALE
    DRIVETEST_SWIMSPEEDFAST = 256 * DRIVETEST_SCALE
    DRIVETEST_SWIMSPEEDSLOW = 120 * DRIVETEST_SCALE
    DRIVETEST_CROUCHSPEED = 63.33 * DRIVETEST_SCALE
    DRIVETEST_WALKINGSPEED = 150 * DRIVETEST_SCALE
    DRIVETEST_RUNNINGSPEED = 200 * DRIVETEST_SCALE
    DRIVETEST_SPRINTINGSPEED = 400 * DRIVETEST_SCALE
    DRIVETEST_PLAYERHEIGHT = 73 * DRIVETEST_SCALE
    DRIVETEST_CROUCHEDPLAYERHEIGHT = 37 * DRIVETEST_SCALE
    DRIVETEST_PLAYEREYEHEIGHT = 64 * DRIVETEST_SCALE
    DRIVETEST_CROUCHEDPLAYEREYEHEIGHT = 28 * DRIVETEST_SCALE
    DRIVETEST_JUMPPOWER = 8.5 * DRIVETEST_SCALE
    DRIVETEST_USETRACEDISTANCE = 82 * DRIVETEST_SCALE
end

function DRIVETEST_START(player, entity, platform)
    DRIVETEST_PLATFORM = platform
    drive.PlayerStartDriving(player, entity, "PLAYERPHYSICSTEST")
end

drive.Register("PLAYERPHYSICSTEST",
{
	--
	-- Calculates the view when driving the entity
	--
	CalcView = function(self, view)
		--
		-- Use the utility method on drive_base.lua to give us a 3rd person view
		--
        local ViewHeight = DRIVETEST_PLAYEREYEHEIGHT

        if DRIVETEST_CROUCHING then
            ViewHeight = DRIVETEST_CROUCHEDPLAYEREYEHEIGHT
        end

        view.origin = view.origin + DRIVETEST_UPDIRECTION * ViewHeight

        if IsValid(self.Entity) and self.Entity.ViewAngles then
            view.angles = self.Entity.ViewAngles
        end
	end,

	--
	-- Called before each move. You should use your entity and cmd to
	-- fill mv with information you need for your move.
	--
	StartMove = function(self, mv, cmd)
		-- Set observer mode to chase, so the entity will be drawn.
		self.Player:SetObserverMode(OBS_MODE_IN_EYE)
		--
		-- Update move position and velocity from our entity
		--
        local addedvel = Vector(0, 0, 0)

        if IsValid(DRIVETEST_PLATFORM) then
            DRIVETEST_FORWARDDIRECTION = DRIVETEST_PLATFORM:GetForward()
            DRIVETEST_RIGHTDIRECTION = DRIVETEST_PLATFORM:GetRight()
            DRIVETEST_UPDIRECTION = DRIVETEST_PLATFORM:GetUp()
        end

        local WaterLevel = self.Entity:WaterLevel()

        if WaterLevel > 1 then
            DRIVETEST_CROUCHING = false
            DRIVETEST_PLAYERINAIR = false
            DRIVETEST_PLAYERONGROUND = false
            DRIVETEST_PLAYERINWATER = true

            if mv:KeyDown(IN_FORWARD) then
                if mv:KeyDown(IN_SPEED) then
                    mv:SetForwardSpeed(DRIVETEST_SWIMSPEEDFAST)
                elseif mv:KeyDown(IN_WALK) then
                    mv:SetForwardSpeed(DRIVETEST_SWIMSPEEDSLOW)
                else
                    mv:SetForwardSpeed(DRIVETEST_SWIMSPEED)
                end
            elseif mv:KeyDown(IN_BACK) then
                mv:SetForwardSpeed(-DRIVETEST_SWIMSPEED)
            else
                mv:SetForwardSpeed(0)
            end

            if mv:KeyDown(IN_MOVERIGHT) then
                if mv:KeyDown(IN_WALK) then
                    mv:SetSideSpeed(DRIVETEST_SWIMSPEEDSLOW)
                else
                    mv:SetSideSpeed(DRIVETEST_SWIMSPEED)
                end
            elseif mv:KeyDown(IN_MOVELEFT) then
                if mv:KeyDown(IN_WALK) then
                    mv:SetSideSpeed(-DRIVETEST_SWIMSPEEDSLOW)
                else
                    mv:SetSideSpeed(-DRIVETEST_SWIMSPEED)
                end
            else
                mv:SetSideSpeed(0)
            end

            if mv:KeyDown(IN_JUMP) then
                if mv:KeyDown(IN_WALK) then
                    mv:SetUpSpeed(DRIVETEST_SWIMSPEEDSLOW)
                else
                    mv:SetUpSpeed(DRIVETEST_SWIMSPEED)
                end
            elseif mv:KeyDown(IN_DUCK) then
                if mv:KeyDown(IN_WALK) then
                    mv:SetUpSpeed(-DRIVETEST_SWIMSPEEDSLOW)
                else
                    mv:SetUpSpeed(-DRIVETEST_SWIMSPEED)
                end
            else
                mv:SetUpSpeed(-48)
            end

            mv:SetMaxSpeed(DRIVETEST_SPRINTINGSPEED)
        else
            if DRIVETEST_PLAYERINWATER then
                addedvel = addedvel + DRIVETEST_UPDIRECTION * DRIVETEST_JUMPPOWER * 0.65
                DRIVETEST_PLAYERINWATER = false
            end

            local jump = bit.band(mv:GetButtons(), IN_JUMP) ~= 0 and bit.band(mv:GetOldButtons(), IN_JUMP) == 0 and DRIVETEST_PLAYERONGROUND
            if jump then
                addedvel = addedvel + DRIVETEST_UPDIRECTION * DRIVETEST_JUMPPOWER
                DRIVETEST_PLAYERJUMP = true
                DRIVETEST_PLAYERJUMPBOOST = true
            end

            if mv:KeyDown(IN_DUCK) then
                DRIVETEST_CROUCHING = true
            else
                DRIVETEST_CROUCHING = false
            end

            if mv:KeyDown(IN_FORWARD) then
                if DRIVETEST_CROUCHING then
                    mv:SetForwardSpeed(DRIVETEST_CROUCHSPEED)
                else
                    if mv:KeyDown(IN_SPEED) then
                        mv:SetForwardSpeed(DRIVETEST_SPRINTINGSPEED)
                    elseif mv:KeyDown(IN_WALK) then
                        mv:SetForwardSpeed(DRIVETEST_WALKINGSPEED)
                    else
                        mv:SetForwardSpeed(DRIVETEST_RUNNINGSPEED)
                    end
                end
            elseif mv:KeyDown(IN_BACK) then
                if DRIVETEST_CROUCHING then
                    mv:SetForwardSpeed(-DRIVETEST_CROUCHSPEED)
                else
                    mv:SetForwardSpeed(-DRIVETEST_RUNNINGSPEED)
                end
            else
                mv:SetForwardSpeed(0)
            end

            if mv:KeyDown(IN_MOVERIGHT) then
                if DRIVETEST_CROUCHING then
                    mv:SetSideSpeed(DRIVETEST_CROUCHSPEED)
                else
                    if mv:KeyDown(IN_WALK) then
                        mv:SetSideSpeed(DRIVETEST_WALKINGSPEED)
                    else
                        mv:SetSideSpeed(DRIVETEST_RUNNINGSPEED)
                    end
                end
            elseif mv:KeyDown(IN_MOVELEFT) then
                if DRIVETEST_CROUCHING then
                    mv:SetSideSpeed(-DRIVETEST_CROUCHSPEED)
                else
                    if mv:KeyDown(IN_WALK) then
                        mv:SetSideSpeed(-DRIVETEST_WALKINGSPEED)
                    else
                        mv:SetSideSpeed(-DRIVETEST_RUNNINGSPEED)
                    end
                end
            else
                mv:SetSideSpeed(0)
            end

            mv:SetUpSpeed(0)
            mv:SetMaxSpeed(DRIVETEST_SPRINTINGSPEED)
        end

        local cmdang = cmd:GetViewAngles()
        local test = DRIVETEST_FORWARDDIRECTION:AngleEx(DRIVETEST_UPDIRECTION)

        -- yaw
        test:RotateAroundAxis(test:Up(), cmdang.yaw)

        -- pitch
        test:RotateAroundAxis(test:Right(), -cmdang.pitch)

        self.Entity.CommandAngles = cmdang

        mv:SetMoveAngles(test)
		mv:SetOrigin(self.Entity:GetNetworkOrigin())
		mv:SetVelocity(self.Entity:GetAbsVelocity() + addedvel)
	end,

	--
	-- Runs the actual move. On the client when there's
	-- prediction errors this can be run multiple times.
	-- You should try to only change mv.
	--
	Move = function(self, mv)
		local ang = mv:GetMoveAngles()
		local pos = mv:GetOrigin()
		local vel = mv:GetVelocity()

        vel = vel * 0.99

        local relativepos, relativeang = WorldToLocal(pos, ang, Vector(0, 0, 0), DRIVETEST_FORWARDDIRECTION:AngleEx(DRIVETEST_UPDIRECTION))
        local relativevel = WorldToLocal(vel, Angle(0, 0, 0), Vector(0, 0, 0), DRIVETEST_FORWARDDIRECTION:AngleEx(DRIVETEST_UPDIRECTION))

        local TraceData = {
            start = pos + (DRIVETEST_UPDIRECTION * DRIVETEST_MAXSTEPDISTANCE),
            endpos = pos + (-DRIVETEST_UPDIRECTION * (DRIVETEST_GROUNDSTICKDISTANCE + math.abs(math.min(relativevel.Z, 0)))),
            filter = self.Entity,
            mask = MASK_PLAYERSOLID
        }

        local result = util.TraceLine(TraceData)
        DRIVETEST_PLAYERINAIR = not result.Hit

        if DRIVETEST_PLAYERINWATER then
            relativevel = (relativeang:Forward() * mv:GetForwardSpeed() + relativeang:Right() * mv:GetSideSpeed() + Vector(0, 0, 1) * mv:GetUpSpeed()) * FrameTime()
        elseif not DRIVETEST_PLAYERINAIR and not DRIVETEST_PLAYERONGROUND then
            relativevel.Z = 0
            pos.Z = result.HitPos.Z
            DRIVETEST_PLAYERONGROUND = true
            DRIVETEST_PLAYERJUMP = false
        elseif DRIVETEST_PLAYERINAIR then
            local oldz = relativevel.Z
            local walkang = relativeang
            walkang.pitch = 0
            relativevel = relativevel + walkang:Forward() * mv:GetForwardSpeed() * FrameTime() * DRIVETEST_AIRSPEEDMODIFIER
            relativevel = relativevel + walkang:Right() * mv:GetSideSpeed() * FrameTime() * DRIVETEST_AIRSPEEDMODIFIER
            relativevel.Z = oldz + DRIVETEST_GRAVITY * FrameTime()
            DRIVETEST_PLAYERONGROUND = false
        elseif DRIVETEST_PLAYERONGROUND and not DRIVETEST_PLAYERJUMP then
            local walkang = relativeang
            local oldvel = Vector(relativevel)
            local friction = (DRIVETEST_FRICTION[util.GetSurfacePropName(result.SurfaceProps)] or DRIVETEST_FRICTION["default"]) * FrameTime() * 8.5
            walkang.pitch = 0
            relativevel = (walkang:Forward() * mv:GetForwardSpeed() + walkang:Right() * mv:GetSideSpeed()) * FrameTime()
            relativevel = relativevel + (oldvel * (1 - friction))
            relativevel.Z = 0

            local biggest = math.max(math.abs(mv:GetForwardSpeed()), math.abs(mv:GetSideSpeed())) * FrameTime()
            if biggest > 0 then
                relativevel = relativevel * (biggest / relativevel:Length2D())
            end
            pos.Z = result.HitPos.Z
        end

        if DRIVETEST_PLAYERJUMPBOOST then
            -- Compute the speed boost

            -- HL2 normally provides a much weaker jump boost when sprinting
            -- For some reason this never applied to GMod, so we won't perform
            -- this check here to preserve the "authentic" feeling
            local walkang = relativeang
            walkang.pitch = 0
            local speedBoostPerc = ((not DRIVETEST_CROUCHING) and 0.1) or 0.02

            local speedAddition = math.abs(mv:GetForwardSpeed() * speedBoostPerc)
            local maxSpeed = mv:GetMaxSpeed() * (1 + speedBoostPerc)
            local newSpeed = speedAddition + relativevel:Length2D()

            -- Clamp it to make sure they can't bunnyhop to ludicrous speed
            if newSpeed > maxSpeed then
                speedAddition = speedAddition - (newSpeed - maxSpeed)
            end

            -- Reverse it if the player is running backwards
            if mv:GetForwardSpeed() < 0 then
                speedAddition = -speedAddition
            end

            -- Apply the speed boost
            relativevel = relativevel + walkang:Forward() * speedAddition * FrameTime()

            DRIVETEST_PLAYERJUMPBOOST = false
        end

        relativevel.Z = math.Clamp(relativevel.Z, -DRIVETEST_TERMINALVELOCITY, DRIVETEST_TERMINALVELOCITY)
        vel = LocalToWorld(relativevel, Angle(0, 0, 0), Vector(0, 0, 0), DRIVETEST_FORWARDDIRECTION:AngleEx(DRIVETEST_UPDIRECTION))

		pos = pos + vel

		mv:SetVelocity(vel)
		mv:SetOrigin(pos)
	end,

	--
	-- The move is finished. Use mv to set the new positions
	-- on your entities/players.
	--
	FinishMove = function(self, mv)
		--
		-- Update our entity!
		--
        local ang = mv:GetMoveAngles()
        local newang = DRIVETEST_FORWARDDIRECTION:AngleEx(DRIVETEST_UPDIRECTION)

        -- yaw
        if self.Entity.CommandAngles then
            newang:RotateAroundAxis(newang:Up(), self.Entity.CommandAngles.yaw)
        end

		self.Entity:SetNetworkOrigin(mv:GetOrigin())
		self.Entity:SetAbsVelocity(mv:GetVelocity())
		self.Entity:SetAngles(newang)
        self.Entity.ViewAngles = ang

		--
		-- If we have a physics object update that too. But only on the server.
		--
		if (SERVER and IsValid(self.Entity:GetPhysicsObject())) then
			self.Entity:GetPhysicsObject():EnableMotion(true)
			self.Entity:GetPhysicsObject():SetPos(mv:GetOrigin())
			self.Entity:GetPhysicsObject():Wake()
			self.Entity:GetPhysicsObject():EnableMotion(false)
		end
	end,

}, "drive_base")