--#region Class Defs

---Data to be turned into an individual data element in the searchpanel's data layout
---All of the fields are optional. Additional fields can be added for filtering.
---@class SearchPanelData : table
---@field TextMain string #The main display text of the element. Used for things you want big, like names.
---@field TextSub string #The sub text of the element. Used for things like numbers or descriptions.
---@field ModelPath string #The path to a model to use as the element's icon.
---@field ModelSkinID number #The index of the skin to use on the icon model.
---@field OnMousePressed function #Called when the element is clicked on. `OnMousePressed(panel Element, number KeyCode)`

---A string to be turned into a filter expression.
---The expression has the syntax: `[VariableName] [Operator] [TestValue]`
---
---**VariableName** is the name of a field in the `SearchPanelData` object to extract the value from
---**Operator** is the operation to perform on **VariableName** and **TestValue**
---**TestValue** is what the extracted value from the field should be operated against
---
---**Valid Operators:**
---`==` Equal
---`~=` Not-Equal
---`>` Greater-Than
---`<` Less-Than
---`>=` Greater-Than-Or-Equal
---`<=` Less-Than-Or-Equal
---`contains` String Contains
---`~contains` String Does Not Contain
---
---TestValue is checked to see if it is a boolean or a number. If it is neither it is interpreted as a string literal.
---
---**Examples:**
---`TextMain == Corn` Filter in elements with `TextMain` equal to the string "Corn"
---`TextSub contains foo` Filters in elements with `TextSub` containing "foo"
---`InAtmosphere == false` Filters in elements where `InAtmosphere` is false
---`PlayerDistance > 1000` Filters in elements where `PlayerDistance` is greater than 1000
---@class SearchPanelFilter : string

--#endregion

--#region Filter Expressions

-- Filter function factories
local function OP_EQUAL(Variable, Value) return function(A) return A[Variable] == Value end end
local function OP_NOTEQUAL(Variable, Value) return function(A) return A[Variable] ~= Value end end
local function OP_GT(Variable, Value) return function(A) return A[Variable] > Value end end
local function OP_LT(Variable, Value) return function(A) return A[Variable] < Value end end
local function OP_GTOE(Variable, Value) return function(A) return A[Variable] >= Value end end
local function OP_LTOE(Variable, Value) return function(A) return A[Variable] <= Value end end
local function OP_CONTAINS(Variable, Value) return function(A) return string.find(A[Variable], Value) and true or false end end
local function OP_NOTCONTAINS(Variable, Value) return function(A) return not (string.find(A[Variable], Value) and true or false) end end

-- Substitutions
local FilterSubs = {
    ["true"] = true,
    ["false"] = false
}

-- Operator symbol to factory association
local Operators = {
    ["=="] = OP_EQUAL,
    ["~="] = OP_NOTEQUAL,
    [">"] = OP_GT,
    ["<"] = OP_LT,
    [">="] = OP_GTOE,
    ["=>"] = OP_GTOE,
    ["<="] = OP_LTOE,
    ["=<"] = OP_LTOE,
    ["contains"] = OP_CONTAINS,
    ["~contains"] = OP_NOTCONTAINS
}

local function SubstituteFilterCompareString(CompareString)
    return FilterSubs[CompareString] or tonumber(CompareString) or CompareString
end
-- TODO: Use a more advanced expression system for these (https://github.com/toggledbits/luaxp)
---Create a filter rule from a filter expression string
---@param FilterString string
local function CreateFilter(FilterString)
    local Parts = {}
    for Part in string.gmatch(FilterString, "%S+") do
        table.insert(Parts, Part)
    end

    local Variable = Parts[1]
    local Operation = Operators[Parts[2]]
    local CompareValue = SubstituteFilterCompareString(Parts[3] or "")

    if not (Variable and Operation and CompareValue) then
        SC.Error("[Filter]Invalid filter expression!", 5)
        return
    end

    -- Construct the filter function from the factory
    Operation = Operation(Variable, CompareValue)

    return Operation
end
--#endregion



local PANEL = {}

function PANEL:Init()
    -- Data-related tables
    self.Data = nil -- The table of data to be displayed
    self.DataElements = {}  -- The table of elements created from the table of data
    self.VisibleElements = {} -- A table of elements that are currently being shown

    -- Filters
    self.Filters = {}  -- The table of loaded filters
    self.EnabledFilters = {}    -- The table of filters that are currently active

    -- Element selection
    self.MultiSelect = false
    self.PendingSelectedElements = {}
    self.SelectedElements = {}

    --#region Top Control Bar
    -- Create the top control bar
    local TopControls = vgui.Create("DPanel", self)
    self.TopControls = TopControls
    TopControls:Dock(TOP)

    TopControls.OnSizeChanged = function(Panel, Width, Height)
        -- When the top control bar is resized, resize the search bar
        TopControls.SearchBar:SetWidth(Width * 0.75)
    end

    -- Search bar
    local SearchBar = vgui.Create("DTextEntry", TopControls)
    TopControls.SearchBar = SearchBar
    SearchBar:SetPlaceholderText("Search")
    SearchBar:SetUpdateOnType(true)
    SearchBar:Dock(LEFT)
    SearchBar:RequestFocus()

    SearchBar.OnValueChange = function(Panel, Str)
        self:FilterDataElements()
    end

    -- Filter selection
    local FilterButton = vgui.Create("Button", TopControls)
    FilterButton:SetText("Filter")
    FilterButton:Dock(RIGHT)
    FilterButton.DoClick = function()
        local FilterMenu = DermaMenu()
        -- TODO: Allow users to create custom filters and save them

        -- Populate the menu with a list of filters
        for FilterName, Filter in pairs(self.Filters) do
            local FilterOption = FilterMenu:AddOption(FilterName)
            FilterOption:SetIsCheckable(true)
            FilterOption:SetChecked(self.EnabledFilters[FilterName] and true or false)
            FilterOption.OnChecked = function(Option, B)
                if B then
                    self.EnabledFilters[FilterName] = Filter
                else
                    self.EnabledFilters[FilterName] = nil
                end
                self:FilterDataElements()
            end
        end

        FilterMenu:Open()
    end
    --#endregion

    -- Create the default "Search Bar" filter
    self.Filters["Search Bar"] = function(E)
        if not E.SearchableKeys then return false end   -- Elements with no searchable keys are discarded
        local SearchText = string.lower(SearchBar:GetValue())   -- Convert the searchbar's text toe lowercase
        for _, Key in ipairs(E.SearchableKeys) do
            local Value = string.lower(E[Key])   -- Get the value of the field to search and convert it to lowercase
            if string.find(Value, SearchText) then
                return true
            end
        end

        return false
    end

    self.EnabledFilters["Search Bar"] = self.Filters["Search Bar"]

    --#region Data Element Layout
    self.Layout = vgui.Create("DScrollPanel", self)
    self.Layout:Dock(FILL)
    --#endregion
end

--#region Filtering

---Filter the data elements exclusively based on what filters are currently selected
function PANEL:FilterDataElements()
    self.VisibleElements = {}   -- Clear the table of visible elements
    local Elements = self.DataElements
    for DataIndex, DataEntry in ipairs(self.Data) do
        local Element = Elements[DataIndex]
        local IncludeElement = true
        for FilterName, Filter in pairs(self.EnabledFilters) do
            IncludeElement = IncludeElement and Filter(Element.Data)
            if not IncludeElement then break end    -- Break out early if the element no-longer matches the filters
        end

        if IncludeElement then
            -- Add this element to the table of elements that are visible
            table.insert(self.VisibleElements, DataIndex)
        else
            -- Unselect this element if it was selected
            local SelectedElement = self.SelectedElements[DataIndex]
            if SelectedElement then
                SelectedElement:SetSelected(false)
                self.SelectedElements[DataIndex] = nil
            end
        end
        Element:SetVisible(IncludeElement)
    end

    self.Layout:InvalidateChildren()
end
--#endregion
--#region Element Selection
---Called when an element gets selected
---@param Element panel #The element that was selected
function PANEL:OnClickLine(ClickedElement)
    local ClickedElementIndex = ClickedElement:GetIndex()
    local ExistingSelection = self.SelectedElements[ClickedElementIndex] ~= nil -- Was the element clicked on already selected?
    local DeselectPrevious = true   -- Should the previously selected element(s) be cleared?

    if self.MultiSelect then
        if input.IsKeyDown(KEY_LCONTROL) or input.IsKeyDown(KEY_RCONTROL) then
            -- Deselect the element if it was ctrl-clicked again
            if ExistingSelection then
                ClickedElement:SetSelected(false)
                self.SelectedElements[ClickedElementIndex] = nil
            end

            DeselectPrevious = false
        end

        if input.IsKeyDown(KEY_LSHIFT) or input.IsKeyDown(KEY_RSHIFT) then
            -- TODO: Shift selection
            DeselectPrevious = false
        end
    end

    if DeselectPrevious then
        -- Deselect any elements that were previously selected
        for ElementIndex, Element in pairs(self.SelectedElements) do
            if ElementIndex ~= ClickedElementIndex then
                Element:SetSelected(false)
                self.SelectedElements[ElementIndex] = nil
            end
        end
    end

    if not ExistingSelection then
        -- Select this element and add it to the selected elements list
        ClickedElement:SetSelected(true)
        self.SelectedElements[ClickedElementIndex] = ClickedElement
    end
end
--#endregion
--#region Panel Layout Updating and Element Creation

---Append a new data element to the data element layout
---@param Data any #The data to pass into the created element for it to configure itself
function PANEL:CreateElement(Data)
    local ElementIndex = #self.DataElements + 1
    local Element = vgui.Create(self.ElementDeterminator(Data), self.Layout)
    Element:SetIndex(ElementIndex)
    Element:SetData(Data)
    Element:Dock(TOP)

    -- Element hook functions
    Element.OnMousePressed = function(Panel, Key)  -- Override the element's OnMousePressed hook
        -- Call the user-overridable hook
        local UserReturn = self:OnElementClicked(Panel, Key)
        if UserReturn == nil then UserReturn = true end -- If nothing was returned, just select the element

        -- Handle element selection
        if UserReturn and (Key == MOUSE_LEFT) then
            self:OnClickLine(Panel)
        end
    end

    -- Add this new element to some tables
    table.insert(self.DataElements, Element)
    table.insert(self.VisibleElements, ElementIndex)

    return Element
end

---Update the layout of the panel with new/changed data or a new/changed VGUIElement type
function PANEL:UpdateLayout()
    -- TODO: Make this smarter about updating the layout, and only update changed elements

    -- Delete all of the existing elements
    for _, Element in ipairs(self.DataElements) do
        Element:Remove()
    end
    self.DataElements = {}


    if (not self.Data) or (not self.ElementDeterminator) or (#self.Data == 0) then return end    -- Don't do anything if we have no data or element determinator

    for DataIndex, Data in ipairs(self.Data) do
        local NewElement = self:CreateElement(Data)

        -- If this element has a pending selection, select it
        if self.PendingSelectedElements[DataIndex] then
            self.PendingSelectedElements[DataIndex] = nil
            NewElement:SetSelected(true)
            self.SelectedElements[DataIndex] = NewElement
        end
    end
end

--#endregion
--#region Panel Data Loading/Setting

---Sets which element to use for each entry in the data
---@param ElementName string #The name of the element to use, or a function that returns a string of the element to use
function PANEL:SetDataVGUIElement(ElementName)
    if isstring(ElementName) then
        self.ElementDeterminator = function(Data) return ElementName end
    else
        self.ElementDeterminator = ElementName
    end

    if self.Data then
        self:UpdateLayout()
    end
end

function PANEL:SetMultiselect(Enabled)
    self.MultiSelect = Enabled
end

---Sets the data to be displayed in this search panel
---@param Data table #A sequential table of data elements to be displayed
function PANEL:SetData(Data)
    self.Data = Data
    self:UpdateLayout()
end

---Set the filters to be used to filter data
---@param Filters SearchPanelFilter[]
function PANEL:SetFilters(Filters)
    for FilterName, FilterString in pairs(Filters) do
        self.Filters[FilterName] = CreateFilter(FilterString)
    end
end

---Clears the currently selected elements
function PANEL:ClearSelection()
    for ElementIndex, Element in pairs(self.SelectedElements) do
        Element:SetSelected(false)
        self.SelectedElements[ElementIndex] = nil
    end
end

---Returns a non-sequential table of panel elements that are currently selected
---@return table SelectedElements #A table of elements, indexed by the element's index. The index matches 1-to-1 with the input data indicies.
function PANEL:GetSelectedElements()
    return self.SelectedElements()
end

---Select multiple elements by their index
---@param SelectedIndices table #A sequential list of elements to set selected
function PANEL:SetSelectedElements(SelectedIndices)
    self:ClearSelection()

    for _, ElementIndex in ipairs(SelectedIndices) do
        local Element = self.DataElements[ElementIndex]
        if Element then
            -- If the element exists, select it
            Element:SetSelected(true)
            self.SelectedElements[ElementIndex] = Element
        else
            -- Otherwise add the selection state to the pending selections table
            self.PendingSelectedElements[ElementIndex] = true
        end
    end
end

---Returns a sequential table of the raw data elements that are currently selected
---@return table SelectedData #The elements in the input data set that are currently selected
function PANEL:GetSelectedData()
    local ReturnData = {}
    for ElementIndex, _ in pairs(self.SelectedElements) do
        table.insert(ReturnData, self.Data[ElementIndex])
    end

    return ReturnData
end
--#endregion
--#region User-Overridable Hooks

---Called when an element in the searchpanel is clicked
---Use this for right-click option menus and stuff
---@param Panel panel #The element panel that was clicked
---@param Key integer #The keycode of the button that clicked the element
---@return boolean DoSelect #When true, the element gets selected
function PANEL:OnElementClicked(Panel, Key)
    -- Override me!
end
--#endregion

vgui.Register("SearchPanel", PANEL, "Panel")